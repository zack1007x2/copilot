#include <jni.h>
#include <string.h>
#include <stdio.h>
#include <android/log.h>
#include <pthread.h>
#include <fcntl.h>
#include "com_moremote_copilot_queue_endpoints_thread_ThreadReaderUART.h"

#define JAVA_CLASS_PATH(funtion_name) Java_com_moremote_copilot_queue_endpoints_thread_ThreadReaderUART_##funtion_name
#define UART_NATIVE_TAG "NativeUART"
#define BUFFER_SIZE 64

typedef void (* CALLBACK) (int, unsigned char);
typedef struct StartUartRead_PARAM {
		int FD;
		int len;
	} param;

int allowRead;
JavaVM *g_jvm;
jobject g_obj = NULL;


void callBack(int avaliable, unsigned char *bytes)
{
	// __android_log_print(ANDROID_LOG_DEBUG, UART_NATIVE_TAG, "callBack avaliable =  %d\n", avaliable);
	JNIEnv *env;
	jclass cls;
    jmethodID mid;
    if((*g_jvm)->AttachCurrentThread(g_jvm, &env, NULL) != JNI_OK)
    {
        __android_log_print(ANDROID_LOG_ERROR, UART_NATIVE_TAG, "AttachCurrentThread() Error.....");
        return;
    }

	cls = (*env)->GetObjectClass(env,g_obj);
    if(cls == NULL)
    {
        __android_log_print(ANDROID_LOG_ERROR, UART_NATIVE_TAG, "FindClass() Error.....");
        
    }
    
    mid = (*env)->GetMethodID(env, cls, "readThreadCallBack", "([B)V");
    if (mid == NULL) 
    {
        __android_log_print(ANDROID_LOG_ERROR, UART_NATIVE_TAG, "GetMethodID() Error.....");
        
    }
    jbyteArray carr = (*env)->NewByteArray(env, (jsize)avaliable);
    (*env)->SetByteArrayRegion(env,carr,0, avaliable, (jbyte *)bytes);
    (*env)->CallVoidMethod(env, g_obj, mid, carr);

    //Since you didn't call GetByteArrayElements you also shouldn't call ReleaseByteArrayElements.
    // see: http://stackoverflow.com/a/33021257
    // (*env)->ReleaseByteArrayElements(env, carr, bytes, 0);
    	
    // Detach Main thread(necessary)
    if((*g_jvm)->DetachCurrentThread(g_jvm) != JNI_OK)
    {
        __android_log_print(ANDROID_LOG_ERROR, UART_NATIVE_TAG, "DetachCurrentThread() Error.....");
    }
}

void *StartUartRead(void *par){

	param* t_par;
	t_par = (param*) par;
	allowRead = 1;

	int uartFD = t_par->FD;
	int length = t_par->len;
	//unsigned char bytes[length];
	unsigned char bytes[1024] = {0};
	//unsigned char *bytes = NULL;
	//bytes = malloc(length);
	

	__android_log_print(ANDROID_LOG_DEBUG, UART_NATIVE_TAG, "StartUartRead uartFD =  %d length = %d ", uartFD, length);

	fd_set rd;
	int ret,SelRet; 
	struct timeval timeout;
	while(allowRead){
		timeout.tv_sec = 0;
		timeout.tv_usec = 10000;
		FD_ZERO(&rd);
		FD_SET(uartFD,&rd);
		
		SelRet = select(uartFD+1, &rd, NULL, NULL, NULL);
		// __android_log_print(ANDROID_LOG_DEBUG, UART_NATIVE_TAG, "Select() Ret = %d", SelRet);
		switch(SelRet){
			case -1:
				//select error
				//for debug
				// callBack(1, s);
				continue;	
				break;
			case 0:
				//time out
				continue;
				break;
			default:
				if(FD_ISSET(uartFD,&rd)){
					select(NULL, NULL, NULL, NULL, &timeout);
					ret = read(uartFD, bytes, length);
					if(ret>0){
						// __android_log_print(ANDROID_LOG_DEBUG, UART_NATIVE_TAG, "Ret = %d", ret);
						callBack(ret, bytes);
					}else{
						FD_CLR(uartFD,&rd);
					}
					
				}
				break;
		}
		
		 
	}
	__android_log_print(ANDROID_LOG_DEBUG, UART_NATIVE_TAG, "Stop UartReadThread");
	free(bytes);
}




/* Begin the JNI wrapper functions for the UART app */

jint JAVA_CLASS_PATH(uartOpen)(JNIEnv *env, jobject this, jint device, jint bdrate)
{
	jint ret;
	ret = uartOpen(device, bdrate) ;

	if ( ret == -1 ) {
		__android_log_print(ANDROID_LOG_ERROR, UART_NATIVE_TAG, "uartOpen(%d, %d) failed!", (unsigned int) device, (unsigned int) bdrate);
		ret = -1;
	} else {
		__android_log_print(ANDROID_LOG_DEBUG, UART_NATIVE_TAG, "uartOpen(%d, %d) succeeded", (unsigned int) device, (unsigned int) bdrate);
	}

	return ret;
}

jboolean JAVA_CLASS_PATH(uartWrite)(JNIEnv *env, jobject this, jint uartFD, jint length, jbyteArray barray)
{
	jint ret;
	int i;

	jbyte* bufferPtr = (*env)->GetByteArrayElements(env, barray, NULL);

	unsigned char bytes[length] ;

	for(i=0; i<length; i++)
	{
		bytes[i] = bufferPtr[i];
	}

	(*env)->ReleaseByteArrayElements(env, barray, bufferPtr, 0);

	ret = uartWrite(uartFD, length, bytes) ;

	if ( ret == -1 ) {
		__android_log_print(ANDROID_LOG_ERROR, UART_NATIVE_TAG, "uartWrite(%d, %d, bytearray) failed!", (unsigned int) uartFD, (unsigned int) length);
		return JNI_FALSE;
	} else {
		__android_log_print(ANDROID_LOG_DEBUG, UART_NATIVE_TAG, "uartWrite(%d, %d, bytearray) succeeded", (unsigned int) uartFD, (unsigned int) length);
	}

	return JNI_TRUE;
}

jint JAVA_CLASS_PATH(uartRead)(JNIEnv *env, jobject this, jint uartFD, jint length, jintArray barray)
{
	jint ret;
	int i;
	unsigned char bytes[length] ;

	ret = uartRead(uartFD, length, bytes);

	if ( ret == -1 ) {
		__android_log_print(ANDROID_LOG_ERROR, UART_NATIVE_TAG, "uartRead(%d, %d, bytearray) failed!", (unsigned int) uartFD, (unsigned int) length);
		return JNI_FALSE;
	} else {
		__android_log_print(ANDROID_LOG_DEBUG, UART_NATIVE_TAG, "uartRead(%d, %d, bytearray) succeeded", (unsigned int) uartFD, (unsigned int) length);
	}

	jbyte* bufferPtr = (*env)->GetByteArrayElements(env, barray, NULL);

	for(i=0; i<ret; i++)
	{
		bufferPtr[i] = bytes[i];
	}

	(*env)->ReleaseByteArrayElements(env, barray, bufferPtr, 0);

	return ret;
}

void JAVA_CLASS_PATH(uartClose)(JNIEnv *env, jobject this, jint uartFD)
{
	allowRead = 0;
	uartClose(uartFD);
	__android_log_print(ANDROID_LOG_DEBUG, UART_NATIVE_TAG, "uartClose(%d) succeeded", (unsigned int) uartFD);
}


void JNICALL JAVA_CLASS_PATH(runUartReadThread)(JNIEnv *env, jobject jobj, jint uartFD, jint length){
	//save global JVM for thread
    (*env)->GetJavaVM(env,&g_jvm);
    //don't use g_obj = obj
    g_obj = (*env)->NewGlobalRef(env,jobj);

	pthread_t thread;
	int ret;
	param *par;
	par = (param *)malloc(sizeof(param));
	par->FD = uartFD;
	par->len = length;

	if( (ret=pthread_create( &thread, NULL, &StartUartRead, (void*)par))){
      __android_log_print(ANDROID_LOG_ERROR, UART_NATIVE_TAG, "pthread_create ERROR");
   	}
}


/* End the JNI wrapper funtions for the UART app */

/* End the JNI wrapper functions for the Complete app */