LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

LOCAL_MODULE := NativeUART
LOCAL_SRC_FILES := jni_wrapper.c uart.c

LOCAL_LDLIBS += -llog
include $(BUILD_SHARED_LIBRARY)