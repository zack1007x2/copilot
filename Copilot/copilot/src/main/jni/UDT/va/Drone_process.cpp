#include "Drone_process.h"
#define PI 3.141592

#include <android/log.h>
#include <stdio.h>

#define  TAG "INFO"
#define LOGI(...) __android_log_print(ANDROID_LOG_INFO,TAG,__VA_ARGS__)



vector<vector<Point> > findLargesquare( Mat img, vector<vector<Point> > allbigGontours,vector<Vec4i> maskBHierarchy ,int comtourSize, int srcArea)
	{
		vector<vector<Point> > bigContours;
		bigContours.clear();
		for(int n = 0; n < comtourSize; n++)
		{
			if ( maskBHierarchy[n][2] == -1){           //過濾掉內輪廓
				continue;
			}
			//if ( maskBHierarchy[n][1] == -1){           //過濾掉內輪廓 for CV_RETR_TREE
			//	continue;
			//}
			Rect contourRect = boundingRect(allbigGontours[n]);
			//if(contourRect.height*contourRect.width > 1000){

			if(contourRect.area() >2000  && contourRect.area()< srcArea -10000){// 大正方型大小
				double ratio = 0;
				ratio = (double)contourRect.width/(double)contourRect.height;  
				//if (ratio >0.93 && ratio < 1.07){
				if (ratio >0.7 && ratio < 1.3){
				LOGI("largeContours lWidth= %d, lHeight=%d, lRatio=%f", contourRect.width, contourRect.height, ratio);
				//drawContours(img,allbigGontours,n,Scalar(255,255,0),1);
				//drawContours(img,allContours,n,Scalar(255,246,0),1);
				 bigContours.push_back(allbigGontours[n]);
				}
			}
		}
		allbigGontours.clear();
		return bigContours;
	}

vector<vector<Point> > findSmallsquare (Rect smallImagRegion , Mat smallImag, vector<vector<Point> > allSmallcontours, vector<Vec4i> maskHierarchy, int areaThreshold, int smallcomtourSize)
	{
		vector<vector<Point> > smallContours;
		smallContours.clear();
		for(int n = 0; n < smallcomtourSize; n++) {

				if ( maskHierarchy[n][2] == -1){
					continue;
				}
				//if ( maskHierarchy[n][1] == -1){           //過濾掉內輪廓 for CV_RETR_TREE
				//	continue;
				//}
				
				Rect contourRect2 = boundingRect(allSmallcontours[n]);

				if (contourRect2.x > (smallImagRegion.width/4) && contourRect2.x <(smallImagRegion.width/2)){
					if(contourRect2.y >(smallImagRegion.height/4) && contourRect2.y <(smallImagRegion.height/2))
					continue;
				}

				if(contourRect2.area() < (areaThreshold/4) && contourRect2.area() > (areaThreshold/80) ){
					double ratio2 = 0;
					ratio2 = (double)contourRect2.width/(double)contourRect2.height;  
					//if (ratio2 >0.92 && ratio2 < 1.08)
					if (ratio2 > 0.7 && ratio2 < 1.3)
					{
						LOGI("smallContours sWidth= %d, sHeight=%d, sRatio=%f", contourRect2.width, contourRect2.height, ratio2);
						smallContours.push_back(allSmallcontours[n]);
					}
				}
			}
		return smallContours;

	}

R2 orderSmallsquare ( vector<vector<Point> > sContour, int sContournum)
	{
		R2 output = {0};

		int squaretemp;
		int idtemp;

		for ( int i = 0; i < sContournum; i++ ){        
			Rect contour2Rect = boundingRect(sContour[i]);
			output.A[i] = contour2Rect.area();
			output.B[i] = i;


			if (i == sContournum - 1){
				for ( int j = 0; j <= i; j++ ){
					for ( int k = j+1; k <= i+1 ; k++){
						if ( output.A[k] > output.A[j]){
							squaretemp = output.A[k] ;
							output.A[k] = output.A[j];
							output.A[j] = squaretemp;
							idtemp = output.B[k] ;
							output.B[k] = output.B[j];
							output.B[j] = idtemp;
							
						}
					}
				}
			}
		}
			return output;

	}

Point pointcenter (int x1, int y1, int x2, int y2, int x3, int y3, int x4, int y4)
	{
		
		int a1 =  x1 -x2;  //(x1 - x2)
		int b1 =  y1 - y2 ; //(y1 - y2)
		int c1 =  x1 * y2 - x2 * y1  ; //(x1 * y2 - x2 * y1)

		int a2 =  x3 - x4; //(x3 - x4)
		int b2 =  y3 - y4 ; //(y3 - y4)
		int c2 =  x3 * y4 - x4 * y3 ;// (x3 * y4 - x4 * y3)

	    int xx = ( a1 * c2 - a2 *c1 ) / ( a2 * b1 - a1 * b2); 
		int yy = ( b1 * c2 - b2 *c1 ) / ( a2 * b1 - a1 * b2); 

		
		return Point (xx ,yy);

	}

float rotationAngle (Mat objAabb, R2 smsuqareOrder, vector<vector<Point> > smcontours,Rect objAabbArea, int contoursSize)
{

    Point point[8];
	int idx = 0;
	for (int i = 0; i < contoursSize; i++ ){

		point[i] = pointcenter (smcontours[smsuqareOrder.B[i]][0].x, smcontours[smsuqareOrder.B[i]][0].y,
								smcontours[smsuqareOrder.B[i]][2].x, smcontours[smsuqareOrder.B[i]][2].y,
								smcontours[smsuqareOrder.B[i]][1].x, smcontours[smsuqareOrder.B[i]][1].y,
								smcontours[smsuqareOrder.B[i]][3].x, smcontours[smsuqareOrder.B[i]][3].y);
						idx = i;
	}



	int d[10] = {0};
	int maxdistance = 0;
	Point keypoint1,keypoint2, keypointc, keypoint3;

	int n = 0;
	for (int i = 0; i < idx; i++ )
	{
		for ( int j = i+1 ; j <=idx; j++)
		{
			d[n] =  sqrt(pow(point[i].x-point[j].x,2)+pow(point[i].y-point[j].y,2));  //Euclidean distance
			if ( d[n] >  maxdistance){
				maxdistance = d[n];
				keypoint1 = Point (point[i].x , point[i].y) ;
				keypoint2 = Point (point[j].x , point[j].y) ;

			}
			n++;
		}
					
	}
	int ds[10] = {0};
	int m = 0;
	int maxdistance2 = 0;
	keypointc = Point ((keypoint1.x + keypoint2.x)/2, (keypoint1.y + keypoint2.y)/2) ; //�̪��䤤���I
	for (int i = 0; i <= idx; i++ ){
		if (point[i] !=  keypoint1 && point[i] !=  keypoint2) {
			ds[m] = sqrt(pow(point[i].x - keypointc.x, 2) + pow(point[i].y - keypointc.y, 2));  //Euclidean distance 距離
			if (ds[m] > maxdistance2) {
				maxdistance2 = d[m];
				keypoint3 = point[i];
			}
			m++;
		}
	}

	if (keypointc.x != 0 && keypoint3.x !=0) {
		fp ab1 ; //固定  三角短邊正方形3座標點 keypoint3
		fp cb1 ;

		ab1.x = keypoint3.x - keypoint1.x;
		ab1.y = keypoint3.y - keypoint1.y;

		cb1.x = keypoint3.x - keypoint2.x;
		cb1.y = keypoint3.y - keypoint2.y;
		float dot1 = (ab1.x * cb1.x + ab1.y * cb1.y); // dot product
		float cross1 = (ab1.x * cb1.y - ab1.y * cb1.x); // cross product
		float alpha1 = atan2(cross1, dot1);
		float rrs1 = floor(alpha1 * 180. / PI + 0.5);

		if (abs(rrs1) < 95 && abs(rrs1) > 85) {
			line(objAabb, keypointc, keypoint3, Scalar(255, 255, 0), 2, CV_AA, 0);
			line(objAabb, keypoint1, keypoint2, Scalar(255, 0, 0), 2, CV_AA, 0);
			line(objAabb, keypoint1, keypoint3, Scalar(255, 0, 0), 2, CV_AA, 0);
			line(objAabb, keypoint2, keypoint3, Scalar(255, 0, 0), 2, CV_AA, 0);

			fp ab;
			fp cb;

			ab.x = keypointc.x - keypoint3.x;
			ab.y = keypointc.y - keypoint3.y;

			cb.x = keypointc.x - (objAabbArea.width / 2);
			cb.y = keypointc.y - 0;

			line(objAabb, keypointc, Point((objAabbArea.width / 2), 0), Scalar(128, 128, 0), 2,
				 CV_AA, 0);
			float dot = (ab.x * cb.x + ab.y * cb.y); // dot product
			float cross = (ab.x * cb.y - ab.y * cb.x); // cross product
			float alpha = atan2(cross, dot);
			float rrs = floor(alpha * 180. / PI + 0.5);


			return rrs;
		}
	}
}