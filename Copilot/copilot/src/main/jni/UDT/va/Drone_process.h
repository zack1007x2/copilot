#include <cv.h>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <vector>
using namespace cv;
using namespace std;


vector<vector<Point> > findLargesquare( Mat , vector<vector<Point> > , vector<Vec4i> , int , int ) ;
vector<vector<Point> > findSmallsquare( Rect , Mat, vector<vector<Point> > , vector<Vec4i> , int , int );

typedef struct Return_2{
	int A[20];
	int B[20];
} R2;
R2 orderSmallsquare( vector<vector<Point> > , int );

class fp{
public:
	float x;
	float y;
};

Point pointcenter( int  ,int , int ,int , int  ,int , int  ,int );

float rotationAngle(Mat, R2, vector<vector<Point> >, Rect,int );