#include <com_moremote_copilot_drone_manager_IdentifyManager.h>
#include <opencv2/video/tracking.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/core/core.hpp>
#include <string>
#include <opencv2/opencv.hpp>
#include <stdio.h>   
#include <math.h>
#include <time.h>

#include <iostream>
#include <ctype.h>

#include <android/log.h>

using namespace cv;
using namespace std;
void arrowedLine(Mat img, Point pt1, Point pt2, const Scalar& color,int thickness, int line_type, int shift, double tipLength);

#define TAG "img"
#define LOGI(...) __android_log_print(ANDROID_LOG_INFO,TAG,__VA_ARGS__)
#define PI 3.14159265

Point2f point;
bool addRemovePt = false;

// image parameters
Mat image, prevGray;
bool selectObject = false;
Point origin;
Rect selection;
int trackObject = 0;

// feature point set
vector<Point2f> points[2];
const int MAX_COUNT = 100;
bool needToInit = true;
bool pVal; // false: return NULL ; true: return data
// preframe info
int cnt =0;
int preAvgX = 0;
int preAvgY = 0;

JNIEXPORT jobjectArray JNICALL Java_com_moremote_copilot_drone_manager_IdentifyManager_DistanceMove
  (JNIEnv *, jobject, jlong);


JNIEXPORT jobjectArray JNICALL Java_com_moremote_copilot_drone_manager_IdentifyManager_DistanceMove
  (JNIEnv *env, jobject, jlong matCamera)
{
    /*VideoCapture cap;*/
    TermCriteria termcrit(CV_TERMCRIT_ITER|CV_TERMCRIT_EPS, 20, 0.03);
    Size subPixWinSize(10,10), winSize(31,31);

    Mat& frame = *(Mat*)matCamera;

    //bool nightMode = false;
    Mat gray;



    // import android bitmap
    frame.copyTo(image);
    cvtColor(image, gray, COLOR_BGR2GRAY);

	int sum = 0;
	int sumX = 0;
	int sumY = 0;

	int avgX = 0;
	int avgY = 0;

	float sumAngle = 0.0;
	float avgAngle = 0.0;

    //if( nightMode )
	//    image = Scalar::all(0);

    char position[2][10];
    jobjectArray dataArray;
    memset(position, 0x00, sizeof(position));


    //memset(rotation, 0x00, sizeof(rotation));

	//tracking points
	if (points[1].size() <= 25 ||needToInit)
    {
	    // automatic initialization
		//Determines strong corners on an image.
        goodFeaturesToTrack(gray, points[1], MAX_COUNT, 0.01, 10, Mat(), 3, 0, 0.04);
	    //Refines the corner locations.
        cornerSubPix(gray, points[1], subPixWinSize, Size(-1,-1), termcrit);
	    //points[0].insert(points[0].end(), points[1].begin(),points[1].end());
	    //initial.insert(initial.end(), points[1].begin(),points[1].end());
        addRemovePt = false;
		preAvgX = 0;
		preAvgY = 0;

        pVal = false;
    }
    else if( !points[0].empty() )
    {
        vector<uchar> status;
        vector<float> err;
        if(prevGray.empty())
            gray.copyTo(prevGray);
        calcOpticalFlowPyrLK(prevGray, gray, points[0], points[1], status, err, winSize, 3, termcrit, 0, 0.001);
        size_t i, k;
        for( i = k = 0; i < points[1].size(); i++ )
        {
            if( addRemovePt )
            {
                if( norm(point - points[1][i]) <= 5 )
                {
                        addRemovePt = false;
                        continue;
                }
            }

            if( !status[i] )
                continue;
            points[1][k++] = points[1][i];

		    circle( image, points[0][i], 4, Scalar(0,0,255), -1, 8);
            circle( image, points[1][i], 4, Scalar(0,255,255), -1, 8);
		    arrowedLine(image,points[0][i],points[1][i],Scalar(0,255,0),3,8,0,0.8);

		    sumX = sumX + (points[1][i].x - points[0][i].x);
			sumY = sumY + (points[1][i].y - points[0][i].y);
        }
        points[1].resize(k);

	    if (points[1].size()==0)
        {
            LOGI("No feature points.");
            //return NULL;
            pVal = false;
        }
		else
		{


		    avgX = (sumX / (int)points[1].size()) + preAvgX;
			avgY = (sumY / (int)points[1].size()) + preAvgY;

            sprintf(position[0], "%d", -avgX);
            sprintf(position[1], "%d", avgY);

			preAvgX = avgX;
			preAvgY = avgY;

            LOGI("total move ... X=%d, Y=%d", -avgX, avgY);

            pVal = true;
			//if (avgX <0 )
			//	cout <<"X�V������: "<< avgX << "����" <<endl;
			//else
			//	cout <<"X�V�k����: "<< avgX << "����" <<endl;
			//if (avgY <0 )
			//	cout <<"Y�V�W����: "<< avgY<< "����" <<endl;
			//else
			//	cout <<"Y�V�U����: "<< avgY << "����" <<endl;

		}

    }

    if( addRemovePt && points[1].size() < (size_t)MAX_COUNT )
    {
        vector<Point2f> tmp;
        tmp.push_back(point);
        cornerSubPix( gray, tmp, winSize, cvSize(-1,-1), termcrit);
        points[1].push_back(tmp[0]);
        addRemovePt = false;
        //return NULL;
        pVal = false;

    }
    needToInit = false;
    cnt++;
    std::swap(points[1], points[0]);
    cv::swap(prevGray, gray);

    image.copyTo(frame);

    if(pVal == false)
        return NULL;
    else{
        dataArray = (jobjectArray)env->NewObjectArray(2, env->FindClass("java/lang/String"), env->NewStringUTF(""));
        env->SetObjectArrayElement(dataArray, 0, env->NewStringUTF(position[0]));
        env->SetObjectArrayElement(dataArray, 1, env->NewStringUTF(position[1]));

         //pVal = false;
        return dataArray;
    }

}

void arrowedLine(Mat img, Point pt1, Point pt2, const Scalar& color,
           int thickness, int line_type, int shift, double tipLength)
{
    const double tipSize = norm(pt1-pt2)*tipLength; // Factor to normalize the size of the tip depending on the length of the arrow

    line(img, pt1, pt2, color, thickness, line_type, shift);

    const double angle = atan2( (double) pt1.y - pt2.y, (double) pt1.x - pt2.x );

    Point p(cvRound(pt2.x + tipSize * cos(angle + CV_PI / 4)),
        cvRound(pt2.y + tipSize * sin(angle + CV_PI / 4)));
    line(img, p, pt2, color, thickness, line_type, shift);

    p.x = cvRound(pt2.x + tipSize * cos(angle - CV_PI / 4));
    p.y = cvRound(pt2.y + tipSize * sin(angle - CV_PI / 4));
    line(img, p, pt2, color, thickness, line_type, shift);
}