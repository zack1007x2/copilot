#include <com_moremote_copilot_drone_manager_IdentifyManager.h>
#include <iostream>
#include <cv.h>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <math.h>
#include <string.h>
#include <vector>
#include <Drone_process.h>

#include <android/log.h>
#include <stdio.h>
#define  TAG "INFO"

using namespace cv;
using namespace std;

#define LOGI(...) __android_log_print(ANDROID_LOG_INFO,TAG,__VA_ARGS__)

JNIEXPORT void JNICALL Java_com_moremote_copilot_drone_manager_IdentifyManager_contour_1image
(JNIEnv *, jobject, jlong);

JNIEXPORT void JNICALL Java_com_moremote_copilot_drone_manager_IdentifyManager_binary_1image
(JNIEnv *, jobject, jlong);


JNIEXPORT jobjectArray JNICALL Java_com_moremote_copilot_drone_manager_IdentifyManager_QRLocation
  (JNIEnv *, jobject, jlong);

// large square
vector<vector<Point> > findLargesquare( Mat , vector<vector<Point> > ,  vector<Vec4i>  , int, int ) ;
// small square
vector<vector<Point> > findSmallsquare( Rect, Mat,  vector<vector<Point> > ,vector<Vec4i> , int , int );
// point cross
Point pointcenter( int  ,int , int  ,int , int  ,int , int  ,int );
//????
float rotationAngle(Mat, R2, vector<vector<Point> >, Rect,int );

string int2str(int &i)
{
	string s;
	stringstream ss(s);
	ss << i;
	return ss.str();
}

int Otsu_threshold(Mat f)
{
	int i, j, k, thresh;
	double p[256], P1[256], m[256], var_B[256], mean, max_var;

	if ( f.channels() != 1 ) return 0;

	int nr = f.rows;
	int nc = f.cols;

	// Normalized Histogram
	for ( i = 0 ; i < 256 ; i++ ) p[i] = 0.0;

	for ( i = 0 ; i < nr ; i++ )
	for ( j = 0 ; j < nc; j++ )
		p[f.ptr<uchar>(i)[j]] += 1.0;

	for ( i = 0 ; i < 256 ; i++ )
		p[i] /= ( (double)nr * (double)nc );

	// Cumulative Sums
	for ( k = 0 ; k < 256 ; k++ )
	{
		P1[k] = 0.0;
		for ( i = 0 ; i <= k ; i++ )
			P1[k] += p[i];
	}

	// Cumulative Means
	for ( k = 0 ; k < 256 ; k++ )
	{
		m[k] = 0.0;
		for ( i = 0 ; i <= k ; i++ )
			m[k] += ( (double)i * p[i] );
	}

	// Global Mean
	mean = 0.0;
	for ( i = 0 ; i < 256 ; i++ )
		mean += ( (double)i * p[i] );

	// Between-Class Variance
	for ( k = 0 ; k < 256 ; k++ )
	var_B[k] = ( mean * P1[k] - m[k] ) * ( mean * P1[k] - m[k] ) / ( P1[k] * ( 1.0 - P1[k] ) );

	// Obtain Otsu Threshold
	thresh = 0;
	max_var = 0.0;
	for ( k = 0 ; k < 256 ; k++ )
	{
		if ( max_var < var_B[k] )
		{
			thresh = k;
			max_var = var_B[k];
		}
	}
	return thresh;
}

JNIEXPORT jobjectArray JNICALL Java_com_moremote_copilot_drone_manager_IdentifyManager_QRLocation
  (JNIEnv *env, jobject, jlong matCamera)
  {
	LOGI("Start Drone main ...");

	Point centerPoint;
	Point outputPoint;

		Mat& srcImage = *(Mat*)matCamera;
		Mat grayImage;
		//Mat TImage;

		int TValue;

		LOGI("SrcImage width= %d, height=%d", srcImage.rows, srcImage.cols);
		int srcArea = srcImage.rows*srcImage.cols;

		centerPoint = Point(srcImage.cols/2 , srcImage.rows/2);
	    Mat cSrcImage;
	  	srcImage.copyTo(cSrcImage);
		//circle(srcImage, centerPoint,3, Scalar(255,0,128), -1, 8, 0 );
		cvtColor( cSrcImage, grayImage, COLOR_BGR2GRAY);
		//Bernsen_threshold(grayImage, TImage);
		TValue = Otsu_threshold(grayImage);
		LOGI("Threshold value=%d", TValue);
     	threshold( grayImage, grayImage, TValue, 255, THRESH_BINARY);

		//Mat CImage;
		//TImage.copyTo(CImage);
		//bitwise_not( CImage, CImage);

		vector<vector<Point> >allContours;
		vector<vector<Point> >contours;
		vector<Vec4i> maskHierarchy;

		findContours( grayImage, allContours, maskHierarchy, CV_RETR_CCOMP  , CV_CHAIN_APPROX_SIMPLE, Point(0, 0));
		contours.clear();

		//========================test====================================================================================//
		/*
		vector<vector<Point> > allContours_poly( allContours.size() );
        vector<Rect> boundRect( allContours.size() );

        for( int i = 0; i < allContours.size(); i++ )
        {
        	approxPolyDP( Mat(allContours[i]), allContours_poly[i], 3, true );
            boundRect[i] = boundingRect( Mat(allContours_poly[i]) );

            if(boundRect[i].area() > 8000)
            {//????????n?W?L900?A?h?e?b?v???W
            	drawContours( srcImage, allContours_poly, i, Scalar(255,246,0), 1, 8, vector<Vec4i>(), 0, Point() );
                rectangle( srcImage, boundRect[i].tl(), boundRect[i].br(), Scalar(255,246,0), 2, 8, 0 );
           	}
        }
		*/
		//========================test====================================================================================//


	    contours = findLargesquare( grayImage, allContours , maskHierarchy, allContours.size(), srcArea);

		allContours.clear();
		maskHierarchy.clear();

		vector<vector<cv::Point> > contours_poly( contours.size() );
		vector<Vec4i> maskHierarchy3;
		vector<vector<Point> > allContours2;
		Mat graySrcImage2;
		vector<vector<Point> > contours2;
		Point bigTargetpoint;


		for( int p = 0; p < contours.size(); p++ )
		{
			approxPolyDP( Mat(contours[p]), contours_poly[p], 3, true );

			if(contours_poly[p].size() !=4)
			{
				continue;
			}

			if(!isContourConvex(contours_poly[p]))
			{
				continue;
			}

			Rect contourareaRect = boundingRect(contours_poly[p]);
			//LOGI("Contours width= %d, height=%d", contourareaRect.width, contourareaRect.height);

			int areaThreshold = contourareaRect.area();

			//position[0] = outputPoint.x;
			//position[1] = outputPoint.y;
			// memory set
			char position[2][10];
			char rotation[10];
			jobjectArray dataArray;

			memset(position, 0x00, sizeof(position));
        	memset(rotation, 0x00, sizeof(rotation));


			Rect maxAabbArea = boundingRect(contours_poly[p]);
			Mat matMaxObjAabb = cSrcImage(maxAabbArea);

			cvtColor(matMaxObjAabb, graySrcImage2, COLOR_BGR2GRAY);

			//normalize
			//normalize(graySrcImage2, graySrcImage2, 0, 255, CV_MINMAX, CV_8U);
			int small_Tvalue = Otsu_threshold(graySrcImage2);
			LOGI("small Threshold value=%d", small_Tvalue);
			threshold(graySrcImage2, graySrcImage2, small_Tvalue,255,THRESH_BINARY);

			//bitwise_not(graySrcImage2,graySrcImage2);

			findContours(graySrcImage2, allContours2, maskHierarchy3, CV_RETR_CCOMP , CV_CHAIN_APPROX_SIMPLE , Point(0, 0));
			contours2.clear();


			contours2 = findSmallsquare (maxAabbArea , matMaxObjAabb, allContours2,  maskHierarchy3, areaThreshold, allContours2.size() );
			maskHierarchy3.clear();
			allContours2.clear();

			R2 suqareOrder;
			vector<vector<cv::Point> > contours_poly2( contours2.size() );
			vector<vector<Point> > contours2_point;

			contours2_point.clear();


			for( int i = 0; i < contours2.size(); i++ )
			{
				approxPolyDP( Mat(contours2[i]), contours_poly2[i], 3, true );
				if(contours_poly2[i].size() !=4){
					continue;
				}
				if(!isContourConvex(contours_poly2[i])){
					continue;
				}
				drawContours(matMaxObjAabb,contours_poly2,i,Scalar(0,0,255), 4, 8);
				contours2_point.push_back(contours_poly2[i]);
			}
			contours_poly2.clear();
			circle(cSrcImage, centerPoint,3, Scalar(255,0,128), -1, 8, 0 );
			cSrcImage.copyTo(srcImage);

			//  Determine the p-th square  if the number of Small squares more then 1
			if( contours2_point.size() >1)
			{
				drawContours(cSrcImage,contours_poly,p,Scalar(0,255,0), 6, 8);
				bigTargetpoint = pointcenter (contours_poly[p][0].x,  contours_poly[p][0].y, contours_poly[p][2].x, contours_poly[p][2].y, contours_poly[p][1].x, contours_poly[p][1].y, contours_poly[p][3].x, contours_poly[p][3].y);

				circle(cSrcImage, bigTargetpoint,3, Scalar(255,0,255), 3, 8, 5 );

				outputPoint = centerPoint - bigTargetpoint;
				sprintf(position[0], "%d", -outputPoint.x);
				sprintf(position[1], "%d", outputPoint.y);
				// center line
				line(cSrcImage,bigTargetpoint,centerPoint,Scalar(0,128,255),2,CV_AA,0);
				cSrcImage.copyTo(srcImage);
				float rrs;
				//  Determine the Rotation Angle if the number of Small squares more then 2
				if( contours2_point.size() >2 )
				{
					suqareOrder = orderSmallsquare ( contours2_point, contours2_point.size());
					//Angle Calculate and output angle

					rrs = rotationAngle (matMaxObjAabb, suqareOrder, contours2_point , maxAabbArea, contours2_point.size() );
					sprintf(rotation, "%f", rrs);

					dataArray = (jobjectArray)env->NewObjectArray(3, env->FindClass("java/lang/String"), env->NewStringUTF(""));

					env->SetObjectArrayElement(dataArray, 0, env->NewStringUTF(position[0]));
					env->SetObjectArrayElement(dataArray, 1, env->NewStringUTF(position[1]));
					env->SetObjectArrayElement(dataArray, 2, env->NewStringUTF(rotation));

					cSrcImage.copyTo(srcImage);

					return dataArray;
				}
				else{

					rrs = 0.0;
					sprintf(rotation, "%f", rrs);
					dataArray = (jobjectArray)env->NewObjectArray(3, env->FindClass("java/lang/String"), env->NewStringUTF(""));

                	env->SetObjectArrayElement(dataArray, 0, env->NewStringUTF(position[0]));
                	env->SetObjectArrayElement(dataArray, 1, env->NewStringUTF(position[1]));
                	env->SetObjectArrayElement(dataArray, 2, env->NewStringUTF(rotation));

                	cSrcImage.copyTo(srcImage);
                    return dataArray;
				}
			}
			else
				continue;

		}
		circle(cSrcImage, centerPoint,3, Scalar(255,0,128), -1, 8, 0 );
	  	cSrcImage.copyTo(srcImage);
		// result to srcImage (No little rect)
		return NULL;

  }// JNI C
//}//external C
JNIEXPORT void JNICALL Java_com_moremote_copilot_drone_manager_IdentifyManager_binary_1image
(JNIEnv *env, jobject, jlong matCamera)
{
	Mat& srcImage = *(Mat*)matCamera;
	Mat grayImage;
	Mat TImage;
	int TValue;

	cvtColor( srcImage, grayImage, COLOR_BGR2GRAY);
	TValue = Otsu_threshold(grayImage);
	//LOGI("Threshold value=%d", TValue);
	threshold( grayImage, TImage, TValue, 255, THRESH_BINARY);

	TImage.copyTo(srcImage);
}

JNIEXPORT void JNICALL Java_com_moremote_copilot_drone_manager_IdentifyManager_contour_1image
(JNIEnv *env, jobject, jlong matCamera)
{
	Mat& srcImage = *(Mat*)matCamera;
	Mat grayImage;
	Mat TImage;
	Mat CImage;
	int TValue;

	vector<vector<Point> >allContours;
	vector<Vec4i> maskHierarchy;

	cvtColor( srcImage, grayImage, COLOR_BGR2GRAY);
	TValue = Otsu_threshold(grayImage);
	//LOGI("Threshold value=%d", TValue);
	threshold( grayImage, TImage, TValue, 255, THRESH_BINARY);

	TImage.copyTo(CImage);
	//bitwise_not( CImage, CImage);
	findContours( CImage, allContours, maskHierarchy, CV_RETR_CCOMP, CHAIN_APPROX_NONE, Point(0, 0));

	vector<vector<Point> > contours_poly( allContours.size() );
	vector<Rect> boundRect( allContours.size() );
	RNG rng(12345);

	//LOGI("Contour size: %d", allContours.size());

	for( int i = 0; i < allContours.size(); i++ )
	{
		approxPolyDP( Mat(allContours[i]), contours_poly[i], 3, true );
		//計算可以包含輪廓的最小長方形區域

		boundRect[i] = boundingRect( Mat(contours_poly[i]) );

		Scalar smallcolor = Scalar( 255,255,  0 );
		Scalar largecolor = Scalar(   0,  0,255 );

		if(boundRect[i].area() < 900 && boundRect[i].area() > 30 )
		{
			drawContours( srcImage, contours_poly, i, smallcolor, 1, 8, vector<Vec4i>(), 0, Point() );
			rectangle( srcImage, boundRect[i].tl(), boundRect[i].br(), smallcolor, 2, 8, 0 );
		}
		else if(boundRect[i].area() >= 900)
		{
		drawContours( srcImage, contours_poly, i, largecolor, 1, 8, vector<Vec4i>(), 0, Point() );
		rectangle( srcImage, boundRect[i].tl(), boundRect[i].br(), largecolor, 2, 8, 0 );
		}
	}

}