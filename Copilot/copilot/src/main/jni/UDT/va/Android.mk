LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

OPENCV_LIB_TYPE:=STATIC
include /Applications/adt-bundle-mac-x86_64-20140702/sdk/OpenCV-2.4.9-android-sdk/sdk/native/jni/OpenCV.mk

LOCAL_MODULE    := Drone_main
LOCAL_SRC_FILES := Drone_main.cpp Drone_process.cpp moving.cpp

LOCAL_LDLIBS += -llog

include $(BUILD_SHARED_LIBRARY)