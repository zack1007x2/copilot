/**
 * Created by kentpon on 2015/4/30.
 */
#include <string.h>
#include <jni.h>
#include <android/log.h>
#include <android/bitmap.h>
#include <stdlib.h>
#include <stdio.h>
/*-------------------------------- for UDT -----------------------------------------*/

#ifndef WIN32
   #include <unistd.h>
   #include <cstdlib>
   #include <cstring>
   #include <netdb.h>
#else
   #include <winsock2.h>
   #include <ws2tcpip.h>
   #include <wspiapi.h>
#endif
#include <iostream>
#include <udt.h>
#include "cc.h"
#include "test_util.h"
#include "api.h"
using namespace std;
#include <netinet/in.h>
#include <arpa/inet.h>
/*-------------------------------- UDT END------------------------------------------*/
extern "C"{
//#include <unistd.h>

#define LOG_TAG "UDTServerJni"
#define LOGI(...) {__android_log_print(ANDROID_LOG_INFO, LOG_TAG, __VA_ARGS__);}
#define LOGE(...) {__android_log_print(ANDROID_LOG_ERROR, LOG_TAG, __VA_ARGS__);}
#define LOGD(...) {__android_log_print(ANDROID_LOG_DEBUG, LOG_TAG, __VA_ARGS__);}
/*-------------------------------- for UDT -----------------------------------------*/
#define MTU 1000
bool sendUDTPacket(UDTSOCKET UDTserverFD, char* data, int size);
// Automatically start up and clean up UDT module.
//UDTUpDown _udt_;

struct packet_info
{
	int buffLen;
	//unsigned int seq;
	u_int8_t buff[MTU];
};

int createUDPSocket(int nPort)
{
	srand(time(NULL));
    int sockfd;
    struct sockaddr_in address;

    sockfd = socket(AF_INET, SOCK_DGRAM, 0);
    if(sockfd < 0) {
        LOGE("create socket error");
        cout<<"error in line "<<__LINE__<<endl;
        return 0;
    }
    memset(&address, 0, sizeof(address));
    address.sin_family = AF_INET;
    address.sin_addr.s_addr = htonl(INADDR_ANY);
    address.sin_port = htons(nPort);
    if( bind(sockfd, (struct sockaddr *) &address, sizeof(address)) < 0) {
		LOGE("create socket error at bind");
		cout<<"error in line "<<__LINE__<<endl;
		return 0;
	}
    LOGE("create socked success %d at %d",sockfd,nPort);
	return sockfd;
}

JNIEXPORT void Java_com_moremote_copilot_connection_p2p_UDTServer_startupUDT(JNIEnv *env, jobject obj)
{
	UDT::startup();
}

JNIEXPORT void Java_com_moremote_copilot_connection_p2p_UDTServer_cleanupUDT(JNIEnv *env, jobject obj)
{
	UDT::cleanup();
}

JNIEXPORT void Java_com_moremote_copilot_connection_p2p_UDTServer_closeUDTSocket(JNIEnv *env, jobject obj, jint UDTFD)
{
	UDT::close( (UDTSOCKET)UDTFD );
}

JNIEXPORT jbyteArray Java_com_moremote_copilot_connection_p2p_UDTServer_receiveByUDT( JNIEnv* env, jobject obj, jint UDTFD)
{
	UDTSOCKET UDTserverFD = (UDTSOCKET)UDTFD;

	int nSize= 500;
	char* data;
	data = new char[nSize];
	int rs, len;

	int rsize = 0;
	while (rsize < nSize)
	{
//		int rcv_size;
//		int var_size = sizeof(int);
//		UDT::getsockopt(UDTserverFD, 0, UDT_RCVDATA, &rcv_size, &var_size);
		if (UDT::ERROR == (rs = UDT::recvmsg(UDTserverFD, data + rsize, nSize - rsize)))
		{
		   LOGE("recv: %s", UDT::getlasterror().getErrorMessage());
		   return NULL;
		}
		rsize += rs;
	}

	struct packet_info *pinfo = (struct packet_info *)data;
	jbyteArray AudioBuffer = env->NewByteArray(pinfo->buffLen);

    LOGD("##get 0:%2x",pinfo->buff[0]);
    LOGD("##get 0:%2x",pinfo->buff[1]);
	env->SetByteArrayRegion(AudioBuffer, 0, pinfo->buffLen, (jbyte *)pinfo->buff);

	LOGD("##before return0:%2x",AudioBuffer[0]);
	LOGD("##before return1:%2x",AudioBuffer[1]);

	delete [] data;

	return AudioBuffer;
}

time_t start = 0, now;
unsigned int sum = 0;
JNIEXPORT jboolean Java_com_moremote_copilot_connection_p2p_UDTServer_sendByUDT( JNIEnv* env, jobject obj, jint UDTFD, jbyteArray buff, jint buffLen)
{
    if( start == 0 )
        time(&start);
    //LOGD("UDT SEND JNI");
	signed char* pBuff=env->GetByteArrayElements(buff,NULL);

	bool ret = sendUDTPacket( (UDTSOCKET)UDTFD, (char*)pBuff, buffLen );
	env->ReleaseByteArrayElements(buff, pBuff, 0);

	if(++sum %100 == 1){
		time(&now);
		LOGD("avg %d n %d s", sum, now-start);
	}
	return ret;
}

bool sendUDTPacket(UDTSOCKET UDTserverFD, char* data, int size)
{
      int ssize = 0, retryCounts = 0;
      int ss;
      while (ssize < size)
      {
		  if (UDT::ERROR == (ss = UDT::sendmsg(UDTserverFD, data + ssize, size - ssize, 1000, false)))
         {
            LOGE("send: %s",UDT::getlasterror().getErrorMessage());
            if(UDT::getlasterror().getErrorCode() == CUDTException::EASYNCSND) {

				int eid = UDT::epoll_create();
				LOGE("eid = %d", eid);
				int events = UDT_EPOLL_OUT;
				int res = UDT::epoll_add_usock(eid, UDTserverFD, &events);
				LOGE("epoll_add_usock res = %d", res);
				UDT::UDSET writefds;
				res = UDT::epoll_wait(eid, NULL, &writefds, 1000);
				LOGE("epoll wakeup %d", res);
				res = UDT::epoll_release(eid);
				retryCounts ++;
				if (retryCounts > 30) {
					return (ssize < size);
				}
            	continue;
            }
            else break;

         }
         ssize += ss;
         if(ssize < size) LOGE("send %d", ssize);
      }

	  return (ssize<size);
}

JNIEXPORT jint Java_com_moremote_copilot_connection_p2p_UDTServer_createUDTServer(JNIEnv *env, jobject obj, jint port) {


	char *sPort="2000";
	if (sPort == NULL) {
		LOGE("createUDTServer sPort is NULL");
	}
	else{
		LOGD("createUDTServer sport = %s",sPort);
	}

	addrinfo hints;
	addrinfo* res;

	memset(&hints, 0, sizeof(struct addrinfo));

	hints.ai_flags = AI_PASSIVE;
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_DGRAM;


	if (0 != getaddrinfo(NULL, sPort, &hints, &res))
	{
		LOGE("createUDTServer illegal port number or port is busy.");
		return 0;
	}

	UDTSOCKET serv = UDT::socket(res->ai_family, res->ai_socktype, res->ai_protocol);
	int size;
	int sizelen = sizeof(int);
	UDT::setsockopt(serv, 0, UDT_SNDSYN, new bool(false), sizeof(bool));
	LOGD("UDT_SNDSYN %d", size);
	UDT::getsockopt(serv, 0, UDT_SNDBUF, &size, &sizelen);
	LOGD("UDT_SNDBUF %d", size);
	size = 300000;
	UDT::setsockopt(serv, 0, UDT_SNDBUF, &size, sizelen);
	size = 512;
	UDT::setsockopt(serv, 0, UDT_FC, &size, sizelen);
	UDT::getsockopt(serv, 0, UDT_FC, &size, &sizelen);
//    LOGD("UDT_FC %d", size);
//    size = 5;
//    UDT::setsockopt(serv, 0, UDT_SNDTIMEO, &size, sizelen);
//    UDT::getsockopt(serv, 0, UDT_SNDTIMEO, &size, &sizelen);
    LOGD("UDT_SNDTIMEO %d", size);
	int sockfd = createUDPSocket(port);
	LOGD("createUDTServer get socket fd %d at %d",sockfd,port);

	if(sockfd == 0){
		LOGE("createUDTServer Create UDP socket error @@");
		return 0;
	}

	if (UDT::ERROR == UDT::bind2(serv, sockfd))
	{
		LOGE("createUDTServer bind2 error:%s @@",UDT::getlasterror().getErrorMessage());
		return 0;
	}
	else
		LOGD("createUDTServer bind2 success!! %d",serv);

	freeaddrinfo(res);

	LOGD("createUDTServer server is ready at port: %s @@", sPort)

	if (UDT::ERROR == UDT::listen(serv, 10))
	{
		LOGE("createUDTServer listen: %s",UDT::getlasterror().getErrorMessage());
		return 0;
	}
	LOGD("createUDTServer after listen");

    return (int)serv;

//	   UDT::close(serv);

}

JNIEXPORT jint Java_com_moremote_copilot_connection_p2p_UDTServer_AcceptUDTServer(JNIEnv *env, jobject obj, jint UDTServ) {
    UDTSOCKET serv = (UDTSOCKET) UDTServ;
    sockaddr_storage clientaddr;
    int addrlen = sizeof(clientaddr);

    UDTSOCKET UDTserverFD;

    if (UDT::INVALID_SOCK == (UDTserverFD = UDT::accept(serv, (sockaddr*)&clientaddr, &addrlen))){
         LOGE("createUDTServer accept: %s",UDT::getlasterror().getErrorMessage());
         return 0;
    }

    char clienthost[NI_MAXHOST];
    char clientservice[NI_MAXSERV];
    getnameinfo((sockaddr *)&clientaddr, addrlen, clienthost, sizeof(clienthost), clientservice, sizeof(clientservice), NI_NUMERICHOST|NI_NUMERICSERV);
    LOGD("createUDTServer new connection: %s:%s", clienthost, clientservice);

    return (int)UDTserverFD;
}

}//extern c
