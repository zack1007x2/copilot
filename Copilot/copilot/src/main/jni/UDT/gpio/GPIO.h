//
// Created by 高宗永 on 2016/4/29.
//
#include <jni.h>

#ifndef IOTBOXMANGER_GPIO_H
#define IOTBOXMANGER_GPIO_H

#ifdef __cplusplus
extern "C" {
#endif
JNIEXPORT void Java_com_moremote_copilot_utils_gpio_GPIO_setGPIO(JNIEnv *env, jobject obj , jint pinNum , jint status);
JNIEXPORT jstring Java_com_moremote_copilot_utils_gpio_GPIO_getGPIO(JNIEnv *env, jobject obj , jint pinNum);
JNIEXPORT jstring Java_com_moremote_copilot_utils_gpio_GPIO_getDirection(JNIEnv *env, jobject obj , jint pinNum);

#ifdef __cplusplus
}
#endif

#endif //IOTBOXMANGER_GPIO_H
