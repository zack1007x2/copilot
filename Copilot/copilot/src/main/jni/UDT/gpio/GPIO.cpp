//
// Created by 高宗永 on 2016/4/29.
//

#include "GPIO.h"
#include <string.h>
#include <jni.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <string.h>
#include <stdint.h>
#include <termios.h>
#include <android/log.h>
#include <sys/ioctl.h>

#include <android/log.h>

#define LOG_TAG "GPIO"
#define LOGI(...) {__android_log_print(ANDROID_LOG_INFO, LOG_TAG, __VA_ARGS__);}
#define LOGE(...) {__android_log_print(ANDROID_LOG_ERROR, LOG_TAG, __VA_ARGS__);}
#define LOGD(...) {__android_log_print(ANDROID_LOG_DEBUG, LOG_TAG, __VA_ARGS__);}

#define GPIO_PIN_DIR "/sys/class/gpio/"
#define GPIO_IF_PREFIX "gpio"



JNIEXPORT void Java_com_moremote_copilot_utils_gpio_GPIO_setGPIO(JNIEnv *env, jobject obj , jint pinNum , jint status) {

    LOGD("hello");

    char gpio_path[30];
    char statusStr[10];
    FILE* fd;
    memset(gpio_path,0,30);
    memset(statusStr,0,10);
    sprintf(gpio_path, "%s%s%d/value", GPIO_PIN_DIR, GPIO_IF_PREFIX, pinNum);

    LOGD("hello2 : %s",gpio_path);
    fd = fopen(gpio_path, "w");

    sprintf(statusStr,"%d",status);
    LOGD("hello3 : %s",statusStr);
    fwrite(statusStr,1,1,fd);
    fclose(fd);


}

JNIEXPORT jstring Java_com_moremote_copilot_utils_gpio_GPIO_getGPIO(JNIEnv *env, jobject obj , jint pinNum) {

    char gpio_path[30];
    char statusStr[10];
    FILE* fd;
    memset(gpio_path,0,30);
    memset(statusStr,0,10);
    sprintf(gpio_path, "%s%s%d/value", GPIO_PIN_DIR, GPIO_IF_PREFIX, pinNum);
    fd = fopen(gpio_path, "r");

    fread(statusStr,1,1,fd);
    fclose(fd);

    jstring  result = env->NewStringUTF(statusStr);

    return result;
}

JNIEXPORT jstring Java_com_moremote_copilot_utils_gpio_GPIO_getDirection(JNIEnv *env, jobject obj , jint pinNum) {


    char gpio_path[30];
    char statusStr[10];
    FILE* fd;
    memset(gpio_path,0,30);
    memset(statusStr,0,10);
    sprintf(gpio_path, "%s%s%d/direction", GPIO_PIN_DIR, GPIO_IF_PREFIX, pinNum);
    fd = fopen(gpio_path, "r");

    fread(statusStr,1,10,fd);
    fclose(fd);

    jstring  result = env->NewStringUTF(statusStr);

    return result;
}