package moremote.com.iotboxmanger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by gaozongyong on 2016/4/15.
 */
public class GPIO {

    static {
        System.loadLibrary("GPIOControl");
    }
    private native void setGPIO(int pinNum , int status);
    private native String getGPIO(int port);
    private native String getDirection(int port);

    public String port;
    public int pin;
    public GPIO(int pin){
        this.port = "gpio"+pin;
        this.pin = pin;
    }

    private String executeCommand(String command)
    {

        String readString = "";
        try {
            Process p = Runtime.getRuntime().exec(new String[]{"su", "-c", command});
            BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
            StringBuilder text = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                text.append(line);
                text.append("\n");
            }
            readString = text.toString();
        }catch (IOException e){

        }
        return  readString;
    }

    // Get GPIO Direction
    public String getInOut()
    {
//        String command = String.format("cat /sys/class/gpio/%s/direction",this.port);
//        return executeCommand(command);

        return getDirection(pin);
    }

    // Get GPIO State
    public int getState()
    {
        return Integer.valueOf(getGPIO(pin));

//        String command = String.format("cat /sys/class/gpio/%s/value",this.port);
//        String ret =  executeCommand(command);
//        try {
//            if(ret.equals("")){
//                return -1;
//            }else{
//                return Integer.parseInt(ret.substring(0,1));
//            }
//
//        }catch (NumberFormatException e)
//        {
//            return -1;
//        }

    }

    // Set value of output
    public boolean setState(int value)
    {
        setGPIO(pin,value);
//        String command = String.format("echo %d > /sys/class/gpio/%s/value", value, this.port);
//        try{
//
//            Runtime.getRuntime().exec(new String[]{"su", "-c", command});
//            return true;
//
//        }catch(IOException e){
//            return false;
//        }
        return true;

    }

    // Set direction
    public boolean setInOut(String direction)
    {
        String command = String.format("echo %s > /sys/class/gpio/%s/direction", direction, this.port);
        try{

            Runtime.getRuntime().exec(new String[]{"su", "-c", command});
            return true;

        }catch(IOException e){
            return false;
        }
    }

    // export GPIO
    public boolean activePin(){
        String command = String.format("echo %d > /sys/class/gpio/export",this.pin);
        try{
            Runtime.getRuntime().exec(new String[] {"su", "-c", command});
            return true;
        }catch (IOException e){
            return false;
        }
    }

    // unexport GPIO
    public  boolean inactivePin(){
        String command = String.format("echo %d > /sys/class/gpio/unexport",this.pin);
        try{
            Runtime.getRuntime().exec(new String[] {"su", "-c", command});
            return true;
        }catch (IOException e)
        {
            return false;
        }
    }

    // init pin
    public int initPin(String direction){

        int retour = 0;
        boolean ret = true;

        // Check if gpio is already set
        retour = getState();
//        if(retour == -1) {
//            // unexport pin
//            ret = inactivePin();
//            if(ret == false){
//                retour = -1;
//            }
//
//            // export pin
//            ret = activePin();
//            if(ret == false){
//                retour = -2;
//            }
//        }
//
//        // check gpio direction
//        String inout = getInOut();
//        if (!inout.contains(direction)){
//            ret = setInOut(direction);
//            if(ret == false){
//                retour = -3;
//            }
//        }

        return retour;
    }

}
