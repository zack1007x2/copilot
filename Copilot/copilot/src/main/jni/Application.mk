APP_STL := gnustl_static
APP_PLATFORM := android-8
APP_MODULES := UDT UDTServerJni Drone_main NativeUART GPIOControl
APP_ABI := armeabi armeabi-v7a x86 mips
APP_CPPFLAGS := -frtti -fexceptions
