// XmppServiceAIDL.aidl
package com.moremote.copilot;

// Declare any non-default types here with import statements

interface XmppServiceAIDL {
    /**
     * Demonstrates some basic types that you can use as parameters
     * and return values in AIDL.
     */
    void onReceiveMsg(inout byte[] PacketBytes);
}
