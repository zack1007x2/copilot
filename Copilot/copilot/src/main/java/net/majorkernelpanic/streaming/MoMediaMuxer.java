package net.majorkernelpanic.streaming;

import android.annotation.TargetApi;
import android.media.MediaCodec;
import android.media.MediaFormat;
import android.media.MediaMuxer;
import android.os.Build;
import android.util.Log;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;

/**
 * Created by lintzuhsiu on 15/2/12.
 */
public class MoMediaMuxer {

    public static final String TAG = "MoMediaMuxer";

    private String dirPath;
    private String currentFilePath;

    private long nextTime;
    private long startTime;

    private MediaMuxer muxer;
    private MediaFormat videoFormat;
    private MediaFormat audioFormat;
    private int videoTrack;
    private int audioTrack;
    private Boolean isMuxerStart = false;
    private boolean isVideoRecord;
    private boolean isAudioRecord;
    private boolean firstTime = true;
    private String curFileName;

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
    public MoMediaMuxer(String path, boolean isVideoRecord, boolean isAudioRecord) {
        this.nextTime = System.currentTimeMillis();
        this.dirPath = path + "/motivecam";

        this.isVideoRecord = isVideoRecord;
        this.isAudioRecord = isAudioRecord;
        this.videoFormat = null;
        this.audioFormat = null;

        initFolder();
        initMuxer();
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
    public void setMediaFormat(boolean isVideo, MediaFormat mediaFormat) {
        if (isVideo) {
            videoFormat = mediaFormat;
        }
        else {
            audioFormat = mediaFormat;
        }

//        Log.d(TAG,"Video record : "+ (isVideoRecord && videoFormat != null) + "Audio record : " +
//                ""+(isAudioRecord && audioFormat != null));

        if ((isVideoRecord && videoFormat != null)) {
            videoTrack = muxer.addTrack(videoFormat);
            if((isAudioRecord && audioFormat != null)){
                audioTrack = muxer.addTrack(audioFormat);
            }
            muxer.start();
            isMuxerStart = true;
        }
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
    public void writeSampleData(boolean isVideo, ByteBuffer byteBuffer, MediaCodec.BufferInfo bufferInfo) {
        if (muxer == null) {
            return;
        }
        if(!isMuxerStart){
            return ;
        }
        boolean canWrite = true;

        synchronized (isMuxerStart) {
            if (bufferInfo.flags == MediaCodec.BUFFER_FLAG_SYNC_FRAME) {
                if (startTime == 0) {
                    startTime = System.currentTimeMillis();
                }
                else if (System.currentTimeMillis() - startTime >= 120000L) {
                    startTime = 0;
                    stop();

                    initMuxer();
                    muxer.start();
                    isMuxerStart = true;
                }
            }
            else if(firstTime){
                canWrite = false;
                firstTime = false;
            }
            if(canWrite) {
                int track = isVideo ? videoTrack : audioTrack;
                byteBuffer.position(0);
                muxer.writeSampleData(track, byteBuffer, bufferInfo);
                byteBuffer.position(0);
            }
        }
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
    public void stop() {
        Log.d(TAG, "stop muxer");
        if (muxer == null) {
            return;
        }

        try {
            isMuxerStart = false;
            muxer.stop();
            muxer.release();
            muxer = null;
        } catch (IllegalStateException e) {
            Log.d(TAG, "stop muxer failed");
            e.printStackTrace();
            File file = new File(currentFilePath);
            if (file.exists()) {
                file.delete();
            }
        }

        File file = new File(currentFilePath);
        if(file.exists()){
            if(file.length()==0){
                file.delete();
            }
        }
    }

    private void initFolder() {
        File file = new File(dirPath);

        if (!file.exists()) {
            file.mkdirs();
        }
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
    public void initMuxer() {
//        currentFilePath = dirPath + "/test-" + String.valueOf(nextTime) + ".mp4";
//        currentFilePath = dirPath + "/test.mp4";
        currentFilePath = dirPath + "/video-" + android.text.format.DateFormat.format("yyyy-MM-dd-HH-mm-ss", new java.util.Date()) + ".mp4";
        curFileName = "video-" + android.text.format.DateFormat.format("yyyy-MM-dd-HH-mm-ss", new java.util.Date()) + ".mp4";
        startTime = 0;

        try {
            Log.d(TAG, "init muxer");
            muxer = new MediaMuxer(currentFilePath, MediaMuxer.OutputFormat.MUXER_OUTPUT_MPEG_4);
            if (videoFormat != null) {
                videoTrack = muxer.addTrack(videoFormat);
            }

            if (audioFormat != null) {
                audioTrack = muxer.addTrack(audioFormat);
            }
        } catch (IOException e) {
            Log.d(TAG, "init muxer failed");
        }
//        nextTime += 60000;
    }

    public boolean enableVideo(){
        return isVideoRecord;
    }
    public boolean enableAudio(){
        return isAudioRecord;
    }

    public String getCurrentFilePath() {
        return curFileName;
    }
}
