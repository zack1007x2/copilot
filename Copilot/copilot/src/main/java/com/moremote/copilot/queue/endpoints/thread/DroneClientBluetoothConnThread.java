package com.moremote.copilot.queue.endpoints.thread;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.util.Log;

import com.moremote.copilot.IPCamApplication;
import com.moremote.copilot.enums.APP_STATE;
import com.moremote.copilot.queue.endpoints.drone.DroneClientBluetooth;

import java.io.IOException;
import java.util.UUID;

public class DroneClientBluetoothConnThread extends Thread {

	private static final String UUID_SPP = "00001101-0000-1000-8000-00805F9B34FB";

	private static final String TAG = DroneClientBluetoothConnThread.class.getSimpleName();

	private final BluetoothAdapter mmBluetoothAdapter;
	private final BluetoothSocket mmSocket;
	private final BluetoothDevice mmDevice;
	private final DroneClientBluetooth parentConnector;

	public DroneClientBluetoothConnThread(DroneClientBluetooth parent, BluetoothAdapter adapter,
										  BluetoothDevice device) {

		BluetoothSocket tmp = null;
		mmBluetoothAdapter = adapter;
		mmDevice = device;
		parentConnector = parent;

		try {
			tmp = mmDevice.createInsecureRfcommSocketToServiceRecord(UUID.fromString(UUID_SPP));
		}
		catch (IOException e) {
			e.printStackTrace();
		}
		mmSocket = tmp;
	}

	public void run() {

		// Cancel discovery because it will slow down the connection
		mmBluetoothAdapter.cancelDiscovery();

		try {
			Log.d(TAG, "Connecting socket..");
			mmSocket.connect();
		}
		catch (IOException connectException) {

			Log.d(TAG, "Exception: [Failed Connection Attempt]" + connectException.getMessage());
			try {
				mmSocket.close();
				final String msgTxt = connectException.getMessage();
				IPCamApplication.sendAppMsg(APP_STATE.MSG_DRONE_CONNECTION_ATTEMPT_FAILED, msgTxt);
			}
			catch (IOException closeException) {
				Log.d(TAG, "Exception: [Failed Connection Attempt: close failed as well]" + closeException.getMessage());
			}
			return;
		}

		Log.d(TAG, "Connected..");
		// start Receiver on socket
		parentConnector.startClientReaderThread(mmSocket);
	}
}