package com.moremote.copilot.activity;

import android.app.Activity;
import android.os.Bundle;

import com.facebook.FacebookSdk;
import com.moremote.copilot.R;
import com.moremote.copilot.fragments.SettingFragment;


public class SettingsActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        FacebookSdk.sdkInitialize(getApplicationContext());
        setTheme(R.style.SettingTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        getFragmentManager().beginTransaction().replace(R.id.fragment_settings_layout, new SettingFragment()).commit();
    }
}
