package com.moremote.copilot.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ScrollView;
import android.widget.TextView;

import com.moremote.copilot.IPCamApplication;
import com.moremote.copilot.enums.APP_STATE;
import com.moremote.copilot.interfaces.IDataUpdateSysLog;
import com.moremote.copilot.R;

/**
 * Created by Zack on 2015/11/13.
 */
public class NetWorkStateFragment extends HUBFragment implements IDataUpdateSysLog {


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_sys_connection_log, container, false);
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        IPCamApplication.messenger.register(this, APP_STATE.MSG_DATA_UPDATE_SYSLOG);
        onDataUpdateSysLog();
    }

    @Override
    public void onPause() {
        super.onPause();
        IPCamApplication.messenger.unregister(this, APP_STATE.MSG_DATA_UPDATE_SYSLOG);
    }

    @Override
    public void onDataUpdateSysLog() {
        final TextView mTextViewBytesLog = (TextView) (getView().findViewById(R.id.TextView_logSysLog));

        mTextViewBytesLog.setText(IPCamApplication.logger.inMemSysLogBuffer);

        // scroll down
        final ScrollView mScrollView = (ScrollView) (getView().findViewById(R.id.scrollView_logSys));

        mScrollView.post(new Runnable() {
            @Override
            public void run() {
                mScrollView.fullScroll(View.FOCUS_DOWN);
            }
        });
    }
}
