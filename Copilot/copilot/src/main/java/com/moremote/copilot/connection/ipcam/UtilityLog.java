package com.moremote.copilot.connection.ipcam;

import android.util.Log;

import com.moremote.copilot.service.MainService;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by chingchun on 2015/6/30.
 */
public class UtilityLog {
    public static Level level = Level.Debug;
    private static String fileName;

    public static void d(String tag, String msg){
        if (level == Level.Debug) {
            Log.d(tag, msg);
            LogWrite("d", tag, msg);
        }
    }

    public static void e(String tag, String msg){
        Log.e(tag, msg);
    }

    private static void LogWrite(String type, String tag, String msg){
        try {

            fileName = android.text.format.DateFormat.format("yyyy-MM-dd", new java.util.Date()) + ".txt";
            File file = new File(MainService.Instance.getExternalFilesDir(null), fileName);
            FileOutputStream fos = new FileOutputStream(file, true);
            Date curTime = new Date(System.currentTimeMillis());
            SimpleDateFormat formatter = new SimpleDateFormat("MM-dd HH:mm:ss");
            String strCurTime = formatter.format(curTime);
            StringBuffer buffer = new StringBuffer();
            buffer.append(strCurTime);
            buffer.append( " [" + type + "]" );
            buffer.append( " [" + tag + "] " );
            buffer.append( msg + "\n" );
            fos.write(buffer.toString().getBytes());
            fos.flush();
            fos.close();
        } catch (Exception e) {
        }
    }

    public enum Level {
        Release,
        Debug
    }
}
