package com.moremote.copilot;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;
import android.text.TextUtils;
import android.util.Log;

import com.ftdi.j2xx.D2xxManager;
import com.moremote.copilot.broadcaster.BroadCastIntent;
import com.moremote.copilot.enums.APP_STATE;
import com.moremote.copilot.enums.DEVICE_INTERFACE;
import com.moremote.copilot.enums.UI_MODE;
import com.moremote.copilot.messenger.HUBMessenger;
import com.moremote.copilot.queue.endpoints.DroneClient;
import com.moremote.copilot.queue.endpoints.GroundStationServer;
import com.moremote.copilot.queue.endpoints.drone.DroneClientBluetooth;
import com.moremote.copilot.queue.endpoints.drone.DroneClientCDCUSB;
import com.moremote.copilot.queue.endpoints.drone.DroneClientFTDICUSB;
import com.moremote.copilot.queue.endpoints.drone.DroneClientUART;
import com.moremote.copilot.queue.endpoints.gs.GroundStationServerXMPP;
import com.moremote.copilot.queue.hub.HUBQueue;
import com.moremote.copilot.queue.items.ItemMavLinkMsg;
import com.moremote.copilot.service.MainService;
import com.moremote.copilot.utils.CrashHandler;
import com.moremote.copilot.utils.HUBLogger;
import com.moremote.copilot.utils.Utils;
import com.moremote.copilot.viewadapters.devicelist.ItemPeerDevice;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by lintzuhsiu on 14/11/6.
 */
public class IPCamApplication extends MultiDexApplication {

    private static final String TAG = IPCamApplication.class.getSimpleName();

    //
    public static final String ACTION_USB_PERMISSION_BASE = "com.moremote.USB_PERMISSION";
    public static final String LOCAL_STORAGE_PATH = Environment.getExternalStorageDirectory()+"/motivecam/";

    // xmpp config
    public static final String XMPP_PASSWORD = "piuser166";
    public static final String XMPP_SERVER_IP = "xmpp.moremote.com";
                                                //"128.199.174.175";
    public static class XMPPCommand {
		public static final String STREAM_START 	= "streamstart";
		public static final String STREAM_STOP		= "streamstop";
        public static final String PLAY_MUSIC 		= "play_music";
        public static final String PREV_SONG		= "prev_song";
        public static final String NEXT_SONG 		= "next_song";
        public static final String STOP_MUSIC 		= "stop_music";
        public static final String OPEN_LED 		= "open_led";
        public static final String CLOSE_LED		= "close_led";
        public static final String ASK_RELAY		= "askrelay";
        public static final String RELAY 			= "relay:";
        public static final String STUN 			= "stun:";
        public static final String STUN_STOP 		= "stun_stop";
        public static final String STUN_SUCCESS 	= "stun_success:";
        public static final String BRIGHTNESS_UP 	= "brightnessup";
        public static final String BRIGHTNESS_DOWN  = "brightnessdown";
        public static final String ASK_TCP			= "ask_tcp";
        public static final String TCP_CONNECTION	= "tcp_connection:";
        public static final String FINISH 			= "finish";
        public static final String AUDIO_BACK		= "audioback:";
        public static final String ALARM_BABY       = "alarm_baby:";
        public static final String ALARM_SOUND      = "alarm_sound:";
        public static final String CAMERA_TYPE      = "camera_type:";
        public static final String APP_DEVICE       = "app_device:";
        public static final String ASK_SNAPSHOT     = "ask_snapshot";
        public static final String RSP_SNAPSHOT     = "rsp_snapshot";
        public static final String CAMERA_FACING    = "camera_facing:";
        public static final String CAMERA_FACING_SWITCH   = "camera_facing_switch";
        public static final String TCP_CONNECTION_SUCCESS   = "tcp_connection_success:";
        public static final String DRONE            = "drone:";
        public static final String DRONE_CONNECT    = "drone_connect";
        public static final String DRONE_DISCONNECT = "drone_disconnect";
        public static final String GPIO_CMD         = "gpio_";
        public static final String RS232_CMD        = "rs232_command";
        public static final String IOT_GETINFO      = "iot_getinfo";
        public static final String IOT_WIFI_ON      = "iot_wifi_on";
        public static final String IOT_WIFI_OFF     = "iot_wifi_off";
    }

    public static class cameraType{
        public static final int AndroidCam = 0;
        public static final int LinuxCam = 1;
    }
	
	// tcp relay config
	public static final String UUID = "23454567890123456789012345678911";
	public static final String AUTH = "12345000000000000000000000000000";
    public static final String TCP_RELAY_SERVER_IP = "188.166.208.172";
    public static final int TCP_RELAY_SERVER_PORT = 9010;


    // recorder address name
    public static final String VIDEO_ADDRESS_NAME = "com.moremote.motivecam";

	public static class ConnectionType {
		public static final int P2P_WAN_UDT = 0;
		public static final int P2P_LAN_TCP = 1;
		public static final int RELAY = 2;
		public static final int NONE_CONNECT = 3;
	}

    // p2p config
    public static final String P2PSERVER_IP = "128.199.221.156";
    public static int P2PSERVER_PORT = 9010;
    public static final String P2PSERVER_IP2 = "128.199.250.104";
    public static final int P2PSERVER_PORT2 = 9011;

    public static enum NatType {
        NAT_ERROR(-1),
        NAT_NORMAL(0),
        NAT3_UNUSUAL(1),
        NAT4_CGN(2),
        NAT4_LINUX(3),
        NAT4_RTOS(4),
        NAT4_OTHER(5);

        private int index;

        NatType(int idx){
            this.index = idx;
        }
        public int toInt(){
            return index;
        }

        public static NatType get( int idx ){
            switch (idx){
                case -1:    return NAT_ERROR;
                case 0:     return NAT_NORMAL;
                case 1:     return NAT3_UNUSUAL;
                case 2:     return NAT4_CGN;
                case 3:     return NAT4_LINUX;
                case 4:     return NAT4_RTOS;
                case 5:     return NAT4_OTHER;
            }
            return null;
        }
        public boolean compare(int i){
            return index == i;
        }
        public boolean compare(NatType type){
            return index == type.toInt();
        }
    }

    public static String getLocalIpAddress() {
        try {
            for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements();) {
                NetworkInterface intf = en.nextElement();
                
                if (intf.getDisplayName().contains("usbnet")) {
                	continue;
                }
                
                for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements();) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress() && inetAddress instanceof Inet4Address) {
                        return inetAddress.getHostAddress();
                    }
                }
            }
        } catch (SocketException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public static String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return capitalize(model);
        }
        //return capitalize(manufacturer) + " " + model;
        return model;
    }

    private static String capitalize(String str) {
        if (TextUtils.isEmpty(str)) {
            return str;
        }
        char[] arr = str.toCharArray();
        boolean capitalizeNext = true;
        String phrase = "";
        for (char c : arr) {
            if (capitalizeNext && Character.isLetter(c)) {
                phrase += Character.toUpperCase(c);
                capitalizeNext = false;
                continue;
            } else if (Character.isWhitespace(c)) {
                capitalizeNext = true;
            }
            phrase += c;
        }
        return phrase;
    }
	
	@Override
	public void onCreate() {
		super.onCreate();
        //to catch crash log
        CrashHandler crashHandler = CrashHandler.getInstance();
        crashHandler.init(this);
    }

    ///////////////////////////////////////


    public static final int visibleByteLogSize = 256 * 4;
    public static final int visibleMsgList = 50;
    public static final int serverTCP_port = 5760;
    public static final int serverUDP_port = 14550;

    // messages handler
    public static HUBMessenger messenger;

    // main Drone connector
    public DroneClient droneClient;

    // main GS connector
    public GroundStationServer gcsServer;

    // main ItemMavLinkMsg objects queue
    public static HUBQueue queue;

    // sys log stats holder object
    public static HUBLogger logger;

    public static D2xxManager usbHub = null;

    public SharedPreferences prefs;

    public static MainService mService;

    // with initial state as "created"
    public UI_MODE uiMode;

    private static final int FTDI_DEVICE_VENDOR_ID = 0x0403;

    public void hubInit() {
        uiMode = UI_MODE.UI_MODE_CREATED;

        prefs = PreferenceManager.getDefaultSharedPreferences(this);

        // start application asynchronous messaging - has to be first !!!
        messenger = new HUBMessenger(this);

        //start system wide logger
        logger = new HUBLogger(this);

		/*
		<usb-device vendor-id="1027" product-id="24577" /> <!-- FT232RL -->
		<usb-device vendor-id="1027" product-id="24596" /> <!-- FT232H -->
		<usb-device vendor-id="1027" product-id="24597" /> <!-- FT231X 0x0403 / 0x6015 -->
		<usb-device vendor-id="1027" product-id="24592" /> <!-- FT2232C/D/HL -->
		<usb-device vendor-id="1027" product-id="24593" /> <!-- FT4232HL -->
		<usb-device vendor-id="1027" product-id="24597" /> <!-- FT230X -->
		<usb-device vendor-id="1412" product-id="45088" /> <!-- REX-USB60F -->
		<usb-device vendor-id="9025" product-id="16" /><!-- APM 2.5 device -->
		<usb-device vendor-id="9900" product-id="16" /><!-- APM 2.5 device -->
		<usb-device vendor-id="5824" product-id="1155" /><!--  Teensyduino  -->
		<usb-device vendor-id="4292" product-id="60000" /><!-- CP210x UART Bridge -->
		<usb-device vendor-id="1118" product-id="688"/>
		*/



        //start USB driver
        try {
            usbHub = D2xxManager.getInstance(this);
            // setup the additional VIDPIDPAIRs
            if (!usbHub.setVIDPID(0x2341, 0x0010)) Log.d(TAG, "APM 2.5 VIDPID1 setting error");
            if (!usbHub.setVIDPID(0x26ac, 0x0010)) Log.d(TAG, "APM 2.5 VIDPID2 setting error");

        }
        catch (D2xxManager.D2xxException ex) {
            ex.printStackTrace();
        }

        // Default is the BT client
        droneClient = new DroneClientUART(this);

        gcsServer = new GroundStationServerXMPP(this);

        // finally start parsers and distributors
        queue = new HUBQueue(this, 1000);
//        queue.startQueue();
//        try {
//            droneClient.startClient(null);
//        } catch (Exception e) {
//            e.printStackTrace();
//            logger.sysLog("start UART connection failed");
//            droneClient.stopClient();
//        }

        if(!Utils.isMyServiceRunning(this, MainService.class)){
            Intent intent = new Intent(this,MainService.class);
            startService(intent);
            Log.d(TAG, "Start Service");
        }
    }

    public void switchClient(ItemPeerDevice newDevice) {

        if (null != droneClient && droneClient.isConnected()) {
            droneClient.stopClient();
        }

        DEVICE_INTERFACE devs[] = DEVICE_INTERFACE.values();

        switch (devs[newDevice.getDevInterface().ordinal()]) {
            case Bluetooth:
                Log.d(TAG, "start Bluetooth connection");
                logger.sysLog("start Bluetooth connection");
                droneClient = new DroneClientBluetooth(this);
                break;
            case USB:
                if(isFTDIdevice(this)){
                    Log.d(TAG, "start FTDIC USB connection");
                    final DroneClient tmp = new DroneClientFTDICUSB(this);
                    droneClient = tmp;
                }else{
                    Log.d(TAG,"start CDC USB connection");
                    final DroneClient tmp = new DroneClientCDCUSB(this);
                    droneClient = tmp;
                }

                break;
            default:
                break;
        }

        droneClient.setMyPeerDevice(newDevice);
        try {
            droneClient.startClient(newDevice);
        } catch (Exception e) {
            e.printStackTrace();
        }
//        MavCmdList2Drone.requestUSBData();
//        startGroundstationHeartBeat();
    }

    //let's have the app wide STATIC messaging method for our children/application classes
    public static final void sendAppMsg(APP_STATE msgId, String msgTxt) {
        messenger.appMsgHandler.obtainMessage(msgId.ordinal(), msgTxt.length(), -1, msgTxt.getBytes()).sendToTarget();
    }

    public static final void sendAppMsg(APP_STATE msgId) {
        messenger.appMsgHandler.obtainMessage(msgId.ordinal()).sendToTarget();
    }

    public static final void sendAppMsg(APP_STATE msgId, Message msg) {
        messenger.appMsgHandler.obtainMessage(msgId.ordinal(), msg.arg1, msg.arg2, msg.obj).sendToTarget();
    }

    public static void sendAppMsg(APP_STATE msgId, ItemMavLinkMsg item) {
        messenger.appMsgHandler.obtainMessage(msgId.ordinal(), -1, -1, item).sendToTarget();
    }

    public static void sendAppMsg(APP_STATE msgId, String[] item) {
        messenger.appMsgHandler.obtainMessage(msgId.ordinal(), -1, -1, item).sendToTarget();
    }

    public static boolean isFTDIdevice(Context context) {
        UsbManager manager = (UsbManager) context.getSystemService(Context.USB_SERVICE);
        final HashMap<String, UsbDevice> deviceList = manager.getDeviceList();
        if (deviceList == null || deviceList.isEmpty()) {
            return false;
        }

        for (Map.Entry<String, UsbDevice> device : deviceList.entrySet()) {
            if (device.getValue().getVendorId() == FTDI_DEVICE_VENDOR_ID) {
                return true;
            }
        }
        return false;
    }

    public void registService(MainService service){
        registerReceiver(mMessageReceiver, eventFilter);
        mService = service;
    }

    public void unregistService(){
        unregisterReceiver(mMessageReceiver);
        mService = null;
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }

    private int HBCounter = 0;
    private void startGroundstationHeartBeat(){
        heartBeatHandler.sendEmptyMessage(0);
    }

    Handler heartBeatHandler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
//            MavCmdList2Drone.sentHeartBeats(HBCounter);
            this.sendEmptyMessageDelayed(0, 500);
//            HBCounter++;
        }
    };



    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    private static final IntentFilter eventFilter = new IntentFilter();

    static {
        eventFilter.addAction(BroadCastIntent.OnNetworkTypeChange);
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        private boolean firstConnect = true;
        @Override
        public void onReceive(Context context, Intent intent) {
            switch (intent.getAction()){
                case BroadCastIntent.OnNetworkTypeChange:
                    if(mService!=null)
                        mService.resetXmpp();
                    break;
            }
        }
    };
}
