package com.moremote.copilot.connection.relay;

public interface RelayConnectionListener {

	public void connected(boolean isSuccess);
	public void authed(boolean isSuccess, String uuid, String auth);
	
}
