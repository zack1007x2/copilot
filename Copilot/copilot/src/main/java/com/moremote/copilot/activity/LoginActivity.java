package com.moremote.copilot.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;

import android.os.Bundle;
import android.os.Handler;

import android.os.Message;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.moremote.copilot.activity.Util.UIComponent;
import com.moremote.copilot.R;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import com.moremote.copilot.connection.oauth.IOTManager;
import com.moremote.copilot.connection.oauth.UserAccount;



public class LoginActivity extends Activity {
    private static final String TAG = "LoginActivity";

    private ImageButton mskip, mnext;
    private ImageView page1_icon, page2_icon, page3_icon;
    private Button mbtn_register, mbtn_login;
    private ViewPager mViewPager;
    private View view, view2, view3;
    private List<View> viewList;
    private int action;
    private UserAccount userAccount;
    private IOTManager iotManager;
    private ProgressDialog loading;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.firstpage);
        userAccount = new UserAccount(this);
        userAccount.readUserData();
        iotManager = new IOTManager(this);

//        /** TEST **/
//        userAccount.OAuthType = UserAccount.OAUTHTYPE.MOREMOTE;
//        userAccount.ACCOUNT_EMAIL = "kenkaq6313@gmail.com";
//        userAccount.ACCOUNT_PASSWORD = "123456";

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                
                if (userAccount.OAuthType == UserAccount.OAUTHTYPE.NONEAUTH) {
                    // first time show advertisement
                    ad();
                }else if (userAccount.OAuthType == UserAccount.OAUTHTYPE.LOGOUT) {
                    // not login show homepage
                    homepage();
                }else if(userAccount.OAuthType == UserAccount.OAUTHTYPE.MOREMOTE){
                    updateMoremoteToken();
                }else if(userAccount.OAuthType == UserAccount.OAUTHTYPE.FACEBOOK){
                    mainPage();
                }else {
                    mainPage();
                }
            }
        }, 100);
    }
    
    public void ad(){
        setContentView(R.layout.advertisement);
        LayoutInflater mInflater = getLayoutInflater().from(this);
        
        View v1 = mInflater.inflate(R.layout.advertisement_page1, null);
        View v2 = mInflater.inflate(R.layout.advertisement_page2, null);
        View v3 = mInflater.inflate(R.layout.advertisement_page3, null);
        
        viewList = new ArrayList<View>();
        viewList.add(v1);
        viewList.add(v2);
        viewList.add(v3);
        
        mViewPager = (ViewPager) findViewById(R.id.viewpager);
        mViewPager.setAdapter(new MyPagerAdapterr(viewList));
        mViewPager.setCurrentItem(0);
        view = viewList.get(0);
        
        mskip = (ImageButton) findViewById(R.id.skip);
        page1_icon = (ImageView) findViewById(R.id.imageSlideIcon1);
        page2_icon = (ImageView) findViewById(R.id.imageSlideIcon2);
        page3_icon = (ImageView) findViewById(R.id.imageSlideIcon3);
        mnext = (ImageButton) findViewById(R.id.next);
        
        mskip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                homepage();
            }
        });
        
        mnext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mViewPager.setCurrentItem(1);
                view = viewList.get(1);
            }
        });
        
        mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == 0) {
                    page1_icon.setImageResource(R.drawable.blue_dot);
                    page2_icon.setImageResource(R.drawable.white_dot);
                    page3_icon.setImageResource(R.drawable.white_dot);
                    mnext.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mViewPager.setCurrentItem(1);
                            view = viewList.get(1);
                        }
                    });
                } else if (position == 1) {
                    page1_icon.setImageResource(R.drawable.white_dot);
                    page2_icon.setImageResource(R.drawable.blue_dot);
                    page3_icon.setImageResource(R.drawable.white_dot);
                    mnext.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mViewPager.setCurrentItem(2);
                            view = viewList.get(2);
                        }
                    });
                } else if (position == 2) {
                    page1_icon.setImageResource(R.drawable.white_dot);
                    page2_icon.setImageResource(R.drawable.white_dot);
                    page3_icon.setImageResource(R.drawable.blue_dot);
                    mnext.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            homepage();
                        }
                    });
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        
    }
    
    public void homepage(){
        setContentView(R.layout.register_login);
        mbtn_register = (Button)findViewById(R.id.btn_register);
        mbtn_login = (Button)findViewById(R.id.btn_login);
        
        
        mbtn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                action = 0;
                Intent intent = new Intent();
                Bundle bundle = new Bundle();
                bundle.putInt("action", action);
                intent.putExtras(bundle);
                intent.setClass(LoginActivity.this, RegisterLoginActivity.class);
                startActivity(intent);
                finish();
            }
        });
        mbtn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                action = 1;
                Intent intent = new Intent();
                Bundle bundle = new Bundle();
                bundle.putInt("action", action);
                intent.putExtras(bundle);
                intent.setClass(LoginActivity.this, RegisterLoginActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }

    private void mainPage() {

        Intent intent = new Intent();
        Bundle bundle = new Bundle();

        intent.putExtras(bundle);
        intent.setClass(LoginActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    private void updateMoremoteToken(){

        if(loading == null){
            loading = UIComponent.createProgressDialog(LoginActivity.this);
        }
        loading.show();
        iotManager.login(UserAccount.OAUTHTYPE.MOREMOTE, null, userAccount.ACCOUNT_EMAIL, userAccount.ACCOUNT_PASSWORD, null, MoremoteLogin);
    }



    public class MyPagerAdapterr extends PagerAdapter {
        List<View> list;
        
        public MyPagerAdapterr(List<View> list){
            this.list = list;
        }
        
        @Override
        public int getCount() {
            return list.size();
        }
        
        @Override
        public boolean isViewFromObject(View view, Object o) {
            return view==o;
        }
        
        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            container.addView(list.get(position));
            return list.get(position);
        }
        
        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView(list.get(position));
        }
    }

    final private MsgHandler MoremoteLogin = new MsgHandler(this) {
        @Override
        public void handleMessage(android.os.Message msg) {
            super.handleMessage(msg);

            if(loading.isShowing()) {
                loading.dismiss();
            }

            if(msg.what == 1){
                mainPage();
            }else{
                Log.d(TAG,"Refresh Moremote Token Fail!");
            }
        }
    };

    static class MsgHandler extends Handler {
        protected WeakReference<Activity> mActivity;

        MsgHandler(Activity activity) {
            mActivity = new WeakReference<Activity>(activity);
        }

        @Override
        public void handleMessage(Message msg) {

        }
    }
}