package com.moremote.copilot.database.item;

/**
 * Created by Zack on 2016/1/28.
 */
public class UploadFile {

    private String mLocalFileName;
    private String fileLocation;
    private boolean hasUpload;
    private long duration;
    private String yyyyMMddhhmmss;
    private String mUploadFileName;

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    public String getFileLocation() {
        return fileLocation;
    }

    public void setFileLocation(String fileLocation) {
        this.fileLocation = fileLocation;
    }

    public boolean isHasUpload() {
        return hasUpload;
    }

    public void setHasUpload(boolean hasUpload) {
        this.hasUpload = hasUpload;
    }

    public String getmLocalFileName() {
        return mLocalFileName;
    }

    public void setmLocalFileName(String mLocalFileName) {
        this.mLocalFileName = mLocalFileName;
    }

    public String getYyyyMMddhhmmss() {
        return yyyyMMddhhmmss;
    }

    public void setYyyyMMddhhmmss(String yyyyMMddhhmmss) {
        this.yyyyMMddhhmmss = yyyyMMddhhmmss;
    }

    public String getmUploadFileName() {
        return mUploadFileName;
    }

    public void setmUploadFileName(String mUploadFileName) {
        this.mUploadFileName = mUploadFileName;
    }
}
