package com.moremote.copilot.queue.hub;


import com.MAVLink.MAVLinkPacket;
import com.MAVLink.Parser;
import com.moremote.copilot.IPCamApplication;
import com.moremote.copilot.enums.APP_STATE;
import com.moremote.copilot.enums.MSG_SOURCE;
import com.moremote.copilot.queue.items.ItemMavLinkMsg;

import java.nio.ByteBuffer;

public class ThreadCollectorParser extends Thread {

	@SuppressWarnings("unused")
	private static final String TAG = ThreadCollectorParser.class.getSimpleName();

	private final Parser parserDrone, parserGS;
	MAVLinkPacket tmpPacket = null;

	private ByteBuffer tmpBuffer = null;

	private boolean running = true;

	private final IPCamApplication hub;

	public ThreadCollectorParser(IPCamApplication hubContext) {

		hub = hubContext;

		parserDrone = new Parser();
		parserGS = new Parser();

		running = true;

	}

	public void run() {

		IPCamApplication.logger.sysLog("MavLink Parser", "Parser Start..");

		while (running) {

			int ret1 = parse(MSG_SOURCE.FROM_DRONE);

			// save transmission from drone
			IPCamApplication.logger.byteLog(MSG_SOURCE.FROM_DRONE, tmpBuffer);

			int ret2 = parse(MSG_SOURCE.FROM_GS);

			if(ret1<0 && ret2<0){
				try {
//					Log.d("Zack","ThreadCollectorParser sleep");
					Thread.sleep(20);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}

		IPCamApplication.logger.sysLog("MavLink Parser", "Parser ..Stop");
	}

	private int parse(MSG_SOURCE direction) {

		switch (direction) {
			case FROM_DRONE:
				tmpBuffer = hub.droneClient.getInputByteQueueItem();
				break;
			case FROM_GS:
				tmpBuffer = hub.gcsServer.getInputByteQueueItem();
				break;
			default:
				break;
		}

		if (null != tmpBuffer) {

			tmpPacket = null;

			// add bytes count to the stats
			IPCamApplication.logger.hubStats.addByteStats(direction, tmpBuffer.limit());

			// parse
			for (int i = 0; i < tmpBuffer.limit(); i++) {

				switch (direction) {
					case FROM_DRONE:
						tmpPacket = parserDrone.mavlink_parse_char(tmpBuffer.get(i) & 0x00ff);
						break;
					case FROM_GS:
						tmpPacket = parserGS.mavlink_parse_char(tmpBuffer.get(i) & 0x00ff);
						break;
					default:
						break;
				}

				if (null != tmpPacket) {
					// 1 is here for msg repetition count; it's 1 as we do not
					// support
					// packet multiplication yet :(
					ItemMavLinkMsg tmpMsgItem = new ItemMavLinkMsg(tmpPacket, direction, 1);

					// store item for distribution and UI update
					IPCamApplication.queue.addHubQueueItem(tmpMsgItem);

					// stream for syslog
//					 hub.logger.sysLog("MavlinkMsg",tmpMsgItem.humanDecode());

					// store parser stats
					switch (direction) {
						case FROM_DRONE:
							IPCamApplication.logger.hubStats.setParserStats(direction, parserDrone.stats);
							break;
						case FROM_GS:
							IPCamApplication.logger.hubStats.setParserStats(direction, parserGS.stats);
							break;
						default:
							break;

					}

				}

			}
			// update UI stats display - both byte and parsers stats
			IPCamApplication.sendAppMsg(APP_STATE.MSG_DATA_UPDATE_STATS);
			return 0;
		}else{
			return -1;
		}

	}

	public void stopMe() {
		running = false;
	}

}