package com.moremote.copilot.connection.oauth;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.google.android.gms.auth.GoogleAuthException;
import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.gms.auth.UserRecoverableAuthException;

import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * Created by chingchun on 2015/7/20.
 */
public class GoogleOAuth extends AsyncTask<Void,Void,Void> {

    private final static String TAG = "GoogleOAuth";
    private static final int REQUEST_CODE_USER_RECOVERABLE_ERROR = 3;
    private static final String SCOPE = "oauth2:https://www.googleapis.com/auth/userinfo.profile https://www.googleapis.com/auth/userinfo.email";
    private static final String USERINFO = "https://www.googleapis.com/oauth2/v1/userinfo?alt=json&access_token=";
    boolean userDataAcquired = false;

    private Handler handler;
    private Activity activity;
    private UserAccount account;
    private String scope;
    private String status;
    private String token;

    public GoogleOAuth(Activity activity, UserAccount account, Handler handler) {
        this.activity = activity;
        this.account = account;
        this.scope = SCOPE;
        this.handler = handler;
    }

    @Override
    protected Void doInBackground(Void... params) {
        try {
            token = getAccessToken(this.activity, this.account.ACCOUNT_EMAIL, this.scope);
            if (token != null) {
                //Access Token sent
                status = "Access Token Acquired";
                userDataAcquired = getUserDataJSON(token);
            }
        } catch (Exception ex) {
            status = ex.getMessage();
        }
        return null;
    }

    protected void onPostExecute(Void result) {
        //Toast.makeText(activity, token, Toast.LENGTH_LONG).show();
        //Log.e(TAG, status);
        if (userDataAcquired)
        {
            //Log.e(TAG, "Name : " + account.ACCOUNT_NAME);
            //Toast.makeText(activity, "Name : " + account.ACCOUNT_NAME, Toast.LENGTH_LONG).show();
            if (this.handler != null)
            {
                Message msg = new Message();
                msg.what = 1;
                handler.sendMessage(msg);
            }
        }
        else {
            if (this.handler != null)
            {
                Message msg = new Message();
                msg.what = 0;
                handler.sendMessage(msg);
            }
        }
    }

    public String getAccessToken(Activity activity, String accountName, String scope) {
        try {
            return GoogleAuthUtil.getToken(activity, accountName, scope);
        } catch (UserRecoverableAuthException userRecoverableError) {
            status = "User Recoverable Error";
            Intent intent = ((UserRecoverableAuthException) userRecoverableError).getIntent();
            this.activity.startActivityForResult(intent, REQUEST_CODE_USER_RECOVERABLE_ERROR);
        } catch (GoogleAuthException googleAuthException) {
            status = "Google Auth Exception";
        } catch (IOException ioException) {
            status = "IO Exception";
        }
        return null;
    }

    public boolean getUserDataJSON(String token){
        boolean result = false;
        try {
            String url = USERINFO + token;
            HttpClient httpclient = new DefaultHttpClient();
            HttpResponse response = httpclient.execute(new HttpGet(url));
            StatusLine statusLine = response.getStatusLine();
            if(statusLine.getStatusCode() == 200){
                ByteArrayOutputStream out = new ByteArrayOutputStream();
                response.getEntity().writeTo(out);
                out.close();
                String responseString = out.toString();
                JSONObject userProfileJSON = new JSONObject(responseString);

                this.account.ACCOUNT_TOKEN = token;
                this.account.ACCOUNT_NAME = userProfileJSON.getString("name");
                Log.d("Google",token );
//                Log.e(TAG, "Name : " + userProfileJSON.getString("name"));
//                Log.e(TAG, "Given Name : " + userProfileJSON.getString("given_name"));
//                Log.e(TAG, "Family Name : " + userProfileJSON.getString("family_name"));

                result = true;
            }
        }
        catch(Exception ex) {
            status = ex.getMessage();
            result = false;
        }
        return result;
    }
}



