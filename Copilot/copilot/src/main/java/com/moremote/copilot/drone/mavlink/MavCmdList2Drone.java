package com.moremote.copilot.drone.mavlink;

import com.MAVLink.ardupilotmega.msg_mount_configure;
import com.MAVLink.common.msg_command_long;
import com.MAVLink.common.msg_heartbeat;
import com.MAVLink.common.msg_moremote_cmd;
import com.MAVLink.common.msg_param_request_list;
import com.MAVLink.common.msg_rc_channels_override;
import com.MAVLink.common.msg_request_data_stream;
import com.MAVLink.common.msg_set_mode;
import com.moremote.copilot.IPCamApplication;
import com.moremote.copilot.enums.MSG_SOURCE;
import com.moremote.copilot.queue.items.ItemMavLinkMsg;

/**
 * Created by Zack on 2015/11/26.
 */
public class MavCmdList2Drone {

    //no use
    public static void notifyUSBConnectionClose(){
        msg_mount_configure closeMsg1 = new msg_mount_configure();
        closeMsg1.target_system = 1;
        closeMsg1.target_component = 1;
        closeMsg1.mount_mode = 3;
        closeMsg1.stab_roll = 0;
        closeMsg1.stab_pitch = 0;
        closeMsg1.stab_yaw = 0;
        IPCamApplication.queue.addHubQueueItem(new ItemMavLinkMsg(closeMsg1.pack(), MSG_SOURCE.FROM_GS, 1));

        msg_command_long closeMsg2 = new msg_command_long();
        closeMsg2.param1 = 0;
        closeMsg2.param2 = 0;
        closeMsg2.param3 = 0;
        closeMsg2.param4 = 0;
        closeMsg2.param5 = 0;
        closeMsg2.param6 = 0;
        closeMsg2.param7 = 0;
        closeMsg2.command = 42426;
        closeMsg2.target_system = 1;
        closeMsg2.target_component = 1;
        closeMsg2.confirmation = 0;
        IPCamApplication.queue.addHubQueueItem(new ItemMavLinkMsg(closeMsg2.pack(),MSG_SOURCE.FROM_GS,1));
    }

    public static void sentHeartBeats(int HeartBeatCounter){
        msg_heartbeat myHeartBeat = new msg_heartbeat();
        myHeartBeat.custom_mode = 0;
        myHeartBeat.type = 6;
        myHeartBeat.autopilot = 0;
        myHeartBeat.base_mode = 0;
        myHeartBeat.system_status = 0;
        myHeartBeat.mavlink_version = 0;
        IPCamApplication.queue.addHubQueueItem(new ItemMavLinkMsg(myHeartBeat.pack(),MSG_SOURCE.FROM_GS,HeartBeatCounter));
    }


    public static void requestUSBData(){
        short[] req_stream_id_arr = {2,10,11,12,6,1,4,3,25};
        for(int i=0; i<req_stream_id_arr.length; i++){

            msg_request_data_stream curReqMsg = new msg_request_data_stream();
            curReqMsg.req_message_rate = 2;
            curReqMsg.target_system = 1;
            curReqMsg.target_component = 1;
            curReqMsg.req_stream_id = req_stream_id_arr[i];
            curReqMsg.start_stop = 1;

            IPCamApplication.queue.addHubQueueItem(new ItemMavLinkMsg(curReqMsg.pack(), MSG_SOURCE.FROM_GS,1));
        }

        msg_param_request_list preqListMsg = new msg_param_request_list();
        preqListMsg.target_system = 1;
        preqListMsg.target_component = 1;

        IPCamApplication.queue.addHubQueueItem(new ItemMavLinkMsg(preqListMsg.pack(), MSG_SOURCE.FROM_GS, 1));
    }

    public static void sentRCoverride(int... channal){
        int[] ch = new int[8];
        System.arraycopy(channal, 0, ch, 0, channal.length);

        msg_rc_channels_override rc_channels_override = new msg_rc_channels_override();
        rc_channels_override.target_component = 1;
        rc_channels_override.target_system = 1;
        rc_channels_override.chan1_raw = ch[0];
        rc_channels_override.chan2_raw = ch[1];
        rc_channels_override.chan3_raw = ch[2];
        rc_channels_override.chan4_raw = ch[3];
        rc_channels_override.chan5_raw = ch[4];
        rc_channels_override.chan6_raw = ch[5];
        rc_channels_override.chan7_raw = ch[6];
        rc_channels_override.chan8_raw = ch[7];
        ItemMavLinkMsg rc_channels_override_Item_Msg = new ItemMavLinkMsg(rc_channels_override.pack(), MSG_SOURCE.FROM_GS, 0);
        IPCamApplication.queue.addHubQueueItem(rc_channels_override_Item_Msg);
    }

    public static void sentMoremoteCmd(double real_x, double real_y, int rotate){
        msg_moremote_cmd moremote_cmd = new msg_moremote_cmd();
        moremote_cmd.param1 = Float.valueOf(Double.toString(real_x));
        moremote_cmd.param2 = Float.valueOf(Double.toString(real_y));
        moremote_cmd.param3 = rotate;
        moremote_cmd.command = 2;
        moremote_cmd.target_system = 1;
        moremote_cmd.target_component = 1;
        moremote_cmd.confirmation = 0;
        IPCamApplication.queue.addHubQueueItem(new ItemMavLinkMsg(moremote_cmd.pack(), MSG_SOURCE.FROM_GS, 0));
    }

    public static void setDroneMode(int mode){
        msg_set_mode setModeMsg = new msg_set_mode();
        setModeMsg.custom_mode = mode;
        setModeMsg.base_mode = 1;
        setModeMsg.target_system = 1;
        ItemMavLinkMsg mav_mode_msg = new ItemMavLinkMsg(setModeMsg.pack(), MSG_SOURCE.FROM_GS, 0);
        IPCamApplication.queue.addHubQueueItem(mav_mode_msg);
    }
}
