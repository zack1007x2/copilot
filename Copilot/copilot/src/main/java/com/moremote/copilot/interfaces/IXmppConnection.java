package com.moremote.copilot.interfaces;

public interface IXmppConnection {

	void onXmppConnected();
	void onXmppDisConnect();
	void onXmppReceiveMessage();
	void onXmppConnectListUpdate(String[] user);

}
