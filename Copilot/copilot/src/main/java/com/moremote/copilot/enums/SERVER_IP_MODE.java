package com.moremote.copilot.enums;

public enum SERVER_IP_MODE {
	UDP, TCP, XMPP
}
