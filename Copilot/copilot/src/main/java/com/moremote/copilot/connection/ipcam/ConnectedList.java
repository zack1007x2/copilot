package com.moremote.copilot.connection.ipcam;

import android.content.Context;
import android.util.Log;

import com.moremote.copilot.IPCamApplication;
import com.moremote.copilot.R;
import com.moremote.copilot.connection.p2p.TCPServer;
import com.moremote.copilot.connection.p2p.UDTServer;
import com.moremote.copilot.connection.relay.RelayClient;

import net.majorkernelpanic.streaming.rtp.RtpSocket;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Semaphore;

/**
 * Created by kentpon on 2015/4/28.
 *
 *
 *                                 _oo8oo_
 *                                o8888888o
 *                                88" . "88
 *                                (| -_- |)
 *                                0\  =  /0
 *                              ___/'==='\___
 *                            .' \\|     |// '.
 *                           / \\|||  :  |||// \
 *                          / _||||| -:- |||||_ \
 *                         |   | \\\  -  /// |   |
 *                         | \_|  ''\---/''  |_/ |
 *                         \  .-\__  '-'  __/-.  /
 *                       ___'. .'  /--.--\  '. .'___
 *                    ."" '<  '.___\_<|>_/___.'  >' "".
 *                   | | :  `- \`.:`\ _ /`:.`/ -`  : | |
 *                   \  \ `-.   \_ __\ /__ _/   .-` /  /
 *               =====`-.____`.___ \_____/ ___.`____.-`=====
 *                                 `=---=`
 *
 *
 *      ~~~~~~~Powered by https://github.com/ottomao/bugfreejs~~~~~~~
 *
 *
 */
public class ConnectedList {

    private static final String TAG = "ConnectedList";
    private static RelayClient mRelayClient;
    private static TCPServer mTCPServer;
    private static UDTServer mUDTServer;
    private Map<String,UserInfo> UserList;
    private Semaphore lock;
    private static final int MTU = RtpSocket.MTU;
    private ByteRingQueue relayRTPPacketBuffer;
    /** save sps+pps packet(nalu type 24) index before Iframe actually */
    private IntQueue relayIFrameIndexQueue;
    private Thread relaySendPacketThead;
    private int relayConnectNum;
    private int TcpUDTConnectNum;
    private static final int BufferQueueCapacity = 256;

    private UserConnectionListener listener;
    public Map<String,String> TempUserDisplayName;

    public ConnectedList(UserConnectionListener listener) {
        UserList = new HashMap<String, UserInfo>();
        TempUserDisplayName =  new HashMap<String, String>();
        mRelayClient = null;
        mTCPServer = null;
        mUDTServer = null;
        lock = new Semaphore(1);
        relayRTPPacketBuffer = new ByteRingQueue(BufferQueueCapacity);
        relayIFrameIndexQueue = new IntQueue();
        relaySendPacketThead = null;
        relayConnectNum = 0;
        TcpUDTConnectNum = 0;
        this.listener = listener;
    }

    private class ByteRingQueue {
        private ByteBuffer[] mBuffer;
        private int mBufferCount, mBufferIn, mBufferOut;

        public ByteRingQueue(int capacity) {
            mBufferCount = capacity;
            mBufferIn = 0;
            mBufferOut = 0;
            mBuffer = new ByteBuffer[mBufferCount];
        }

        public void clear() {
            synchronized(mBuffer) {
                for (int i = 0; i < mBufferCount; i++) {
                    if (mBuffer[i] != null) {
                        mBuffer[i].clear();
                        mBuffer[i] = null;
                    }
                }
                mBuffer = null;
                mBufferCount = 0;
                mBufferIn = 0;
                mBufferOut = 0;
            }
        }

        public void push(byte[] data, int length) {
            synchronized(mBuffer) {
                mBuffer[mBufferIn] = ByteBuffer.wrap(data, 0, length);
                if (mBuffer[mBufferIn].limit() != length) {   //check memory bug
                    Log.e(TAG, "RTPPacketBuffer[mBufferIn].limit() != RTPLength");
                }

                if (++mBufferIn >= mBufferCount) mBufferIn = 0;
            }
        }
        
        public byte[] pop() {
            synchronized(mBuffer) {
                if (isEmpty()) return null;
                byte[] data = new byte[mBuffer[mBufferOut].remaining()];
                mBuffer[mBufferOut].get(data);
                mBuffer[mBufferOut] = null;
                if (++mBufferOut >= mBufferCount) mBufferOut = 0;
                return data;
            }
        }

        public boolean isFull() {
            synchronized(mBuffer) {
                int i = mBufferIn + 1;
                i = i % mBufferCount;
                return i == mBufferOut;
            }
        }

        public boolean isEmpty() {
            synchronized(mBuffer) {
                return mBufferIn == mBufferOut;
            }
        }

        public void setPopIndex(int index) {
            synchronized(mBuffer) {
                int i = mBufferOut;
                while (i != index) {
                    mBuffer[i].clear();
                    mBuffer[i] = null;
                    i++;
                    i %= mBufferCount;
                }
                mBufferOut = index;
            }
        }

        public int getPushIndex() {
            synchronized(mBuffer) {
                return mBufferIn;
            }
        }

        public int getPopIndex() {
            synchronized(mBuffer) {
                return mBufferOut;
            }
        }
     }

    private class IntQueue {
        private LinkedList<Integer> linkedList;

        public IntQueue() {
            linkedList = new LinkedList<Integer>();
        }

        public void clear() {
            synchronized(linkedList) {
                linkedList.clear();
                linkedList = null;
            }
        }

        public void push(int i) {
//            Log.d(TAG, "############# iframe "+i);
            synchronized(linkedList) {
                linkedList.add(i);
            }
        }

        public int front() {
            synchronized(linkedList) {
                if (linkedList.size() == 0) return -1;
                return linkedList.getFirst().intValue();
            }
        }

        public int pop() {
            synchronized(linkedList) {
                if (linkedList.size() == 0) return -1;
                return linkedList.removeFirst().intValue();
            }
        }

        public boolean isEmpty() {
            synchronized(linkedList) {
                return linkedList.isEmpty();
            }
        }
    }


    /**
     * class UserInfo
     *
     **/
    private class UserInfo {
        public String userXmpp;
        public String userDisplayName;
        public int connectionType;
        public Object socket;
        public Thread stunAsyncTask;
        private ByteRingQueue RTPPacketBuffer;
        /** save sps+pps packet(nalu type 24) index before Iframe actually */
        private IntQueue IFrameIndexQueue;
        private Thread sendPacketThead;

        // AudioBack Buffer, add by cc
        public ByteBuffer audioBackBuffer;

        public UserInfo(String userXmpp) {
            this(userXmpp, IPCamApplication.ConnectionType.NONE_CONNECT);
        }
        public UserInfo(String userXmpp, int connectionType ) {
            this(userXmpp, connectionType, null);
        }
        public UserInfo(String userXmpp, int connectionType, Object socket) {
            this.userXmpp = userXmpp;
            this.connectionType = connectionType;
            this.socket = socket;
            this.stunAsyncTask = null;
            RTPPacketBuffer = null;
            IFrameIndexQueue = null;
            sendPacketThead = null;
            userDisplayName = null;
            // add by cc
            audioBackBuffer = ByteBuffer.allocate(102400);
        }

        public void close() {
            if(stunAsyncTask != null) {
                stunAsyncTask.interrupt();
                stunAsyncTask = null;
            }
            if(sendPacketThead != null) {
                sendPacketThead.interrupt();
                sendPacketThead = null;
            }
            if(socket != null) {
                if (connectionType == IPCamApplication.ConnectionType.P2P_LAN_TCP) {
                    try {
                        ((SocketChannel) socket).close();
                    } catch (IOException e) {
                        Log.e(TAG, userXmpp + " close socket fail");
                    }
                }
                else if(connectionType == IPCamApplication.ConnectionType.P2P_WAN_UDT) {
                    Map<String,Integer> mapUDT = (Map<String,Integer>) socket;
                    int UDTServerFD = mapUDT.get("UDTServerFD");

                    mUDTServer.close(UDTServerFD);
                }
            }
            if(RTPPacketBuffer != null) {
                RTPPacketBuffer.clear();
                RTPPacketBuffer = null;
            }
            if(IFrameIndexQueue != null) {
                IFrameIndexQueue.clear();
                IFrameIndexQueue = null;
            }
            if (audioBackBuffer != null){
                audioBackBuffer.clear();
                audioBackBuffer = null;
            }
            userXmpp = null;
            socket = null;
            connectionType = IPCamApplication.ConnectionType.NONE_CONNECT;
        }

        public ByteRingQueue getRTPPacketBuffer() {
            if( RTPPacketBuffer == null ) {
                RTPPacketBuffer = new ByteRingQueue(BufferQueueCapacity);
            }
            return RTPPacketBuffer;
        }

        public IntQueue getIFrameIndexQueue() {
            if(IFrameIndexQueue == null) {
                IFrameIndexQueue = new IntQueue();
            }
            return IFrameIndexQueue;
        }

        public void startSendPacketThead() {
            if(sendPacketThead != null) {
                sendPacketThead.interrupt();
            }
            sendPacketThead = new sendThread(getRTPPacketBuffer(), getIFrameIndexQueue(), connectionType, socket, userXmpp);
            sendPacketThead.start();
        }

        public Thread getsendPacketThead() {
            return sendPacketThead;
        }
    }

    public void disconnect(UserInfo user) {
        if(UserList == null || user == null) return;
        UserList.remove(user.userXmpp);
        if( user.connectionType == IPCamApplication.ConnectionType.RELAY ) {
            relayConnectNum--;
            if(relayConnectNum <= 0) {
                closeRelaySendThread();
                if(mRelayClient != null) {
                    mRelayClient.close();
                    mRelayClient = null;
                }
            }
        }
        else if ( user.connectionType == IPCamApplication.ConnectionType.P2P_LAN_TCP ||
                user.connectionType == IPCamApplication.ConnectionType.P2P_WAN_UDT ) {
            TcpUDTConnectNum--;
        }
        //15.08.05
        listener.notifyUserConnectionChanged(user.userXmpp, UserConnectionListener.DisConnected);
        //15.08.05
        user.close();
        Log.e(TAG, "User Closed....................TCP/UDP: " + TcpUDTConnectNum + " Relay: " + relayConnectNum);
    }

    private void closeRelaySendThread() {
        if(relaySendPacketThead != null) {
            relaySendPacketThead.interrupt();
            relaySendPacketThead = null;
        }

        if(relayRTPPacketBuffer != null) {
            relayRTPPacketBuffer.clear();
            relayRTPPacketBuffer = null;
        }

        if( relayIFrameIndexQueue != null) {
            relayIFrameIndexQueue.clear();
            relayIFrameIndexQueue = null;
        }
    }

    public void close() {
        Log.e(TAG, "ConnectedList close!!");
        try {
            lock();
        } catch (Exception e) {
            Log.e(TAG, "close "+ e.getMessage());
        }
        closeRelaySendThread();
        relayConnectNum = 0;
        TcpUDTConnectNum = 0;
        /** close connect socket in user list */
        for(UserInfo user : UserList.values()) {
            user.close();
        }

        /** clear List */
        UserList.clear();
        UserList = null;

        /** close UDT */
        if(mUDTServer != null){
            mUDTServer.close();
            mUDTServer = null;
        }

        /** close Relay Client socket */
        if(mRelayClient != null) {
            mRelayClient.close();
            mRelayClient = null;
        }

        /** close local TCP server socket */
        if(mTCPServer != null) {
            mTCPServer.close();
            mTCPServer = null;
        }
        unlock();
        lock = null;
    }

    public int getUserNum() {
        return UserList.size();
    }

    public HashMap<String, String> getUserDisplayName(Context mContext){
        HashMap<String, String> userInfo = new HashMap<String, String>();
        int i = 1;
        for(UserInfo user: UserList.values())
        {
            String name = String.format(mContext.getResources().getString(R.string.connection_devices_name), i);
            if(user.userDisplayName != null) {
                name +=  user.userDisplayName;
                i++;
            }
            userInfo.put(user.userXmpp, name);
        }
        return userInfo;
    }

    public boolean add(String userXmpp) {
        try {
            lock();
        } catch (Exception e) {
            Log.e(TAG, "add userXmpp "+ e.getMessage());
            return false;
        }
        if(UserList.containsKey(userXmpp)) {
            Log.d(TAG, "user over loggin");
            UserInfo user = UserList.get(userXmpp);
            if( user.stunAsyncTask != null ) {
                Log.e(TAG, "the user is still stun");
                unlock();
                return false;
            }
            else {
                disconnect(user);
            }
        }

        UserInfo newUser = new UserInfo(userXmpp);
        if (TempUserDisplayName.containsKey(userXmpp)) {
            newUser.userDisplayName = TempUserDisplayName.get(userXmpp);
            TempUserDisplayName.remove(userXmpp);
        }
        UserList.put(userXmpp, newUser);
        unlock();

        // 15.08.05
        listener.notifyUserConnectionChanged(userXmpp, UserConnectionListener.Connected);
        return true;
    }

    public boolean setUserStunAsyncTask(String userXmpp, Thread stunAsyncTask) {
        UserInfo user;
        user = UserList.get(userXmpp);
        if(user == null) {
            Log.e(TAG,"doesn't exist this user account in list");
            return false;
        }
        user.stunAsyncTask = stunAsyncTask;
        return true;
    }

    public synchronized boolean interruptUserStunAsyncTask(String userXmpp) {
        UserInfo user;
        user = UserList.get(userXmpp);
        if(user == null) {
            Log.e(TAG,"doesn't exist this user account in list");
            return false;
        }
        if(user.stunAsyncTask != null) {
            user.stunAsyncTask.interrupt();
            user.stunAsyncTask = null;
        }
        return true;
    }

    synchronized public boolean setUserConnectType(String userXmpp, int type, Object socket){
        Log.e(TAG,"setUserConnectType  user:" + userXmpp + " , " + ConnectionTypeString[type]);
        if(UserList == null) return false;
        UserInfo user = UserList.get(userXmpp);
        if(user == null) {
            Log.e(TAG,"doesn't exist this user account in list");
            return false;
        }
        if(type == IPCamApplication.ConnectionType.NONE_CONNECT) {
            try {
                lock();
            } catch (Exception e) {
                Log.e(TAG, "set User NONE_CONNECT "+ e.getMessage());
                return false;
            }
            disconnect(user);
            unlock();
            return true;
        }
        user.connectionType = type;
        user.socket = socket;
        if(type == IPCamApplication.ConnectionType.RELAY) {
            if(relaySendPacketThead == null) {
                relaySendPacketThead = new sendThread(getRelayRTPPacketBuffer(), getRelayIFrameIndexQueue(), type, socket, userXmpp);
                relaySendPacketThead.start();
            }
            relayConnectNum++;
            return true;
        }

        if(type == IPCamApplication.ConnectionType.P2P_LAN_TCP || type == IPCamApplication.ConnectionType.P2P_WAN_UDT) {
            user.startSendPacketThead();
            TcpUDTConnectNum++;
            return true;
        }

        Log.e(TAG,"setUserConnectType no this type");
        return false;
    }

    private ByteRingQueue getRelayRTPPacketBuffer() {
        if( relayRTPPacketBuffer == null ) {
            relayRTPPacketBuffer = new ByteRingQueue(BufferQueueCapacity);
        }
        return relayRTPPacketBuffer;
    }
    private IntQueue getRelayIFrameIndexQueue() {
        if(relayIFrameIndexQueue == null) {
            relayIFrameIndexQueue = new IntQueue();
        }
        return relayIFrameIndexQueue;
    }

    // AudioBack Buffer, add by cc
    synchronized public ByteBuffer getUserAudioBackBuffer(int SocketPort) {
        UserInfo user;
        Set<Map.Entry<String, UserInfo>> aEntrySet = UserList.entrySet();
        for(Map.Entry<String, UserInfo> entry : aEntrySet){
            user = entry.getValue();
            SocketChannel socketChannel = (SocketChannel)(user.socket);
            if (socketChannel != null && socketChannel.socket() != null && socketChannel.socket().getPort() == SocketPort) {
                return user.audioBackBuffer;
            }
        }
        return null;
    }

    public RelayClient getRelayClient() {
        return mRelayClient;
    }

    public void setRelayClient(RelayClient mRelayClient) {
        this.mRelayClient = mRelayClient;
    }

    public TCPServer getTCPServer() {
        return mTCPServer;
    }

    public void setTCPServer(TCPServer mTCPServer) {
        this.mTCPServer = mTCPServer;
    }

    public UDTServer getUDTServer() {
        return mUDTServer;
    }

    public void setUDTServer(UDTServer mUDTServer) {
        this.mUDTServer = mUDTServer;
    }

    private void lock() throws Exception {
        if ( lock != null) {
            try {
                lock.acquire();
            } catch (InterruptedException e) {
                throw new Exception("lock acq fail");
            }
        }
    }

    private void unlock() {
        if ( lock != null) {
            lock.release();
        }
    }

    public void pushRTPPacket(byte[] RTPPacket, int RTPLength) {
        /** Semaphore lock avoid A/V RtpSocket conflict */
        try {
            lock();
        } catch (Exception e) {
            Log.e(TAG, "pushRTPPacket "+ e.getMessage());
            return;
        }
        try {
            if (relayConnectNum > 0) { /** push packet to relay buffer*/
                if (RTPPacket[12] == 24) {
                    /** if this packet is nalu 24 (sps+pps)
                     * record this sps packet index */
                    relayIFrameIndexQueue.push(relayRTPPacketBuffer.getPushIndex());
                }
                relayRTPPacketBuffer.push(RTPPacket, RTPLength);

                if (relayRTPPacketBuffer.isFull()) { /** RTP Packet Buffer full */
                    Log.e(TAG, "Relay Buffer full, drop packets!!");
                    int pos = relayIFrameIndexQueue.pop();
                    if (pos == -1) {
                        pos = relayRTPPacketBuffer.getPushIndex();
                    }
                    relayRTPPacketBuffer.setPopIndex(pos);

                }
            }
            if (TcpUDTConnectNum > 0) {
                for (UserInfo user : UserList.values()) { /***/
                    if (user.connectionType == IPCamApplication.ConnectionType.P2P_LAN_TCP ||
                            user.connectionType == IPCamApplication.ConnectionType.P2P_WAN_UDT) {
                        if (RTPPacket[12] == 24) {
                            /** if this packet is nalu 24 (sps+pps)
                             * record this sps packet index */
                            user.getIFrameIndexQueue().push(user.getRTPPacketBuffer().getPushIndex());
                        }

                        user.getRTPPacketBuffer().push(RTPPacket, RTPLength);

                        if (user.getRTPPacketBuffer().isFull()) { /** RTP Packet Buffer full */
                            Log.e(TAG, user.userXmpp + " Buffer full, drop packets!!");
                            int pos = user.getIFrameIndexQueue().pop();
                            if (pos == -1) {
                                pos = user.getRTPPacketBuffer().getPushIndex();
                            }
                            user.getRTPPacketBuffer().setPopIndex(pos);
                        }
                    }
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "pushRTPPacket E: " + e.toString());
        }
        finally {
            unlock();
        }
    }
    private boolean send(byte[] rtpData, int rtpLen, int connectionType, Object socket){

        if( connectionType == IPCamApplication.ConnectionType.NONE_CONNECT ) {
            return false;
        }
        if( connectionType == IPCamApplication.ConnectionType.P2P_WAN_UDT ) {
            byte[] UDTPacketHeader = ByteBuffer.allocate(4).putInt(rtpLen).array();
            for( int i =0 ; i < UDTPacketHeader.length/2 ; i++)
            {
                byte tmp = UDTPacketHeader[i];
                UDTPacketHeader[i] = UDTPacketHeader[ UDTPacketHeader.length -i -1 ];
                UDTPacketHeader[ UDTPacketHeader.length -i -1 ] = tmp;
            }

            byte[] UDTPacket = new byte[MTU+4];
            System.arraycopy(UDTPacketHeader, 0, UDTPacket, 0, UDTPacketHeader.length);
            System.arraycopy(rtpData, 0, UDTPacket, UDTPacketHeader.length, rtpLen);

            /** TODO UDT send method */
            Map<String,Integer> mapUDT = (Map<String,Integer>) socket;
            int UDTSocketFD = mapUDT.get("UDTSocketFD");

            if( mUDTServer.send(UDTSocketFD, UDTPacket) ){
                Log.e(TAG,"send by UDT fail");
                return false;
            }
            return true;
        }

        byte[] TCPPacket = new byte[rtpLen+4];
        TCPPacket[0] = 0x24;
        TCPPacket[1] = 0x00;
        TCPPacket[2] = (byte) ((rtpLen >> 8)& 0xFF);
        TCPPacket[3] = (byte) (rtpLen & 0xFF);
        System.arraycopy(rtpData, 0, TCPPacket, 4, rtpLen);

        if(connectionType == IPCamApplication.ConnectionType.RELAY) {
            if(mRelayClient.send(TCPPacket, TCPPacket.length) == -1) {
                Log.e(TAG, "send to Relay Server fail");
                return false;
            }
            return true;
        }
        if(connectionType == IPCamApplication.ConnectionType.P2P_LAN_TCP) {
            if( mTCPServer.send((SocketChannel)socket, TCPPacket, TCPPacket.length ) == -1) {
                Log.e(TAG, "send by local tcp fail");
                return false;
            }
            return true;
        }

        Log.e(TAG, "send error: no this type");
        return false;
    }
    class sendThread extends Thread {
        private boolean isStop = false;
        private ByteRingQueue RTPPacketBuffer;
        private IntQueue IFrameIndexQueue;
        private int connectionType;
        private Object socket;
        String userXmpp;
        public sendThread(ByteRingQueue RTPPacketBuffer, IntQueue IFrameIndexQueue, int connectionType, Object socket, String userXmpp) {
            this.RTPPacketBuffer = RTPPacketBuffer;
            this.IFrameIndexQueue = IFrameIndexQueue;
            this.connectionType = connectionType;
            this.socket = socket;
            this.userXmpp = userXmpp;
            this.setPriority(Thread.MAX_PRIORITY);
        }
        @Override
        public void run() {
            byte[] data = null;
            while(!isStop) {
                try {

                    try {
                        if (RTPPacketBuffer.getPopIndex() == IFrameIndexQueue.front()) {
                            IFrameIndexQueue.pop();
                        }
                        data = RTPPacketBuffer.pop();
                    } catch (Exception e) {
                        isStop = true;
                        data = null;
                        Log.e(TAG, "sendThread IFrameIndexQueue E: "+ e.getMessage());
                    }

                    if (data == null) {
                        continue;
                    }
                    if (!send(data, data.length, connectionType, socket)) {
                        isStop = true;
                    }
                    //Log.e(TAG, "sendThread send() len: " + data.length);
                } catch (Exception e) {
                    Log.e(TAG, "sendThread E: "+ e.getMessage());
                    isStop = true;
                } finally {
                    if (isStop) {
                        setUserConnectType(userXmpp, IPCamApplication.ConnectionType.NONE_CONNECT, null);
                    }
                }
            }
        }

        @Override
        public void interrupt() {
            super.interrupt();
            isStop = true;
        }
    }

    //15.08.05
    public interface UserConnectionListener {
        final public static int Connected = 1;
        final public static int DisConnected = 2;
        public void notifyUserConnectionChanged(String client, int status);
//        public void notifyDisconnected(String XmppClient);
//        public void notifyConnected(String XmppClient);
    }

    public static final String ConnectionTypeString [] = {
        "P2P_WAN_UDT",
        "P2P_LAN_TCP",
        "RELAY",
        "NONE_CONNECT",
    };
}
