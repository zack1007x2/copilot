package com.moremote.copilot.fragments;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.moremote.copilot.IPCamApplication;
import com.moremote.copilot.R;
import com.moremote.copilot.activity.ConnectListView;
import com.moremote.copilot.activity.MainActivity;
import com.moremote.copilot.enums.APP_STATE;
import com.moremote.copilot.fragments.dialogs.FragmentDialogSelectDevice;
import com.moremote.copilot.interfaces.IXmppConnection;

import java.util.ArrayList;

/**
 * Created by Zack on 2015/11/13.
 */
public class ConnectionListFragment extends HUBFragment implements IXmppConnection, View.OnClickListener {

    private static final String TAG = ConnectionListFragment.class.getSimpleName();
    private ArrayList<String> mConnectionUserDisplayName;
    private ConnectListView connListView;
    private ImageView img_bt_bluetooth, img_bt_usb, img_bt_debug;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_connectionlist_main_control, container, false);
        connListView = new ConnectListView(getActivity(), rootView);
        img_bt_bluetooth = (ImageView) rootView.findViewById(R.id.img_bt_bluetooth);
        img_bt_usb = (ImageView) rootView.findViewById(R.id.img_bt_usb);
        img_bt_debug = (ImageView)rootView.findViewById(R.id.img_bt_debug);
        img_bt_bluetooth.setOnClickListener(this);
        img_bt_usb.setOnClickListener(this);
        img_bt_debug.setOnClickListener(this);
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        IPCamApplication.messenger.register(this, APP_STATE.MSG_XMPP_CLIENT_LIST_UPDATE);
        mConnectionUserDisplayName = ((MainActivity) getActivity()).getConnectionUserDisplayName();
        String[] user = new String[mConnectionUserDisplayName.size()];
        mConnectionUserDisplayName.toArray(user);
        connListView.setConnectionList(user);
    }

    @Override
    public void onPause() {
        super.onPause();
        IPCamApplication.messenger.unregister(this, APP_STATE.MSG_XMPP_CLIENT_LIST_UPDATE);
    }

    @Override
    public void onXmppConnected() {

    }

    @Override
    public void onXmppDisConnect() {

    }

    @Override
    public void onXmppReceiveMessage() {

    }

    @Override
    public void onXmppConnectListUpdate(String[] user) {
        connListView.setConnectionList(user);
    }

    @Override
    public void onClick(View v) {
        int vId = v.getId();
        switch (vId) {
            case R.id.img_bt_bluetooth:
                final FragmentTransaction fragManager = getActivity().getSupportFragmentManager()
                        .beginTransaction();

                final Fragment prev = getActivity().getSupportFragmentManager().findFragmentByTag
                        ("bluetooth");
                if (null != prev) {
                    fragManager.remove(prev);
                }
                fragManager.addToBackStack(null);

                final DialogFragment deviceSelectDialogBT = FragmentDialogSelectDevice.newInstance();
                deviceSelectDialogBT.setCancelable(false);
                deviceSelectDialogBT.show(fragManager, "bluetooth");
                break;
            case R.id.img_bt_usb:
                final FragmentTransaction fragManager_usb = getActivity()
                        .getSupportFragmentManager().beginTransaction();

                final Fragment prev_usb = getActivity().getSupportFragmentManager()
                        .findFragmentByTag("usb");
                if (null != prev_usb) {
                    fragManager_usb.remove(prev_usb);
                }
                fragManager_usb.addToBackStack(null);

                final DialogFragment deviceSelectDialogUSB = FragmentDialogSelectDevice.newInstance();
                deviceSelectDialogUSB.setCancelable(false);
                deviceSelectDialogUSB.show(fragManager_usb, "usb");
                break;

            case R.id.img_bt_debug:
                ((MainActivity)getActivity()).switchVADebugMode();
                break;
        }

    }
}
