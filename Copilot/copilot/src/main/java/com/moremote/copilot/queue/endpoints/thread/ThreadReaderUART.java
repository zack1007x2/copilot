package com.moremote.copilot.queue.endpoints.thread;

import android.os.Handler;
import android.util.Log;

import com.moremote.copilot.enums.CONNECTOR_STATE;

import java.nio.ByteBuffer;
import java.util.Arrays;

public class ThreadReaderUART extends Thread {

    //JNI call back path need change if move file

    @SuppressWarnings("unused")
    private static final String TAG = ThreadReaderUART.class.getSimpleName();

    private static final int BUFFSIZE = 1024;

    private final Handler connHandler;

    private boolean running = true;

    private native int uartOpen(int device, int bdrate);

    private native boolean uartWrite(int uartFD, int length, byte data[]);

    private native int uartRead(int uartFD, int length, byte data[]);

    private native void uartClose(int uartFD);

    private native void runUartReadThread(int uartFD, int length);


    int uartFD;


    static {
        System.loadLibrary("NativeUART");
    }

    public ThreadReaderUART(Handler handlerReceiver) {
        connHandler = handlerReceiver;
    }

    public int openUart() {
        uartFD = uartOpen(4, 57600);
        Log.d(TAG, "Open Uart UART :4, Buadrate : 57600, uartFD : " + uartFD);
        return uartFD;
    }


    public void run() {
        running = true;
        runUartReadThread(uartFD, BUFFSIZE);
    }

    public void writeBytes(byte[] bytes) {
        Log.d(TAG, "writeBytes UART =" + bytes);
        uartWrite(uartFD, bytes.length + 1, bytes);
    }

    public void stopMe() {
        // stop threads run() loop
        running = false;
        uartClose(uartFD);
        connHandler.obtainMessage(CONNECTOR_STATE.MSG_CONN_CLOSED.ordinal()).sendToTarget();
    }

    public boolean isRunning() {
        return running;
    }

    //receive data from uart.c
    private void readThreadCallBack(byte[] b) {
        int avaliable = b.length;
//        Log.d(TAG, "receive UART " + avaliable + " bytes => " + Arrays.toString(b));
        final ByteBuffer byteMsg = ByteBuffer.allocate(b.length);
        byteMsg.put(b, 0, avaliable);
        byteMsg.flip();
        connHandler.obtainMessage(CONNECTOR_STATE.MSG_CONN_BYTE_DATA_READY.ordinal(), avaliable,
                -1, byteMsg).sendToTarget();
    }
}