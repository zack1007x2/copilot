// $codepro.audit.disable unnecessaryOverride
package com.moremote.copilot.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.moremote.copilot.IPCamApplication;


public class HUBFragment extends Fragment {

	protected IPCamApplication hub;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		hub = (IPCamApplication) getActivity().getApplication();
		setRetainInstance(true); // keep HUBFragments in the memory
	}

}
