// $codepro.audit.disable com.instantiations.assist.eclipse.analysis.audit.rule.effectivejava.alwaysOverridetoString.alwaysOverrideToString
package com.moremote.copilot.queue;


import com.moremote.copilot.IPCamApplication;
import com.moremote.copilot.enums.APP_STATE;
import com.moremote.copilot.queue.items.ItemMavLinkMsg;

import java.util.ArrayDeque;

public class QueueMsgItems {

	@SuppressWarnings("unused")
	private static final String TAG = QueueMsgItems.class.getSimpleName();

	private final ArrayDeque<ItemMavLinkMsg> hubQueue;

	protected final IPCamApplication hub;

	public QueueMsgItems(IPCamApplication hubContext, int capacity) {

		hubQueue = new ArrayDeque<ItemMavLinkMsg>(capacity);

		hub = hubContext;

	}

	public final ItemMavLinkMsg getHubQueueItem() {
		synchronized (hubQueue) {
			return hubQueue.poll();
		}

	}

	public final void addHubQueueItem(ItemMavLinkMsg item) {
		synchronized (hubQueue) {
			hubQueue.addLast(item);
		}

		IPCamApplication.sendAppMsg(APP_STATE.MSG_QUEUE_MSGITEM_READY, item);
		if(IPCamApplication.mService!=null){
			//sent message via xmpp
			IPCamApplication.mService.onQueueMsgItemReady(item);
		}
	}

	public final int getItemCount() {
		return hubQueue.size();
	}

}
