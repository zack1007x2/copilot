package com.moremote.copilot.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.moremote.copilot.activity.Util.UIComponent;
import com.moremote.copilot.R;
import com.moremote.copilot.connection.oauth.IOTManager;
import com.moremote.copilot.connection.oauth.UserAccount;


/**
 * Created by register_login on 2015/8/11.
 */
public class LoginByEmailActivity extends Activity {

    private static final String TAG = "LoginByEmail";

    private TextView mtextview1;
    private EditText mlogin_id, mlogin_pw;
    private Button mbtn_login_by_email;
    private IOTManager iotManager;
    private ProgressDialog loading;
    
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_email);

        iotManager = new IOTManager(this);
        mtextview1 = (TextView)findViewById(R.id.textview1);
        mlogin_id = (EditText)findViewById(R.id.edit_login_id);
        mlogin_pw = (EditText)findViewById(R.id.edit_login_pw);
        mbtn_login_by_email = (Button)findViewById(R.id.btn_email_log);
        
        mbtn_login_by_email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateTopken();
            }
        });
        
    }

    private void updateTopken() {

        if(loading == null){
            loading = UIComponent.createProgressDialog(LoginByEmailActivity.this);
        }
        loading.show();
        iotManager.login(UserAccount.OAUTHTYPE.MOREMOTE, null, mlogin_id.getText().toString(), mlogin_pw.getText().toString(), null, loginResult);

    }

    private void mainPage() {

        Intent intent = new Intent();
        intent.setClass(LoginByEmailActivity.this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();

    }

    final private LoginActivity.MsgHandler loginResult = new LoginActivity.MsgHandler(this) {
        @Override
        public void handleMessage(android.os.Message msg) {
            super.handleMessage(msg);

            if(loading.isShowing()) {
                loading.dismiss();
            }

            if(msg.what == 1){
                mainPage();

            }else{
                /** Login Fail **/
                Log.d(TAG, "Login By Email Fail!");
                showAlertWithMsg(getString(R.string.login_email_fail));
            }
        }

    };


    private void showAlertWithMsg(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(message).setTitle(R.string.login_fail)
                .setPositiveButton(R.string.settings_button_ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }



}
