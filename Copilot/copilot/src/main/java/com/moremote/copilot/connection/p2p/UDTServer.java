package com.moremote.copilot.connection.p2p;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import java.net.MulticastSocket;
import java.util.Map;

/**
 * Created by kentpon on 2015/4/30.
 */
public class UDTServer {
    private final static String TAG = "UDTServer";

    static{
        System.loadLibrary("UDTServerJni");
    }
    private native int createUDTServer(int port);
    private native boolean sendByUDT(int UDTFD, byte[] buff, int buffLen);
    private native byte[] receiveByUDT(int UDTFD);
    private native int AcceptUDTServer(int UDTServ);
    private native void closeUDTSocket(int UDTFD);
    private native void startupUDT();
    private native void cleanupUDT();

    private int epollID;
    private static Handler handler;

    public UDTServer(Handler handler) {
        this.handler = handler;
        epollID = -1;
        startupUDT();
    }

    class AccepctRunnable implements Runnable {
        Map<String,Integer> mapUDT;
        MulticastSocket socket;
        String userXmpp;
        AccepctRunnable(Map<String,Integer> mapUDT, MulticastSocket socket, String userXmpp) {
            this.mapUDT = mapUDT;
            this.socket =socket;
            this.userXmpp = userXmpp;
        }
        @Override
        public void run() {
            int port = socket.getLocalPort();
            socket.close();
            int UDTServerFD = createUDTServer(port);
            int UDTSocketFD = AcceptUDTServer(UDTServerFD);
            mapUDT.put("UDTServerFD", UDTServerFD);
            mapUDT.put("UDTSocketFD", UDTSocketFD);
            Log.d(TAG, "sockfd " + UDTSocketFD + " " + mapUDT.get("UDTSocketFD"));
            Message msg = new Message();
            Bundle bundle = new Bundle();
            bundle.putString(StunAsyncTask.DataField.DEST_JID, userXmpp);
            msg.setData(bundle);
            msg.obj = mapUDT;
            handler.sendMessage(msg);
        }
    }

    public void transformToUDTSocket(Map<String,Integer> mapUDT, MulticastSocket socket, String userXmpp) {
        Thread AcceptThread = new Thread(new AccepctRunnable(mapUDT, socket, userXmpp));
        AcceptThread.setPriority(Thread.MAX_PRIORITY);
        AcceptThread.start();
    }


    public int create(int port) {
        return createUDTServer(port);
    }

    public int accept(int UDTServerFD) {
        return AcceptUDTServer(UDTServerFD);
    }

    public byte[] receive(int UDTFD) {
        return receiveByUDT(UDTFD);
    }

    public boolean send(int UDTFD, byte[] buff) {
        return sendByUDT(UDTFD, buff, buff.length);
    }

    public void close(int UDTFD) {
        closeUDTSocket(UDTFD);
    }

    public void close() {
        cleanupUDT();
    }
}
