package com.moremote.copilot.interfaces;

public interface IUiModeChanged {

	void onUiModeChanged();

}
