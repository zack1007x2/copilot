package com.moremote.copilot.enums;

//for copter
public class DRONE_MODE {
	public final static int MODE_STABLE = 0;
	public final static int MODE_ACRO = 1;
	public final static int MODE_ALT_HOLD = 2;
	public final static int MODE_AUTO = 3;
	public final static int MODE_GUIDED = 4;
	public final static int MODE_LOITER = 5;
	public final static int MODE_RTL = 6;
	public final static int MODE_CIRCLE = 7;
	public final static int MODE_LAND = 9;
	public final static int MODE_DRIFT = 11;
	public final static int MODE_SPORT = 13;
	public final static int MODE_FLIP = 14;
	public final static int MODE_AUTOTUNE = 15;
	public final static int MODE_POSHOLD = 16;
	public final static int MODE_BRAKE = 17;
}
