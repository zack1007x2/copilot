// $codepro.audit.disable
// com.instantiations.assist.eclipse.analysis.audit.rule.effectivejava.alwaysOverridetoString.alwaysOverrideToString
package com.moremote.copilot.queue.endpoints.gs;


import android.util.Base64;
import android.util.Log;

import com.moremote.copilot.IPCamApplication;
import com.moremote.copilot.enums.APP_STATE;
import com.moremote.copilot.enums.SERVER_IP_MODE;
import com.moremote.copilot.queue.endpoints.GroundStationServer;
import com.moremote.copilot.service.MainService;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class GroundStationServerXMPP extends GroundStationServer {

	@SuppressWarnings("unused")
	private static final String TAG = GroundStationServerXMPP.class.getSimpleName();

	private static final int SIZEBUFF = 1024;
	private int counter;
	private String MsgToSent="";
	private IPCamApplication hub;
	private List<String> userList = new ArrayList<String>();


	public GroundStationServerXMPP(IPCamApplication hub) {
		super(hub, SIZEBUFF);
		this.hub = hub;
		serverMode = SERVER_IP_MODE.XMPP;
	}

	@Override
	public void startServer(int port) {
		userList.clear();
	}

	@Override
	public void stopServer() {
		stopMsgHandler();
		IPCamApplication.sendAppMsg(APP_STATE.MSG_SERVER_STOPPED);
		userList.clear();
	}

	@Override
	public boolean isRunning() {
		return (MainService.xmppConnector!=null);
	}

	@Override
	public boolean writeBytes(byte[] bytes) throws IOException {
//		if(isRunning() && MainService.CurJID!=null){
		if(!userList.isEmpty()){
			MsgToSent +=  IPCamApplication.XMPPCommand.DRONE+ Base64.encodeToString(bytes, Base64.DEFAULT);
			counter++;
			if(counter>=5){
				Log.d(TAG, "Length = " + MsgToSent.length() + "  |  " + MsgToSent);
				for(int i = 0;i<userList.size();i++){
					MainService.xmppConnector.sendMessage(userList.get(i), MsgToSent);
				}
				counter=0;
				MsgToSent="";
			}
			return true;
		}
		return false;
	}
	@Override
	public boolean isClientConnected() {
		return true;
	}

	public void addUser(String user){
		if(!userList.contains(user))
			userList.add(user);

	}

	public void removeUser(String user){
		if(userList.contains(user))
			userList.remove(user);
	}

}
