package com.moremote.copilot.drone;

import com.MAVLink.common.msg_heartbeat;
import com.MAVLink.enums.MAV_MODE_FLAG;
import com.MAVLink.enums.MAV_STATE;
import com.moremote.copilot.IPCamApplication;
import com.moremote.copilot.interfaces.IDorneEnvironmentChangeListener;
import com.moremote.copilot.interfaces.IDroneStateChangeListener;

import java.util.ArrayList;

/**
 * Created by Zack on 2016/1/29.
 */
public class Drone {

    private IPCamApplication hub;
    private State mState;
    private boolean wasFlying;
    private float mAltitude;
    private ArrayList<Object> mListeners = new ArrayList<>();

    public Drone(IPCamApplication context) {
        hub = context;
        mState = new State();
    }

    public State getState() {
        return mState;
    }


    public void updateDroneState(msg_heartbeat msg_heartbeat) {

        updateCurMode(msg_heartbeat.custom_mode);

        updateIsFlying(msg_heartbeat);

        updateArmed(msg_heartbeat);

    }

    public float getRealAltitude() {
        return mAltitude;
    }

    public void setRealAltitude(float altitude) {
        mAltitude = altitude;
        for(Object listener:mListeners){
            ((IDorneEnvironmentChangeListener)listener).onRealAltitudeChange(altitude);
        }
    }

    private void updateArmed(msg_heartbeat msgHeartbeat) {
        mState.setIsArmed((msgHeartbeat.base_mode & MAV_MODE_FLAG.MAV_MODE_FLAG_SAFETY_ARMED) == MAV_MODE_FLAG.MAV_MODE_FLAG_SAFETY_ARMED);
    }

    private void updateIsFlying(msg_heartbeat msgHeartbeat) {
        short systemStatus = msgHeartbeat.system_status;

        final boolean isFlying = systemStatus == MAV_STATE.MAV_STATE_ACTIVE
                || (wasFlying
                && (systemStatus == MAV_STATE.MAV_STATE_CRITICAL || systemStatus == MAV_STATE.MAV_STATE_EMERGENCY));

        if(isFlying!=wasFlying){
            wasFlying =isFlying;
            mState.setIsFlying(wasFlying);
        }
    }

    private void updateCurMode(long custom_mode){
        mState.setMode(custom_mode);
    }

    public void registerListener(Object listener){
        if(listener instanceof IDorneEnvironmentChangeListener){
            mListeners.add(listener);
        }

        if(listener instanceof IDroneStateChangeListener){
            mState.registerStateChangeListener(listener);
        }

    }

    public void unregisterListener(Object listener){
        if(listener instanceof IDorneEnvironmentChangeListener){
            mListeners.remove(listener);
        }

        if(listener instanceof IDroneStateChangeListener){
            mState.unregisterStateChangeListener(listener);
        }

    }
}
