package com.moremote.copilot.activity;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.ActivityInfo;
import android.hardware.Camera;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.util.SparseArray;
import android.view.SurfaceHolder;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.FacebookSdk;
import com.moremote.copilot.IPCamApplication;
import com.moremote.copilot.R;
import com.moremote.copilot.connection.oauth.GoogleOAuth;
import com.moremote.copilot.connection.oauth.UserAccount;
import com.moremote.copilot.enums.APP_STATE;
import com.moremote.copilot.enums.COPILOT_PREFS;
import com.moremote.copilot.fragments.ConnectionListFragment;
import com.moremote.copilot.fragments.DroneStateFragment;
import com.moremote.copilot.fragments.HUBFragment;
import com.moremote.copilot.fragments.NetWorkStateFragment;
import com.moremote.copilot.interfaces.IVAInfoUpdate;
import com.moremote.copilot.service.MainService;
import com.moremote.copilot.utils.IntentAction;
import com.moremote.copilot.utils.Utils;

import net.majorkernelpanic.streaming.gl.SurfaceView;

import java.util.ArrayList;


/**
 * A straightforward example of how to stream AMR and H.263 to some public IP using libstreaming.
 * Note that this example may not be using the latest version of libstreaming !
 */
public class MainActivity extends FragmentActivity implements OnClickListener, SurfaceHolder
        .Callback {

    private final static String TAG = MainActivity.class.getSimpleName();

    // UI
    private View connListLayout;
    private ImageView mCameraStatImageIcon;
    private ImageView mSettingsImageIcon;
    private ImageView mCameraSwitchImageIcon;
    public SurfaceView mActivitySurfaceView;
    public android.view.SurfaceView mDebugSurfaceView;

    private TextView ConnUserNumTxt;
    private TextView CameraStatTxt;



    // User
    private UserAccount userAccount;
    private ArrayList<String> mConnectionUserDisplayName;

    // Facebook
    private AccessTokenTracker accessTokenTracker;
    // Service Binder
    private MainService.ServiceBinder mBinder;

    //tab fragment
    private FragmentManager fragmentManager;
    private FragmentTransaction fragmentTransaction;
    private SparseArray<Fragment> navigateMap = new SparseArray<>();
    private TextView tvNetWorkState, tvDroneState;
    private TextView tvVAInfo;
    private IVAInfoUpdate mVAInfoUpdateListener;

    //Application
    IPCamApplication hub;


    private String DisplayName;
    private static final IntentFilter eventFilter = new IntentFilter();

    static {
        eventFilter.addAction(IntentAction.OnAuthenticated);
        eventFilter.addAction(IntentAction.OnConnectionClosedOnError);
        eventFilter.addAction(IntentAction.OnPictureUploadOAuthTypeGoogle);
        eventFilter.addAction(IntentAction.NotifyUserConnectionChanged);
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(IntentAction.OnAuthenticated)) {
                mCameraStatImageIcon.setImageResource(R.drawable.btn_online_selector);
                CameraStatTxt.setVisibility(View.INVISIBLE);
                Toast.makeText(MainActivity.this, getResources().getString(R.string.login_success),
                        Toast.LENGTH_SHORT).show();

            }else if(action.equals(IntentAction.OnConnectionClosedOnError)){
                mCameraStatImageIcon.setImageResource(R.drawable.offline);
                CameraStatTxt.setVisibility(View.VISIBLE);
                Toast.makeText(MainActivity.this, getResources().getString(R.string.connection_fail),
                        Toast.LENGTH_SHORT).show();

            }else if(action.equals(IntentAction.OnPictureUploadOAuthTypeGoogle)){
                final String friendJID = intent.getStringExtra("JID");
                Log.d(TAG, "OnPictureUploadOAuthTypeGoogle, JID : " + friendJID);
                new GoogleOAuth(MainActivity.this, userAccount, new LoginActivity.MsgHandler(MainActivity.this) {
                    @Override
                    public void handleMessage(android.os.Message msg) {
                        super.handleMessage(msg);
                        if (msg.what == 1) {
                            Log.d("Google", userAccount.ACCOUNT_NAME);
                            Log.d("Google", userAccount.ACCOUNT_EMAIL);
                            Log.d("Google", userAccount.ACCOUNT_TOKEN);
                            userAccount.SaveUserData();
                            if (mBinder != null ) {
                                mBinder.upDateUserAccount();
                                mBinder.calltakePicture(friendJID);
                            }
                        } else {
                            /** Google Oauth Fail **/
                            Log.e("Google", "Google Oauth Fail");
                        }
                    }
                }).execute();

            }else if(action.equals(IntentAction.NotifyUserConnectionChanged)){
                ConnUserNumTxt.setText(String.format(getResources().getString(R.string.connection_user_num), DisplayName, intent.getIntExtra("mUserListNUM", 0)));
                if ( mBinder != null) {
                    mConnectionUserDisplayName.clear();
                    mBinder.getUserDisplayName(mConnectionUserDisplayName);
                }
                String[] user = new String[mConnectionUserDisplayName.size()];
                mConnectionUserDisplayName.toArray(user);
                IPCamApplication.sendAppMsg(APP_STATE.MSG_XMPP_CLIENT_LIST_UPDATE, user);
            } else if (action.equals(IntentAction.SurfaceCreated)){
                if (mBinder != null && mActivitySurfaceView!= null) {
                    mBinder.SurfaceCreated(mActivitySurfaceView);
                }
            }
        }
    };

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i(TAG, "Act onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        hub = (IPCamApplication) this.getApplication();
        if (null == savedInstanceState) { // init only if we are just created
            hub.hubInit();
        }
        initActivity();
    }

    private void initActivity() {
        userAccount = new UserAccount(this);
        mConnectionUserDisplayName = new ArrayList<String>();
        connListLayout  = (View) findViewById(R.id.connect_list_layout);
        mCameraStatImageIcon = (ImageView)findViewById(R.id.camera_stat_icon);
        mSettingsImageIcon = (ImageView)findViewById(R.id.settings_icon);
        mCameraSwitchImageIcon = (ImageView)findViewById(R.id.camera_switch_icon);
        CameraStatTxt = (TextView) findViewById(R.id.camera_stat_tv);
        ConnUserNumTxt = (TextView) findViewById(R.id.conn_usernum_tv);
        mActivitySurfaceView = (SurfaceView)findViewById(R.id.activity_surface);
        mActivitySurfaceView.getHolder().addCallback(this);
        mDebugSurfaceView = (android.view.SurfaceView)findViewById(R.id.activity_debug_surface);
        DisplayName = "[ " + userAccount.ACCOUNT_NAME + " ] " + IPCamApplication.getDeviceName();
        ConnUserNumTxt.setText(String.format(getResources().getString(R.string.connection_user_num), DisplayName, 0));
        tvVAInfo = (TextView) findViewById(R.id.tvVAInfo);
        //tab
        tvNetWorkState = (TextView)findViewById(R.id.tvNetWorkState);
        tvDroneState = (TextView)findViewById(R.id.tvDroneState);

        // set default connection type
        mCameraStatImageIcon.setOnClickListener(this);
        ConnUserNumTxt.setOnClickListener(this);
        mSettingsImageIcon.setOnClickListener(this);
        mCameraSwitchImageIcon.setOnClickListener(this);
        // Enable Switch Camera Icon
        int numberOfCameras = Camera.getNumberOfCameras();
        Log.d(TAG, "Camera Num : " + numberOfCameras);
        if ( numberOfCameras <= 1) {
            mCameraSwitchImageIcon.setVisibility(View.GONE);
        }

        // Start Facebook Token tracking
        if (userAccount.OAuthType == UserAccount.OAUTHTYPE.FACEBOOK) {
            //初始化FacebookSdk，記得要放第一行，不然setContentView會出錯
            if (!FacebookSdk.isInitialized()) {
                FacebookSdk.sdkInitialize(getApplicationContext());
            }
            startFacebookTokenTracking();
        }

        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver, eventFilter);
        bindService(new Intent(this, MainService.class), mConnection, Context.BIND_AUTO_CREATE);

        mVAInfoUpdateListener = new IVAInfoUpdate() {
            @Override
            public void onVAInfoUpdate(String updateInfo) {
                final String info = updateInfo;
//                if(tvVAInfo.getVisibility()==View.VISIBLE){
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            tvVAInfo.setText(info);
                        }
                    });
//                }
            }
        };
    }

    @Override
    public void onStart() {
        Log.i(TAG, "Act onStart");
        super.onStart();
    }

    @Override
    public void onResume() {
        Log.i(TAG, "Act onRusume");
        super.onResume();
        refreshTab();
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.i(TAG, "Act onPause");
    }

    @Override
    protected void onStop() {
        Log.i(TAG, "Act onStop");
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i(TAG, "Act onDestroy");
        Intent iStopService = new Intent(MainActivity.this,MainService.class);
        stopService(iStopService);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
        unbindService(mConnection);

        // Stop Facebook token Tracking
        if (accessTokenTracker != null) {
            accessTokenTracker.stopTracking();
        }
    }

    /** Defines callbacks for service binding, passed to bindService() */
    private ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            Log.d(TAG,"onServiceConnected");
            mBinder = (MainService.ServiceBinder)service;
            if (mActivitySurfaceView != null) {
                mBinder.SurfaceCreated(mActivitySurfaceView);
//                mBinder.setDebugView(mDebugSurfaceView);
//                mBinder.setVAInfoUpdateListener(mVAInfoUpdateListener);
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            Log.d(TAG, "onServiceDisconnected");
        }
    };

    /** TODO modified button1/2 to xmpp login/logout by kentpon 2014/12/02**/
    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.camera_switch_icon) {
            mBinder.onCameraSwitchIconClick();
        }
        if (v.getId() == R.id.conn_usernum_tv) {
            if (connListLayout.getVisibility() == View.INVISIBLE) {
                String[] user = new String[mConnectionUserDisplayName.size()];
                mConnectionUserDisplayName.toArray(user);
                IPCamApplication.sendAppMsg(APP_STATE.MSG_XMPP_CLIENT_LIST_UPDATE, user);
                connListLayout.setVisibility(View.VISIBLE);
            }
            else {
                connListLayout.setVisibility(View.INVISIBLE);
            }
        }
        if (v.getId() == R.id.settings_icon) {

            Intent intent = new Intent();
            Bundle bundle = new Bundle();

            intent.putExtras(bundle);
            intent.setClass(MainActivity.this, SettingsActivity.class);
            startActivityForResult(intent, 1);
        }

        if(v.getId() == R.id.camera_stat_icon){
//            if(hub.prefs.getBoolean(COPILOT_PREFS.PREF_UVC_CAMERA,false))
//                mBinder.uvcOnClick();
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK && data != null) {
            boolean logout = data.getBooleanExtra("Logout", false);
            if (logout) {
                if(Utils.isMyServiceRunning(this, MainService.class)){
                    Intent intent = new Intent(this,MainService.class);
                    stopService(intent);
                    Log.e(TAG, "Stop Service");
                }

                Intent intent = new Intent();
                intent.setClass(MainActivity.this, LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                this.finish();
            }
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        Log.d(TAG, "Act surfaceChanged");
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        Log.d(TAG, "Act surfaceCreated");
        if(mBinder != null){
            mBinder.SurfaceCreated(mActivitySurfaceView);
        }
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        Log.d(TAG, "Act surfaceDestroyed");
        if(mBinder != null){
            mBinder.SurfaceDestroy();
        }
    }

    /** Facebook Function **/
    private void startFacebookTokenTracking(){
        accessTokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken oldAccessToken, AccessToken currentAccessToken) {
                // Set the access token using
                // currentAccessToken when it's loaded or set.
                // If the access token is available already assign it.
                if(currentAccessToken != null) {
                    userAccount.ACCOUNT_TOKEN = AccessToken.getCurrentAccessToken().getToken();
                    userAccount.SaveUserData();
                    if(mBinder != null){
                        mBinder.upDateUserAccount();
                    }
                }
            }
        };
        accessTokenTracker.startTracking();
    }

    public ArrayList<String> getConnectionUserDisplayName(){
        return mConnectionUserDisplayName;
    }

    private void refreshTab() {

        fragmentManager = getSupportFragmentManager();
        navigateMap.clear();
        mapNaviToFragment(R.id.connect_list_tv, new ConnectionListFragment());
        if(hub.prefs==null || hub.prefs.getBoolean(COPILOT_PREFS.PREF_LOG_SYSTEM, true)){
            mapNaviToFragment(R.id.tvNetWorkState, new NetWorkStateFragment());
            tvNetWorkState.setVisibility(View.VISIBLE);
        }else{
            tvNetWorkState.setVisibility(View.GONE);
        }
        if(hub.prefs==null || hub.prefs.getBoolean(COPILOT_PREFS.PREF_LOG_MAVLINK_BYTE, true)){
            mapNaviToFragment(R.id.tvDroneState, new DroneStateFragment());
            tvDroneState.setVisibility(View.VISIBLE);
        }else {
            tvDroneState.setVisibility(View.GONE);
        }
        navigateToFragment(fragmentManager, R.id.connect_list_tv);
    }


    private void mapNaviToFragment(int id, HUBFragment fragment) {
        View view = findViewById(id);

        view.setOnClickListener(TabClickListener);
        view.setSelected(false);
        navigateMap.put(id, fragment);
    }

    private void navigateToFragment(FragmentManager fm, int id) {
        String tag = String.valueOf(id);
        fragmentTransaction = fm.beginTransaction();
        if (null == fm.findFragmentByTag(tag)) {
            fragmentTransaction.replace(R.id.contentframe, navigateMap.get(id), tag);
        } else {
            fragmentTransaction.show(navigateMap.get(id));
        }
        fragmentTransaction.commitAllowingStateLoss();
        for (int i = 0, size = navigateMap.size(); i < size; i++) {
            int curId = navigateMap.keyAt(i);
            if (curId == id) {
//                mFragment = navigateMap.get(id);
                findViewById(id).setSelected(true);
            } else {
                findViewById(curId).setSelected(false);
            }
        }
    }

    private OnClickListener TabClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            int id = v.getId();
            if (navigateMap.indexOfKey(id) >= 0) {
                if (!v.isSelected()) {
                    navigateToFragment(getSupportFragmentManager(), id);
                }
            }
        }

    };

    public void switchVADebugMode() {
        if(mActivitySurfaceView.getVisibility()==View.VISIBLE){
            mActivitySurfaceView.setVisibility(View.GONE);
            mDebugSurfaceView.setVisibility(View.VISIBLE);
//            tvVAInfo.setVisibility(View.VISIBLE);
        }else{
            mActivitySurfaceView.setVisibility(View.VISIBLE);
            mDebugSurfaceView.setVisibility(View.GONE);
//            tvVAInfo.setVisibility(View.GONE);
        }
    }
}
