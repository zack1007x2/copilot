package com.moremote.copilot.connection.relay;

public interface RelayMessageListener {

	public void processMessage(byte[] data, int length);
	
}
