package com.moremote.copilot.queue.endpoints;


import com.moremote.copilot.IPCamApplication;
import com.moremote.copilot.queue.ConnectorBytes;
import com.moremote.copilot.viewadapters.devicelist.ItemPeerDevice;

import java.io.IOException;

public abstract class DroneClient extends ConnectorBytes {

	@SuppressWarnings("unused")
	private static final String TAG = DroneClient.class.getSimpleName();

	// application handler used to report connection states

	private ItemPeerDevice myPeerDevice;

	public abstract void startClient(ItemPeerDevice drone) throws Exception;

	public abstract void stopClient();

	public abstract boolean isConnected();

	public abstract String getMyName();

	public abstract String getMyAddress();

	public abstract String getPeerName();

	public abstract String getPeerAddress();

	public abstract boolean writeBytes(byte[] bytes) throws IOException;

	protected DroneClient(IPCamApplication hub, int capacity) {
		super(hub, capacity);
	}

	public final ItemPeerDevice getMyPeerDevice() {
		return myPeerDevice;
	}

	public final void setMyPeerDevice(ItemPeerDevice myPeerDevice) {
		this.myPeerDevice = myPeerDevice;
	}

}
