package com.moremote.copilot.interfaces;

/**
 * Created by Zack on 2016/1/29.
 */
public interface IDorneEnvironmentChangeListener {

    void onRealAltitudeChange(float realAltitude);
}
