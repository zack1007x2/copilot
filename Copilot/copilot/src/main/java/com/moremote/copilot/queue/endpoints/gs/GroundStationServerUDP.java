// $codepro.audit.disable
// com.instantiations.assist.eclipse.analysis.audit.rule.effectivejava.alwaysOverridetoString.alwaysOverrideToString
package com.moremote.copilot.queue.endpoints.gs;

import android.util.Log;

import com.moremote.copilot.IPCamApplication;
import com.moremote.copilot.enums.APP_STATE;
import com.moremote.copilot.enums.CONNECTOR_STATE;
import com.moremote.copilot.enums.SERVER_IP_MODE;
import com.moremote.copilot.queue.endpoints.GroundStationServer;
import com.moremote.copilot.queue.endpoints.thread.ThreadReaderDatagramBased;
import com.moremote.copilot.utils.Utils;

import java.io.IOException;
import java.net.SocketException;

public class GroundStationServerUDP extends GroundStationServer {

	private static final String TAG = GroundStationServerUDP.class.getSimpleName();

	private static final int SIZEBUFF = 1024;

	private ThreadReaderDatagramBased serverThread;

	public GroundStationServerUDP(IPCamApplication hub) {
		super(hub, SIZEBUFF);
		serverMode = SERVER_IP_MODE.UDP;
	}

	@Override
	public void startServer(int port) {

		// start received bytes handler

		setAddress(Utils.getIPAddress(true));
		setPort(port);

		try {
			serverThread = new ThreadReaderDatagramBased(Utils.getBroadcastAddress(hub), getPort(), ConnMsgHandler);

			ConnMsgHandler.obtainMessage(CONNECTOR_STATE.MSG_CONN_SERVER_STARTED.ordinal(), -1, -1, "UDP:" + Utils.getIPAddress(true) + ":" + getPort()).sendToTarget();
		}catch (SocketException e) {
			Log.d(TAG, "Thread creation exception: " + e.getMessage());
			return;
		}catch (IOException e) {
			Log.d(TAG, "Thread creation exception: " + e.getMessage());
			return;
		}

		serverThread.start();

	}

	@Override
	public void stopServer() {

		stopMsgHandler();

		serverThread.stopMe();

		IPCamApplication.sendAppMsg(APP_STATE.MSG_SERVER_STOPPED);
	}

	@Override
	public boolean isRunning() {
		return serverThread.isRunning();
	}

	@Override
	public boolean writeBytes(byte[] bytes) throws IOException {
		if (null != serverThread) {
			if (isClientConnected()) {
				serverThread.writeBytes(bytes);
				return true;
			}
			else {
				return false;
			}
		}
		else {
			return false;
		}
	}

	@Override
	public boolean isClientConnected() {
		//for UDP broadcast we assume someone is listening 
		return true;
	}
}
