package com.moremote.copilot.connection.p2p;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetSocketAddress;
import java.net.MulticastSocket;
import java.net.SocketException;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

import org.jivesoftware.smack.chat.Chat;
import org.jivesoftware.smack.chat.ChatMessageListener;
import org.jivesoftware.smack.packet.Message;

import android.os.*;
import android.util.Log;

import com.moremote.copilot.IPCamApplication;
import com.moremote.copilot.connection.xmpp.XMPPConnector;

/**
 * Created by lintzuhsiu on 14/11/7.
 */
public class StunAsyncTask extends Thread {

	public static final String TAG = "stun";
	public static final int SUCCESS = 1;
	public static final int FAILED = -1;
	public static class DataField {
		public static final String DEST_IP   	   = "dest_ip";
		public static final String DEST_PORT 	   = "dest_port";
		public static final String NETWORK_TYPE    = "net_type";
        public static final String DEST_JID    = "dest_jid";
	}

    // xmpp 
    private XMPPConnector xmppConnector;
    private Handler stunMsgHandler;
    private ChatMessageListener xmppMessageListener;
    private String jid;
    
    // stun
    private Thread punchThread;
    private Thread receiveThread;
    private MulticastSocket socket;
    private boolean isLocalSuccess;
    private boolean isDestSuccess;
    private int connectionType;
    // WAN IP
    private String selfPublicIP;
    private int selfPublicPort;
    // LOCAL IP
    private String selfLocalIP;    
    // DEST IP
    private String destinationIP;
    private int destinationPort;    
    // DEST LOCAL IP
    private String desLocalIP;
    private int desLocalPort;

    // NAT TYPE
    private IPCamApplication.NatType selfNatType;
    private IPCamApplication.NatType destNatType;

    public StunAsyncTask(XMPPConnector xmppConnector, String jid, Handler handler) {
        this.xmppConnector = xmppConnector;
        this.jid = jid;
        this.stunMsgHandler = handler;

		isLocalSuccess = false;
		isDestSuccess = false;
    }

    public StunAsyncTask(XMPPConnector xmppConnector, String jid, Handler handler, 
    		String desPublicIP, int desPublicPort, String desLocalIP, int desLocalPort, String selfLocalIP, IPCamApplication.NatType natType) {
        this.xmppConnector = xmppConnector;
        this.jid = jid;
        this.stunMsgHandler = handler;
        this.destinationIP = desPublicIP;
        this.destinationPort = desPublicPort;
        this.desLocalIP = desLocalIP;
        this.desLocalPort = desLocalPort;
        this.selfLocalIP = selfLocalIP;
        this.destNatType = natType;

        isLocalSuccess = false;
		isDestSuccess = false;
    }

    @Override
    public void run() {
        super.run();
        
        try {
            // init socket
            socket = new MulticastSocket();
            
            // start stun
            setXmppMsgListener();
			getPublicIPAndPort();
			setDestionIPAndPort();
            if(connectionType == IPCamApplication.ConnectionType.P2P_WAN_UDT)
                checkNatType();
            else
                selfNatType = IPCamApplication.NatType.NAT_NORMAL;
	        // Send XMPP: STUN
            sendIPAndPort();
            setReceiveDatagramThread();
	        punchDatagram();
	        
	        // stop 30s and display message if stun failed
			sleep(30*1000);
			if (!isLocalSuccess && !isDestSuccess) {
				Log.d(TAG, "stun failed , JID: " + jid);
				connectionType = IPCamApplication.ConnectionType.RELAY;
				returnToMainThread(FAILED);
			}
		} catch (InterruptedException e) {
			Log.d(TAG, "InterruptedException");
		} 
        catch (IOException e) {
			Log.d(TAG, "IOException");
		} finally {
	        interrupt();
		}
    }

    @Override
    public void interrupt() {
        try {
            if (receiveThread != null && !receiveThread.isInterrupted()) {
                receiveThread.interrupt();
            }
        }
        catch (Exception e) {
            Log.e(TAG, "receiveThread interrupt Exception E:" + e.getMessage());
        }
        try {
            if (punchThread != null && !punchThread.isInterrupted()) {
                punchThread.interrupt();
            }
        }
        catch (Exception e) {
            Log.e(TAG, "punchThread interrupt Exception E:" + e.getMessage());
        }
        try {
            if (xmppMessageListener != null) {
                xmppConnector.removeMessageListener(xmppMessageListener);
            }
        }
        catch (Exception e) {
            Log.e(TAG, "xmppMessageListener removeMessageListener Exception E:" + e.getMessage());
        }

        receiveThread = null;
        punchThread = null;
        xmppMessageListener = null;

        super.interrupt();
    }



    private void getPublicIPAndPort() {
    	final byte[] command = {0x01, 0x0e};
		byte[] receiveData = new byte[20];
		int count = 0;
        getReceiveData(command, receiveData, IPCamApplication.P2PSERVER_IP, IPCamApplication.P2PSERVER_PORT);

        StringBuffer ip = new StringBuffer();
        for (int i = 2; i < 18; i++) {
            if (receiveData[i] != 0x00) {
                ip.append((char)(receiveData[i] & 0xFF));
            }
        }
        selfPublicIP = ip.toString();

        selfPublicPort = (receiveData[18] & 0xFF) << 8;
        selfPublicPort |= receiveData[19] & 0xFF;

        Log.d(TAG, "got public ip and port: " + selfPublicIP + ", " + selfPublicPort);

    }

    private void setReceiveDatagramThread() {
        receiveThread = new ReceiveDatagramThread();
        receiveThread.setPriority(Thread.MAX_PRIORITY);
        receiveThread.start();
    }

    /**
     *  SendIPAndPort
     *
     */
    private void sendIPAndPort() {
    	Log.d(TAG, "sendIPAndPort:" + selfPublicIP + "," + selfPublicPort + "-" +
                selfLocalIP + "," + socket.getLocalPort() + ":" + selfNatType.toInt());
    	
        xmppConnector.sendMessage(jid, "stun:" + selfPublicIP + "," + selfPublicPort + "-" +
                selfLocalIP + "," + socket.getLocalPort() + ":" + selfNatType.toInt());
    }

    private void punchDatagram() {
    	punchThread = new PunchThread();
    	punchThread.setPriority(Thread.MIN_PRIORITY);
    	punchThread.start();
    }

    private void setXmppMsgListener() {
    	xmppMessageListener = new ChatMessageListener() {
            @Override
            public void processMessage(Chat chat, Message message) {
                String msg = message.getBody();
                String from = message.getFrom();

                if (msg == null) {
                	return;
                }
                
                Log.d(TAG, "xmpp message received:" + msg + ", from: " + from);
                if (msg.contains(IPCamApplication.XMPPCommand.STUN_SUCCESS) && from.equals(jid)) {
                    destinationPort = Integer.valueOf(msg.substring(msg.indexOf(":")+1, msg.length()));
                    Log.d(TAG, "send xmpp success message to " + jid);
                    isLocalSuccess = true;
                    isDestSuccess = true;
                    returnToMainThread(SUCCESS);
                }
            }
        };
        xmppConnector.addMessageListener(xmppMessageListener);
    }
    
    private synchronized void returnToMainThread(int result) {
    	android.os.Message handleMsg = new android.os.Message(); 
		handleMsg.what = result;     
		
    	Bundle extras = new Bundle();
        extras.putInt(DataField.NETWORK_TYPE, connectionType);
        extras.putString(DataField.DEST_JID, jid);
    	if (result == FAILED) {
            handleMsg.setData(extras);
            stunMsgHandler.sendMessage(handleMsg);
            socket.close();
    	}
    	else if (isLocalSuccess && isDestSuccess) {
	        extras.putString(DataField.DEST_IP, destinationIP);
	        extras.putInt(DataField.DEST_PORT, destinationPort);
            if ( connectionType == IPCamApplication.ConnectionType.P2P_WAN_UDT) {
                handleMsg.obj = socket;
            } else {
                socket.close();
            }
	        handleMsg.setData(extras);
            stunMsgHandler.sendMessage(handleMsg);
            Log.d(TAG, "return message to main thread");
    	}
    }
    
    private void setDestionIPAndPort() {
    	if (selfPublicIP.equals(destinationIP)) {
        	destinationIP = desLocalIP;
        	destinationPort = desLocalPort;
        	connectionType = IPCamApplication.ConnectionType.P2P_LAN_TCP;
            Log.d(TAG, "P2P_LAN_TCP");
        }
    	else {
    		connectionType = IPCamApplication.ConnectionType.P2P_WAN_UDT;
            Log.d(TAG, "P2P_WAN_UDT");
    	}
    }

    class PunchThread extends Thread {
    	
    	private boolean isStop = false;
    	
    	@Override
		public void run() {
			try {
                byte[] msg = {0x01, 0x02, 0, 0};
                boolean fChangePort = false;
                int punchUnit = 30;
                int punchRetry = 3;
                int punchInterval = 500;
                if( connectionType != IPCamApplication.ConnectionType.P2P_LAN_TCP && selfNatType.compare(IPCamApplication.NatType.NAT_NORMAL) && !destNatType.compare(IPCamApplication.NatType.NAT_NORMAL) ) {
                    if( !destNatType.compare(IPCamApplication.NatType.NAT3_UNUSUAL) )
                        fChangePort = true;
                    if(destNatType.compare(IPCamApplication.NatType.NAT4_RTOS)) {
                        destinationPort = destinationPort - computeRTOSPort(IPCamApplication.P2PSERVER_IP) + computeRTOSPort(selfPublicIP) + selfPublicPort - IPCamApplication.P2PSERVER_PORT;
                        if(destinationPort > 65536) destinationPort %= 65536;
                        else if( destinationPort < 0) destinationPort += 65536;
                    }
                    else if(destNatType.compare(IPCamApplication.NatType.NAT4_OTHER)) {
                        destinationPort = 1024;
                        punchUnit = 120;
                        punchInterval = 50;
                        punchRetry = 2;
                    }
                    Thread.sleep(2000);
                }
                msg[2] = (byte)((destinationPort>>8) & 0xff);
                msg[3] = (byte)(destinationPort & 0xff);
                InetSocketAddress dstAddress = new InetSocketAddress(destinationIP, destinationPort);
                DatagramPacket sendPacket = new DatagramPacket(msg, msg.length, dstAddress);

	            for (int i = 1+punchRetry; !isLocalSuccess && !isStop; i++) {
                    try {
                        socket.send(sendPacket);
                    } catch (SocketException e) {
                        Log.d(TAG, "punch: socket exception, send port: " + destinationPort);
                    } catch (IOException e){
                        Log.d(TAG, "punch: IO exception, send port: " + destinationPort);
                    }

		            if (i % punchRetry == 0) {
                        Log.d(TAG, "punch start" + destinationIP + "," + destinationPort);
                        if( i % punchUnit == 0) {
                            sleep(punchInterval);
                        }
                        if(fChangePort){
                            /** if dest is NAT4, need to change destinationPort
                             *** TODO if NAT TYPE == NAT4_OTHER, need change port by random
                             **/
                            if(destNatType.compare(IPCamApplication.NatType.NAT4_CGN) ){
                                destinationPort++;
                                destinationPort %= 65536;
                            }
                            else if(destNatType.compare(IPCamApplication.NatType.NAT4_LINUX)){
                                destinationPort++;
                                destinationPort %= 1024;
                            }
                            else if(destNatType.compare(IPCamApplication.NatType.NAT4_RTOS)){
                                if( i%(punchRetry*3) == 0)
                                    destinationPort += 2048;
                                else
                                    destinationPort -= 1024;
                                if( destinationPort < 0 ) destinationPort += 65536;
                                else destinationPort %= 65536;
                            }
                            else if( destNatType.compare(IPCamApplication.NatType.NAT4_OTHER) )
                            {
                                destinationPort += (int)(Math.random()*3+1);
                                destinationPort %= 65536;
                            }
                            msg[2] = (byte)((destinationPort>>8) & 0xff);
                            msg[3] = (byte)(destinationPort & 0xff);
                            dstAddress = new InetSocketAddress(destinationIP, destinationPort);
                            sendPacket = new DatagramPacket(msg, msg.length, dstAddress);
                        }
		            }
	            }
	            
	            Log.d(TAG, "punch over");
	        } catch (SocketException e) {
	        	Log.d(TAG, "punch: socket exception");
	        } catch (IOException e) {
	        	Log.d(TAG, "punch: IO exception");
	        } catch (InterruptedException e) {
	        	Log.d(TAG, "punch: InterruptedException");
			} finally {
                isStop = true;
			}
		}
    	
    	@Override
        public void interrupt() {
            super.interrupt();
            isStop = true;
        }
    	
    }
    
    class ReceiveDatagramThread extends Thread {

        @Override
        public void run() {            
            byte[] buf = new byte[4];
            DatagramPacket receiveDatagramPacket = new DatagramPacket(buf, buf.length);
           
            try {
                Log.d(TAG, "start receive punch data");
                while(!isDestSuccess) {
                    socket.receive(receiveDatagramPacket);
                    Log.d(TAG, "receive punch data");
                    if (buf[0] == 0x01 && buf[1] == 0x02 && !isDestSuccess) {
                        int selfPort;
                        selfPort = (buf[2] & 0xff) <<8;
                        selfPort |= buf[3] & 0xff;
                        Log.d(TAG, "send success message over xmpp");
                        isDestSuccess = true;
                        isLocalSuccess = true;
                        xmppConnector.sendMessage(jid, IPCamApplication.XMPPCommand.STUN_SUCCESS + selfPort);
                        returnToMainThread(SUCCESS);
                    }
                }
            } catch (IOException e) {
            	Log.d(TAG, "receive datagram: IO Exception");
            	interrupt();
            }
        }

        @Override
        public void interrupt() {
            super.interrupt();
        }

    }


    private void checkNat3()
    {
        byte[] command = {0x01, 0x1f};
        byte[] receiveData = new byte[20];
        getReceiveData(command, receiveData, IPCamApplication.P2PSERVER_IP, IPCamApplication.P2PSERVER_PORT);

        try {
            sleep(2);
        }catch (InterruptedException e) {
            Log.e(TAG, "checkNat3 1st: InterruptedException");
            selfNatType = IPCamApplication.NatType.NAT_ERROR;
            return;
        }

        command[1] = 0x0e;
        getReceiveData(command, receiveData, IPCamApplication.P2PSERVER_IP, IPCamApplication.P2PSERVER_PORT2);

        int portNAT3;
        portNAT3 = (receiveData[18] & 0xFF) <<8;
        portNAT3 |= receiveData[19] & 0xFF;
        Log.i(TAG, "checkNat3 get port2 " + portNAT3);
        if(portNAT3 != selfPublicPort)
            selfNatType = IPCamApplication.NatType.NAT3_UNUSUAL;
        else
            selfNatType = IPCamApplication.NatType.NAT_NORMAL;
    }

    private int computeRTOSPort(String ip)
    {
        int ipv4[] = new int[4], i;
        String[] s = ip.split("\\.");
        for( i=0 ; i<4 ; i++){
            Log.d(TAG,"computeRTOSPort "+s[i]);
            ipv4[i] = Integer.valueOf(s[i]);
        }
        int port =
                //((((((ipv4[0]-1)<<2) + ipv4[1])<<2) + ipv4[2])<<8) + ipv4[3];
                4096*(ipv4[0]-1)+1024*ipv4[1]+256*ipv4[2]+ipv4[3];
        if(ipv4[2] >= 19) port += 1024;
        if(ipv4[1] >=5 ) {
            port += (((ipv4[1]-5)/63)+1)*1024;
        }
        int[] excA = new int[]{3,18,34,50,64,71,85,101,117,133,148,164,180,196,211,227,243};
        for( i=0 ; i<17 ; i++){
            if(ipv4[0] < excA[i]) break;
        }
        port += i*1024;
        if(ipv4[0] >= 64) port+=48128;
        return port%65536;
    }

    private void checkNat4()
    {
        byte[] command = {0x01, 0x0e};
        byte[] receiveData = new byte[20];

        getReceiveData(command, receiveData, IPCamApplication.P2PSERVER_IP2, IPCamApplication.P2PSERVER_PORT);

        int portNAT4;
        portNAT4 = (receiveData[18] & 0xFF) << 8;
        portNAT4 |= receiveData[19] & 0xFF;
        Log.i(TAG, "checkNat4 1st got port: " + portNAT4);

        if(portNAT4 == selfPublicPort) {
            selfNatType = IPCamApplication.NatType.NAT_NORMAL;
            return;
        }

        getReceiveData(command, receiveData, IPCamApplication.P2PSERVER_IP2, IPCamApplication.P2PSERVER_PORT2);

        int port2NAT4;
        port2NAT4 = (receiveData[18] & 0xFF) << 8;
        port2NAT4 |= receiveData[19] & 0xFF;
        Log.i(TAG, "checkNat4 2th got port2: " + port2NAT4);

        /** after got 2 port, start check NAT4 TYPE*/
        //check CGN
        int dif[] = new int[2];
        dif[0] = portNAT4 - selfPublicPort;
        dif[1] = port2NAT4 - selfPublicPort;
        if( dif[0] < 0 ) dif[0] += 65536;
        if( dif[1] < 0 ) dif[1] += 65536;
        if( dif[0] < 30 && dif[1] < 30 ){
            selfNatType = IPCamApplication.NatType.NAT4_CGN;
            return;
        }

        //check linux base
        if(socket.getLocalPort() <=1024 && portNAT4 <= 1024 && port2NAT4 <= 1024){
            selfNatType = IPCamApplication.NatType.NAT4_LINUX;
            return;
        }

        //check RTOS
        int ip1PortAddNum = computeRTOSPort(IPCamApplication.P2PSERVER_IP);
        int ip2PortAddNum = computeRTOSPort(IPCamApplication.P2PSERVER_IP2);

        if( portNAT4 == selfPublicPort - ip1PortAddNum + ip2PortAddNum){
            //selfPublicPort = selfPublicPort - ip1PortAddNum + computeRTOSPort(destinationIP) + destinationPort - IPCamApplication.P2PSERVER_PORT;
            selfNatType = IPCamApplication.NatType.NAT4_RTOS;
            return;
        }
        selfNatType = IPCamApplication.NatType.NAT4_OTHER;
    }


    private void checkNatType()
    {
        Log.i(TAG, "check self NAT Type");
        checkNat4();
        if( selfNatType.toInt() == IPCamApplication.NatType.NAT_NORMAL.toInt())
            checkNat3();
        Log.i(TAG, "self NAT TYPE = "+selfNatType);
    }

    private void getReceiveData(byte[] sendData, byte[] receiveData, final String dstIP, final int dstPort) {
        InetSocketAddress dstAddress = new InetSocketAddress(dstIP, dstPort);
        Thread sendDataThread = null;
        try {
            final DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, dstAddress);

            final Semaphore lock = new Semaphore(0);
            sendDataThread = new Thread (new Runnable(){
                @Override
                public void run(){
                    //int i=0;
                    while(true) {
                        try {
                            //Log.d(TAG, "send to P2Pserver " + i + " times");
                            //i++;
                            socket.send(sendPacket);
                            if (lock.tryAcquire(500, TimeUnit.MILLISECONDS))
                                break;
                        } catch (IOException e) {
                            Log.e(TAG, "getReceiveData send: IO Exception");
                            break;
                        } catch (InterruptedException e) {
                            Log.e(TAG, "getReceiveData send: Interrupted Exception");
                            break;
                        }
                    }
                    //Log.e(TAG, "getReceiveData send: finish");
                }
            });
            sendDataThread.setPriority(Thread.MIN_PRIORITY);
            sendDataThread.start();

            DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
            do{
                socket.receive(receivePacket);

                if( receivePacket.getAddress().getHostAddress().compareTo( dstIP )!=0 ||
                        receivePacket.getPort() != dstPort ){
                    Log.d(TAG,receivePacket.getAddress().getHostAddress() +" vs "+ dstIP);
                    Log.d(TAG,receivePacket.getPort() + " vs " +dstPort);
                    receiveData[1] = 0x00;
                }
            }while(receiveData[0] != 0x00 || receiveData[1] != 0x0E);
            lock.release();

            //Log.e(TAG, "getReceiveData send: receiveData finish");
        } catch (SocketException e) {
            Log.e(TAG, "getReceiveData: Socket Exception");
        } catch (IOException e) {
            Log.e(TAG, "getReceiveData recv: IO Exception");
        }
        if (sendDataThread != null)
            sendDataThread.interrupt();
    }
}
