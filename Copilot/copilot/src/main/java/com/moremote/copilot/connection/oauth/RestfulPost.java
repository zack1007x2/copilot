package com.moremote.copilot.connection.oauth;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by gaozongyong on 2015/9/9.
 */
public class RestfulPost extends AsyncTask<String,Void,String>{


    private static final String TAG = "HttpPost";
    private Handler handler;
    private ArrayList<NameValuePair> params;

    public RestfulPost(Handler handler , ArrayList<NameValuePair> params) {
        this.handler = handler;
        this.params = params;
    }

    @Override
    protected String doInBackground(String... urls){

        String url = urls[0];
        String content="";
        try{

            HttpEntity entity = new UrlEncodedFormEntity(params, HTTP.UTF_8);
            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpPost post = new HttpPost(url);
            post.addHeader(entity.getContentType());
            post.setEntity(entity);
            HttpResponse resp = httpClient.execute(post);
            content = EntityUtils.toString(resp.getEntity());
            String status = String.valueOf(resp.getStatusLine().getStatusCode());
            Log.d(TAG, "Status Code" + status + " --- Url : " + params.toString());
            if(content.isEmpty()){
                Log.d(TAG,"Response Empty");
            }else{
                Log.d(TAG,"Content:"+content);
            }

            if(handler != null) {
                Bundle bundle = new Bundle();
                bundle.putString("StatusCode", status);
                bundle.putString("Content", content);
                Message msg = new Message();
                msg.setData(bundle);
                handler.sendMessage(msg);
            }

        }catch(Exception e){
            Log.d(TAG, e.toString());
        }
        return content;
    }

    protected void onPostExecute(String result)
    {
        try {
            JSONObject data = new JSONObject(result.toString());
            Log.d(TAG,"JSON DATA"+data);
        } catch (JSONException e) {
            Log.d(TAG, e.toString());
        }

    }
}
