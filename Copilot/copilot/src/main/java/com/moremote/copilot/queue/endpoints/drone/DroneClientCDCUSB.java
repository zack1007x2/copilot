package com.moremote.copilot.queue.endpoints.drone;

import android.annotation.TargetApi;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.os.Build;
import android.util.Log;

import com.hoho.android.usbserial.driver.UsbSerialDriver;
import com.hoho.android.usbserial.driver.UsbSerialProber;
import com.moremote.copilot.IPCamApplication;
import com.moremote.copilot.enums.APP_STATE;
import com.moremote.copilot.enums.COPILOT_PREFS;
import com.moremote.copilot.queue.endpoints.DroneClient;
import com.moremote.copilot.queue.endpoints.thread.ThreadReaderCDCUSB;
import com.moremote.copilot.viewadapters.devicelist.ItemPeerDevice;

import java.io.IOException;
import java.util.List;

/**
 * Created by Zack on 15/9/25.
 */
public class DroneClientCDCUSB extends DroneClient {


    private static final String TAG = DroneClientCDCUSB.class.getSimpleName();
    private static final int SIZEBUFF = 1024*16;

    private ThreadReaderCDCUSB readerThreadUSB;
    private IPCamApplication hub;

    private UsbSerialDriver mUSBdriver;
    private UsbManager USBmanager;
    private UsbDevice ConnectedDevice;
    private PendingIntent mPermissionIntent = null;


    public DroneClientCDCUSB(IPCamApplication hub) {
        super(hub, SIZEBUFF);
        this.hub = hub;
    }

    @Override
    public void startClient(ItemPeerDevice drone){

        mPermissionIntent = PendingIntent.getBroadcast(hub, 0, new Intent(IPCamApplication.ACTION_USB_PERMISSION_BASE), 0);

        // Get UsbManager from Android.
        USBmanager = (UsbManager) hub.getSystemService(Context.USB_SERVICE);

        //Get the list of available devices
        List<UsbDevice> availableDevices = UsbSerialProber.getAvailableSupportedDevices(USBmanager);
        if (availableDevices.isEmpty()) {
            Log.d(TAG, "No Devices found");
        }else{
            Log.d(TAG, "availableDevices size =" +availableDevices.size());

            //Pick the first device
            for(int i = 0;i<availableDevices.size(); i++){
                UsbDevice device = availableDevices.get(i);
                if (USBmanager.hasPermission(device)) {
                    mUSBdriver = getDriver(device);
                }else{
                    USBmanager.requestPermission(device, mPermissionIntent);
                    mUSBdriver = null;
                }
                if(mUSBdriver!=null){
                    ConnectedDevice = device;
                    break;
                }

            }
            if(mUSBdriver!= null){
                readerThreadUSB = new ThreadReaderCDCUSB(mUSBdriver, ConnMsgHandler);
                readerThreadUSB.start();
                IPCamApplication.sendAppMsg(APP_STATE.MSG_DRONE_CONNECTED);
            }
        }

    }

    public void resumeConnection(UsbDevice drone){

        mUSBdriver = getDriver(drone);
        if(mUSBdriver!=null){
            ConnectedDevice = drone;
        }

        if(mUSBdriver!= null){
            readerThreadUSB = new ThreadReaderCDCUSB(mUSBdriver, ConnMsgHandler);
            readerThreadUSB.start();
            IPCamApplication.sendAppMsg(APP_STATE.MSG_DRONE_CONNECTED);
        }

    }

    private UsbSerialDriver getDriver(UsbDevice device){
        try {
            mUSBdriver = openUsbDevice(device);
        } catch (IOException e) {
            e.printStackTrace();
            IPCamApplication.logger.sysLog("openUsbDevice fail");
            mUSBdriver = null;
            return null;
        }
        return mUSBdriver;
    }

    @Override
    public void stopClient() {
        Log.d(TAG, "Closing connection..");

        stopMsgHandler();

        // stop thread
        if (isConnected()) {
            readerThreadUSB.stopMe();
        }

        IPCamApplication.sendAppMsg(APP_STATE.MSG_DRONE_DISCONNECTED);
    }

    @Override
    public boolean isConnected() {
        if (null == readerThreadUSB) {
            return false;
        }
        else {
            return readerThreadUSB.isRunning();
        }
    }

    @Override
    public String getMyName() {
        return "CDC";
    }

    @Override
    public String getMyAddress() {
        return null;
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    public String getPeerName() {
        if(ConnectedDevice!=null)
            return ConnectedDevice.getProductName();
        else
            return null;
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    public String getPeerAddress() {
        if(ConnectedDevice!=null)
            return ConnectedDevice.getManufacturerName();
        else
            return null;
    }

    @Override
    public boolean writeBytes(byte[] bytes) throws IOException {
        if (isConnected()) {
            readerThreadUSB.writeBytes(bytes);
            return true;
        }
        return false;
    }


    private UsbSerialDriver openUsbDevice(UsbDevice device) throws IOException {
        // Get UsbManager from Android.
        UsbManager manager = (UsbManager) hub.getSystemService(Context.USB_SERVICE);

        // Find the first available driver.
        final UsbSerialDriver serialDriver = UsbSerialProber.openUsbDevice(manager, device);

        if (serialDriver == null) {

            Log.d(TAG, "No Devices found");

            throw new IOException("No Devices found");

        } else {
            Log.d(TAG, "Opening using Baud rate " + hub.prefs.getString(COPILOT_PREFS
                    .PREF_USB_BAUD,null));
            try {
                serialDriver.open();
                serialDriver.setParameters(Integer.valueOf(hub.prefs.getString(COPILOT_PREFS
                                .PREF_USB_BAUD,"115200")), 8,
                        UsbSerialDriver
                                .STOPBITS_1,
                        UsbSerialDriver
                        .PARITY_NONE);
                return serialDriver;
            } catch (IOException e) {
                Log.e(TAG, "Error setting up device: " + e.getMessage(), e);
                try {
                    serialDriver.close();
                } catch (IOException e2) {
                    // Ignore.
                }
                return null;
            }
        }
    }

}
