package com.moremote.copilot.broadcaster;

/**
 * Created by Zack on 15/10/22.
 */
public class BroadCastIntent {
    public final static String SYSTEM_ON_USB_ATTACHED = "sys_on_usb_attached";
    public final static String APP_ON_USB_ATTACHED = "app_on_usb_attached";
    public final static String SYSTEM_ON_USB_DETACHED = "sys_on_usb_detached";
    public final static String APP_ON_USB_DETACHED = "app_on_usb_detached";
    public final static String SYSTEM_ON_NOTIFICATION_CLICK = "sys_on_notification_click";
    public final static String APP_ON_NOTIFICATION_CLICK = "app_on_notification_click";
    public final static String BT_DISCONNECT = "bt_connection_error";
    public final static String OnNetworkTypeChange = "onNetworkChange";
}
