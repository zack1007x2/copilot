package com.moremote.copilot.drone.manager;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ImageFormat;
import android.hardware.Camera;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.View;

import com.moremote.copilot.IPCamApplication;
import com.moremote.copilot.enums.COPILOT_PREFS;
import com.moremote.copilot.interfaces.IDorneEnvironmentChangeListener;
import com.moremote.copilot.interfaces.onUVCPreviewCallBack;

import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.imgproc.Imgproc;

import java.nio.ByteBuffer;

/**
 * Created by Zack on 2015/11/23.
 */
public class IdentifyManager implements Camera.PreviewCallback , SurfaceHolder.Callback,
        onUVCPreviewCallBack, IDorneEnvironmentChangeListener {
    private final static String TAG = IdentifyManager.class.getSimpleName();

    static {
        if (!OpenCVLoader.initDebug()) {
            Log.i("opencv", "opencv initialization failed");
        } else {
            System.loadLibrary("Drone_main");
            Log.i("opencv", "opencv initialization success");

        }
    }

    private long startTime, stopTime, slottime;
    private IPCamApplication hub;

    private Mat mRgb;
    private Mat mYuv;

    //onPreviewFrame
    private int frameCount=0;
    String position_x, position_y;
    String ra;
    Bitmap screenBmp;
    Mat rotateImage;

    private long msMax=0;
    private long msMin=9999;

    private int previewHeight = 720;
    private int previewWidth = 1280;
    private int fixWidth = 960;

    private double curHeight;

    private boolean enableLandingMode;
    private boolean BaseParamTuning = false;

    private SurfaceHolder mSurfaceHolder = null;
    private android.view.SurfaceView mSurfaceView;
    private QRLocationListener mQRLocationListener;

    int angle = 0; //default
    double scale = 1.0;

    private final static double camera_FOV = 56;

    // JNI main function
    public native String[] QRLocation(long frameData);
    public native void binary_image(long frameData2);
    public native void contour_image(long frameDate3);
    public native String[] DistanceMove(long frameData);

    public IdentifyManager(IPCamApplication context) {
        hub = context;
        //kill below after enableLandingMode done
        mYuv = new Mat((int)(720*1.5), 1280, CvType.CV_8UC1);
        mRgb = new Mat(720 , 960, CvType.CV_8UC3);
        rotateImage = new Mat(960, 720,CvType.CV_8UC3);
        screenBmp = Bitmap.createBitmap(720, 960, Bitmap.Config.RGB_565);
        enableLandingMode = hub.prefs.getBoolean(COPILOT_PREFS.PREF_ENABLE_VA_LANDING, false);
    }

    @Override
    public void onPreviewFrame(byte[] data, Camera camera) {
        if(enableLandingMode){
            Camera.Parameters parameters;
            try{
                parameters = camera.getParameters();
            }catch (Exception e){
                e.printStackTrace();
                return;
            }
            if (parameters.getPreviewFormat() == ImageFormat.NV21) {
                if (mSurfaceView != null && mSurfaceView.getVisibility()==View.VISIBLE) {
                    runDebugMode(data, false);
                } else {
                    //without rotate and draw bitmap as well
                    runBackgroundMode(data, false);
                }
            }
        }
    }

    @Override
    public void onUVCPreviewFrame(ByteBuffer frame) {
        if(enableLandingMode){
            byte[] imgArr = new byte[frame.capacity()];
            frame.get(imgArr,0,frame.capacity());
            if (mSurfaceView != null && mSurfaceView.getVisibility()==View.VISIBLE) {
                runDebugMode(imgArr, true);
            } else {
                //without rotate and draw bitmap as well
                runBackgroundMode(imgArr, true);
            }
        }
    }

    public void EnableLandingMode(boolean enableLandingMode) {
        this.enableLandingMode = enableLandingMode;
        if(enableLandingMode){
            mYuv = new Mat((int)(720*1.5), 1280, CvType.CV_8UC1);
            mRgb = new Mat(720 , 960, CvType.CV_8UC3);
            rotateImage = new Mat(960, 720,CvType.CV_8UC3);
            screenBmp = Bitmap.createBitmap(720, 960, Bitmap.Config.RGB_565);
        }else{
            mRgb.release();
            mYuv.release();
            rotateImage.release();
            screenBmp.recycle();
        }
    }

    public void setSurfaceView(android.view.SurfaceView mView){
        mSurfaceView = mView;
        mSurfaceView.getHolder().addCallback(this);
    }


    double real_distance(int d, double highINFO){
        if(highINFO==0){
            highINFO=1;
        }
        double FOV = 90-(camera_FOV/2);
        double tanAngle = Math.tan(FOV * Math.PI / 180.0);
        double distance = Math.abs(highINFO / tanAngle);
        double hypotenuse = (Math.hypot(previewWidth, previewHeight))/2;
        double ratioDistance = distance/hypotenuse;
        double world_distance = d*ratioDistance;

        return world_distance;
    }

    public void setQRLocationListener(QRLocationListener lmQRLocationListener){
        mQRLocationListener = lmQRLocationListener;
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        mSurfaceHolder = holder;
        //landscape
        msMax = 0;
        msMin = 9999;
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
    }


    @Override
    public void onRealAltitudeChange(float realAltitude) {
        //remove outliers
        if(Math.abs(curHeight-realAltitude)<50000){
            Log.d(TAG, "upDateRealHeight = "+realAltitude);
            curHeight = realAltitude;
        }
    }

    public interface QRLocationListener{
        void onQRLocationUpDate(int position_x, int position_y, float rotation, double real_x,
                                double real_y);
    }

    private void runBackgroundMode(byte[] data, boolean isUVC) {
        startTime = System.currentTimeMillis();

        mYuv.put(0, 0, data);


        try {
            Rect roi = new Rect(160, 0, 960, (int)(720*1.5));
            Mat cropped = new Mat(mYuv, roi);
            Imgproc.cvtColor(cropped, mRgb, Imgproc.COLOR_YUV2RGB_NV21);
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }

        String[] locations = null;

        if(!BaseParamTuning)
            locations = QRLocation(mRgb.getNativeObjAddr()); // QR code evaluation
        else
            locations = DistanceMove(mRgb.getNativeObjAddr());

        frameCount++;

        if(locations == null )
        {
            Log.d(TAG, "None....");
            position_x = null;
            position_y = null;
            ra = null;
        }else{
            int x = Integer.valueOf(locations[0]);
            int y = Integer.valueOf(locations[1]);
            float rotation=0;
            if(!BaseParamTuning)
                rotation = Float.valueOf(locations[2]);

//              get real distance
            double meter_x =  real_distance(x, curHeight)*100;
            double meter_y =  real_distance(y, curHeight)*100;


            if (mQRLocationListener!=null){
                mQRLocationListener.onQRLocationUpDate(x,y,rotation,meter_x,meter_y);
            }

        }

        stopTime = System.currentTimeMillis();
        slottime = stopTime - startTime;
        if(slottime > msMax)
            msMax = slottime;
        if(slottime < msMin)
            msMin = slottime;

        double fps = 1000/slottime;

        String t =  "Calculate frame: "+ Integer.toString(frameCount) + " fps = " + Double.toString(fps)
                + "\nNow min= " + String.valueOf(msMin) + "ms, Now Max= " + String.valueOf(msMax) +"ms";
        Log.d(TAG, t);

    }

    private void runDebugMode(byte[] data, boolean isUVC) {
        startTime = System.currentTimeMillis();
        mYuv.put(0, 0, data);
        try {
            Rect roi = new Rect(160, 0, 960, (int)(720*1.5));
            Mat cropped = new Mat(mYuv, roi);
            Imgproc.cvtColor(cropped, mRgb, Imgproc.COLOR_YUV2RGB_NV21);
            //rotate
            Mat dummy = new Mat(960 , 960, CvType.CV_8UC3);
            Point center =new Point(dummy.cols()/2,dummy.rows()/2);
            Mat RotImage;
            Rect roi2;
            if(isUVC){
                RotImage = Imgproc.getRotationMatrix2D(center, 90, scale);
                Imgproc.warpAffine(mRgb, dummy, RotImage, dummy.size(), Imgproc.INTER_LINEAR);
                roi2 = new Rect(0, 0, 720, 960);
            }else{
                RotImage = Imgproc.getRotationMatrix2D(center, 270, scale);
                Imgproc.warpAffine(mRgb, dummy, RotImage, dummy.size(), Imgproc.INTER_LINEAR);
                roi2 = new Rect(240, 0, 720, 960);
            }

            Mat cropped2 = new Mat(dummy, roi2);
            cropped2.copyTo(rotateImage);
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }

//1280
//            Imgproc.cvtColor(mYuv, mRgb, Imgproc.COLOR_YUV2RGB_NV21);
//            try {
//                Mat dummy = new Mat(1280 , 1280, CvType.CV_8UC3);
//                Point center =new Point(dummy.cols()/2,dummy.rows()/2);
//                Mat RotImage = Imgproc.getRotationMatrix2D(center, 270, scale);
//                Imgproc.warpAffine(mRgb, dummy, RotImage, dummy.size(), Imgproc.INTER_CUBIC);
//                Rect roi2 = new Rect(560, 320, 720, 960);
//                Mat cropped2 = new Mat(dummy, roi2);
//                cropped2.copyTo(rotateImage);
//            } catch (Exception e) {
//                e.printStackTrace();
//                return;
//            }

        String[] locations = null;

        if(!BaseParamTuning)
            locations = QRLocation(rotateImage.getNativeObjAddr());
        else
            locations = DistanceMove(rotateImage.getNativeObjAddr());

        // QR code evaluation

        frameCount++;

        if(locations == null )
        {
            Log.d(TAG, "None....");
            position_x = null;
            position_y = null;
            ra = null;
        }else{
            int x = Integer.valueOf(locations[0]);
            int y = Integer.valueOf(locations[1]);
            float rotation=0;
            if(!BaseParamTuning)
                rotation = Float.valueOf(locations[2]);

//              get real distance
            double meter_x =  real_distance(x, curHeight)*100;
            double meter_y =  real_distance(y, curHeight)*100;


            if (mQRLocationListener!=null){
                mQRLocationListener.onQRLocationUpDate(x,y,rotation,meter_x,meter_y);
            }

        }

        //draw back to surface view
        if(mSurfaceView.getVisibility()== View.VISIBLE){
            Utils.matToBitmap(rotateImage, screenBmp);
            deliverAndDrawFrame(screenBmp);
        }



        stopTime = System.currentTimeMillis();
        slottime = stopTime - startTime;
        if(slottime > msMax)
            msMax = slottime;
        if(slottime < msMin)
            msMin = slottime;

        double fps = 1000/slottime;

        String t =  "Calculate frame: "+ Integer.toString(frameCount) + " fps = " + Double.toString(fps)
                + "\nNow min= " + String.valueOf(msMin) + "ms, Now Max= " + String.valueOf(msMax) +"ms";
//            if((position_x != null) && (position_y != null) && (ra != null) )
//                Log.d(TAG, position_x + " " + position_y + " " + ra);
//
        Log.d(TAG, t);

//save debug image to local
//            FileOutputStream fileOutputStream = null;
//            try {
//                fileOutputStream = new FileOutputStream(Environment.getExternalStorageDirectory()
//                        +"/Image.jpg");
//                screenBmp.compress(Bitmap.CompressFormat.JPEG, 50, fileOutputStream);
//            } catch (Exception e) {
//                e.printStackTrace();
//            } finally {
//                try {
//                    fileOutputStream.close();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
    }

    private void deliverAndDrawFrame(Bitmap mCacheBitmap) {
        if (mCacheBitmap != null && mSurfaceHolder!=null)
        {
            Canvas canvas = mSurfaceHolder.lockCanvas();
            if (canvas != null)
            {
                canvas.drawColor(0, android.graphics.PorterDuff.Mode.CLEAR);
                canvas.drawBitmap(
                        mCacheBitmap,
                        null,
                        new android.graphics.Rect(0, 0, canvas.getWidth(), canvas.getHeight()),
                        null);
                mSurfaceHolder.unlockCanvasAndPost(canvas);
            }
        }
    }

}
