package com.moremote.copilot.connection.p2p;

public interface TCPMessageListener {

	public void processMessage(byte[] data, int length);
	
}
