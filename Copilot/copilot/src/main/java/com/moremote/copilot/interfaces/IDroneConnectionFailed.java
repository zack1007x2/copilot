package com.moremote.copilot.interfaces;

public interface IDroneConnectionFailed {

	void onDroneConnectionFailed();

}
