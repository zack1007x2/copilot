package com.moremote.copilot.interfaces;

public interface IDataUpdateByteLog {

	void onDataUpdateByteLog();

}
