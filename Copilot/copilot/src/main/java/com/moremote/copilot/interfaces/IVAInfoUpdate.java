package com.moremote.copilot.interfaces;

/**
 * Created by Zack on 2016/1/18.
 */
public interface IVAInfoUpdate {
    void onVAInfoUpdate(String updateInfo);
}
