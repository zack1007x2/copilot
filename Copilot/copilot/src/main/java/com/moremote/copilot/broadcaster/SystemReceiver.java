package com.moremote.copilot.broadcaster;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.util.Log;

import com.moremote.copilot.activity.LoginActivity;
import com.moremote.copilot.utils.Utils;

/**
 * Created by Zack on 15/10/21.
 */
public class SystemReceiver extends BroadcastReceiver {

    final static String TAG = "SystemReceiver";
    static final String ACTION_BOOT_COMPLETE = "android.intent.action.BOOT_COMPLETED";
    final static String ACTION_RETRY_START_APP = "com.moremote.copilot.retry";
    final static String ACTION_NETWORK_STATE_CHANGE = "android.net.conn.CONNECTIVITY_CHANGE";


    Handler mHandler = new Handler();
    @Override
    public void onReceive(final Context context, Intent arg1) {
        String Action = arg1.getAction();
        Log.w(TAG , "onReceive Intent..." + Action);

        switch(Action){
            case ACTION_BOOT_COMPLETE:
                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent = new Intent();
                        if(Utils.isConnect(context)){
                            Intent intentone = new Intent(context.getApplicationContext(), LoginActivity.class);
                            intentone.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                            context.startActivity(intentone);
                        }else{
                            intent.setAction(ACTION_RETRY_START_APP);
                            context.sendBroadcast(intent);
                        }
                    }
                }, 5000);

                break;
            case ACTION_RETRY_START_APP:
                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent = new Intent();
                        if (Utils.isConnect(context)) {
                            Intent intentone = new Intent(context.getApplicationContext(), LoginActivity.class);
                            intentone.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                            context.startActivity(intentone);
                        } else {
                            intent.setAction(ACTION_RETRY_START_APP);
                            context.sendBroadcast(intent);
                        }
                    }
                }, 3000);
                break;
            case ACTION_NETWORK_STATE_CHANGE:
                Intent intent = new Intent();
                intent.setAction(BroadCastIntent.OnNetworkTypeChange);
                context.sendBroadcast(intent);
                break;
//            case UsbManager.ACTION_USB_DEVICE_ATTACHED:
//                intent.setAction(BroadCastIntent.APP_ON_USB_ATTACHED);
//                context.sendBroadcast(intent);
//                break;
//            case UsbManager.ACTION_USB_DEVICE_DETACHED:
//                intent.setAction(BroadCastIntent.APP_ON_USB_DETACHED);
//                context.sendBroadcast(intent);
//                break;
//            case BroadCastIntent.SYSTEM_ON_NOTIFICATION_CLICK:
//                Log.d(TAG,"receive SYSTEM_ON_NOTIFICATION_CLICK");
//                intent.setAction(BroadCastIntent.APP_ON_NOTIFICATION_CLICK);
//                context.sendBroadcast(intent);
//                break;
        }
    }
}
