package com.moremote.copilot.interfaces;

public interface IDataUpdateStats {

	void onDataUpdateStats();

}
