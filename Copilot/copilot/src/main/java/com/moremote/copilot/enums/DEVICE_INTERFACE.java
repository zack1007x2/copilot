package com.moremote.copilot.enums;

public enum DEVICE_INTERFACE {
	Bluetooth, USB, UART
}
