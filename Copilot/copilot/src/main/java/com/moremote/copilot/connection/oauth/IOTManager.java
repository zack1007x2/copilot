package com.moremote.copilot.connection.oauth;

import android.content.Context;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.moremote.copilot.IPCamApplication;
import com.moremote.copilot.utils.Utils;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

/**
 * Created by gaozongyong on 2015/9/15.
 */
public class IOTManager {

    private static final String TAG = "IOTManager";



//    private static final String apiUrl = "http://192.168.0.85/";
//    private static final String accountUrl = "http://192.168.0.86/";
    private static final String storageOAuthUrl = "http://54.169.169.50/Oauth/oauth/access_token";
    private static final String storageServer = "http://52.77.222.192/";
    private static final String apiUrl = "http://iotapis.moremote.com/";
    private static final String tokenUrl = "http://iotaccounts.moremote.com/token";
//    private static final String tokenUrl = accountUrl+"token";
    private static final String tagUrl = "http://iotapis.moremote.com/tag";
    private static final String relayUrl = "http://iotapis.moremote.com/relay/getuuid";
    private static final String grantType = "password";
    private static final String responseType = "token";
    private static final String clientID = "momomomomotivecam";
    private static final String clientSecret = "c61c6db6775ac7c223c1d7eefc13ffb9";
    private static final String storageID = "MotiveCam";
    private static final String storageSecret = "mo2re0mo1te6";

    private static final String MOREMOTE = "moremote";
    private static final String FACEBOOK = "facebook";
    private static final String GOOGLE = "google";

    public UserAccount userAccount;
    private Context mContext;
    private Handler resultHandler;
    private String currentType;
    private boolean isRegister = false;

    public IOTManager (Context lContext){
        this.mContext = lContext;
        getUserAccount();
    }

    private void getUserAccount(){

        if(userAccount == null) {
            userAccount = new UserAccount(mContext);
        }
        userAccount.readUserData();
    }

    public void register(int type,String name,String email ,String password,String accessToken,Handler callback){
        userAccount.OAuthType = type;
        resultHandler = callback;

        switch (type){
            case UserAccount.OAUTHTYPE.MOREMOTE:
                userAccount.ACCOUNT_NAME = name;
                userAccount.ACCOUNT_EMAIL = email;
                userAccount.ACCOUNT_PASSWORD = password;
                currentType = MOREMOTE;
                iotRegister();
                break;
            case UserAccount.OAUTHTYPE.FACEBOOK:
                userAccount.ACCOUNT_NAME = name;
                userAccount.ACCOUNT_EMAIL = email;
                userAccount.ACCOUNT_TOKEN = accessToken;
                currentType = FACEBOOK;
                iotRegister();
                break;
            case UserAccount.OAUTHTYPE.GOOGLE:
                userAccount.ACCOUNT_NAME = name;
                userAccount.ACCOUNT_EMAIL = email;
                userAccount.ACCOUNT_TOKEN = accessToken;
                currentType = GOOGLE;
                iotRegister();
                break;
            default:
                Message message = new Message();
                message.what = 0;
                resultHandler.sendMessage(message);
                break;
        }
    }

    public void login(int type,String name, String email , String password ,String accessToken, Handler callback){

        userAccount.OAuthType = type;
        resultHandler = callback;

        switch (type){
            case UserAccount.OAUTHTYPE.MOREMOTE:
                userAccount.ACCOUNT_EMAIL = email;
                userAccount.ACCOUNT_PASSWORD = password;
                currentType = MOREMOTE;
                updateToken();
                getStorageToken();
                break;
            case UserAccount.OAUTHTYPE.FACEBOOK:
                userAccount.ACCOUNT_NAME = name;
                userAccount.ACCOUNT_EMAIL = email;
                userAccount.ACCOUNT_TOKEN = accessToken;
                currentType = FACEBOOK;
                iotLogin();
                break;
            case UserAccount.OAUTHTYPE.GOOGLE:
                userAccount.ACCOUNT_NAME = name;
                userAccount.ACCOUNT_EMAIL = email;
                userAccount.ACCOUNT_TOKEN = accessToken;
                currentType = GOOGLE;
                iotLogin();
                break;
            default:
                Message message = new Message();
                message.what = 0;
                resultHandler.sendMessage(message);
                break;
        }



    }

    public void getCameraTag(Handler callback){

        userAccount.readUserData();
        switch (userAccount.OAuthType){
            case UserAccount.OAUTHTYPE.MOREMOTE:
                currentType = MOREMOTE;
                break;
            case UserAccount.OAUTHTYPE.FACEBOOK:
                currentType = FACEBOOK;
                break;
            case UserAccount.OAUTHTYPE.GOOGLE:
                currentType = GOOGLE;
                break;
            default:
                break;
        }

        String url = tagUrl+"?type="+currentType+"&access_token="+userAccount.ACCOUNT_TOKEN;
        new RestfulGet(callback).execute(url);
    }

    public void updateCameraName(String cameraID,String updateName ,Handler callback){

        userAccount.readUserData();
        switch (userAccount.OAuthType){
            case UserAccount.OAUTHTYPE.MOREMOTE:
                currentType = MOREMOTE;
                break;
            case UserAccount.OAUTHTYPE.FACEBOOK:
                currentType = FACEBOOK;
                break;
            case UserAccount.OAUTHTYPE.GOOGLE:
                currentType = GOOGLE;
                break;
            default:
                break;
        }

        final ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("type", currentType));
        params.add(new BasicNameValuePair("access_token", userAccount.ACCOUNT_TOKEN));
        params.add(new BasicNameValuePair("tag_name", cameraID+"::Name"));
        params.add(new BasicNameValuePair("tag_value", updateName));
        new RestfulPost(callback,params).execute(tagUrl);

    }

    public void getRelayUUID() {

        switch (userAccount.OAuthType){
            case UserAccount.OAUTHTYPE.MOREMOTE:
                currentType = MOREMOTE;
                break;
            case UserAccount.OAUTHTYPE.FACEBOOK:
                currentType = FACEBOOK;
                break;
            case UserAccount.OAUTHTYPE.GOOGLE:
                currentType = GOOGLE;
                break;
            default:
                break;
        }

        final ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("type", currentType));
        params.add(new BasicNameValuePair("access_token", userAccount.ACCOUNT_TOKEN));
        params.add(new BasicNameValuePair("node_token", getMacAddress(mContext)));
        new RestfulPost(getUUIDResult,params).execute(relayUrl);
    }

    /*********************************************************************************************************/

    private void iotRegister() {

        String url = apiUrl+"users";
        final ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("type", currentType));
        if(currentType.equals(MOREMOTE)) {
            isRegister = true;
            params.add(new BasicNameValuePair("email", userAccount.ACCOUNT_EMAIL));
            params.add(new BasicNameValuePair("password", userAccount.ACCOUNT_PASSWORD));
            params.add(new BasicNameValuePair("nick_name", userAccount.ACCOUNT_NAME));
        }else{
            params.add(new BasicNameValuePair("access_token", userAccount.ACCOUNT_TOKEN));
        }
        new RestfulPost(registerResult,params).execute(url);
    }

    private void updateToken() {

        final ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("grant_type", grantType));
        params.add(new BasicNameValuePair("client_id",clientID ));
        params.add(new BasicNameValuePair("client_secret",clientSecret));
        params.add(new BasicNameValuePair("username", userAccount.ACCOUNT_EMAIL));
        params.add(new BasicNameValuePair("password", userAccount.ACCOUNT_PASSWORD));
        new RestfulPost(tokenResult,params).execute(tokenUrl);
    }

    private void iotLogin() {
        isRegister = false;
        String url = apiUrl+"users/user?type="+currentType+"&access_token="+userAccount.ACCOUNT_TOKEN;
        new RestfulGet(loginResult).execute(url);
    }

    private void iotGetJid() {

        String xmppUrl = apiUrl+"xmpp/getjid";
        final ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("access_token", userAccount.ACCOUNT_TOKEN));
        params.add(new BasicNameValuePair("type",currentType ));
        if (userAccount.cameraType == IPCamApplication.cameraType.AndroidCam) {
            params.add(new BasicNameValuePair("node_token", getMacAddress(mContext)));

            params.add(new BasicNameValuePair("client_id", "device"));  // app or device
        }
        else {
            params.add(new BasicNameValuePair("access_token", userAccount.ACCOUNT_TOKEN));
            params.add(new BasicNameValuePair("client_id", "app"));  // app or device
        }
        new RestfulPost(xmppResult,params).execute(xmppUrl);
    }

    private void sendVerifyMail() {
        String url = apiUrl+"verifyMail/sendmail?type="+currentType+"&access_token="+userAccount.ACCOUNT_TOKEN;
        new RestfulGet(null).execute(url);
    }

    final private IOTHandler registerResult = new IOTHandler(mContext) {
        @Override
        public void handleMessage(android.os.Message msg) {
            super.handleMessage(msg);

            Bundle bundle = msg.getData();
            String status = bundle.getString("StatusCode");
            String content = bundle.getString("Content");

            if(status.equals("200")){
                if(currentType.equals(MOREMOTE)){
                    updateToken();
                    getStorageToken();
                }else{
                    iotLogin();
                }
            }else if(status.equals("400") && content.equals("user exists")){
                if(currentType.equals(MOREMOTE)){
                    updateToken();
                    getStorageToken();
                }else{
                    iotLogin();
                }
            }else{
               sendResultMsg(0);
            }
        }

    };

    final private IOTHandler tokenResult = new IOTHandler(mContext) {
        @Override
        public void handleMessage(android.os.Message msg) {
            super.handleMessage(msg);

            Bundle bundle = msg.getData();
            String status = bundle.getString("StatusCode");
            String content = bundle.getString("Content");

            if (status.equals("200")) {
                /** Update Token Success , Save User Account Info **/
                try {
                    JSONObject data = new JSONObject(content);
                    Log.d(TAG, "JSON DATA" + data);
                    userAccount.ACCOUNT_TOKEN = data.getString("access_token");
                    userAccount.SaveUserData();

                    if (isRegister) {
                        sendVerifyMail();
                    }
                    iotLogin();

                } catch (JSONException e) {
                    Log.d(TAG, e.toString());
                    sendResultMsg(0);
                }

            } else {
                /** Update Token Fail **/
                 sendResultMsg(0);
            }
        }

    };

    final private IOTHandler loginResult = new IOTHandler(mContext) {
        @Override
        public void handleMessage(android.os.Message msg) {
            super.handleMessage(msg);

            Bundle bundle = msg.getData();
            String status = bundle.getString("StatusCode");
            String content = bundle.getString("Content");

            if (status.equals("200")) {
                try {

                    /** Get Relay UUID & secret **/
                    if (userAccount.cameraType == IPCamApplication.cameraType.AndroidCam) {
                        getRelayUUID();
                    }

                    if(currentType.equals(MOREMOTE)) {
                        JSONObject data = new JSONObject(content);
                        Log.d(TAG, "JSON DATA" + data);
                        userAccount.ACCOUNT_NAME = data.getString("nick_name");
                    }
                    /** Check if XMPP Info Exist **/
                    if(userAccount.XMPP_ACCOUNT == null || userAccount.XMPP_ACCOUNT.isEmpty()){
                        iotGetJid();

                    }else {
                        userAccount.SaveUserData();
                        sendResultMsg(1);
                    }

                } catch (JSONException e) {
                    Log.d(TAG, e.toString());
                    sendResultMsg(0);
                }

            } else {
                /** Update Token Fail **/
                sendResultMsg(0);
            }
        }

    };

    final private IOTHandler xmppResult = new IOTHandler(mContext) {
        @Override
        public void handleMessage(android.os.Message msg) {
            super.handleMessage(msg);

            Bundle bundle = msg.getData();
            String status = bundle.getString("StatusCode");
            String content = bundle.getString("Content");

            if (status.equals("200")) {
                try {
                    JSONObject data = new JSONObject(content);
                    Log.d(TAG, "JSON DATA" + data);
                    userAccount.XMPP_ACCOUNT = data.getString("jid");
                    userAccount.XMPP_PASSWORD = data.getString("password");
                    userAccount.SaveUserData();

                    if (userAccount.cameraType == IPCamApplication.cameraType.AndroidCam) {
                        getCameraTag(getCameraNameResult);
                    }
                    sendResultMsg(1);

                } catch (JSONException e) {
                    Log.d(TAG, e.toString());
                    sendResultMsg(0);
                }

            } else {
                /** Update Token Fail **/
                sendResultMsg(0);
            }
        }

    };

    final private IOTHandler getUUIDResult = new IOTHandler(mContext) {
        @Override
        public void handleMessage(android.os.Message msg) {
            super.handleMessage(msg);

            Bundle bundle = msg.getData();
            String status = bundle.getString("StatusCode");
            String content = bundle.getString("Content");

            if (status.equals("200")) {
                try {
                    JSONObject data = new JSONObject(content);
                    Log.d(TAG, "JSON DATA" + data);
                    userAccount.RELAY_UUID = data.getString("uuid");
                    userAccount.RELAY_KEY = data.getString("key");
                    userAccount.SaveUserData();

                } catch (JSONException e) {
                    Log.d(TAG, e.toString());

                }

            } else {
                /** Update Token Fail **/
            }
        }

    };

    private void sendResultMsg(int resultCode) {
        Message message = new Message();
        message.what = resultCode;
        resultHandler.sendMessage(message);
    }

    public static class IOTHandler extends android.os.Handler {
        protected WeakReference<Context> mContext;

        public IOTHandler(Context lContext) {
            mContext = new WeakReference<Context>(lContext);
        }

        @Override
        public void handleMessage(Message msg) {

        }
    }

    public static String getMacAddress(Context context) {
        WifiManager wifiMan = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        WifiInfo wifiInf = wifiMan.getConnectionInfo();
//        Log.d(TAG, "WIFI MAC = " + getMacAddress(context)+" | eth0 MAC = " + Utils
//        .getMACAddress("eth0") +" | wlan0 MAC = "+Utils.getMACAddress("wlan0")+" | else = " +
//        ""+Utils.getMACAddress(null));
        if(wifiInf.getMacAddress() !=null) {
            return wifiInf.getMacAddress();
        }else if(Utils.getMACAddress("eth0")!=null){
           return Utils.getMACAddress("eth0");
        }else if(Utils.getMACAddress("wlan0")!=null){
            return Utils.getMACAddress("wlan0");
        }else{
            Log.d(TAG, "Can not get mac address");
            return null;
        }


    }

    final private IOTHandler getCameraNameResult = new IOTHandler(mContext) {
        @Override
        public void handleMessage(android.os.Message msg) {
            super.handleMessage(msg);

            Bundle bundle = msg.getData();
            String status = bundle.getString("StatusCode");
            String content = bundle.getString("Content");

            if (status.equals("200")) {
                /** Update Token Success , Save User Account Info **/
                try {
                    JSONObject data = new JSONObject(content);
                    Log.d(TAG, "JSON DATA" + data);
                    if ( data.getString("tags").compareTo("[]") != 0) {
                        JSONObject cameraTags = data.getJSONObject("tags");

                        try {
                            String nameKey = userAccount.XMPP_ACCOUNT + "::Name";
                            String CameraName = cameraTags.getString(nameKey);
                            Log.d(TAG, "JSON DATA CameraName : " + CameraName);
                        } catch (JSONException e) {
                            Log.d(TAG, e.toString());
                            updateCameraName(userAccount.XMPP_ACCOUNT, IPCamApplication.getDeviceName(), updateCameraNameResult);
                        }
                    }
                    else
                    {
                        updateCameraName(userAccount.XMPP_ACCOUNT, IPCamApplication.getDeviceName(), updateCameraNameResult);
                    }
                } catch (JSONException e) {
                    Log.d(TAG, e.toString());
                }
            } else {
                /** Update Token Fail **/
            }
        }

    };

    final private IOTHandler updateCameraNameResult = new IOTHandler(mContext) {
        @Override
        public void handleMessage(android.os.Message msg) {
            super.handleMessage(msg);

            Bundle bundle = msg.getData();
            String status = bundle.getString("StatusCode");
            String content = bundle.getString("Content");

            if (status.equals("200")) {
                /** Update Token Success , Save User Account Info **/
                try {
                    JSONObject data = new JSONObject(content);
                    Log.d(TAG, "JSON DATA" + data);

                } catch (JSONException e) {
                    Log.d(TAG, e.toString());

                }

            } else {
                /** Update Token Fail **/
            }
        }

    };

    /*********************************************************************************************************/

    public void updatePicture(String cameraID,String updateImage ,Handler callback){

        userAccount.readUserData();
        switch (userAccount.OAuthType){
            case UserAccount.OAUTHTYPE.MOREMOTE:
                currentType = MOREMOTE;
                break;
            case UserAccount.OAUTHTYPE.FACEBOOK:
                currentType = FACEBOOK;
                break;
            case UserAccount.OAUTHTYPE.GOOGLE:
                currentType = GOOGLE;
                break;
            default:
                break;
        }

        final ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("type", currentType));
        params.add(new BasicNameValuePair("access_token", userAccount.ACCOUNT_TOKEN));
        params.add(new BasicNameValuePair("tag_name", cameraID+"::Image"));
        params.add(new BasicNameValuePair("tag_value", updateImage));
        new RestfulPost(callback,params).execute(tagUrl);

    }

    /*********************************************************************************************************/

    /** ================ Mostorage Function ================ **/
    /**
     *
     * @param callback
     * @param localFilePath
     * @param saveFileName  XMPPID_yyyyMMddHHmmss_duration(s).mp4
     */
    public void uploadFile(final Handler callback , String localFilePath , String saveFileName) {
        String uploadUrl = storageServer+"files_put/"+saveFileName;
        new RestfulFile(callback, localFilePath , userAccount.STORAGE_TOKEN).execute(uploadUrl);
    }

    /**
     *
     * @param callback
     * @param startTime -- yyyyMMddHHmmss
     * @param endTime -- yyyyMMddHHmmss
     */

    public void getFileList(final Handler callback , String startTime , String endTime ) {

        String listUrl = storageServer+"list?modified_start="+startTime+"&modified_end="+endTime;
        listUrl= listUrl.replaceAll(" ", "%20");
        new RestfulFile(callback, null , userAccount.STORAGE_TOKEN).execute(listUrl);
    }

    public void downloadFile(final Handler callback , String downloadName , String downloadPath) {
        String downloadUrl = storageServer+"files/"+downloadName;
        new RestfulDownload(callback , downloadPath , userAccount.STORAGE_TOKEN).execute(downloadUrl);
    }

    public void getShareLink(final Handler callback , String filePath) {
        String shareUrl = storageServer+"shares/"+filePath;
        new RestfulFile(callback, null , userAccount.STORAGE_TOKEN).execute(shareUrl);
    }


    /*********************************************************************************************************/


    private void getStorageToken() {
        final ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("grant_type", grantType));
        params.add(new BasicNameValuePair("client_id",storageID ));
        params.add(new BasicNameValuePair("client_secret",storageSecret));
        params.add(new BasicNameValuePair("username", userAccount.ACCOUNT_EMAIL));
        params.add(new BasicNameValuePair("password", userAccount.ACCOUNT_PASSWORD));
        params.add(new BasicNameValuePair("response_type", responseType ));
        new RestfulPost(storageTokenResult,params).execute(storageOAuthUrl);
//        new RestfulPost(storageTokenResult,params).execute(tokenUrl);
    }

    final private IOTHandler storageTokenResult = new IOTHandler(mContext) {
        @Override
        public void handleMessage(android.os.Message msg) {
            super.handleMessage(msg);

            Bundle bundle = msg.getData();
            String status = bundle.getString("StatusCode");
            String content = bundle.getString("Content");

            if (status.equals("200")) {
                /** Update MoStorage Token Success  **/
                try {
                    JSONObject data = new JSONObject(content);
                    userAccount.STORAGE_TOKEN = data.getString("access_token");
                    userAccount.SaveUserData();
                    Log.d(TAG, "JSON DATA Storage Token" + data);

                } catch (JSONException e) {
                    Log.d(TAG, e.toString());
//                    sendResultMsg(0);
                }

            } else {
                /** Update Token Fail **/
//                Log.d(TAG, "JSON DATA Storage Token Fail");
//                sendResultMsg(0);
            }
        }

    };
}
