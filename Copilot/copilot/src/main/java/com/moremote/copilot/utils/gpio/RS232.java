package com.moremote.copilot.utils.gpio;

import android.content.Context;
import android.hardware.usb.UsbManager;
import android.util.Log;
import android.widget.Toast;

import tw.com.prolific.driver.pl2303.PL2303Driver;

/**
 * Created by gaozongyong on 2016/4/25.
 */
public class RS232 {

    private static final String TAG = "RS232";
    // 19200, 8N 1
    private PL2303Driver.BaudRate mBaudrate = PL2303Driver.BaudRate.B19200;
    private PL2303Driver.DataBits mDataBits = PL2303Driver.DataBits.D8;
    private PL2303Driver.Parity mParity = PL2303Driver.Parity.NONE;
    private PL2303Driver.StopBits mStopBits = PL2303Driver.StopBits.S1;
    private PL2303Driver.FlowControl mFlowControl = PL2303Driver.FlowControl.OFF;
    private PL2303Driver mSerial;

    private static final String ACTION_USB_PERMISSION = "com.prolific.pl2303hxdsimpletest.USB_PERMISSION";

    public RS232(Context context) {
        mSerial = new PL2303Driver((UsbManager) context.getSystemService(Context.USB_SERVICE),
                context, ACTION_USB_PERMISSION);
        if (!mSerial.PL2303USBFeatureSupported()) {

            Toast.makeText(context, "No Support USB host API", Toast.LENGTH_SHORT)
                    .show();

            Log.d(TAG, "No Support USB host API");

            mSerial = null;

        }

        if(!mSerial.isConnected()) {
            if( !mSerial.enumerate() ) {
                Toast.makeText(context, "no more devices found", Toast.LENGTH_SHORT).show();
            } else {
                Log.d(TAG, "onResume:enumerate succeeded!");
            }
        }
    }

    public boolean checkFeatureSupport(){
        return mSerial.PL2303USBFeatureSupported();
    }

    public boolean enumerateSerial() {
        if(!mSerial.isConnected()) {
            return mSerial.enumerate();

        }
        return true;
    }

    public boolean openUsbSerial() {

        try {
            if (mSerial == null) {
                return false;
            }

            if (mSerial.isConnected()) {
                // Try initial USB Serial
                if (!mSerial.InitByBaudRate(mBaudrate, 700)) {
                    return false;
                }
                mSerial.setup(mBaudrate, mDataBits, mStopBits, mParity, mFlowControl);
            }else {
                return false;
            }
        }catch (Exception e) {
            return false;
        }

        return true;
    }

    public String readDataFromSerial() {

        int len;
        byte[] rbuf = new byte[4096];
        StringBuffer sbHex = new StringBuffer();

        if(mSerial == null || !mSerial.isConnected()) {
            return "";
        }

        len = mSerial.read(rbuf);

        if( len < 0){
            // Read Data Error
            return "";
        }else if( len <= 0) {
            // Read Empty Data
            return "";
        }else {

            for (int j = 0; j < len; j++) {
                sbHex.append((char) (rbuf[j]&0x000000FF));
            }
            return sbHex.toString();
        }

    }

    public void writeDataToSerial(byte[] data) {

        if(mSerial == null || !mSerial.isConnected()) {
            Log.d(TAG , "Error ");
            return ;
        }

        int ret = mSerial.write(data, data.length);
        Log.d(TAG , "Send Length : " + ret);
        if( ret<0 ) {
            Log.d(TAG, "setup2: fail to controlTransfer: " + ret);
        }
    }

    public boolean isConnect(){
        return mSerial.isConnected();
    }

}
