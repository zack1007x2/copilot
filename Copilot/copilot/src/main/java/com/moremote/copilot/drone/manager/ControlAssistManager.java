package com.moremote.copilot.drone.manager;

import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.MAVLink.common.msg_moremote_cmd;
import com.moremote.copilot.IPCamApplication;
import com.moremote.copilot.drone.mavlink.MavCmdList2Drone;
import com.moremote.copilot.enums.DRONE_MODE;
import com.moremote.copilot.enums.MSG_SOURCE;
import com.moremote.copilot.interfaces.IDroneStateChangeListener;
import com.moremote.copilot.interfaces.IVAInfoUpdate;
import com.moremote.copilot.queue.items.ItemMavLinkMsg;

/**
 * Created by Zack on 2016/1/18.
 */
public class ControlAssistManager implements IdentifyManager.QRLocationListener,IDroneStateChangeListener {

    private final static String TAG = ControlAssistManager.class.getSimpleName();
    private IPCamApplication hub;
    private int prev_abs_x, prev_abs_y;
    private int BASE_CH1, BASE_CH2;
    private static final double FACTOR = 3;
    private static final double FIX_FACTOR = 5;
    private static final short MAX_LIMIT = 80;
    private static final short MIN_LIMIT = 50;
    private IVAInfoUpdate mVAInfoUpdateListener;
    private boolean TakeOverControl = true;
    private long CurMode;
    private long LastCmdUpdateMilliSec = -1;

    private static final int TIMER_CHECK = 101;
    private static final int TEST_MOREMOTE_CMD = 102;

    private boolean SwitchCmd;

    private Handler mHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {

            Double LeftCmd = -10d, RightCmd = 10d;
            Double CmdSent;
            super.handleMessage(msg);
            switch (msg.what) {
                case TIMER_CHECK:
                    if (LastCmdUpdateMilliSec > 0 && TakeOverControl) {
                        if ((System.currentTimeMillis() - LastCmdUpdateMilliSec) > 500) {
                            //Stop moving...
                            short base_1, base_2;
                            if(BASE_CH1>0)
                                base_1 = MIN_LIMIT;
                            else
                                base_1 = -MIN_LIMIT;

                            if(BASE_CH2>0)
                                base_2 = MIN_LIMIT;
                            else
                                base_2 = -MIN_LIMIT;

                            MavCmdList2Drone.sentRCoverride(1500 + base_1, 1500 + base_2, 1500, 1500);
                            Log.e(TAG, ">>>>>>>> LOST QR <<<<<<<<\n前後 = " + (1500 + base_2) + " 左右 = " +
                                    (1500 + base_1) + " 升力 = " + 1500 + " 自轉 = " + 1500);

                            if (mVAInfoUpdateListener != null) {
                                mVAInfoUpdateListener.onVAInfoUpdate("Lost QR");
                            }
                        }
                    }else{
                        SwitchCmd=!SwitchCmd;
                    }
                    this.sendEmptyMessageDelayed(TIMER_CHECK, 500);
                    break;
                case TEST_MOREMOTE_CMD:
                    if (SwitchCmd)
                        CmdSent = RightCmd;
                    else
                        CmdSent = LeftCmd;
                    msg_moremote_cmd moremote_cmd = new msg_moremote_cmd();
                    moremote_cmd.param1 = Float.valueOf(Double.toString(CmdSent));
                    moremote_cmd.param2 = Float.valueOf(Double.toString(0));
                    moremote_cmd.command = 2;
                    moremote_cmd.target_system = 1;
                    moremote_cmd.target_component = 1;
                    moremote_cmd.confirmation = 0;
                    IPCamApplication.queue.addHubQueueItem(new ItemMavLinkMsg(moremote_cmd.pack(), MSG_SOURCE.FROM_GS, 0));
                    this.sendEmptyMessageDelayed(TEST_MOREMOTE_CMD, 2);
                    Log.d(TAG, "=====" + CmdSent);
//                    if(CmdSent<=-10)
//                        SwitchCmd = false;
//                    if(CmdSent>=10)
//                        SwitchCmd = true;
//
//                    if(SwitchCmd){
//                        CmdSent-=10;
//                    }else{
//                        CmdSent+=10;
//                    }
                    break;
            }
        }
    };

    public ControlAssistManager(IPCamApplication context) {
        hub = context;
        mHandler.sendEmptyMessageDelayed(TIMER_CHECK, 1000);
//        mHandler.sendEmptyMessage(TEST_MOREMOTE_CMD);
    }


    @Override
    public void onQRLocationUpDate(int position_x, int position_y, float rotation, double real_x, double real_y) {

        if(LastCmdUpdateMilliSec<0){
            mHandler.sendEmptyMessageDelayed(TIMER_CHECK, 500);
        }

        if (TakeOverControl) {
            String LogStr = "";
            if (real_x != 0 && real_y != 0) {
                int abs_x = Math.abs(position_x);
                double abs_real_x = Math.abs(real_x);
                int move_x = (int) (abs_real_x * FACTOR);
                if (move_x < MIN_LIMIT) {
                    move_x = MIN_LIMIT;
                } else if (move_x > MAX_LIMIT) {
                    move_x = MAX_LIMIT;
                }
                int final_x = 1500;

                if (position_x > 0) {
                    LogStr += "請往右方移動: " + Integer.toString(Math.abs(position_x)) + " 單位" + " / " + Double
                            .toString(Math.abs(real_x)) + " 公分";
                    if (BASE_CH1 < 0)
                        BASE_CH1 = 0;
                    if (((abs_x - prev_abs_x) > 10 || abs_x > 300) && BASE_CH1<30)
                        BASE_CH1 += FIX_FACTOR;
                    final_x = 1500 + move_x + BASE_CH1;
                    if (final_x > (1500 + MAX_LIMIT)) {
                        final_x = 1500 + MAX_LIMIT;
                    }else if(final_x < (1500 + MIN_LIMIT)){
                        final_x = 1500+MIN_LIMIT;
                    }
                } else if (position_x < 0) {
                    LogStr += "請往左方移動: " + Integer.toString(Math.abs(position_x)) + " 單位" + " / " + Double
                            .toString(Math.abs(real_x)) + " 公分";
                    if (BASE_CH1 > 0)
                        BASE_CH1 = 0;
                    if (((abs_x - prev_abs_x) > 10 || abs_x > 300) && BASE_CH1>-30)
                        BASE_CH1 -= FIX_FACTOR;
                    final_x = 1500 - move_x + BASE_CH1;
                    if (final_x < (1500 - MAX_LIMIT)) {
                        final_x = 1500 - MAX_LIMIT;
                    }else if(final_x > (1500 - MIN_LIMIT)){
                        final_x = 1500 - MIN_LIMIT;
                    }
                }

                int abs_y = Math.abs(position_y);
                double abs_real_y = Math.abs(real_y);
                int move_y = (int) (abs_real_y * FACTOR);
                if (move_y < MIN_LIMIT) {
                    move_y = MIN_LIMIT;
                } else if (move_y > MAX_LIMIT) {
                    move_y = MAX_LIMIT;
                }
                int final_y=1500;

                if (position_y < 0) {
                    LogStr += "\n請往後方移動: " + Integer.toString(Math.abs(position_y)) + " 單位" + " / " + Double
                            .toString(Math.abs(real_y)) + " 公分";
                    if (BASE_CH2 < 0)
                        BASE_CH2 = 0;
                    if (((abs_y - prev_abs_y) > 10 || abs_y > 50) && BASE_CH2<30)
                        BASE_CH2 += FIX_FACTOR;
                    final_y = 1500 + move_y + BASE_CH2;
                    if (final_y > (1500 + MAX_LIMIT)) {
                        final_y = 1500 + MAX_LIMIT;
                    }else if(final_y < (1500 + MIN_LIMIT)){
                        final_y = 1500 + MIN_LIMIT;
                    }
                } else if (position_y > 0) {
                    LogStr += "\n請往前方移動: " + Integer.toString(Math.abs(position_y)) + " 單位" + " / " + Double
                            .toString(Math.abs(real_y)) + " 公分";
                    if (BASE_CH2 > 0)
                        BASE_CH2 = 0;
                    if (((abs_y - prev_abs_y) > 10 || abs_y > 50) && BASE_CH2>-30)
                        BASE_CH2 -= FIX_FACTOR;

                    final_y = 1500 - move_y + BASE_CH2;
                    if (final_y < (1500 - MAX_LIMIT)) {
                        final_y = 1500 - MAX_LIMIT;
                    }else if(final_y > (1500 - MIN_LIMIT)){
                        final_y = 1500 - MIN_LIMIT;
                    }
                }

                int final_rotate = 1500;
                if (rotation <= 0 && rotation >= -135) {
                    LogStr += "\n請向右偏移 " + String.valueOf(Math.abs(rotation - 45)) + "度";
                    final_rotate = 1500 + (Math.round(Math.abs(rotation - 45))) + MIN_LIMIT;
                } else if (rotation < -135 && rotation > -180) {
                    LogStr += "\n請向左偏移 " + String.valueOf(Math.abs((rotation + 180) + 135)) + "度";
                    final_rotate = 1500 - (Math.round(Math.abs((rotation + 180) + 135))) - MIN_LIMIT;
                } else if (rotation > 0 && rotation <= 45) {
                    LogStr += "\n請向右偏移 " + String.valueOf(Math.abs(rotation - 45)) + "度";
                    final_rotate = 1500 + (Math.round(Math.abs(rotation - 45))) + MIN_LIMIT;
                } else if (rotation > 45 && rotation <= 180) {
                    LogStr += "\n請向左偏移 " + String.valueOf((rotation - 45)) + "度";
                    final_rotate = 1500 - (Math.round(Math.abs(rotation - 45))) - MIN_LIMIT;
                }

                MavCmdList2Drone.sentRCoverride(final_x, final_y, 1500, final_rotate);

                Log.w(TAG, "move_x = " + move_x + " | BASE_CH1 = " + BASE_CH1 + " | move_y " + move_y +
                        " | BASE_CH2 = " + BASE_CH2);

                Log.i(TAG, "前後 = " + final_y + " 左右 = " + final_x + " 升力 = " + 1500 + " 自轉 = " + final_rotate);
                Log.d(TAG, LogStr);
                prev_abs_x = abs_x;
                prev_abs_y = abs_y;
            } else {
                //not receive real x, y or real x, y=0
                MavCmdList2Drone.sentRCoverride(1500, 1500, 1500, 1500);
            }
            if (mVAInfoUpdateListener != null) {
                mVAInfoUpdateListener.onVAInfoUpdate(LogStr);
            }
        } else {
            Log.w(TAG, "msg_moremote_cmd >>>> X_dis : " + real_x + " | Y_dis : " + real_y);
            if (real_x != 0 && real_y != 0) {

                String LogStr = "";
                LogStr += "X_val = " + real_x + " \nX_pix = " + position_x + " \n Y_val = " + real_y + " " +
                        " \nY_pix = " + position_y;

                if (real_x > 0) {
                    LogStr += "\n請往東方移動: " + Double.toString(Math.abs(real_x)) + " 公分";
                } else if (real_x < 0) {
                    LogStr += "\n請往西方移動: " + Double.toString(Math.abs(real_x)) + " 公分";
                }
                if (real_y < 0) {
                    LogStr += "\n請往南方移動: " + Double.toString(Math.abs(real_y)) + " 公分";
                } else if (real_y > 0) {
                    LogStr += "\n請往北方移動: " + Double.toString(Math.abs(real_y)) + " 公分";
                }

                int final_rotate = 1500;
                if (rotation <= 0 && rotation >= -135) {
                    LogStr += "\n請向右偏移 " + String.valueOf(Math.abs(rotation - 45)) + "度";
                    final_rotate = 1500 + (Math.round(Math.abs(rotation - 45))) + MIN_LIMIT;
                } else if (rotation < -135 && rotation > -180) {
                    LogStr += "\n請向左偏移 " + String.valueOf(Math.abs((rotation + 180) + 135)) + "度";
                    final_rotate = 1500 - (Math.round(Math.abs((rotation + 180) + 135))) - MIN_LIMIT;
                } else if (rotation > 0 && rotation <= 45) {
                    LogStr += "\n請向右偏移 " + String.valueOf(Math.abs(rotation - 45)) + "度";
                    final_rotate = 1500 + (Math.round(Math.abs(rotation - 45))) + MIN_LIMIT;
                } else if (rotation > 45 && rotation <= 180) {
                    LogStr += "\n請向左偏移 " + String.valueOf((rotation - 45)) + "度";
                    final_rotate = 1500 - (Math.round(Math.abs(rotation - 45))) - MIN_LIMIT;
                }

                MavCmdList2Drone.sentMoremoteCmd(real_x, real_y, final_rotate);

                if (mVAInfoUpdateListener != null) {
                    mVAInfoUpdateListener.onVAInfoUpdate(LogStr);
                }
                Log.d(TAG, LogStr);
            }
        }

        LastCmdUpdateMilliSec = System.currentTimeMillis();

    }

    public void setVAInfoUpdateListener(IVAInfoUpdate VAInfoUpdateListener) {
        mVAInfoUpdateListener = VAInfoUpdateListener;
    }


    @Override
    public void onArmedChange(boolean isArmed) {

    }

    @Override
    public void onConnectionChange(boolean isConnected) {

    }

    @Override
    public void onFlyingStateChange(boolean isFlying) {
        if(!isFlying && CurMode == DRONE_MODE.MODE_LAND){
            mHandler.removeMessages(TIMER_CHECK);
            MavCmdList2Drone.sentRCoverride();
            Log.w(TAG, "release RC");
            if (mVAInfoUpdateListener != null) {
                mVAInfoUpdateListener.onVAInfoUpdate("release RC");
            }
        }
    }

    @Override
    public void onModeChange(long mode) {
        CurMode = mode;
    }
}
