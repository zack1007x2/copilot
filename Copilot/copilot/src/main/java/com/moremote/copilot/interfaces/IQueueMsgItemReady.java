package com.moremote.copilot.interfaces;


import com.moremote.copilot.queue.items.ItemMavLinkMsg;

public interface IQueueMsgItemReady {

	void onQueueMsgItemReady(ItemMavLinkMsg msgItem);

}
