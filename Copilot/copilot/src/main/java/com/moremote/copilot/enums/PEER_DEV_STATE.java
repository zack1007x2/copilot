package com.moremote.copilot.enums;

public enum PEER_DEV_STATE {
	DEV_STATE_DISCONNECTED, DEV_STATE_CONNECTED, DEV_STATE_UNKNOWN
}
