package com.moremote.copilot.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.util.Log;

import com.facebook.login.LoginManager;
import com.moremote.copilot.IPCamApplication;
import com.moremote.copilot.R;
import com.moremote.copilot.connection.oauth.UserAccount;
import com.moremote.copilot.enums.COPILOT_PREFS;

import java.util.HashSet;

public class SettingFragment extends PreferenceFragment implements SharedPreferences.OnSharedPreferenceChangeListener {

	final static String TAG = SettingFragment.class.getSimpleName();

	protected IPCamApplication hub;

	/**
	 * Keep track of which preferences' summary need to be updated.
	 */
	private final HashSet<String> mDefaultSummaryPrefs = new HashSet<String>();

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		initSummaryPerPrefs();
		hub = (IPCamApplication) getActivity().getApplication();
		// Load the preferences from an XML resource
		addPreferencesFromResource(R.xml.preferences);
		// update the summary for the preferences in the mDefaultSummaryPrefs hash table.
		for (String prefKey : mDefaultSummaryPrefs) {
			final Preference pref = findPreference(prefKey);
			if (pref != null) {
				pref.setSummary(hub.prefs.getString(prefKey, ""));
			}
		}

		EditTextPreference logFolderPref = (EditTextPreference) findPreference("pref_log_mavlink_folder");
		logFolderPref.setSummary(hub.getExternalFilesDir(null).getAbsolutePath());

		Preference button = findPreference("pref_action_logout");
		button.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
			@Override
			public boolean onPreferenceClick(Preference preference) {
				showLogoutAlert();
				return true;
			}
		});
	}

	@Override
	public void onResume() {
		super.onResume();
		getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
	}

	@Override
	public void onPause() {
		super.onPause();
		getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
	}

	/**
	 * Method to show alert dialog
	 * */
	private void showLogoutAlert() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this.getActivity());
		builder.setMessage(R.string.settings_logout_alert_message).setTitle(R.string.logout)
				//.setCancelable(true)
				.setNegativeButton(R.string.settings_button_cancel, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// do nothing
					}
				})
				.setPositiveButton(R.string.settings_button_ok, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						logout();
					}
				});
		AlertDialog alert = builder.create();
		alert.show();
	}

	private void logout() {
		UserAccount acc = new UserAccount(this.getActivity());
		if (acc.OAuthType == UserAccount.OAUTHTYPE.FACEBOOK) {
			LoginManager.getInstance().logOut();
		}
		acc.OAuthType = UserAccount.OAUTHTYPE.LOGOUT;
		acc.ACCOUNT_NAME = "";
		acc.ACCOUNT_EMAIL = "";
		acc.ACCOUNT_PASSWORD = "";
		acc.ACCOUNT_TOKEN = "";
		acc.XMPP_PASSWORD = "";
		acc.XMPP_ACCOUNT = "";
		acc.RELAY_UUID = "";
		acc.RELAY_KEY = "";
		acc.SaveUserData();

		Intent intent = new Intent();
		Bundle bundle = new Bundle();
		bundle.putBoolean("Logout", true);
		intent.putExtras(bundle);
		getActivity().setResult(Activity.RESULT_OK, intent);
		this.getActivity().finish();
	}

	@Override
	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
		final Preference preference = findPreference(key);
		if(key.equals(COPILOT_PREFS.PREF_MAV_BYTE_LOG_OUTPUT)){
			if(hub.prefs.getBoolean(COPILOT_PREFS.PREF_MAV_BYTE_LOG_OUTPUT,false)){
				IPCamApplication.logger.restartByteLog();
			}else{
				IPCamApplication.logger.stopByteLog();
			}
		}

		if(key.equals(COPILOT_PREFS.PREF_MAV_SYS_LOG_OUTPUT)){
			if(hub.prefs.getBoolean(COPILOT_PREFS.PREF_MAV_SYS_LOG_OUTPUT,false)){
				IPCamApplication.logger.restartSysLog();
			}else{
				IPCamApplication.logger.stopSysLog();
			}
		}
		if(key.equals(COPILOT_PREFS.PREF_UVC_CAMERA)){
			//if disable switch to phone cam
			if(!hub.prefs.getBoolean(COPILOT_PREFS.PREF_UVC_CAMERA, false))
				IPCamApplication.mService.onUVCEnableChange();
		}
		if(key.equals(COPILOT_PREFS.PREF_ENABLE_VA_LANDING)){
			IPCamApplication.mService.onEnableLandingChange();
		}


		if (preference == null) {
			return;
		}

		if (mDefaultSummaryPrefs.contains(key)) {
			Log.d(TAG, key);
			preference.setSummary(sharedPreferences.getString(key, ""));
		}
	}

	private void initSummaryPerPrefs() {
		mDefaultSummaryPrefs.clear();
		mDefaultSummaryPrefs.add(COPILOT_PREFS.PREF_USB_BAUD);
	}
}
