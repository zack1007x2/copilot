// $codepro.audit.disable com.instantiations.assist.eclipse.analysis.audit.rule.effectivejava.alwaysOverridetoString.alwaysOverrideToString
package com.moremote.copilot.queue.hub;


import com.moremote.copilot.IPCamApplication;
import com.moremote.copilot.queue.QueueMsgItems;

public class HUBQueue extends QueueMsgItems {

	// MAVLink class fields names holder/object
	public MAVLinkCollector msgCollector;
	public MAVLinkDistributor msgDistributor;

	public HUBQueue(IPCamApplication hubContext, int capacity) {
		super(hubContext, capacity);
		msgCollector = new MAVLinkCollector(hubContext);
		msgDistributor = new MAVLinkDistributor(hubContext);

	}

	public void stopQueue() {
//		msgCollector.stopMAVLinkParserThread();
//		msgDistributor.stopMAVLinkDistThread();
	}

	public void startQueue() {
//		msgCollector.startMAVLinkParserThread();
//		msgDistributor.startMAVLinkDistThread();
	}

}
