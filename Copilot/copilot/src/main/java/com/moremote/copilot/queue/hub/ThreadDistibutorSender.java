package com.moremote.copilot.queue.hub;

import android.util.Log;

import com.moremote.copilot.IPCamApplication;
import com.moremote.copilot.enums.APP_STATE;
import com.moremote.copilot.queue.items.ItemMavLinkMsg;

import java.io.IOException;

public class ThreadDistibutorSender extends Thread {

	private static final String TAG = ThreadDistibutorSender.class.getSimpleName();

	private boolean running = true;

	private final IPCamApplication hub;

	public ThreadDistibutorSender(IPCamApplication hubContext) {

		hub = hubContext;

		running = true;

	}

	public void run() {

		ItemMavLinkMsg tmpItem = null;

		IPCamApplication.logger.sysLog("MavLink Distributor", "Distributor Start..");

		while (running) {

			try {

				tmpItem = IPCamApplication.queue.getHubQueueItem();


				if (null != tmpItem) {
					switch (tmpItem.direction) {
						case FROM_DRONE:
							if (hub.gcsServer.isClientConnected()) {
								if (hub.gcsServer.writeBytes(tmpItem.getPacketBytes())) {
									Log.w(TAG, "Packet sent to gs :"+tmpItem.toString());
//								hub.logger.sysLog("Packet sent to gs :"+tmpItem.toString());
								}
								else {
									Log.d(TAG, "gcsServer: No Client connected failure");
								}
							}
							else {
								Log.d(TAG,
										"gcsServer: Packet Silently discarded");
							}
							break;
						case FROM_GS:
//							IPCamApplication.logger.byteLog(MSG_SOURCE.FROM_GS,tmpItem.getPacketBytes());
//							Log.d("Zack","[byteLog] "+ Arrays.toString(tmpItem.getPacketBytes()));
							if (hub.droneClient.isConnected()) {
								if (hub.droneClient.writeBytes(tmpItem.getPacketBytes())) {
									Log.d(TAG, "Packet sent to drone :"+tmpItem.toString());
//								hub.logger.sysLog("Packet sent to drone :" + tmpItem.toString());
								}
								else {
									Log.d(TAG, "droneClient: Not connected.");
								}
							}
							else {
								// Log.d(TAG,
								// "droneClient: Packet Silently discarded.");
							}
							break;
						default:
							break;

					}

					// Store queue items count left after last read but
					// only if it changed - for UI update -
					if (IPCamApplication.logger.hubStats.getQueueItemsCnt() != IPCamApplication.queue.getItemCount()) {
						IPCamApplication.logger.hubStats.setQueueItemsCnt(IPCamApplication.queue.getItemCount());
						IPCamApplication.sendAppMsg(APP_STATE.MSG_DATA_UPDATE_STATS);
					}

				}else{
					try {
//						Log.v("Zack","ThreadDistibutorSender sleep");
						Thread.sleep(20);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
			catch (IOException e) {
				Log.e(TAG, "gcsServer: Socket  write exception:" + e.getMessage());
				e.printStackTrace();

			}
		}
		IPCamApplication.logger.sysLog("MavLink Distributor", "Distributor ..Stop");
	}

	public void stopMe() {
		running = false;
	}

}