package com.moremote.copilot.media.uploadfile;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.moremote.copilot.IPCamApplication;
import com.moremote.copilot.connection.oauth.IOTManager;
import com.moremote.copilot.connection.oauth.RestfulFile;
import com.moremote.copilot.database.item.UploadFile;
import com.moremote.copilot.interfaces.IDroneStateChangeListener;
import com.moremote.copilot.service.MainService;
import com.moremote.copilot.utils.Utils;

import java.io.File;
import java.util.ArrayDeque;

/**
 * Created by Zack on 2016/1/28.
 */
public class UploadManager implements IDroneStateChangeListener{

    private static final String TAG = UploadManager.class.getSimpleName();
    private ArrayDeque<UploadFile> waitList = new ArrayDeque<>();
    private Context mContext;
    private IOTManager mIotManager;
    private UploadFile CurFile;
    private boolean allowUpload;

    private static final int TRY_AGAIN_LATER = 1112;
    private static final int NO_INTERNET = 1113;
    private static final int RETRY_UPLOAD= 1114;
    private Handler mHandler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch(msg.what){
                case RestfulFile.UPLOAD_VIDEO_WHAT:
                    Bundle bundle = msg.getData();
                    if( bundle.getString(RestfulFile.key_status_code,"").equals("200")){
                        //upload success
                        File f = new File(CurFile.getFileLocation());
                        if(f.exists()){
                            f.delete();
                        }
                        CurFile = null;

                        if(allowUpload){
                            if(!waitList.isEmpty())
                                doUpload(waitList.pollFirst());
                            else
                                sendEmptyMessageDelayed(RETRY_UPLOAD, 30*1000);
                        }
                    }else{
                        sendEmptyMessageDelayed(TRY_AGAIN_LATER, 30*1000);
                    }
                    Log.d(TAG, "UPLOAD CALLBACK : " + bundle.getString(RestfulFile.key_status_code));
                    break;
                case TRY_AGAIN_LATER:
                    Log.e(TAG, "TRY_AGAIN_LATER");
                    doUpload(CurFile);
                    break;
                case NO_INTERNET:

                    break;
                case RETRY_UPLOAD:
                    Log.e(TAG, "RETRY_UPLOAD");
                    startUpload();
                    break;
            }



        }
    };

    public UploadManager(Context context,IOTManager iotManager) {
        mContext = context;
        mIotManager = iotManager;
    }

    public void startUpload(){
        allowUpload = true;
        initWaitingList();
        UploadFile file = waitList.pollFirst();
        if(file!=null)
            doUpload(file);
        else
            mHandler.sendEmptyMessageDelayed(RETRY_UPLOAD, 30 * 1000);
    }

    public void stopUpload(){
        allowUpload = false;
        mHandler.removeMessages(RestfulFile.UPLOAD_VIDEO_WHAT);
        mHandler.removeMessages(TRY_AGAIN_LATER);
        mHandler.removeMessages(RETRY_UPLOAD);
    }

    private void doUpload(UploadFile uploadFile){
        if(((MainService)mContext).getCurrentFilePath()==null || !((MainService)mContext).getCurrentFilePath().equals(uploadFile.getFileLocation())){

            File file = new File(uploadFile.getFileLocation());
            if(file.exists()){
                Log.d(TAG, "doUpload >>>>"+file.toString());
                long aa = Utils.getVideoDuration(mContext, file);
                String uploadfile = mIotManager.userAccount.XMPP_ACCOUNT.split("@")[0]+"_"+uploadFile.getmLocalFileName().substring(6, 25).replace("-","")+"_"+aa/1000+".mp4";
                Log.d(TAG,uploadfile);
                mIotManager.uploadFile(mHandler, file.toString(), uploadfile);
                CurFile = uploadFile;
            }
        }
    }

    private void initWaitingList(){
        File folder = new File(IPCamApplication.LOCAL_STORAGE_PATH);
        File file[] = folder.listFiles();
        Log.d(TAG, "List Size: " + file.length);
        for (int i=0; i < file.length; i++){
            Log.d(TAG, "FileName:" + file[i].getName());
            Log.d(TAG, "Cur file Name: " + ((MainService) mContext).getCurrentFilePath());
            if(!file[i].getName().equals(((MainService)mContext).getCurrentFilePath())){
                File curFile = new File(IPCamApplication.LOCAL_STORAGE_PATH+file[i].getName());
                long duration = Utils.getVideoDuration(mContext, curFile);
                UploadFile uFile = new UploadFile();
                uFile.setmLocalFileName(file[i].getName());
                uFile.setFileLocation(IPCamApplication.LOCAL_STORAGE_PATH + file[i].getName());
                uFile.setDuration(duration);
                uFile.setYyyyMMddhhmmss(file[i].getName().substring(6, 25).replace("-", ""));
                uFile.setHasUpload(false);
                String xmppId = mIotManager.userAccount.XMPP_ACCOUNT.split("@")[0];
                uFile.setmUploadFileName(xmppId + "_" + uFile.getYyyyMMddhhmmss() + "_" + uFile.getDuration());
                if(uFile.getDuration()!=0){
                    Log.w(TAG, "add list : "+uFile.getmLocalFileName());
                    waitList.addLast(uFile);
                }
            }
        }
    }

    @Override
    public void onArmedChange(boolean isArmed) {
        if(isArmed)
            startUpload();
    }

    @Override
    public void onConnectionChange(boolean isConnected) {

    }

    @Override
    public void onFlyingStateChange(boolean isFlying) {
        if(isFlying)
            stopUpload();
    }

    @Override
    public void onModeChange(long mode) {

    }
}
