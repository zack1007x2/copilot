package com.moremote.copilot.enums;

public enum SCREEN_SIZE {
	// order matter for comparisons !!!
	UNDEF, SMALL, NORMAL, LARGE, XLARGE
}
