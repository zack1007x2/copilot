package com.moremote.copilot.service;

import android.annotation.SuppressLint;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.PixelFormat;
import android.hardware.Camera;
import android.hardware.usb.UsbDevice;
import android.media.MediaPlayer;
import android.net.wifi.WifiManager;
import android.os.Binder;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.SurfaceHolder;
import android.view.WindowManager;
import android.widget.Toast;

import com.moremote.copilot.IPCamApplication;
import com.moremote.copilot.R;
import com.moremote.copilot.connection.ipcam.ConnectedList;
import com.moremote.copilot.connection.ipcam.UtilityLog;
import com.moremote.copilot.connection.ipcam.wrap.UserStatus;
import com.moremote.copilot.connection.oauth.IOTManager;
import com.moremote.copilot.connection.oauth.UserAccount;
import com.moremote.copilot.connection.p2p.StunAsyncTask;
import com.moremote.copilot.connection.p2p.TCPMessageListener;
import com.moremote.copilot.connection.p2p.TCPServer;
import com.moremote.copilot.connection.p2p.UDTServer;
import com.moremote.copilot.connection.relay.RelayClient;
import com.moremote.copilot.connection.relay.RelayConnectionListener;
import com.moremote.copilot.connection.relay.RelayMessageListener;
import com.moremote.copilot.connection.xmpp.XMPPConnector;
import com.moremote.copilot.interfaces.IPreferenceFunctionChanged;
import com.moremote.copilot.interfaces.IQueueMsgItemReady;
import com.moremote.copilot.media.audioback.AudioBack;
import com.moremote.copilot.queue.endpoints.gs.GroundStationServerXMPP;
import com.moremote.copilot.queue.items.ItemMavLinkMsg;
import com.moremote.copilot.utils.IntentAction;
import com.moremote.copilot.utils.Utils;
import com.moremote.copilot.utils.gpio.GPIO;
import com.moremote.copilot.utils.gpio.GPIOManager;
import com.moremote.copilot.utils.gpio.RS232;
import com.serenegiant.usb.DeviceFilter;
import com.serenegiant.usb.USBMonitor;

import net.majorkernelpanic.streaming.MediaStream;
import net.majorkernelpanic.streaming.MoMediaMuxer;
import net.majorkernelpanic.streaming.Session;
import net.majorkernelpanic.streaming.SessionBuilder;
import net.majorkernelpanic.streaming.audio.AudioQuality;
import net.majorkernelpanic.streaming.gl.SurfaceView;
import net.majorkernelpanic.streaming.video.VideoQuality;

import org.jivesoftware.smack.ConnectionListener;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.chat.Chat;
import org.jivesoftware.smack.chat.ChatMessageListener;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.roster.RosterListener;
import org.jivesoftware.smackx.ping.PingManager;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.net.MulticastSocket;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by Zack on 15/11/10.
 */
public class MainService extends Service implements Session.Callback, SurfaceHolder.Callback ,
         IQueueMsgItemReady ,IPreferenceFunctionChanged {

    private static final String TAG = MainService.class.getSimpleName();
    private final String AppPackageName = "MoTiveCam";
    public static MainService Instance;
    public IPCamApplication hub;

    //fake view
    private SurfaceView mSurfaceView;

    // Session
    private static Session mSession;
    private MoMediaMuxer muxer = null;

    // XMPP
    public static XMPPConnector xmppConnector;
    private static HashMap<String, UserStatus> friends;

    // LocalTCP/Relay/UDT
    public static ConnectedList mUserList;
    private RelayConnectionListener relayConnectionListener;
    private static AudioBack audioBack;

    // Music Player
    private MediaPlayer statusMediaPlayer;
    private MediaPlayer mediaPlayer;
    private List<String> songs;
    private int currentSong;

    private byte mode = MediaStream.MODE_MEDIACODEC_API;
    private boolean isRecord = false; // SWITCH FOR RECORDING

    private Map<Integer, Object> TempTcpSocket;
    private long LastUpdatePictureTime = 0;
    private final IBinder mBinder = new ServiceBinder();
    private IOTManager iotManager;
    private UserAccount userAccount;

    public boolean enableXmppServer;

//    private IdentifyManager mIdentifyManager;
//    private ControlAssistManager mControlAssistManager;
//    private UploadManager mUploadManager;
//    private Drone mDrone;
    private GPIOManager mGpioManager;

    private RS232 rs232;

    private final static int XMPP_LAST_DRONE_CHECK = 1;
    private final static int XMPP_RESET = 12321;
    private boolean xmppNeedRefresh;

    private long xmppLastDroneMsg = -1;
    private Handler mStateHandler = new Handler(){
        @Override
        public void handleMessage(android.os.Message msg) {
            super.handleMessage(msg);
            switch(msg.what){
//                case XMPP_LAST_DRONE_CHECK:
//                    if(enableXmppServer){
//                        if(mDrone.getState().getMode()== DRONE_MODE.MODE_AUTO && xmppLastDroneMsg > 0){
//                            if((System.currentTimeMillis() - xmppLastDroneMsg)>60*1000){
//                                MavCmdList2Drone.setDroneMode(DRONE_MODE.MODE_RTL);
//                                Log.e(TAG,"FailSafe : XMPP LOST CONNECTION OVER 1 MIN >>>> CHANGE MODE TO RTL");
//                            }
//                        }
//                        this.sendEmptyMessageDelayed(XMPP_LAST_DRONE_CHECK, 1000);
//                    }
//                    break;
                case XMPP_RESET:
                    if(xmppNeedRefresh){
                        if(Utils.isConnect(MainService.this)){
                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    initXmpp();
                                    xmppNeedRefresh = false;
                                }
                            }).start();
                        }else{
                            sendEmptyMessageDelayed(XMPP_RESET, 1000);
                        }
                    }
                    break;
            }
        }
    };

    /**
     * for accessing USB
     */

    // for accessing USB and USB camera
    private USBMonitor mUSBMonitor;

    @Override
    public IBinder onBind(Intent intent) {
        Log.d(TAG, "onBind");
        return mBinder;
    }

    @Override
    public void onCreate() {
        Log.d(TAG, "onCreate");

        initSetting();
        hub.registService(this);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "onStartCommand");
        return START_NOT_STICKY;
    }

    private void initSetting() {
        hub = (IPCamApplication)this.getApplication();
//        isRecord = hub.prefs.getBoolean(COPILOT_PREFS.PREF_ENABLE_RECORD, false);
        iotManager = new IOTManager(this);
        userAccount = new UserAccount(this);
        friends = new HashMap<String, UserStatus>();
//        mDrone = new Drone(hub);

        if (isRecord) {
            muxer = new MoMediaMuxer(Environment.getExternalStorageDirectory().getPath(), true, true);
        }
        WindowManager windowManager = (WindowManager) this.getSystemService(Context.WINDOW_SERVICE);
        mSurfaceView = new SurfaceView(this, null);
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams(
                1, 1,
                WindowManager.LayoutParams.TYPE_SYSTEM_OVERLAY,
                WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH,
                PixelFormat.TRANSLUCENT
        );
        layoutParams.gravity = Gravity.LEFT | Gravity.TOP;
        windowManager.addView(mSurfaceView, layoutParams);

//        mIdentifyManager = new IdentifyManager(hub);
//        mControlAssistManager = new ControlAssistManager(hub);
//        mIdentifyManager.setQRLocationListener(mControlAssistManager);
//        mUploadManager = new UploadManager(this,iotManager);
        mGpioManager = new GPIOManager();
//        mDrone.registerListener(mIdentifyManager);
//        mDrone.registerListener(mControlAssistManager);
//        mDrone.registerListener(mUploadManager);

        mSession = SessionBuilder.getInstance()
                .setCallback(MainService.this)
                .setSurfaceView(mSurfaceView)
                .setPreviewOrientation(90)
                .setContext(getApplicationContext())
                .setAudioEncoder(SessionBuilder.AUDIO_AAC)
                .setAudioQuality(new AudioQuality(16000, 32000))
                .setVideoEncoder(SessionBuilder.VIDEO_H264)
//                .setVideoQuality(new VideoQuality(1280, 720, 15, 1000000))
                .setVideoQuality(new VideoQuality(640, 480, 15, 600000))
                .setMediaMuxer(muxer)
//                .setIdentifyCallback(mIdentifyManager)
                .build();
//        mSession.setDestination("127.0.0.1");
//        mSession.configure();

        // 15.11.05
        // use MODE_MEDIACODEC_API_2, start preview with surfacetexture
        if (mode == MediaStream.MODE_MEDIACODEC_API_2) {
            mSession.getVideoTrack().setStreamingMethod(mode);
        }
        mSurfaceView.getHolder().addCallback(MainService.this);

        int audioBackSampleRate = 44100;
        audioBack = new AudioBack(audioBackSampleRate);

        // Initial XMPP
        new Thread(new Runnable() {
            @Override
            public void run() {
                initXmpp();
            }
        }).start();

        // Initial User List
        if (mUserList == null) {
            mUserList = new ConnectedList(connlistener);
        }
        getSongs();
        initMediaPlayer();

        mUSBMonitor = new USBMonitor(this, mOnDeviceConnectListener);

        mUSBMonitor.register();

        rs232 = new RS232(MainService.this);

        Log.d(TAG, "enumerateSerial ret = " + rs232.enumerateSerial());

    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy");
        mSession.getVideoStream().stopMuxer(true);
        mSession.getVideoStream().destroyUVCCamera();
        mSession.getVideoStream().stopUVCMuxer();

        if (mSession != null) {
            mSession.release();
            mSession = null;
        }

        if (statusMediaPlayer != null) {
            statusMediaPlayer.release();
            statusMediaPlayer = null;
        }

        if (mediaPlayer != null) {
            mediaPlayer.release();
            mediaPlayer = null;
        }

        if (xmppConnector != null) {
            xmppConnector.disconnect();
            xmppConnector = null;
        }
        if (audioBack != null) {
            try{
                audioBack.stop();
            }catch (IllegalStateException e){
                e.printStackTrace();
            }
            audioBack = null;
        }
        if (TempTcpSocket != null) {
            TempTcpSocket.clear();
            TempTcpSocket = null;
        }

        if (mUserList != null) {
            mUserList.close();
            mUserList = null;
        }
        hub.unregistService();

        IPCamApplication.logger.sysLog(TAG, "MavLinkHUB closing ...");
        hub.droneClient.stopClient();
        hub.gcsServer.stopServer();
        IPCamApplication.queue.stopQueue();
        IPCamApplication.logger.stopAllLogs();

        if (mUSBMonitor != null) {
            mUSBMonitor.unregister();
            mUSBMonitor.destroy();
            mUSBMonitor = null;
        }

//        mUploadManager.stopUpload();
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        Log.d(TAG, "surfaceCreated");
        Log.d(TAG, "startPreview.");
        mSession.setDestination("127.0.0.1");
        mSession.configure();
        //取代原生相機直接使用UVC CAM
//TODO        mSession.startPreview();
//        if (mSession.getVideoStream().getUVCCamera() == null) {
//            List<UsbDevice> dlist = refreshUVC();
//            // and connect
//            if(dlist.size()>0){
//                mUSBMonitor.requestPermission(dlist.get(0));
//            }
//        }

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                if (mSession != null) {
                    // Initial User List
                    if (mUserList == null) {
                        mUserList = new ConnectedList(connlistener);
                        mSession.setUserList(mUserList);
                    } else {
                        mSession.setUserList(mUserList);
                    }
                }
            }
        }, 2000);

        LocalBroadcastManager.getInstance(MainService.this).sendBroadcast(new Intent()
                .setAction(IntentAction.SurfaceCreated));

    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        Log.d(TAG, "surfaceChanged");
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        Log.d(TAG,"surfaceDestroyed");
        mSession.stop();
    }

    @Override
    public void onBitrateUpdate(long bitrate) {

    }

    @Override
    public void onSessionError(int reason, int streamType, Exception e) {
        Log.d(TAG, "onSessionError | " + e.getMessage());
    }

    @Override
    public void onPreviewStarted() {
        Log.d(TAG, "Preview started.");
    }

    @Override
    public void onSessionConfigured() {
        // Once the stream is configured, you can get a SDP formated session description
        // that you can send to the receiver of the stream.
        // For example, to receive the stream in VLC, store the session description in a .sdp file
        // and open it with VLC while streming.
        Log.d(TAG, "Preview configured.");
//        Log.d("SDP", mSession.getSessionDescription());
        mSession.start();

    }

    @Override
    public void onSessionStarted() {
        Log.d(TAG, "Session started.");
    }

    @Override
    public void onSessionStopped() {
        Log.d(TAG, "Session stopped.");
    }

    @Override
    public void onCameraSwitched() {
        Log.d(TAG, "Camera Switched.");
        if (mUserList != null) {
            HashMap<String, String> info = mUserList.getUserDisplayName(MainService.this);
            Iterator iter = info.entrySet().iterator();
            while (iter.hasNext()) {
                Map.Entry entry = (Map.Entry) iter.next();
                xmppConnector.sendMessage((String) entry.getKey(), IPCamApplication.XMPPCommand.CAMERA_FACING + mSession.getCamera());
            }
        }
    }

    private Handler UDTAccecptSuccessHandler = new Handler() {
        @Override
        public void handleMessage(android.os.Message msg) {
            String userXmpp = msg.getData().getString(StunAsyncTask.DataField.DEST_JID);
            Map<String, Integer> mapUDT = (Map) msg.obj;
            mUserList.setUserConnectType(userXmpp, IPCamApplication.ConnectionType.P2P_WAN_UDT, mapUDT);

            /** TODO modify this to UDTServer */
            final int UDTSockFD = mapUDT.get("UDTSocketFD");
            Thread getAudioFromC = new Thread(new Runnable() {
                @Override
                public void run() {
                    byte[] bytes_pkg = null;
                    while (true) {
                        bytes_pkg = mUserList.getUDTServer().receive(UDTSockFD);
                        if (bytes_pkg != null) {
                            decodeAudioBack(bytes_pkg, bytes_pkg.length);
                        } else break;
                    }
                }
            });
            getAudioFromC.start();

            Toast.makeText(MainService.this, getResources().getString(R.string.connection_success), Toast
                    .LENGTH_LONG).show();
        }
    };

    /* --------- modify by lintzuhsiu at 14/12/10 ---------*/
    private Handler stunMessageHandler = new Handler() {
        @Override
        public void handleMessage(android.os.Message msg) {
            super.handleMessage(msg);

            Log.d(TAG, "stun result: " + msg.what);

            Bundle extras = msg.getData();
            String destJID = extras.getString(StunAsyncTask.DataField.DEST_JID);
            Log.d(TAG, "stun JID: " + destJID);

            if (msg.what == StunAsyncTask.SUCCESS) {
                int connectionType = extras.getInt(StunAsyncTask.DataField.NETWORK_TYPE);

                /** modifi multi UDT connet*/
                if (connectionType == IPCamApplication.ConnectionType.P2P_WAN_UDT) {
//            		Log.e(TAG,"UDT for streaming stun success!!");
                    MulticastSocket socket = (MulticastSocket) msg.obj;
                    if (mUserList.getUDTServer() == null) {
                        mUserList.setUDTServer(new UDTServer(UDTAccecptSuccessHandler));
                    }
                    mUserList.getUDTServer().transformToUDTSocket(new HashMap<String, Integer>(), socket, destJID);
                } else {
                    // TODO Local tcp need to do anything after stun success?
                }
            }
            mUserList.interruptUserStunAsyncTask(destJID);
        }
    };

    /* --------- add by CC at 15/8/5 ---------*/
    private ConnectedList.UserConnectionListener connlistener = new ConnectedList.UserConnectionListener() {
        @Override
        public void notifyUserConnectionChanged(String client, int status) {
            String Status = (status == 1) ? "Connected" : "DisConnected";
            UtilityLog.d(TAG, "notifyConnectionChange: " + client + " , status: " + Status + " , UserNum: " + mUserList.getUserNum());
            Intent inotifyConnectionChange = new Intent();
            inotifyConnectionChange.setAction(IntentAction.NotifyUserConnectionChanged);
            inotifyConnectionChange.putExtra("mUserListNUM", mUserList.getUserNum());
            LocalBroadcastManager.getInstance(MainService.this).sendBroadcast(inotifyConnectionChange);
        }
    };

    private ChatMessageListener xmppMessageListener = new ChatMessageListener() {
        final String TAG = "xmppMessageListener";
        @Override
        public void processMessage(Chat chat, final Message message) {
            final String from = message.getFrom();
            String msgContent = message.getBody();
            String msgID = message.getStanzaId();
//            Log.d(TAG, msgContent);
            if (msgContent.equals(IPCamApplication.XMPPCommand.OPEN_LED)) {
                turnOnTorch();
            } else if (msgContent.equals(IPCamApplication.XMPPCommand.CLOSE_LED)) {
                turnOffTorch();
            } else if (msgContent.equals(IPCamApplication.XMPPCommand.PLAY_MUSIC)) {
                playMusic();
            } else if (msgContent.equals(IPCamApplication.XMPPCommand.PREV_SONG)) {
                prevSong();
            } else if (msgContent.equals(IPCamApplication.XMPPCommand.NEXT_SONG)) {
                nextSong();
            } else if (msgContent.equals(IPCamApplication.XMPPCommand.STOP_MUSIC)) {
                stopMusic();
            } else if (msgContent.contains(IPCamApplication.XMPPCommand.APP_DEVICE)) {
                // IPCamApplication.XMPPCommand.CAMERA_FACING + Camera.CameraInfo.CAMERA_FACING_BACK
                // IPCamApplication.XMPPCommand.CAMERA_FACING + Camera.CameraInfo.CAMERA_FACING_FRONT
                UtilityLog.d(TAG, "APP_DEVICE: " + from + " , msgContent: " + msgContent);
                String UserDisplayName = msgContent.substring(11);
                mUserList.TempUserDisplayName.put(from, UserDisplayName);
                if (mSession != null) {
                    xmppConnector.sendMessage(from, IPCamApplication.XMPPCommand.CAMERA_FACING + mSession.getCamera());
                }
            } else if (msgContent.equals(IPCamApplication.XMPPCommand.CAMERA_FACING_SWITCH)) {
                UtilityLog.d(TAG, "CAMERA_FACING_SWITCH: " + from + " , msgContent: " + msgContent);
                if (mSession != null) {
                    mSession.switchCamera();
                }

            } else if (msgContent.equals(IPCamApplication.XMPPCommand.ASK_SNAPSHOT)) {
                UtilityLog.d(TAG, "ASK_SNAPSHOT: " + from + " , msgContent: " + msgContent);
                if (mSession != null) {
                    if (lastPictureUpload(from)) {
                        xmppConnector.sendMessage(from, IPCamApplication.XMPPCommand.RSP_SNAPSHOT);
                    }
                }
            } else if (msgContent.equals(IPCamApplication.XMPPCommand.STREAM_START)) {
                UtilityLog.d(TAG, "STREAM_START: " + from + " , msgContent: " + msgContent);
                startStreaming();
            } else if (msgContent.equals(IPCamApplication.XMPPCommand.STREAM_STOP)) {
                UtilityLog.d(TAG, "STREAM_STOP: " + from + " , msgContent: " + msgContent);
                stopStreaming();
                if (mUserList != null) {
                    mUserList.setUserConnectType(from, IPCamApplication.ConnectionType.NONE_CONNECT, null);
                }
            } else if (msgContent.equals(IPCamApplication.XMPPCommand.ASK_RELAY)) {
                UtilityLog.d(TAG, "ASK_RELAY: " + from + " , msgContent: " + msgContent);
                startTCPRelay(from);
            } else if (msgContent.contains(IPCamApplication.XMPPCommand.STUN)) {
                xmppConnector.sendMessage(from, IPCamApplication.XMPPCommand.CAMERA_TYPE + IPCamApplication.cameraType.AndroidCam);

                // add log, by cc
                //Log.d(StunAsyncTask.TAG, msgContent);
                UtilityLog.d(TAG, "STUN: " + from + " , msgContent: " + msgContent);
                // add log, by cc

                if (!mUserList.add(from)) {
                    Log.e(TAG, "add this user fail");
                } else {
                    int dashPosition = msgContent.indexOf("-");
                    String desPublicIP = msgContent.substring(5, msgContent.indexOf(","));
                    int desPublicPort = Integer.valueOf(msgContent.substring(msgContent.indexOf(",") + 1, dashPosition));
                    String desLocalIP = msgContent.substring(dashPosition + 1, msgContent.indexOf(",", dashPosition));
                    int natPosition = msgContent.indexOf(":", dashPosition);
                    int desLocalPort = Integer.valueOf(msgContent.substring(msgContent.indexOf(",", dashPosition) + 1, natPosition));
                    IPCamApplication.NatType destNat = IPCamApplication.NatType.get(Integer.valueOf(msgContent.substring(natPosition + 1, msgContent.length())));
                    Thread stunAsyncTask = new StunAsyncTask(xmppConnector, message.getFrom(), stunMessageHandler,
                            desPublicIP, desPublicPort, desLocalIP, desLocalPort, IPCamApplication.getLocalIpAddress(), destNat);
                    stunAsyncTask.start();
                    mUserList.setUserStunAsyncTask(from, stunAsyncTask);
                }
                UtilityLog.d(TAG, "STUN: " + from + " , UserNum: " + mUserList.getUserNum());
            } else if (msgContent.equals(IPCamApplication.XMPPCommand.ASK_TCP)) {
                UtilityLog.d(TAG, "ACK_TCP: " + from + " , msgContent: " + msgContent + " , msgPacketID : " + msgID);
                startTCPServer(from);
            } else if (msgContent.contains(IPCamApplication.XMPPCommand.TCP_CONNECTION_SUCCESS)) {
                UtilityLog.d(TAG, "TCP_CONNECTION_SUCCESS: " + from + " , msgContent: " + msgContent + " , msgPacketID : " + msgID);
                int natPosition = msgContent.indexOf(":");
                int port = Integer.valueOf(msgContent.substring(natPosition + 1));
                TCPClientConnectSuccess(from, port);
            } else if (msgContent.equals(IPCamApplication.XMPPCommand.FINISH)) {
                mSession.release();
                android.os.Process.killProcess(android.os.Process.myPid());
//                finish();
                stopSelf();
            } else if (msgContent.contains(IPCamApplication.XMPPCommand.AUDIO_BACK)) {

            } else if (msgContent.contains(IPCamApplication.XMPPCommand.DRONE)) {
                String[] con1 = msgContent.split(IPCamApplication.XMPPCommand.DRONE);
                byte[] pktArr = Base64.decode(con1[1], Base64.DEFAULT);
                ByteBuffer pktBuffer = ByteBuffer.allocate(pktArr.length);
                pktBuffer.put(pktArr);
                Log.w(TAG, "Receive xmpp drone message :" + pktArr.toString());
                hub.gcsServer.addInputByteQueueItem(pktBuffer);

                xmppLastDroneMsg = System.currentTimeMillis();
            }else if(msgContent.contains(IPCamApplication.XMPPCommand.DRONE_CONNECT)){
                enableXmppServer = true;
                if(hub.gcsServer instanceof GroundStationServerXMPP){
                   ((GroundStationServerXMPP) hub.gcsServer).addUser(from);
                }

                if(xmppLastDroneMsg<0){
                    mStateHandler.sendEmptyMessage(XMPP_LAST_DRONE_CHECK);
                }
                Log.w(TAG, "Receive xmpp DRONE_CONNECT");
            }else if(msgContent.contains(IPCamApplication.XMPPCommand.DRONE_DISCONNECT)){
                mStateHandler.removeMessages(XMPP_LAST_DRONE_CHECK);
                xmppLastDroneMsg = -1;
                enableXmppServer = false;
                if(hub.gcsServer instanceof GroundStationServerXMPP){
                    ((GroundStationServerXMPP) hub.gcsServer).removeUser(from);
                }
                Log.w(TAG, "Receive xmpp DRONE_DISCONNECT");
            }else if(msgContent.contains(IPCamApplication.XMPPCommand.GPIO_CMD)){
                Log.w(TAG, "Receive xmpp GPIO_CMD");
                if(msgContent.contains("gpio_getall")){
                    JSONObject obj = mGpioManager.getAllGPIO();
                    try {
                        obj.put("network", Utils.getNetWorkInfo(MainService.this).getTypeName());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    String ret = "ret_gpio_getall:" + obj.toString();
                    xmppConnector.sendMessage(from, ret);
                }else if(msgContent.contains("gpio_set")){
                    final String[] numbers = msgContent.replaceAll("[^0-9]+", " ").trim().split(" ");
                    Log.d(TAG, "gpio_set Receive : "+ Arrays.toString(numbers));
                    GPIO cur = mGpioManager.getGpioByPin(Integer.valueOf(numbers[0]));
                    cur.setState(Integer.valueOf(numbers[1]));
                } else if (msgContent.contains("gpio_get")){
                    final String[] numbers = msgContent.replaceAll("[^0-9]+", " ").trim().split(" ");
                    GPIO cur = mGpioManager.getGpioByPin(Integer.valueOf(numbers[0]));
                    String retStr = "ret_gpio_get:" + cur.getJSONStr();
                    Log.d(TAG, retStr);
                    xmppConnector.sendMessage(from, retStr);
                }
            }else if(msgContent.contains(IPCamApplication.XMPPCommand.RS232_CMD)){
                final String haxStr = msgContent.substring(msgContent.indexOf(":") + 1);
                rs232.openUsbSerial();
                byte[] dataByte = new BigInteger(haxStr, 16).toByteArray();
                rs232.writeDataToSerial(dataByte);
            } else if (msgContent.contains(IPCamApplication.XMPPCommand.IOT_GETINFO)){
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        JSONObject ret = new JSONObject();
                        SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd  HH:mm:ss");
                        Date d = new Date();
                        try {
                            ret.put("RSSI", Utils.getRssi(MainService.this));
                            ret.put("IP", Utils.getIP(MainService.this));
                            ret.put("TIME", formatter.format(d));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        String retStr = "ret_iot_getinfo:"+ret.toString();
                        Log.d(TAG, retStr);
                        xmppConnector.sendMessage(from, retStr);
                    }
                }).start();

            } else if (msgContent.contains(IPCamApplication.XMPPCommand.IOT_WIFI_ON)) {
                WifiManager wiFiManager = (WifiManager) MainService.this.getSystemService(Context.WIFI_SERVICE);
                if(!wiFiManager.isWifiEnabled()){
                    if(xmppConnector!=null){
                        xmppConnector.disconnect();
                    }
                    wiFiManager.setWifiEnabled(true);
                    xmppNeedRefresh = true;
                }
                Log.d(TAG, "IOT_WIFI_ON");
            }else if (msgContent.contains(IPCamApplication.XMPPCommand.IOT_WIFI_OFF)) {
                WifiManager wiFiManager = (WifiManager) MainService.this.getSystemService(Context.WIFI_SERVICE);
                if(wiFiManager.isWifiEnabled()){
                    if(xmppConnector!=null){
                        xmppConnector.disconnect();
                    }
                    wiFiManager.setWifiEnabled(false);
                    xmppNeedRefresh = true;
                }
                Log.d(TAG, "IOT_WIFI_OFF");
            }
        }
    };

    /***********  XMPP Connection Listener  **********/
    private ConnectionListener xmppConnectionListener = new ConnectionListener() {

        @Override
        public void connected(XMPPConnection connection) {
            UtilityLog.d(TAG, "XMPP connected");

//            int interval = PingManager.getInstanceFor(connection).getPingInterval();
//            Log.d(TAG, "Ping interval : " + interval);
            PingManager.getInstanceFor(connection).setPingInterval(60);
        }

        @Override
        public void authenticated(XMPPConnection connection, boolean resumed) {
            UtilityLog.d(TAG, "XMPP authenticated");
            try {
                playMusic(getAssets().openFd("login_success.mp3"));
            } catch (IOException e) {
            }

            new Thread(new Runnable() {
                @Override
                public void run() {
                    xmppConnector.getRoster(friends);
                }
            }).start();
            Intent intent = new Intent();
            intent.setAction(IntentAction.OnAuthenticated);
            LocalBroadcastManager.getInstance(MainService.this).sendBroadcast(intent);
        }

        @Override
        public void connectionClosed() {
            UtilityLog.d(TAG, "XMPP ConnectionClosed");
        }

        @Override
        public void connectionClosedOnError(Exception e) {
            UtilityLog.d(TAG, "connectionClosedOnError : " + e.toString());
            mStateHandler.sendEmptyMessage(XMPP_RESET);
//            Intent intent = new Intent();
//            intent.setAction(IntentAction.OnConnectionClosedOnError);
//            LocalBroadcastManager.getInstance(MainService.this).sendBroadcast(intent);
        }

        @Override
        public void reconnectingIn(int seconds) {
        }

        @Override
        public void reconnectionSuccessful() {
            UtilityLog.d(TAG, "reconnectionSuccessful");
        }

        @Override
        public void reconnectionFailed(Exception e) {
        }
    };
    /********** XMPP Connection Listener **********/

    /*********** XMPP roster Listener add 15.11.04 by CC **********/
    protected RosterListener rosterListener = new RosterListener() {
        @Override
        public void entriesAdded(Collection<String> addresses) {
        }

        @Override
        public void entriesUpdated(Collection<String> addresses) {
        }

        @Override
        public void entriesDeleted(Collection<String> addresses) {
        }

        @Override
        public void presenceChanged(final Presence presence) {
            XMPPClientpresenceChanged(presence);
        }
    };

    private synchronized void XMPPClientpresenceChanged(final Presence presence) {
        final String from = presence.getFrom();
        Log.d(TAG, "XMPPClientpresenceChanged() from: " + from + " , " + presence.getType().name());
        // check XmppClient unavailable and stop streaming
        if (presence.getType() == Presence.Type.unavailable) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    Log.d(TAG, "STREAM_STOP: " + from);
                    if (mUserList != null) {
                        mUserList.setUserConnectType(from, IPCamApplication.ConnectionType.NONE_CONNECT, null);
                    }
                }
            }).start();
        }
    }

    public void resetXmpp(){
        mStateHandler.removeMessages(XMPP_RESET);
        mStateHandler.sendEmptyMessage(XMPP_RESET);
    }

    /*********** XMPP roster Listener **********/

    private void initXmpp() {

        if (xmppConnector != null) {
            xmppConnector.disconnect();
        }
        // login to xmpp server
        String resource = Utils.getMacAddress(this) + AppPackageName;
        xmppConnector = new XMPPConnector(this);
        // Set Xmpp Listener
        xmppConnector.addConnectionListener(xmppConnectionListener);
        xmppConnector.addMessageListener(xmppMessageListener);
        xmppConnector.addRosterListener(rosterListener);
        if (!xmppConnector.loginToXMPPServer(userAccount.XMPP_ACCOUNT, userAccount.XMPP_PASSWORD, resource)) {
            try {
                playMusic(getAssets().openFd("login_failed.mp3"));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    synchronized private void startTCPRelay(final String friendJID) {
        if (mUserList.getRelayClient() != null) {
            if (mUserList.getRelayClient().isAuthed()) {
                String uuid = mUserList.getRelayClient().getUUID();
                String auth = mUserList.getRelayClient().getAuth();
                xmppConnector.sendMessage(friendJID, IPCamApplication.XMPPCommand.RELAY + uuid + auth);
                mUserList.setUserConnectType(friendJID, IPCamApplication.ConnectionType.RELAY, null);
            } else {
                xmppConnector.sendMessage(friendJID, IPCamApplication.XMPPCommand.RELAY + "null");
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    public void run() {
                        Toast.makeText(MainService.this, "RELAY Connect Fail", Toast.LENGTH_SHORT).show();
                    }
                });

            }
            return;
        }

        // connect to tcp relay server
//        mUserList.setRelayClient( new RelayClient(IPCamApplication.TCP_RELAY_SERVER_IP, IPCamApplication.TCP_RELAY_SERVER_PORT,
//                true, IPCamApplication.UUID, IPCamApplication.AUTH) );
        mUserList.setRelayClient(new RelayClient(IPCamApplication.TCP_RELAY_SERVER_IP, IPCamApplication.TCP_RELAY_SERVER_PORT,
                true, userAccount.RELAY_UUID, userAccount.RELAY_KEY));
        relayConnectionListener = new RelayConnectionListener() {
            final String JID = friendJID;

            @Override
            public void connected(boolean isSuccess) {
                if (isSuccess) {
                    Log.d(TAG, "relay client connect to relay server success");
                } else {
                    Log.d(TAG, "relay client connect to relay server fail");
                }
            }

            @Override
            public void authed(final boolean isSuccess, final String uuid, final String auth) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        if (isSuccess) {
                            Log.d(RelayClient.TAG, "send relay message over xmpp " + uuid + "," + auth + ", JID: " + JID);
                            xmppConnector.sendMessage(JID, IPCamApplication.XMPPCommand.RELAY + uuid + auth);

                            mUserList.setUserConnectType(JID, IPCamApplication.ConnectionType.RELAY, null);
                            new Handler(Looper.getMainLooper()).post(new Runnable() {
                                public void run() {
                                    Toast.makeText(MainService.this, "RELAY " + getResources().getText(R
                                            .string.connection_success), Toast.LENGTH_SHORT).show();
                                }
                            });

                        } else {
                            xmppConnector.sendMessage(JID, IPCamApplication.XMPPCommand.RELAY + "null");
                            new Handler(Looper.getMainLooper()).post(new Runnable() {
                                public void run() {
                                    Toast.makeText(MainService.this, "RELAY Connect Fail", Toast.LENGTH_SHORT).show();
                                }
                            });
                            // 15.11.03
                            // relayClient auth fail reset relayClient
                            mUserList.setRelayClient(null);
                        }
                    }
                }).start();
            }
        };
        mUserList.getRelayClient().setConnectionListener(relayConnectionListener);

        RelayMessageListener messageListener = new RelayMessageListener() {

            @SuppressLint("NewApi")
            @Override
            public void processMessage(byte[] data, int length) {
                decodeAudioBack(data, length);
            }
        };

        mUserList.getRelayClient().setMessageListener(messageListener);
        new Thread(new Runnable() {
            @Override
            public void run() {
                mUserList.getRelayClient().connect();
            }
        }).start();
    }

    private static void decodeAudioBack(byte[] data, int length) {
        audioBack.decodeAndPlay(data, length);
//        Log.d(TAG, "decodeAudioBack : "+length);
    }

    synchronized private void startTCPServer(final String friendJID) {
        int serverPort;
        /**  if multi user connect TCP at the same time, friendJID will keep the new one */
//        Handler handler = new Handler() {
//            @Override
//            public void handleMessage(android.os.Message msg) {
//                super.handleMessage(msg);
//                Log.d(TAG, friendJID + " startTCPServer");
//                Log.d(TAG, "ip: " + ((java.nio.channels.SocketChannel) (msg.obj)).socket().getInetAddress().getHostAddress());
//                Log.d(TAG, "ip: " + ((java.nio.channels.SocketChannel) (msg.obj)).socket().getLocalAddress().getHostAddress());
//                mUserList.setUserConnectType(friendJID, IPCamApplication.ConnectionType.P2P_LAN_TCP, msg.obj);
//                Toast.makeText(MainService.this,
//                        getResources().getString(R.string.connection_success), Toast.LENGTH_LONG).show();
//            }
//        };
        if (mUserList.getTCPServer() == null) {
            // tcp server start
            mUserList.setTCPServer(new TCPServer(TCPClient_ConnectHandler));
            mUserList.getTCPServer().setMessageListener(new TCPMessageListener() {
                @Override
                public void processMessage(byte[] data, int length) {
                    decodeAudioBack(data, length);

                }
            });
            // thread saft Hash Map
            TempTcpSocket = new ConcurrentHashMap<Integer, Object>();
            serverPort = mUserList.getTCPServer().start();
        } else {
            serverPort = mUserList.getTCPServer().getServerPort();
        }

        Log.d(TAG, "p2p over TCP port number:" + serverPort);
        if (serverPort < 0) {
            Log.d(TCPServer.TAG, "Server Error");
            Toast.makeText(this, "Server Error", Toast.LENGTH_SHORT).show();
        } else {
            xmppConnector.sendMessage(friendJID, IPCamApplication.XMPPCommand.TCP_CONNECTION + serverPort);
        }
    }

    final Handler TCPClient_ConnectHandler = new Handler() {
        @Override
        public void handleMessage(android.os.Message msg) {
            super.handleMessage(msg);
            if (msg.arg1 == 1) {
                SocketChannel socket = (SocketChannel) (msg.obj);
                int port = socket.socket().getPort();
                try{
                    Log.d(TAG, "ip: " + socket.socket().getInetAddress().getHostAddress() + " ,port: " + port);
                }catch (NullPointerException e){
                    e.printStackTrace();
                }

                if (TempTcpSocket.containsKey(port)) {
                    SocketChannel oldSocket = (SocketChannel) TempTcpSocket.get(port);
                    TempTcpSocket.remove(port);
                    if (oldSocket != null) {
                        try {
                            oldSocket.close();
                        } catch (IOException e) {
                            Log.e(TAG, "Close old socket IOException!!, port: " + port);
                        }
                    }
                }
                TempTcpSocket.put(port, msg.obj);
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    public void run() {
                        Toast.makeText(MainService.this, getResources().getString(R.string
                                .connection_success), Toast.LENGTH_LONG).show();
                    }
                });
            } else {
                Log.e(TAG, "Temp Socket Closed port: " + msg.arg2);
                if (TempTcpSocket.containsKey(msg.arg2)) {
                    TempTcpSocket.remove(msg.arg2);
                }
            }
            Log.d(TAG, "Temp Tcp Socket size: " + TempTcpSocket.size());
        }
    };

    synchronized private void TCPClientConnectSuccess(final String friendJID, final int port) {
        Log.d(TAG, "JID: " + friendJID + " ,TCP port number: " + port);
        if (TempTcpSocket.containsKey(port)) {
            Object socket = TempTcpSocket.get(port);
            TempTcpSocket.remove(port);
            mUserList.setUserConnectType(friendJID, IPCamApplication.ConnectionType.P2P_LAN_TCP, socket);
        } else {
            Log.e(TAG, "TcpSocket not found!! JID: " + friendJID + " ,TCP port number: " + port);
        }
        Log.d(TAG, "Temp Tcp Socket size: " + TempTcpSocket.size());
    }

    /*********** torch start  **********/
    private void turnOnTorch() {
        if (!mSession.getVideoTrack().getFlashState())
            mSession.toggleFlash();
    }

    private void turnOffTorch() {
        if (mSession.getVideoTrack().getFlashState())
            mSession.toggleFlash();
    }
    /********** torch start **********/

    /*********** media start **********/
    private void getSongs() {
        songs = new ArrayList<String>();
        File rootDir = Environment.getExternalStorageDirectory();
        String musicDirPath = rootDir + "/music/";
        songs.add("a.mp3");
        songs.add("b.mp3");
        songs.add("c.mp3");
        songs.add("d.mp3");
        songs.add("e.mp3");
        songs.add("f.mp3");
    }

    private void initMediaPlayer() {
        statusMediaPlayer = new MediaPlayer();

        mediaPlayer = new MediaPlayer();
        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                nextSong();
            }
        });
    }

    private void playMusic(AssetFileDescriptor afd) {
        if (statusMediaPlayer == null) {
            return;
        }

        statusMediaPlayer = new MediaPlayer();
        statusMediaPlayer.reset();
        try {
            statusMediaPlayer.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
            statusMediaPlayer.prepare();
            statusMediaPlayer.start();
        } catch (IOException e) {
        }
    }

    private void playMusic() {
        if (mediaPlayer == null) {
            return;
        }

        mediaPlayer.reset();
        try {
            // mediaPlayer.setDataSource(songs.get(currentSong));
            AssetFileDescriptor afd = getAssets().openFd(songs.get(currentSong));
            mediaPlayer.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
            mediaPlayer.prepare();
            mediaPlayer.start();
        } catch (IOException e) {
            Log.d(TAG, "Play Music: IO Exception");
        }
    }

    private void prevSong() {
        if (--currentSong < 0) {
            currentSong = songs.size() - 1;
        }
        playMusic();
    }

    private void nextSong() {
        if (++currentSong > songs.size() - 1) {
            currentSong = 0;
        }
        playMusic();
    }

    private void stopMusic() {
        mediaPlayer.stop();
    }
    /********** media end **********/

    /* --------- end ---------*/

    /********** lastPictureUpload **********/
    /**
     * Method to upload Picture. <br />
     * Nexus 7 II take picture size less than 1280 * 720, will have data not complete issue.
     * Now get picture size 1280 * 720 then scale size to 180 * 135 and upload to Server.
     * upload cycle 3 minutes
     */
    private boolean lastPictureUpload(final String friendJID) {
        long currentTime = System.currentTimeMillis();
        long diff = currentTime - LastUpdatePictureTime;
        final int uploadtimecycle = 3 * 60 * 1000;
        Log.d(TAG, "take Picture, diff : " + diff);
        if (diff >= uploadtimecycle) {
            if (userAccount.OAuthType == UserAccount.OAUTHTYPE.GOOGLE) {
                Intent iPictureUploadOAuthTypeGoogle = new Intent();
                iPictureUploadOAuthTypeGoogle.setAction(IntentAction.OnPictureUploadOAuthTypeGoogle);
                iPictureUploadOAuthTypeGoogle.putExtra("JID", friendJID);
                LocalBroadcastManager.getInstance(MainService.this).sendBroadcast(iPictureUploadOAuthTypeGoogle);
                Log.d(TAG, "sendBroadcast, JID : " + friendJID);
            } else {
                takePicture(friendJID);
            }
            return false;
        } else {
            return true;
        }
    }

    private void startStreaming() {
        Log.d(TAG, "streaming start");
    }

    private void stopStreaming() {
        Log.d(TAG, "streaming stop");
    }

    private void takePicture(final String friendJID) {
        mSession.takePicture(new Camera.PictureCallback() {
            synchronized public void onPictureTaken(byte[] data, Camera camera) {
                Bitmap bmp = BitmapFactory.decodeByteArray(data, 0, data.length);
                Bitmap bmRotated;
                Matrix matrix = new Matrix();
                float scaleWidth = 180 / (float) bmp.getWidth();
                float scaleHeight = 135 / (float) bmp.getHeight();
                if (mSession.getCamera() == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                    matrix.setScale(scaleWidth, scaleHeight);
                    matrix.postRotate(-90);
                    bmRotated = Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(), bmp.getHeight(), matrix, true);
                } else {
                    matrix.setScale(scaleWidth, scaleHeight);
                    matrix.postRotate(90);
                    bmRotated = Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(), bmp.getHeight(), matrix, true);
                }
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bmRotated.compress(Bitmap.CompressFormat.JPEG, 100, stream);

//                    try {
//                        FileOutputStream fop;
//                        fop = new FileOutputStream("/sdcard/dd.jpg");
//                        //實例化FileOutputStream，參數是生成路徑
//                        bmRotated.compress(Bitmap.CompressFormat.JPEG, 100, fop);
//                        //壓缩bitmap寫進outputStream 參數：輸出格式  輸出質量  目標OutputStream
//                        //格式可以為jpg,png,jpg不能存儲透明
//                        fop.close();
//
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                        System.out.println("IOException");
//                    }

                String updateImage = Base64.encodeToString(stream.toByteArray(), Base64.DEFAULT);
                bmp.recycle();
                bmRotated.recycle();
                camera.startPreview();
                //需要手動重新startPreview，否則停在拍下的瞬間

                iotManager.updatePicture(userAccount.XMPP_ACCOUNT, updateImage, new IOTManager
                        .IOTHandler(MainService.this) {
                    @Override
                    public void handleMessage(android.os.Message msg) {
                        super.handleMessage(msg);
                        Bundle bundle = msg.getData();
                        String status = bundle.getString("StatusCode");
                        if (status.equals("200")) {
                            xmppConnector.sendMessage(friendJID, IPCamApplication.XMPPCommand.RSP_SNAPSHOT);
                            LastUpdatePictureTime = System.currentTimeMillis();
                            Log.d(TAG, "JID: " + friendJID + " , msg: " + IPCamApplication.XMPPCommand.RSP_SNAPSHOT);
                        } else {
                            /** Update Token Fail **/
                        }
                    }
                });
            }
        });
    }



    @Override
    public void onQueueMsgItemReady(ItemMavLinkMsg msgItem) {
        //ONLY USE FOR GET INFO

//        switch(msgItem.msgId){
//            case msg_rangefinder.MAVLINK_MSG_ID_RANGEFINDER://173
//                float liderDistance = ((msg_rangefinder) msgItem.msg).distance;
////                mIdentifyManager.upDateRealHeight(liderDistance);
//                mDrone.setRealAltitude(liderDistance);
//                break;
//            case msg_heartbeat.MAVLINK_MSG_ID_HEARTBEAT://0
////                mControlAssistManager.updateDroneState(((msg_heartbeat) msgItem.msg));
//                mDrone.updateDroneState(((msg_heartbeat) msgItem.msg));
//                break;
//        }
    }

    public class ServiceBinder extends Binder {
        public void onCameraSwitchIconClick() {
            mSession.switchCamera();
        }

        public void upDateUserAccount() {
            MainService.this.userAccount = new UserAccount(MainService.this);
        }

        public void calltakePicture(String JID) {
            takePicture(JID);
        }

        public void SurfaceCreated(SurfaceView mActivitySurfaceView) {
            Log.d(TAG, "ServiceBinder SurfaceCreated");
            if (mSurfaceView != null) {
                mSurfaceView.setActivitySurfaceView(mActivitySurfaceView);
                mSession.stopPreview();
                if (mSession.getVideoStream().getUVCCamera() == null) {
                    List<UsbDevice> dlist = refreshUVC();
                    // and connect
                    if(dlist.size()>0){
                        mUSBMonitor.requestPermission(dlist.get(0));
                    }
                }
            }
            else {
                Log.e(TAG, "ServiceBinder not SurfaceCreated");
            }
        }

        public void SurfaceDestroy() {
            Log.d(TAG, "ServiceBinder SurfaceDestroy");
            if (mSurfaceView != null) {
                mSurfaceView.setActivitySurfaceView(null);
                if (mSession.getVideoStream().getUVCCamera() != null) {
                    mSession.getVideoStream().destroyUVCCamera();
                }
            }
        }

//        public void setDebugView(android.view.SurfaceView mDebugSurfaceView){
//            if(mIdentifyManager!=null)
//                mIdentifyManager.setSurfaceView(mDebugSurfaceView);
//        }
//
//        public void setVAInfoUpdateListener(IVAInfoUpdate listener){
//            if(mControlAssistManager!=null)
//                mControlAssistManager.setVAInfoUpdateListener(listener);
//        }

        public void getUserDisplayName(ArrayList<String> list){
            HashMap<String, String> info = mUserList.getUserDisplayName(MainService.this);
            Iterator iter = info.entrySet().iterator();
            while (iter.hasNext()) {
                Map.Entry entry = (Map.Entry) iter.next();
                list.add((String) entry.getValue());
            }
        }

        public void uvcOnClick(){
//            if (mSession.getVideoStream().getUVCCamera() == null) {
//                List<UsbDevice> dlist = refreshUVC();
//                // and connect
//                if(dlist.size()>0){
//                    mUSBMonitor.requestPermission(dlist.get(0));
//                }
//            } else {
//                mSession.getVideoStream().switchUVCCamera();
//            }
            ///////////////////////////
//            Log.d("RS232 PROCESS", "open state = " + rs232.openUsbSerial());
//            String msgContent="";
//            if(!screenOn){
//                msgContent = "rs232_command:24245207306526";
//                screenOn = true;
//            }else{
//                msgContent = "rs232_command:24245207316426";
//                screenOn = false;
//            }
//            Log.d("RS232 PROCESS", "screenOn = "+screenOn +" | "+msgContent);
//            String haxStr = msgContent.split("[^0-9]+")[2];
//            byte[] dataByte = new BigInteger(haxStr,16).toByteArray();
//            rs232.writeDataToSerial(dataByte);
        }
    }

    private List<UsbDevice>refreshUVC(){
        //uvc only
        final List<DeviceFilter> filter = DeviceFilter.getDeviceFilters(this, R.xml.device_filter);
        return mUSBMonitor.getDeviceList(filter.get(0));
    }

    private final USBMonitor.OnDeviceConnectListener mOnDeviceConnectListener = new USBMonitor.OnDeviceConnectListener() {
        @Override
        public void onAttach(final UsbDevice device) {
            Log.v(TAG, "onAttach:");
            if (mSession.getVideoStream().getUVCCamera() != null) {
                mSession.getVideoStream().destroyUVCCamera();
            }
            List<UsbDevice> dlist = refreshUVC();
            // and connect
            if(dlist.size()>0){
                mUSBMonitor.requestPermission(dlist.get(0));
            }
        }

        @Override
        public void onConnect(final UsbDevice device, final USBMonitor.UsbControlBlock ctrlBlock, final boolean createNew) {
            Log.v(TAG, "onConnect:");
            mSession.getVideoStream().onUVCConnect(ctrlBlock);
        }

        @Override
        public void onDisconnect(final UsbDevice device, final USBMonitor.UsbControlBlock ctrlBlock) {
            Log.v(TAG, "onDisconnect:");
        }

        @Override
        public void onDettach(final UsbDevice device) {
            Log.v(TAG, "onDettach:");
            if(device.getDeviceClass()==239 && device.getDeviceSubclass()==2){
                mSession.getVideoStream().destroyUVCCamera();
                mSession.syncStop();
            }
//            mSession.getVideoStream().switchUVCCamera();
        }

        @Override
        public void onCancel() {
            Log.v(TAG, "onCancel:");
        }
    };

    @Override
    public void onEnableLandingChange() {
//        mIdentifyManager.EnableLandingMode(hub.prefs.getBoolean(COPILOT_PREFS
//                .PREF_ENABLE_VA_LANDING, false));
    }

    @Override
    public void onUVCEnableChange() {
        if(mSession.getVideoStream().getUVCCamera()!=null){
//            mSession.getVideoStream().switchUVCCamera();
            mSession.getVideoStream().destroyUVCCamera();
        }
    }

    public boolean getGroundStationConnectionState(){
        return enableXmppServer;
    }

    public String getCurrentFilePath(){
        if(muxer==null)
            return null;
        else
            return muxer.getCurrentFilePath();
    }
}
