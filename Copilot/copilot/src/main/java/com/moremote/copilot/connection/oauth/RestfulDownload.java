package com.moremote.copilot.connection.oauth;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created by gaozongyong on 2016/1/12.
 */
public class RestfulDownload extends AsyncTask<String,Void,String> {

    private static final String TAG = "HttpDownload";
    private Handler handler;
    private String downloadPath;
    private String authorize;



    public RestfulDownload(Handler handler , String downloadPath ,String authorize) {
        this.handler = handler;
        this.downloadPath = downloadPath;
        this.authorize = authorize;
    }



    @Override
    protected String doInBackground(String... urls){

        String url;
        try{
            url = urls[0];
            DefaultHttpClient client = new DefaultHttpClient() ;
            HttpGet get = new HttpGet(url);
            get.addHeader("Authorization",authorize);
            HttpResponse rp = client.execute(get);
            String status = String.valueOf(rp.getStatusLine().getStatusCode());
            Log.d(TAG, "Status Code" + status + "-- Url : " + url + " -- Authorize : " + authorize);



            // Save Download to File
            if( downloadPath != null){
                InputStream in = rp.getEntity().getContent();
                File outFile = new File(downloadPath);
                OutputStream outStream = new FileOutputStream(outFile);
                int bytesRead = 0;
                byte[] buffer = new byte[8192];
                while ((bytesRead = in.read(buffer, 0, 8192)) != -1) {
                    outStream.write(buffer, 0, bytesRead);
                }
                outStream.close();
                in.close();
            }

            // Close Socket
            client.getConnectionManager().shutdown();

            if(handler != null) {
                Bundle bundle = new Bundle();
                bundle.putString("StatusCode", status);
                Message msg = new Message();
                msg.setData(bundle);
                handler.sendMessage(msg);
            }

        }catch(Exception e){
            Log.e(TAG, e.toString());
        }
        return "";
    }

    protected void onPostExecute(String result)
    {
        try {
            JSONObject data = new JSONObject(result.toString());
            Log.d(TAG, "JSON DATA" + data);
        } catch (JSONException e) {
            Log.e(TAG, e.toString());
        }

    }
}
