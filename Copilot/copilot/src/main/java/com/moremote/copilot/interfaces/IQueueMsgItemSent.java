package com.moremote.copilot.interfaces;

public interface IQueueMsgItemSent {

	void onQueueMsgItemSent();

}
