package com.moremote.copilot.connection.p2p;

import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.moremote.copilot.service.MainService;

import java.io.IOException;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.nio.channels.CancelledKeyException;
import java.nio.channels.ClosedSelectorException;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Set;

public class TCPServer {
	
	public static final String TAG = "TCP Server";
	
	private boolean isStop;
	private Selector selector;
	private ServerSocketChannel serverSocketChannel;
	private Thread serverThread;
	private Handler handler;
	private TCPMessageListener messageListener;
	
	private ByteBuffer byteBuffer;

	public TCPServer(Handler handler) {
		this.handler = handler;
	}
	
	public int start() {
		try {
			selector = Selector.open();
			serverSocketChannel = ServerSocketChannel.open();
            serverSocketChannel.configureBlocking(false);
            serverSocketChannel.socket().bind(null);
            serverSocketChannel.register(selector, SelectionKey.OP_ACCEPT);
			startAcceptThread();
			//byteBuffer = ByteBuffer.allocate(102400);
		} catch (UnknownHostException e) {
			return -1;
		} catch (IOException e) {
			return -1;
		}
		return serverSocketChannel.socket().getLocalPort();
	}
	
	public boolean close() {
		isStop = true;
		try {
			serverSocketChannel.close();
			selector.close();
			serverThread.interrupt();
		} catch (IOException e) {
			Log.d(TAG, "close IO Exception");
		}
		return true;
	}
	
	public void setHandler(Handler handler) {
        this.handler = handler;
	}


    public void setMessageListener(TCPMessageListener listener) {
        this.messageListener = listener;
    }
    long prev_seq;
    public int send(SocketChannel socketChannel, byte[] data, int length) {

            int res = 0,retryCounts = 0;
            ByteBuffer buf = ByteBuffer.wrap(data, 0, length);
            Selector writeSelector = null;

            int sev = data[6] & 0xff;
            int eig = data[7] & 0xff;
            long seqNo = (sev << 8) | eig;

            if(Math.abs(seqNo-prev_seq)!=1 && prev_seq!=0){
                Log.d(TAG, "seq:"+seqNo);
            }
            prev_seq = seqNo;

            try {
                writeSelector = Selector.open();
                SelectionKey key = socketChannel.register(writeSelector, SelectionKey.OP_WRITE);
                 do {
                    res = socketChannel.write(buf);

//                     Log.d(TAG, "res = "+res);
                     if (res == 0) {
                         if(retryCounts > 30){
                             Log.d(TAG,"Send TCP Packet Timeout,close socketchannel");
                             return -1;
                         }
                        Log.d(TAG, "send channel full");
                         writeSelector.select(1000);
                         retryCounts ++;
//                         key.cancel();
                        Log.d(TAG, "send channel avable");
                    }
                } while(buf.hasRemaining());
                writeSelector.close();
                writeSelector = null;
            } catch (IOException e) {
				// TODO
                Log.e(TAG,"send IOException" + e.toString());
                try {
                    SelectionKey key = socketChannel.keyFor(selector);
                    if(key != null) {
                        key.cancel();
                    }
                    socketChannel.close();
                } catch (IOException e1) {
                    Log.e(TAG,"close socketChannel fail");
                }
                return -1;
			}catch(Exception e){
                Log.e(TAG,"send other Exception" + e.toString());
                try {
                    SelectionKey key = socketChannel.keyFor(selector);
                    if(key != null) {
                        key.cancel();
                    }
                    socketChannel.close();
                } catch (IOException e1) {
                    Log.e(TAG,"close socketChannel fail");
                }
                return -1;

            } finally {
                try {
                    if (writeSelector != null)
                        writeSelector.close();
                } catch (Exception e) {
                    Log.e(TAG,"close Selector fail");
                }
            }
        return 1;
	}
	
	private void startAcceptThread() {
		Runnable runnable = new ServerRunnable();
		serverThread = new Thread(runnable);
		serverThread.start();
	}
	
	class ServerRunnable implements Runnable {

		@Override
		public void run() {
            while (!isStop) {
                SelectionKey key = null;
                try {
                    selector.select();
                    Set<SelectionKey> keys = selector.selectedKeys();
                    Iterator<SelectionKey> iterator = keys.iterator();
                    while (iterator.hasNext()) {
                        key = (SelectionKey) iterator.next();
                        if (key.isAcceptable()) {
                            SocketChannel socketChannel = serverSocketChannel.accept();
                            socketChannel.configureBlocking(false);
                            socketChannel.register(selector, SelectionKey.OP_READ);
//                            //channels.add(socketChannel);
//                            socketChannel.socket().setSendBufferSize(1024*1024);
                            Log.d(TAG, "socketChannelgetSendBufferSize : " + socketChannel.socket().getSendBufferSize() + " , port: " + socketChannel.socket().getPort());
                            Message msg = new Message();
                            msg.arg1 = 1;
                            msg.obj = socketChannel;
                            handler.sendMessage(msg);
                        } else if (key.isReadable()) {
                            //Log.e(TAG,"isReadable()");
                            // AudioBack Receive Data
                            SocketChannel socketChannel = (SocketChannel) key.channel();
                            if(socketChannel != null && socketChannel.socket() != null) {
                                byteBuffer = MainService.mUserList.getUserAudioBackBuffer(socketChannel
                                        .socket().getPort());
                                if (byteBuffer == null) {
                                    int port = socketChannel.socket().getPort();
                                    Log.e(TAG, "Audio buffer not found!! port: " + port);
                                    key.channel().close();
                                    SendSocketClosedMessage(port);
                                    iterator.remove();
                                    continue;
                                }
                            }else{
                                iterator.remove();
                                continue;
                            }

                            try {
                                //read over tcp
                                //boolean firsttime = true;
                                if (socketChannel.read(byteBuffer) > 0) {
                                    //								    Log.e("audioback","read = "+read);
                                    int Position = byteBuffer.position();
                                    byteBuffer.flip();
                                    //								    Log.e("audioback","1position="+byteBuffer.position()+" / limit="+byteBuffer.limit()+" / remaining="+byteBuffer.remaining());
                                    int len = 0;
                                    while (byteBuffer.remaining() >= 2) {
                                        len = ((byteBuffer.get(byteBuffer.position()) & 0xFF) << 8) | (byteBuffer.get(byteBuffer.position() + 1) & 0xFF);
                                        //									    Log.e("audioback","len = "+len);
                                        if (byteBuffer.remaining() > len + 2) {
                                            byte[] data = new byte[len];
                                            byteBuffer.position(byteBuffer.position() + 2);
                                            byteBuffer.get(data);

                                            if (messageListener != null) {
                                                messageListener.processMessage(data, len);
                                            }
                                        } else
                                            break;
                                    }
                                    if (byteBuffer.position() != 0) {
                                        //										Log.e("audioback","2position="+byteBuffer.position()+" / limit="+byteBuffer.limit()+" / remaining="+byteBuffer.remaining());
                                        byte[] tmp = new byte[byteBuffer.remaining()];//832+1=833
                                        byteBuffer.get(tmp);
                                        byteBuffer.clear();
                                        byteBuffer.put(tmp);
                                        //										Log.e("audioback","3position="+byteBuffer.position()+" / limit="+byteBuffer.limit()+" / remaining="+byteBuffer.remaining());
                                    } else {
                                        //										Log.e("audioback","-------------------------------------------");
                                        byteBuffer.position(Position);
                                        byteBuffer.limit(102400);
                                    }
                                } else { //socketChannel read = 0, remote connection is closed
                                    int port = socketChannel.socket().getPort();
                                    key.channel().close();
                                    Log.e(TAG, "Remote Connection Fail!!! port: " + port);
                                    SendSocketClosedMessage(port);
                                }
                            } catch (IOException e) { // audioback read IOException
                                int port = socketChannel.socket().getPort();
                                key.channel().close();
                                Log.e(TAG, "audioback read IOException failed!! port: " + port);
                                SendSocketClosedMessage(port);
                            }
                        }
                        iterator.remove();
                    }
                } catch (ClosedSelectorException e) {
                    Log.e(TAG, "closed selector exception");
                    isStop = true;
                }catch (CancelledKeyException e) {
                    Log.e(TAG, "Cancelled Key Exception");
                }catch (IOException e) {
                    Log.e(TAG, "accept thread IOException failed");
//                    if(key != null) {
//                        key.cancel();
//                    }
                }
            }

		}
		
	}

    private void SendSocketClosedMessage(int port) {
        Message msg = new Message();
        msg.arg1 = -1;
        msg.arg2 = port;
        handler.sendMessage(msg);
    }

    public int getServerPort() {
        return serverSocketChannel.socket().getLocalPort();
    }
	
}
