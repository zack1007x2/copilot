package com.moremote.copilot.activity;

import android.accounts.AccountManager;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.gms.common.AccountPicker;
import com.google.android.gms.common.ConnectionResult;
import com.moremote.copilot.activity.Util.UIComponent;
import com.moremote.copilot.R;
import com.moremote.copilot.connection.oauth.GoogleOAuth;
import com.moremote.copilot.connection.oauth.IOTManager;
import com.moremote.copilot.connection.oauth.UserAccount;

import org.json.JSONObject;

import java.util.Arrays;

import static com.google.android.gms.common.GooglePlayServicesUtil.*;


public class RegisterLoginActivity extends Activity implements View.OnClickListener{

    private Button mbtn_facebook_reg, mbtn_google_reg, mbtn_email_reg, mbtn_login,
            mbtn_facebook_log, mbtn_google_log, mbtn_email_log, mbtn_register;
    private int action;

    private UserAccount userAccount;
    private IOTManager iotManager;
    private ProgressDialog loading;
    // Facebook Auth
    private AccessTokenTracker accessTokenTracker;
    private CallbackManager callbackManager;
    private AccessToken accessToken;
    // Google Auth
    private GoogleOAuth googleoauth;
    private static final int REQUEST_CODE_CHOOSE_ACCOUNT = 2;
    private static final int REQUEST_CODE_USER_RECOVERABLE_ERROR = 3;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //初始化FacebookSdk，記得要放第一行，不然setContentView會出錯
        FacebookSdk.sdkInitialize(getApplicationContext());
        super.onCreate(savedInstanceState);

        userAccount = new UserAccount(this);
        iotManager = new IOTManager(this);
        accessToken = AccessToken.getCurrentAccessToken();

        Bundle bundle = this.getIntent().getExtras();
        action = bundle.getInt("action");
        //宣告callback Manager
        callbackManager = CallbackManager.Factory.create();

        if (action == 0) {
            setContentView(R.layout.register);
            register();
        } else {
            setContentView(R.layout.login);
            login();
        }

        accessTokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(
                    AccessToken oldAccessToken,
                    AccessToken currentAccessToken) {
                // Set the access token using
                // currentAccessToken when it's loaded or set.
                // If the access token is available already assign it.
                accessToken = AccessToken.getCurrentAccessToken();
                userAccount.ACCOUNT_TOKEN = accessToken.getToken();
                userAccount.SaveUserData();

                Log.d("FB", "access token got:" + accessToken.getToken());
            }

        };
        accessTokenTracker.startTracking();
        LoginManager.getInstance().registerCallback(callbackManager, facebookcallback);



        if ( accessToken != null) {
            Log.d("FB", "access token got:" + accessToken.getToken());
        }

    }

    @Override
    public void onStop() {
        super.onStop();
        accessTokenTracker.stopTracking();
    }

    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.btn_facebook) {
            if (accessToken == null) {
                LoginManager.getInstance().logInWithReadPermissions(RegisterLoginActivity.this, Arrays.asList("public_profile", "user_friends", "email"));
            } else {
                getFacebookUserInfo();
            }
        }
        if (v.getId() == R.id.btn_facebook_log) {
            if (accessToken == null) {
                LoginManager.getInstance().logInWithReadPermissions(RegisterLoginActivity.this, Arrays.asList("public_profile", "user_friends", "email"));
            } else {
                getFacebookUserInfo();
            }
        }
        if (v.getId() == R.id.btn_google) {
            getUserProfile();
        }
        if (v.getId() == R.id.btn_google_log) {
            getUserProfile();
        }
        if (v.getId() == R.id.btn_email) {
            Intent intent = new Intent();
            intent.setClass(RegisterLoginActivity.this, RegisterByEmailActivity.class);
            startActivity(intent);
        }
        if (v.getId() == R.id.btn_email_log) {
            Intent intent = new Intent();
            intent.setClass(RegisterLoginActivity.this, LoginByEmailActivity.class);
            startActivity(intent);
        }
        if (v.getId() == R.id.btn_login) {
            setContentView(R.layout.login);
            login();
        }
        if (v.getId() == R.id.btn_register) {
            setContentView(R.layout.register);
            register();
        }
    }

    private FacebookCallback<LoginResult> facebookcallback = new FacebookCallback<LoginResult>() {
        //登入成功
        @Override
        public void onSuccess(LoginResult loginResult) {
            //accessToken之後或許還會用到 先存起來
            accessToken = loginResult.getAccessToken();

            Log.d("FB", "access token got.");
            getFacebookUserInfo();

        }

        //登入取消
        @Override
        public void onCancel() {
            Log.d("FB", "CANCEL");
        }

        //登入失敗
        @Override
        public void onError(FacebookException exception) {
            Log.d("FB", "FB onError : " + exception.toString());
        }
    };

    private void getFacebookUserInfo() {
        //send request and call graph api
        GraphRequest request = GraphRequest.newMeRequest(accessToken, new GraphRequest.GraphJSONObjectCallback() {
            //當RESPONSE回來的時候
            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {

                //讀出姓名 ID FB個人頁面連結
                Log.d("FB", "complete");
                Log.d("FB", object.optString("name"));
                Log.d("FB", object.optString("link"));
                Log.d("FB", object.optString("id"));
                Log.d("FB", object.optString("email"));

                String name = object.optString("name");
                String email = object.optString("id");
                String token = accessToken.getToken();

                // Auth to IOT
                if(loading == null){
                    loading = UIComponent.createProgressDialog(RegisterLoginActivity.this);
                }
                loading.show();
                if(action == 0) {
                    iotManager.register(UserAccount.OAUTHTYPE.FACEBOOK,name,email,null,token,iotResult);
                }else{
                    iotManager.login(UserAccount.OAUTHTYPE.FACEBOOK,name,email,null,token,iotResult);
                }
            }
        });

        //包入你想要得到的資料 送出request
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,link,email");
        request.setParameters(parameters);
        request.executeAsync();
    }

    private void register(){
        action = 0;
        mbtn_facebook_reg = (Button) findViewById(R.id.btn_facebook);
        mbtn_google_reg = (Button) findViewById(R.id.btn_google);
        mbtn_email_reg = (Button) findViewById(R.id.btn_email);
        mbtn_login = (Button) findViewById(R.id.btn_login);

        mbtn_facebook_reg.setOnClickListener(this);
        mbtn_google_reg.setOnClickListener(this);
        mbtn_email_reg.setOnClickListener(this);
        mbtn_login.setOnClickListener(this);
    }

    private void login(){
        action = 1;
        mbtn_facebook_log = (Button) findViewById(R.id.btn_facebook_log);
        mbtn_google_log = (Button) findViewById(R.id.btn_google_log);
        mbtn_email_log = (Button) findViewById(R.id.btn_email_log);
        mbtn_register = (Button) findViewById(R.id.btn_register);

        mbtn_facebook_log.setOnClickListener(this);
        mbtn_google_log.setOnClickListener(this);
        mbtn_email_log.setOnClickListener(this);
        mbtn_register.setOnClickListener(this);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == REQUEST_CODE_CHOOSE_ACCOUNT){
            //This data is a result of the chooseAccount method
            if(resultCode == RESULT_OK){
                //Avalid Google Account was chosen
                userAccount.OAuthType = UserAccount.OAUTHTYPE.GOOGLE;
                userAccount.ACCOUNT_EMAIL = data.getStringExtra(AccountManager.KEY_ACCOUNT_NAME);
                userAccount.ACCOUNT_TOKEN = data.getStringExtra(AccountManager.KEY_AUTHTOKEN);


                //get the email from the returned data in Bundle using the KEY_ACCOUNT_NAME key
                if(isDeviceConnected()){
                    if(loading == null){
                        loading = UIComponent.createProgressDialog(RegisterLoginActivity.this);
                    }
                    loading.show();
                    new GoogleOAuth(this, userAccount, OAuthResult).execute();
                }
                else{
//                    TextView statusTextView = (TextView)findViewById(R.id.statusTextView);
//                    statusTextView.setText("Tests for one or more of the requirements above has failed, please rectify the problem and try again.");



                    //tv.setText("Tests for one or more of the requirements above has failed, please rectify the problem and try again.");
                }
            }
            else if(resultCode == RESULT_CANCELED){
                //The user did not choose a valid Google Account
//                TextView selectedAccountTextView = (TextView) findViewById(R.id.selectedAccountTextView);
//                selectedAccountTextView.setText("No Account has been selected, a Google Account is required for this demo");
            }
        }
        else if (requestCode == REQUEST_CODE_USER_RECOVERABLE_ERROR) {
            if(resultCode == RESULT_OK){
                new GoogleOAuth(this, userAccount, OAuthResult).execute();
            } else if(resultCode == RESULT_CANCELED){

            }
        }
        // for facebook
        else{
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    public boolean isDeviceConnected(){
        ConnectivityManager connMgr = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isConnected()) {
            //networkConnectionTextView.setText("Internet Connection is available");
            return true;
        }
        else {
            //networkConnectionTextView.setText("No Internet Connection is available, please ensure that Wifi or Mobile Data is turned on");
            return false;
        }
    }

    public void getUserProfile(){
        if(isGoogleServicesAvailable()) {
            chooseAccount();
        }
        else {

        }
    }

    public boolean isGoogleServicesAvailable(){
        int result = isGooglePlayServicesAvailable(this);
        if(result == ConnectionResult.SUCCESS){
            //Google Play services installed, enabled and available
            //googlePlayServicesTextView.setText("Google Play Services is available");
            return true;
        }
        else{
            //Google Play Services not available, show dialog with further instructions
            //googlePlayServicesTextView.setText("Google Play Services is not enabled or installed, this is Required for this demo. Please install or update Google Play services from the Play Store.");
            return false;
        }
    }

    public void chooseAccount(){
        Intent intent = AccountPicker.newChooseAccountIntent(null, null, new String[]{GoogleAuthUtil.GOOGLE_ACCOUNT_TYPE}, true, null, null, null, null);
        startActivityForResult(intent,REQUEST_CODE_CHOOSE_ACCOUNT);
    }

    private void mainPage(){
        Intent intent = new Intent();
        Bundle extra = new Bundle();
        intent.putExtras(extra);
        intent.setClass(RegisterLoginActivity.this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }


    final private LoginActivity.MsgHandler OAuthResult = new LoginActivity.MsgHandler(this) {
        @Override
        public void handleMessage(android.os.Message msg) {
            super.handleMessage(msg);

            if (msg.what == 1) {
                Log.d("Google", userAccount.ACCOUNT_NAME);
                Log.d("Google", userAccount.ACCOUNT_EMAIL);
                Log.d("Google", userAccount.ACCOUNT_TOKEN);

                // Auth to IOT -- Do Register

                if(action == 0){
                    iotManager.register(UserAccount.OAUTHTYPE.GOOGLE,userAccount.ACCOUNT_NAME,userAccount.ACCOUNT_EMAIL,null,userAccount.ACCOUNT_TOKEN,iotResult);
                }else{
                    iotManager.login(UserAccount.OAUTHTYPE.GOOGLE, userAccount.ACCOUNT_NAME, userAccount.ACCOUNT_EMAIL, null, userAccount.ACCOUNT_TOKEN, iotResult);
                }

            }
            else {
                /** Google Oauth Fail **/
                if(loading.isShowing()) {
                    loading.dismiss();
                }
                showAlertWithMsg();
            }
        }
    };

    final private  LoginActivity.MsgHandler iotResult = new LoginActivity.MsgHandler(this) {
        public void handleMessage(android.os.Message msg) {
            super.handleMessage(msg);

            if(loading.isShowing()) {
                loading.dismiss();
            }
            if(msg.what == 1){
                mainPage();
            }else{
                Log.d("RegisterLogin","RegisterLogin Fail!");
                showAlertWithMsg();
            }
        }
    };


    private void showAlertWithMsg() {
        String titleString;
        if(action == 0){
            titleString = getString(R.string.register_fail);
        }else{
            titleString = getString(R.string.login_fail);
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(titleString)
                .setPositiveButton(R.string.settings_button_ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }


}

