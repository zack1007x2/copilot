package com.moremote.copilot.interfaces;

import java.nio.ByteBuffer;

/**
 * Created by Zack on 2016/1/15.
 */
public interface onUVCPreviewCallBack {
    void onUVCPreviewFrame(ByteBuffer frame);
}
