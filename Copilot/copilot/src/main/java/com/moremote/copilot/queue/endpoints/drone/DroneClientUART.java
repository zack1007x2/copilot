package com.moremote.copilot.queue.endpoints.drone;

import android.util.Log;

import com.moremote.copilot.IPCamApplication;
import com.moremote.copilot.enums.APP_STATE;
import com.moremote.copilot.queue.endpoints.DroneClient;
import com.moremote.copilot.queue.endpoints.thread.ThreadReaderUART;
import com.moremote.copilot.viewadapters.devicelist.ItemPeerDevice;

import java.io.IOException;

/**
 * Created by Zack on 15/9/25.
 */
public class DroneClientUART extends DroneClient {


    private static final String TAG = DroneClientUART.class.getSimpleName();
    private static final int SIZEBUFF = 1024;

    private ThreadReaderUART readerThreadUART;
    private IPCamApplication hub;

    public DroneClientUART(IPCamApplication hub) {
        super(hub, SIZEBUFF);
        this.hub = hub;
    }

    @Override
    public void startClient(ItemPeerDevice drone) throws Exception {
        readerThreadUART = new ThreadReaderUART(ConnMsgHandler);

        if(readerThreadUART.openUart()>0){
            readerThreadUART.start();
            IPCamApplication.sendAppMsg(APP_STATE.MSG_DRONE_CONNECTED);
        }else{
            throw new Exception("Open Uart Failed");
        }
    }


    @Override
    public void stopClient() {
        Log.d(TAG, "Closing connection..");

        stopMsgHandler();

        // stop thread
        if (isConnected()) {
            readerThreadUART.stopMe();
        }

        IPCamApplication.sendAppMsg(APP_STATE.MSG_DRONE_DISCONNECTED);
    }

    @Override
    public boolean isConnected() {
        if (null == readerThreadUART) {
            return false;
        }
        else {
            return readerThreadUART.isRunning();
        }
    }

    @Override
    public String getMyName() {
        return "UART";
    }

    @Override
    public String getMyAddress() {
        return null;
    }

    @Override
    public String getPeerName() {
        return null;
    }

    @Override
    public String getPeerAddress() {
        return null;
    }

    @Override
    public boolean writeBytes(byte[] bytes) throws IOException {
        if (isConnected()) {
            readerThreadUART.writeBytes(bytes);
            return true;
        }
        return false;
    }



}
