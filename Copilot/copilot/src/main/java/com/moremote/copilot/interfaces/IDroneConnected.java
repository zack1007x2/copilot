package com.moremote.copilot.interfaces;

public interface IDroneConnected {

	void onDroneConnected();

}
