package com.moremote.copilot.drone;

import com.moremote.copilot.interfaces.IDroneStateChangeListener;

import java.util.ArrayList;

/**
 * Created by Zack on 2016/1/29.
 */
public class State {

    private long mMode;
    private boolean isFlying;
    private boolean isArmed;
    private boolean isConnected;
    private ArrayList<Object> mStateChangeListeners = new ArrayList<>();

    public State() {
    }

    public boolean isArmed() {
        return isArmed;
    }

    public void setIsArmed(boolean isArmed) {
        this.isArmed = isArmed;
        for(Object mStateChangeListener:mStateChangeListeners)
            ((IDroneStateChangeListener)mStateChangeListener).onArmedChange(isArmed);
    }

    public boolean isConnected() {
        return isConnected;
    }

    public void setIsConnected(boolean isConnected) {
        this.isConnected = isConnected;
        for(Object mStateChangeListener:mStateChangeListeners)
            ((IDroneStateChangeListener)mStateChangeListener).onConnectionChange(isConnected);
    }

    public boolean isFlying() {
        return isFlying;
    }

    public void setIsFlying(boolean isFlying) {
        this.isFlying = isFlying;
        for(Object mStateChangeListener:mStateChangeListeners)
            ((IDroneStateChangeListener)mStateChangeListener).onFlyingStateChange(isFlying);
    }

    public long getMode() {
        return mMode;
    }

    public void setMode(long newMode) {
        if(newMode!=mMode){
            mMode = newMode;
            for(Object mStateChangeListener:mStateChangeListeners)
                ((IDroneStateChangeListener)mStateChangeListener).onModeChange(mMode);
        }
    }

    public void registerStateChangeListener(Object stateChangeListener){
        mStateChangeListeners.add(stateChangeListener);
    }

    public void unregisterStateChangeListener(Object stateChangeListener){
        mStateChangeListeners.remove(stateChangeListener);
    }

}
