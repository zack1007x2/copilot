package com.moremote.copilot.queue.items;

import com.MAVLink.MAVLinkPacket;
import com.MAVLink.Messages.MAVLinkMessage;
import com.moremote.copilot.enums.MSG_SOURCE;

import java.io.Serializable;

public class ItemMavLinkMsg implements Serializable {

	/**
	 * serializable MavlinkMsg extended with packet data and the in-stream repetition count
	 */
	private static final long serialVersionUID = -2616788128278070587L;

	public final int count; // how many times the the same msg was repeated
	public int seqNo;
	public MAVLinkMessage msg;
	public long timestamp;
	public MSG_SOURCE direction;
	public int msgId;

//	public int[] ch = new int[10];

	public ItemMavLinkMsg(MAVLinkPacket pkt, MSG_SOURCE direction, int count) {
		super();
		this.count = count;
		msg = pkt.unpack();
		seqNo = pkt.seq;
		msg.sysid = pkt.sysid;
		timestamp = System.currentTimeMillis();
		msgId = pkt.msgid;
		this.direction = direction;
	}

	public String countToString() {
		return String.valueOf(count);
	}

	@Override
	public String toString() {
		return "msgId = " + msgId + " |  CompId:" + msg.compid +" |  SysId:" + msg.sysid + " |  " +
				"SeqNo:"	+ seqNo + "|  msg =  " + msg.toString();
	}

	public String humanDecode() {
		return toString();
	}

	public byte[] getPacketBytes() {
		final MAVLinkPacket pkt = msg.pack();
		//put back the real sysid, compid...
		pkt.seq = seqNo;
		pkt.sysid = msg.sysid;
		pkt.compid = msg.compid;
		return pkt.encodePacket();
	}

}
