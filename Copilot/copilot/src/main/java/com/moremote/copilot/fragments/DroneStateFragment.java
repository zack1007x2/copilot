package com.moremote.copilot.fragments;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.moremote.copilot.IPCamApplication;
import com.moremote.copilot.R;
import com.moremote.copilot.enums.APP_STATE;
import com.moremote.copilot.enums.COPILOT_PREFS;
import com.moremote.copilot.interfaces.IDataUpdateByteLog;
import com.moremote.copilot.interfaces.IQueueMsgItemReady;
import com.moremote.copilot.queue.items.ItemMavLinkMsg;
import com.moremote.copilot.viewadapters.ViewAdapterAnalyzerList;

import java.util.ArrayList;
import java.util.Set;

/**
 * Created by Zack on 2015/11/13.
 */
public class DroneStateFragment extends HUBFragment implements IDataUpdateByteLog,
        IQueueMsgItemReady {
    private static final String TAG = DroneStateFragment.class.getSimpleName();
    private ViewAdapterAnalyzerList listAdapterAnalyzer;
    private ListView listViewAnalyzer;

    private TextView textViewNoData;
    Set<String> logfilter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_drone_output_log, container, false);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        textViewNoData = (TextView) (getView().findViewById(R.id.textView_analyzer_no_data));

        listAdapterAnalyzer = new ViewAdapterAnalyzerList(hub, new ArrayList<ItemMavLinkMsg>());
        listViewAnalyzer = (ListView) (getView().findViewById(R.id.listView_analyzer_msg_list));
        listViewAnalyzer.setAdapter(listAdapterAnalyzer);
    }

    @Override
    public void onResume() {
        super.onResume();
        IPCamApplication.messenger.register(this, APP_STATE.MSG_QUEUE_MSGITEM_READY);
        IPCamApplication.messenger.register(this, APP_STATE.MSG_DATA_UPDATE_BYTELOG);
        logfilter = hub.prefs.getStringSet(COPILOT_PREFS.PREF_FILTER_DRONE_LOG, null);

    }

    @Override
    public void onPause() {
        super.onPause();
        IPCamApplication.messenger.unregister(this, APP_STATE.MSG_QUEUE_MSGITEM_READY);
        IPCamApplication.messenger.unregister(this, APP_STATE.MSG_DATA_UPDATE_BYTELOG);
    }

    @Override
    public void onDataUpdateByteLog() {

        final TextView mTextViewBytesLog = (TextView) (getView().findViewById(R.id.textView_logByte));

        mTextViewBytesLog.setText(IPCamApplication.logger.getByteLog());

        if (hub.prefs.getBoolean("pref_byte_log_autoscroll", true)) {
            // scroll down
            final ScrollView mScrollView = (ScrollView) (getView().findViewById(R.id.scrollView_logByte));
            if (null != mScrollView) {
                mScrollView.post(new Runnable() {
                    @Override
                    public void run() {
                        mScrollView.fullScroll(View.FOCUS_DOWN);
                    }
                });
            }
        }
    }

    @Override
    public void onQueueMsgItemReady(ItemMavLinkMsg msgItem) {
        if (null != msgItem) {
            logfilter = hub.prefs.getStringSet(COPILOT_PREFS.PREF_FILTER_DRONE_LOG,null);
            if(logfilter != null)
                Log.d(TAG,logfilter.toString());

            listAdapterAnalyzer.add(msgItem);
            if(textViewNoData.getVisibility()==View.VISIBLE){
                textViewNoData.setVisibility(View.GONE);
            }
        }
        // scroll down on pref
        if (hub.prefs.getBoolean("pref_msg_items_autoscroll", true)) {
            // trim only if autoscroll enabled
            while (listAdapterAnalyzer.getCount() > IPCamApplication.visibleMsgList) {
                listAdapterAnalyzer.remove(listAdapterAnalyzer.getItem(0));
            }

            listViewAnalyzer.setSelection(listAdapterAnalyzer.getCount());
        }

    }
}
