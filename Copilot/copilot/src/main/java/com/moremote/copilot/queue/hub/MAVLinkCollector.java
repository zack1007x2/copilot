// $codepro.audit.disable com.instantiations.assist.eclipse.analysis.audit.rule.effectivejava.alwaysOverridetoString.alwaysOverrideToString
package com.moremote.copilot.queue.hub;


import com.moremote.copilot.IPCamApplication;

public class MAVLinkCollector {

	@SuppressWarnings("unused")
	private static final String TAG = MAVLinkCollector.class.getSimpleName();

	private final IPCamApplication hub;

	private ThreadCollectorParser parserThread;

	public MAVLinkCollector(IPCamApplication hubContext) {

		hub = ((IPCamApplication) hubContext.getApplicationContext());

	}

	public void startMAVLinkParserThread() {

		parserThread = new ThreadCollectorParser(hub);
		parserThread.start();
	}

	public void stopMAVLinkParserThread() {
		if (null != parserThread) parserThread.stopMe();
	}

}
