package com.moremote.copilot.media.audioback;

import android.annotation.SuppressLint;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.media.MediaCodec;
import android.media.MediaFormat;

import java.io.IOException;
import java.nio.ByteBuffer;

/**
 * Created by Ray on 2015/5/8.
 */
public class AudioBack {

    private AudioTrack player;
    private MediaCodec decoder;
    private short audioFormat = AudioFormat.ENCODING_PCM_16BIT;

    public AudioBack(int sampleRate){

        if(setDecoder(sampleRate)){
            decoder.start();
        }
        if(setPlayer(sampleRate)) {
            player.play();
        }
    }

    public void decodeAndPlay(byte[] data, int length){

        if((decoder == null) || (player == null)){
            return ;
        }
        ByteBuffer[] inputBuffers;
        ByteBuffer[] outputBuffers;

        ByteBuffer inputBuffer;
        ByteBuffer outputBuffer;

        MediaCodec.BufferInfo bufferInfo;
        int inputBufferIndex;
        int outputBufferIndex;
        byte[] outData;
        try
        {
            inputBuffers = decoder.getInputBuffers();
            outputBuffers = decoder.getOutputBuffers();
            inputBufferIndex = decoder.dequeueInputBuffer(-1);
            if (inputBufferIndex >=0 )
            {
                inputBuffer = inputBuffers[inputBufferIndex];
                inputBuffer.clear();
                inputBuffer.put(data);
                decoder.queueInputBuffer(inputBufferIndex, 0, length, 0, 0);
            }
            bufferInfo = new MediaCodec.BufferInfo();

            outputBufferIndex = decoder.dequeueOutputBuffer(bufferInfo, 0);
            while (outputBufferIndex >=0 )
            {
                outputBuffer = outputBuffers[outputBufferIndex];

                outputBuffer.position(bufferInfo.offset);
                outputBuffer.limit(bufferInfo.offset + bufferInfo.size);

                outData = new byte[bufferInfo.size];
                outputBuffer.get(outData);

                //  Log.d("AudioDecoder", outData.length + " bytes decoded");
                player.write(outData, 0, outData.length);
                decoder.releaseOutputBuffer(outputBufferIndex, false);
                outputBufferIndex = decoder.dequeueOutputBuffer(bufferInfo, 0);

            }
        }
        catch (Exception e){
            e.printStackTrace();
        }

    }

    public void stop(){

        if(decoder != null) {
            decoder.stop();
            decoder.release();
            decoder = null;
        }
        if(player != null) {
            player.release();
            player = null;
        }
    }


    @SuppressLint("NewApi")
    private boolean setDecoder(int rate)
    {
        try {
            decoder = MediaCodec.createDecoderByType("audio/mp4a-latm");
        } catch (IOException e) {
            e.printStackTrace();
        }
        MediaFormat format = new MediaFormat();
        format.setString(MediaFormat.KEY_MIME, "audio/mp4a-latm");
        format.setInteger(MediaFormat.KEY_CHANNEL_COUNT, 1);
        format.setInteger(MediaFormat.KEY_SAMPLE_RATE, rate);
        byte[] bytes = new byte[]{(byte)18,(byte)8};//get from encoder
        ByteBuffer bb = ByteBuffer.wrap(bytes);
        format.setByteBuffer("csd-0", bb);
        decoder.configure(format, null, null, 0);
        return true;
    }

    private boolean setPlayer(int rate)
    {
        int bufferSizePlayer = AudioTrack.getMinBufferSize(rate, AudioFormat.CHANNEL_OUT_MONO, audioFormat);
        player= new AudioTrack(AudioManager.STREAM_MUSIC, rate, AudioFormat.CHANNEL_OUT_MONO, audioFormat, bufferSizePlayer, AudioTrack.MODE_STREAM);
        if (player.getState() == AudioTrack.STATE_INITIALIZED){
            return true;
        }
        else{
            return false;
        }
    }
}
