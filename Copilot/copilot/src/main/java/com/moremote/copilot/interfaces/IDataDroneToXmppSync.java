package com.moremote.copilot.interfaces;


import com.moremote.copilot.queue.items.ItemMavLinkMsg;

/**
 * Created by Zack on 15/10/5.
 */
public interface IDataDroneToXmppSync {

    void onReceiveDroneData(ItemMavLinkMsg msgItem);
}
