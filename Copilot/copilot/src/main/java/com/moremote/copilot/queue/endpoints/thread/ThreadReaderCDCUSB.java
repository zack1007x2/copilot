package com.moremote.copilot.queue.endpoints.thread;

import android.os.Handler;

import com.hoho.android.usbserial.driver.UsbSerialDriver;
import com.moremote.copilot.enums.CONNECTOR_STATE;

import java.io.IOException;
import java.nio.ByteBuffer;

/**
 * Created by Zack on 15/10/16.
 */
public class ThreadReaderCDCUSB extends Thread {
    @SuppressWarnings("unused")
    private static final String TAG = ThreadReaderCDCUSB.class.getSimpleName();

    private static final int BUFFSIZE = 1024 * 4;

    private final Handler connHandler;

    private boolean running = true;

    UsbSerialDriver usbDriver;

    public ThreadReaderCDCUSB(UsbSerialDriver usbDriver, Handler handlerReceiver) {

        connHandler = handlerReceiver;
        this.usbDriver = usbDriver;

    }

    public void run() {
        final byte[] buffer = new byte[BUFFSIZE];
        int len = 0; // bytes received

        while (running) {


            if (usbDriver!=null) {


                try {
                    len = usbDriver.read(buffer, 200);
                } catch (IOException e) {
                    e.printStackTrace();
                }

//                Log.d(TAG, "ThreadReaderCDCUSB read len:" + len);

                if (len > 0) {

                    final ByteBuffer byteMsg = ByteBuffer.allocate(len);

                    byteMsg.put(buffer, 0, len);
                    byteMsg.flip();

                    connHandler.obtainMessage(CONNECTOR_STATE.MSG_CONN_BYTE_DATA_READY.ordinal(), len, -1, byteMsg).sendToTarget();
                }

				/*
				 * else { // could happen mostly to the servers thread Log.d(TAG,
				 * "** Server lost connection **");
				 * connHandler.obtainMessage
				 * (CONNECTOR_STATE.MSG_CONN_SERVER_CLIENT_DISCONNECTED
				 * .ordinal()).sendToTarget(); running = false; }
				 */

            }
        }

        try {
            usbDriver.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void writeBytes(byte[] bytes) {
        try {
            usbDriver.write(bytes, 500);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void stopMe() {
        // stop threads run() loop
        running = false;
        // stop it's handler as well
        connHandler.obtainMessage(CONNECTOR_STATE.MSG_CONN_CLOSED.ordinal()).sendToTarget();
    }

    public boolean isRunning() {
        return running;
    }
}
