package com.moremote.copilot.utils;

/**
 * Created by Zack on 15/11/10.
 */
public class IntentAction {
    public final static String OnAuthenticated = "authenticated";
    public final static String OnConnectionClosedOnError = "connectionClosedOnError";
    public final static String OnPictureUploadOAuthTypeGoogle = "PictureUploadOAuthTypeGoogle";
    public final static String NotifyUserConnectionChanged = "NotifyUserConnectionChanged";
    public final static String SurfaceCreated = "surfaceCreated";
}
