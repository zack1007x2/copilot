package com.moremote.copilot.enums;

/**
 * Created by Zack on 2015/11/16.
 */
public class COPILOT_PREFS {
    public static final String PREF_ACTION_LOGOUT = "pref_action_logout";
    public static final String PREF_HISTROY_EVENTS = "pref_history_events";
    public static final String PREF_MOTION_DETECT = "pref_motion_detect";
    public static final String PREF_CRAWL_DETECT = "pref_crawl_detect";
    public static final String PREF_LOG_MAVLINK_BYTE = "pref_log_mavlink_byte";
    public static final String PREF_LOG_SYSTEM = "pref_log_system";
    public static final String PREF_LOG_MAVLINK_FOLDER = "pref_log_mavlink_folder";
    public static final String PREF_USB_BAUD = "pref_usb_baud";
    public static final String PREF_MSG_ITEMS_AUTOSCROLL = "pref_msg_items_autoscroll";
    public static final String PREF_BYTE_LOG_AUTOSCROLL= "pref_byte_log_autoscroll";
    public static final String PREF_FILTER_DRONE_LOG = "pref_drone_log_filter";
    public static final String PREF_FILTER_SYS_LOG = "pref_sys_log_filter";
    public static final String PREF_MAV_BYTE_LOG_OUTPUT = "pref_log_mavlink_byte_output";
    public static final String PREF_MAV_SYS_LOG_OUTPUT = "pref_log_system_output";
    public static final String PREF_UVC_CAMERA = "pref_uvc_camera";
    public static final String PREF_ENABLE_RECORD = "pref_enable_record";
    public static final String PREF_ENABLE_VA_LANDING = "pref_enable_va_landing";
}
