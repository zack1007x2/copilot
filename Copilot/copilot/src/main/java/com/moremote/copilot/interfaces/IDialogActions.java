package com.moremote.copilot.interfaces;

public interface IDialogActions {

	void onDialogPositiveAction();

	void onDialogNegativeAction();

}
