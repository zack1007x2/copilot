package com.moremote.copilot.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.moremote.copilot.activity.Util.UIComponent;
import com.moremote.copilot.R;
import com.moremote.copilot.connection.oauth.IOTManager;
import com.moremote.copilot.connection.oauth.UserAccount;


/**
 * Created by register_login on 2015/8/5.
 */
public class RegisterByEmailActivity extends Activity {

    private static final String TAG = "RegisterByEmail";

    private TextView mtextview_register;
    private EditText minput_id, minput_email, minput_emailensure, minput_pw, minput_pwensure;
    private Button mbtn_register_email;
    private IOTManager iotManager;
    private ProgressDialog loading;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_email);

        iotManager = new IOTManager(this);

        mtextview_register = (TextView)findViewById(R.id.textview_register);
        minput_id = (EditText)findViewById(R.id.edit_signup_id);
        minput_email = (EditText)findViewById(R.id.edit_signup_email);
        minput_emailensure = (EditText)findViewById(R.id.edit_signup_email_confirm);
        minput_pw = (EditText)findViewById(R.id.edit_signup_pw);
        minput_pwensure = (EditText)findViewById(R.id.edit_signup_pw_confirm);
        mbtn_register_email = (Button)findViewById(R.id.btn_email_log);

        mbtn_register_email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            if(checkRegisterInfo()) {
                moremoteRegister();
            }else{
                showAlertWithMsg(getString(R.string.register_info_incorrect));
            }
            }
        });

    }


    private Boolean checkRegisterInfo () {

        if(minput_id.getText().length() == 0 || minput_email.getText().length() == 0 || minput_emailensure.getText().length() == 0
                || minput_pw.getText().length() == 0 || minput_pwensure.getText().length() == 0) {
            return false;
        } else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(minput_email.getText().toString()).matches()) {
            return false;
        }else if(!minput_email.getText().toString().equals(minput_emailensure.getText().toString())){
            return false;
        }else if(!minput_pw.getText().toString().equals(minput_pwensure.getText().toString())) {
            return false;
        }

        return true;
    }


    private void moremoteRegister() {

        if(loading == null){
            loading = UIComponent.createProgressDialog(RegisterByEmailActivity.this);
        }
        loading.show();
        iotManager.register(UserAccount.OAUTHTYPE.MOREMOTE,minput_id.getText().toString(),minput_email.getText().toString(),minput_pw.getText().toString(),null,registerResult);

    }

    private void mainPage() {

        Intent intent = new Intent();
        intent.setClass(RegisterByEmailActivity.this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();

    }

    final private LoginActivity.MsgHandler registerResult = new LoginActivity.MsgHandler(this) {
        @Override
        public void handleMessage(android.os.Message msg) {
            super.handleMessage(msg);

            if(loading.isShowing()) {
                loading.dismiss();
            }

            if(msg.what == 1){
                mainPage();
            }else{
                Log.d(TAG,"Register By Email Fail!");
                showAlertWithMsg(getString(R.string.register_email_fail));
            }
        }
    };

    private void showAlertWithMsg(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(message).setTitle(R.string.register_fail)
                .setPositiveButton(R.string.settings_button_ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }
}
