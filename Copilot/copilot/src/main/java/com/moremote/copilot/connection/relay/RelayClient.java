package com.moremote.copilot.connection.relay;

import android.util.Log;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.ClosedChannelException;
import java.nio.channels.ClosedSelectorException;
import java.nio.channels.NotYetConnectedException;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

public class RelayClient {

	public static final String TAG = "Relay";
	public static final int BUFFER_LENGTH = 1024;

	private SocketChannel socketChannel;
	private Selector selector;
	private boolean isStop;
	private boolean isConnectSuccess;
	private String serverIP;
	private int serverPort;
	private int connectNum;
	private RelayMessageListener messageListener;
	private RelayConnectionListener connectionListener;
	private String auth;
	private String uuid;
	private ByteBuffer byteBuffer;
	private byte[] authCommand;
	private Thread handleEventThread;

	private HashMap<String, ByteBuffer> AudiobyteBuffer;

	public RelayClient(String serverIP, int serverPort, boolean isIPCam, String uuid, String secret) {
		this.serverIP = serverIP;
		this.serverPort = serverPort;
		connectNum = 0;
		byteBuffer = ByteBuffer.allocate(102400);
		AudiobyteBuffer = new HashMap<String, ByteBuffer>();

		this.auth = null;
		this.uuid = null;
		this.isConnectSuccess = false;
		this.authCommand = createAuthCommand(isIPCam, uuid, secret);
		try {
			socketChannel = SocketChannel.open();
			socketChannel.configureBlocking(false);

			selector = Selector.open();
			socketChannel.register(selector, SelectionKey.OP_CONNECT);

		} catch (IOException e) {
			Log.e(TAG, "socket channel open failed");
		}
	}

	public void connect() {
		try {
			if (++connectNum > 5) {
				Log.d(TAG, "connect to server failed");
				isConnectSuccess = false;
				return;
			}
			Log.d(TAG, "connecting..");
			socketChannel.connect(new InetSocketAddress(serverIP, serverPort));
			auth();

			handleEventThread = new HandleEventThread();
			handleEventThread.start();
		} catch (IOException e) {
			Log.e(TAG, "connect IOException " + e.getMessage());
		} catch (Exception e) {
			Log.e(TAG, "reconnecting.." + e.getMessage());
			try {
				socketChannel.close();
				socketChannel = SocketChannel.open();
				socketChannel.configureBlocking(false);

				socketChannel.register(selector, SelectionKey.OP_CONNECT);
			} catch (IOException e1) {
				e1.printStackTrace();
			}

			connect();
		}finally {
			if (connectNum > 0) {
				//Log.d(TAG, "secret:" + auth);
				if (connectionListener != null) {
					connectionListener.authed(isConnectSuccess, uuid, auth);
				}
				connectNum = 0;
			}
		}
	}

	public boolean isAuthed() {
		return isConnectSuccess;
	}

	public void close() {
		try {
			isStop = true;
			messageListener = null;
			connectionListener = null;
			if (handleEventThread != null) {
				handleEventThread.interrupt();
			}
			socketChannel.close();
			selector.close();

			// clear buffer
			byteBuffer.clear();
			for (HashMap.Entry entry : AudiobyteBuffer.entrySet()) {
				ByteBuffer buf = (ByteBuffer)entry.getValue();
				buf.clear();
			}
			AudiobyteBuffer.clear();

		} catch (IOException e) {
			Log.e(TAG, "close IOException");
		}
	}

	public int send(byte[] buffer, int length) {
		if (isConnectSuccess) {
			Selector writeSelector = null;
			try {
				int res, retryCounts = 0;
				ByteBuffer buf = ByteBuffer.wrap(buffer, 0, length);
				writeSelector = Selector.open();
				SelectionKey key = socketChannel.register(writeSelector, SelectionKey.OP_WRITE);
				do {
					res = socketChannel.write(buf);
					if(res == 0) {
						if(retryCounts > 30){
							Log.d(TAG,"Send TCP Relay Packet Timeout,close socketchannel");
							return -1;
						}

						writeSelector.select(1000);
						retryCounts ++;
					}
				} while(buf.hasRemaining());
				writeSelector.close();
				writeSelector = null;
				return length;
			} catch (NotYetConnectedException e) {
				Log.e(TAG, "send NotYetConnectedException");
				return -1;
			} catch (ClosedChannelException e) {
				Log.e(TAG, "send channel is closed");
				return -1;
			} catch (IOException e) {
				Log.e(TAG, "send IOException");
				return -1;

			}finally {
				try {
					if (writeSelector != null)
						writeSelector.close();
				} catch (Exception e) {
					Log.e(TAG,"close Selector fail");
				}
			}
		}
		return -1;
	}

	public void setMessageListener(RelayMessageListener messageListener) {
		this.messageListener = messageListener;
	}

	public void setConnectionListener(RelayConnectionListener connectionListener) {
		this.connectionListener = connectionListener;
	}

	public String getUUID() {
		return uuid;
	}

	public String getAuth() {
		return auth;
	}

	private byte[] createAuthCommand(boolean isIPCam, String uuid, String secret) {
		byte[] uuidBytes = uuid.getBytes();
		byte[] authBytes = secret.getBytes();
		byte[] buffer = new byte[2 + uuidBytes.length + authBytes.length];

		buffer[0] = 0x31;
		buffer[1] = (byte) (isIPCam ? 0x2d : 0x31);
		System.arraycopy(uuidBytes, 0, buffer, 2, uuidBytes.length);
		System.arraycopy(authBytes, 0, buffer, 2 + uuidBytes.length, authBytes.length);

		return buffer;
	}

	private void auth() throws Exception {
		if(selector.select(3000) == 0 )
			throw new Exception("connect timeout");

		Set<SelectionKey> keys = selector.selectedKeys();
		Iterator<SelectionKey> keyIterator = keys.iterator();
		while (keyIterator.hasNext()) {
			SelectionKey key = (SelectionKey) keyIterator.next();
			SocketChannel socketChannel = (SocketChannel) key.channel();

			if (key.isConnectable()) {
				Log.d(TAG, "Found Server");

				if (socketChannel.isConnectionPending()) {
					socketChannel.finishConnect();
				}
				socketChannel.register(selector, SelectionKey.OP_READ);

				isConnectSuccess = true;
				if (socketChannel != null) {
					if(send(authCommand, authCommand.length) < 0 ) {
						isConnectSuccess = false;
						throw new Exception("send authCommand fail");
					}
					else {
						if(!isAuthSuccess()) {
							throw new Exception("recv authCommand fail");
						}
					}
				}
				key.interestOps(SelectionKey.OP_READ);
				return;
			}
			keyIterator.remove();
		}
	}

	private boolean isAuthSuccess() {
		byteBuffer.clear();
		isConnectSuccess = false;
		while(byteBuffer.position() < authCommand.length) {
			SelectionKey key = null;
			try {
				if(selector.select(1000) == 0) {
					if(++connectNum > 5) {
						return false;
					}
					else {
						continue;
					}
				}
				Set<SelectionKey> keys = selector.selectedKeys();
				Iterator<SelectionKey> keyIterator = keys.iterator();
				while (keyIterator.hasNext()) {
					key = (SelectionKey) keyIterator.next();
					SocketChannel socketChannel = (SocketChannel) key.channel();

					if (key.isReadable()) {
						if(socketChannel.read(byteBuffer) < 0) {
							return false;
						}
					}

					keyIterator.remove();
				}
			} catch (IOException e) {
				Log.e(TAG, "Event listener error");
				if (key != null)
					key.cancel();
				return false;
			} catch (ClosedSelectorException e) {
				Log.e(TAG, "closed selector exception");
				return false;
			}
		}
		byteBuffer.flip();
		if (byteBuffer.get(0) == 0x31 && byteBuffer.get(1) == 0x2e) {
			byte[] uuidBytes = new byte[32];
			byte[] authBytes = new byte[32];
			byteBuffer.position(2);
			byteBuffer.get(uuidBytes);
			byteBuffer.position(34);
			byteBuffer.get(authBytes);

			try {
				uuid = new String(uuidBytes, "UTF-8");
				auth = new String(authBytes, "UTF-8");
			} catch (UnsupportedEncodingException e) {
				Log.e(TAG, "isAuthSuccess UnsupportedEncodingException");
				return false;
			}

			byteBuffer.clear();
			isConnectSuccess = true;
		}
		//Log.d(TAG, "uuid "+ uuid + " auth "+ auth);
		return isConnectSuccess;
	}

	class HandleEventThread extends Thread {

		@Override
		public void run() {
			while (!isStop) {
				SelectionKey key = null;
				try {
					selector.select();
					Set<SelectionKey> keys = selector.selectedKeys();
					Iterator<SelectionKey> keyIterator = keys.iterator();
					while (keyIterator.hasNext()) {
						key = (SelectionKey) keyIterator.next();
						SocketChannel socketChannel = (SocketChannel) key.channel();
						if (socketChannel.isConnected()) {
							if (key.isReadable()) {
								read(socketChannel);
							}
							keyIterator.remove();
						} else {
							isStop = true;
						}
					}
				} catch (IOException e) {
					Log.e(TAG, "Event listener error");
					if(key != null)
						key.cancel();
				} catch (ClosedSelectorException e) {
					Log.e(TAG, "closed selector exception");
					isStop = true;
				} catch (java.util.ConcurrentModificationException e) {
					Log.e(TAG, "ConcurrentModification exception");
					isStop = true;
				}
			}
			Log.e(TAG, "HandleEventThread closed");
		}

		private void read(SocketChannel channel) throws IOException, RuntimeException {
			if ((channel.read(byteBuffer))> 0) {
				int Position=byteBuffer.position();
				byteBuffer.flip();
				int len = 0;
				while(byteBuffer.remaining() >= 8){
					// ID(4 byte) + Len(4byte)
					Position = byteBuffer.position();
					byte ClientID[] = new byte[4];
					byteBuffer.get(ClientID, 0, 4);
					String ID = new String(ClientID);
					byte PackageLen[] = new byte[4];
					byteBuffer.get(PackageLen, 0, 4);
					len = ((PackageLen[3] & 0xFF) << 24 ) | ((PackageLen[2] & 0xFF) << 16) | ((PackageLen[1] & 0xFF) << 8) | (PackageLen[0] & 0xFF);

					if(byteBuffer.remaining()>=len && len >= 0){
						byte[] data = new byte[len];
						byteBuffer.get(data);

						ByteBuffer buf = AudiobyteBuffer.get(ID);
						if (buf == null) {
							buf = ByteBuffer.allocate(102400);
							AudiobyteBuffer.put(ID, buf);
						}

						buf.put(data);
						playAudioBack(buf);
					}
					else {
						byteBuffer.position(Position);
						break;
					}

				}

				if(byteBuffer.position()!=0){
					byte[] tmp = new byte[byteBuffer.remaining()];//832+1=833
					byteBuffer.get(tmp);
					byteBuffer.clear();
					byteBuffer.put(tmp);
				}
				else{
					byteBuffer.position(Position);
					byteBuffer.limit(102400);
				}
			}
		}

		private void playAudioBack(ByteBuffer buf)
		{
			int Position=buf.position();
			buf.flip();
			while(buf.remaining()>2){
				int len =  ((buf.get(buf.position()) & 0xFF) << 8) | (buf.get(buf.position()+1) & 0xFF);
				if ( buf.remaining() >= len+2) {
					byte[] data = new byte[len];
					buf.position(buf.position() + 2);
					buf.get(data);
					if (messageListener != null) {
						messageListener.processMessage(data, len);
					}
				}
				else {
					break;
				}
			}

			if(buf.position()!=0){
				byte[] tmp = new byte[buf.remaining()];//832+1=833
				buf.get(tmp);
				buf.clear();
				buf.put(tmp);
			}
			else {
				buf.position(Position);
				buf.limit(102400);
			}
		}
	}

}
