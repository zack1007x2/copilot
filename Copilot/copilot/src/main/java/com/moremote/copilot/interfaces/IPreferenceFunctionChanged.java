package com.moremote.copilot.interfaces;

/**
 * Created by Zack on 2015/12/25.
 */
public interface IPreferenceFunctionChanged {
    void onEnableLandingChange();
    void onUVCEnableChange();
}
