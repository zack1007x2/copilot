package com.moremote.copilot.enums;

/**
 * Created by Zack on 15/10/28.
 */
public class NOTIFY_STATE {
    public final static String SERVICE_STATE_STANDBY = "StandBy";
    public final static String XMPP_STATE_NOT_CONNECT = "not connect";
    public final static String DRONE_STATE_NOT_CONNECT = "not connect";
    public final static String XMPP_STATE_CONNECT = "connected";
    public final static String XMPP_STATE_LOGIN_FAIL = "login fail";
    public final static String XMPP_STATE_SERVER_ERROR = "Server Error";
    public final static String DRONE_STATE_CONNECT = "connecting...";
    public final static String DRONE_STATE_RECONNECT = "reconnecting";
    public final static String DRONE_STATE_RECEIVING = "receiving msg";
    public final static String DRONE_STATE_DEVICE_NOT_FOUND = "Device not found";
    public final static String DRONE_STATE_UNSUPPORT_BLUETOOTH = "unsupport bt";
    public final static String DRONE_STATE_CONNECTION_NO_RESPONSE = "BT no response";
}
