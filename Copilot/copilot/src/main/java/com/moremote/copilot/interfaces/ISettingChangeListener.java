package com.moremote.copilot.interfaces;

/**
 * Created by Zack on 2015/12/11.
 */
public interface ISettingChangeListener {
    void onSettingChange(String key);
}
