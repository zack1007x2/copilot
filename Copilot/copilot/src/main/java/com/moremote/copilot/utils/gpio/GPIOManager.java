package com.moremote.copilot.utils.gpio;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by Zack on 2016/4/25.
 */
public class GPIOManager {
    private final static String TAG = GPIOManager.class.getSimpleName();
    private HashMap<Integer, GPIO> GpioMap = new HashMap<>();
    //INPUT
    private GPIO gpi123, gpi124, gpi125, gpi126;
    //OUTPUT
    private GPIO gpo127, gpo133, gpo134, gpo135, gpo40, gpo3, gpo1, gpo19;

    //fan
    private GPIO gpio44;

    private String usrFrom;

    public GPIOManager() {
        initGPIOs();
    }

    private void initGPIOs(){

        int ret;
        gpi123 = new GPIO(123);
        ret = gpi123.initPin("in");
        if(ret < 0){
            Log.e(TAG, "gpi123 init fail");
        }

        gpi124 = new GPIO(124);
        ret = gpi124.initPin("in");
        if(ret < 0){
            Log.e(TAG, "gpi124 init fail");
        }

        gpi125 = new GPIO(125);
        ret = gpi125.initPin("in");
        if(ret < 0){
            Log.e(TAG, "gpi125 init fail");
        }

        gpi126 = new GPIO(126);
        ret = gpi126.initPin("in");
        if(ret < 0){
            Log.e(TAG, "gpi126 init fail");
        }

        gpo127 = new GPIO(127);
        ret = gpo127.initPin("out");
        if(ret < 0){
            Log.e(TAG, "gpi127 init fail");
        }

        gpo133 = new GPIO(133);
        ret = gpo133.initPin("out");
        if(ret < 0){
            Log.e(TAG, "gpo133 init fail");
        }

        gpo134 = new GPIO(134);
        ret = gpo134.initPin("out");
        if(ret < 0){
            Log.e(TAG, "gpo134 init fail");
        }

        gpo135 = new GPIO(135);
        ret = gpo135.initPin("out");
        if(ret < 0){
            Log.e(TAG, "gpo135 init fail");
        }

        gpo40 = new GPIO(40);
        ret = gpo40.initPin("out");
        if(ret < 0){
            Log.e(TAG, "gpo40 init fail");
        }

        gpo3 = new GPIO(3);
        ret = gpo3.initPin("out");
        if(ret < 0){
            Log.e(TAG, "gpo3 init fail");
        }

        gpo1 = new GPIO(1);
        ret = gpo1.initPin("out");
        if(ret < 0){
            Log.e(TAG, "gpo1 init fail");
        }

        gpo19 = new GPIO(19);
        ret = gpo19.initPin("out");
        if(ret < 0){
            Log.e(TAG, "gpo19 init fail");
        }

        gpio44 = new GPIO(44);
        ret = gpio44.initPin("in");
        if(ret < 0){
            Log.e(TAG, "gpio44 init fail");
        }
        gpio44.setState(1);
        

        GpioMap.put(123, gpi123);
        GpioMap.put(124, gpi124);
        GpioMap.put(125, gpi125);
        GpioMap.put(126, gpi126);

        GpioMap.put(127, gpo127);
        GpioMap.put(133, gpo133);
        GpioMap.put(134, gpo134);
        GpioMap.put(135, gpo135);
        GpioMap.put(40, gpo40);
        GpioMap.put(3, gpo3);
        GpioMap.put(1, gpo1);
        GpioMap.put(19, gpo19);
    }

    public GPIO getGpioByPin(int pin){
        return GpioMap.get(pin);
    }

    public JSONObject getAllGPIO(){
        JSONObject ret = new JSONObject();
        for(Integer pin:GpioMap.keySet()){
            try {
                ret.put(String.valueOf(pin), GpioMap.get(pin).toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return ret;
    }
}
