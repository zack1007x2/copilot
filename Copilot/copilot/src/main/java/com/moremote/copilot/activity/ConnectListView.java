package com.moremote.copilot.activity;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.moremote.copilot.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by chingchun on 2015/8/11.
 */
public class ConnectListView {
    private Context mContext;
    private View mRootLayout;
//    private TextView mTitle;
    private ListView mList;

    public ConnectListView(Context context, View root){
        mContext = context;
        mRootLayout = root;
    }

    public void setConnectionList(String [] connection) {
//        mTitle = (TextView) mRootLayout.findViewById(R.id.connect_list_tv);
//        mTitle.setText(IPCamApplication.getDeviceName() + " / " + mContext.getResources().getString(R.string.connection_devices));
        mList = (ListView) mRootLayout.findViewById(R.id.connect_list);
        //mList.setAdapter(new ArrayAdapter<String>(mContext, android.R.layout.simple_list_item_1, connection));
        ArrayList<String> mStringList= new ArrayList<String>(Arrays.asList(connection));
        mList.setAdapter(new ConnectAdapter(mContext, mStringList));
        mList.deferNotifyDataSetChanged();
    }

    private class ConnectAdapter extends BaseAdapter {
        private LayoutInflater mInflater;
        private List<String> mConnection;

        /* class ViewHolder */
        private class ViewHolder {
            TextView text_name;
        }

        public ConnectAdapter(Context context, List<String> connection) {
            mInflater = LayoutInflater.from(context);
            mConnection = connection;
        }

        @Override
        public int getCount() {
            return mConnection.size();
        }

        @Override
        public Object getItem(int position) {
            return mConnection.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;

            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.connectlist_row,	null);
                holder = new ViewHolder();
                holder.text_name = (TextView) convertView.findViewById(R.id.connectlist_row_tv);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            holder.text_name.setText(mConnection.get(position).toString());
            return convertView;
        }
    }
}
