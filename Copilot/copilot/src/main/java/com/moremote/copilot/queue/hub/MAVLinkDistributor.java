// $codepro.audit.disable com.instantiations.assist.eclipse.analysis.audit.rule.effectivejava.alwaysOverridetoString.alwaysOverrideToString
package com.moremote.copilot.queue.hub;


import com.moremote.copilot.IPCamApplication;

public class MAVLinkDistributor {

	@SuppressWarnings("unused")
	private static final String TAG = MAVLinkDistributor.class.getSimpleName();

	private final IPCamApplication hub;

	private ThreadDistibutorSender distThread;

	public MAVLinkDistributor(IPCamApplication hubContext) {

		hub = ((IPCamApplication) hubContext.getApplicationContext());

	}

	public void startMAVLinkDistThread() {

		distThread = new ThreadDistibutorSender(hub);
		distThread.start();
	}

	public void stopMAVLinkDistThread() {
		if (null != distThread) distThread.stopMe();
	}

}
