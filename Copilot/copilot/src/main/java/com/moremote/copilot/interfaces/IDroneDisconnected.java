package com.moremote.copilot.interfaces;

public interface IDroneDisconnected {

	void onDroneDisconnected();

}
