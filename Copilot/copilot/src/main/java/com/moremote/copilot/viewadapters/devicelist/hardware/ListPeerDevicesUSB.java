// $codepro.audit.disable
// com.instantiations.assist.eclipse.analysis.audit.rule.effectivejava.alwaysOverridetoString.alwaysOverrideToString
package com.moremote.copilot.viewadapters.devicelist.hardware;

import android.content.Context;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.util.Log;

import com.ftdi.j2xx.D2xxManager;
import com.hoho.android.usbserial.driver.UsbSerialProber;
import com.moremote.copilot.IPCamApplication;
import com.moremote.copilot.enums.DEVICE_INTERFACE;
import com.moremote.copilot.enums.DEV_LIST_STATE;
import com.moremote.copilot.enums.PEER_DEV_STATE;
import com.moremote.copilot.viewadapters.devicelist.ItemPeerDevice;
import com.moremote.copilot.viewadapters.devicelist.ItemPeerDeviceUSB;
import com.moremote.copilot.viewadapters.devicelist.ListPeerDevices;

import java.util.List;

public class ListPeerDevicesUSB extends ListPeerDevices {

	@SuppressWarnings("unused")
	private static final String TAG = ListPeerDevicesUSB.class.getSimpleName();

	public ListPeerDevicesUSB(IPCamApplication hubContext) {
		super(hubContext);

	}

	public DEV_LIST_STATE refresh() {

		devList.clear();


		if(IPCamApplication.isFTDIdevice(hub)){
			int devCount = IPCamApplication.usbHub.createDeviceInfoList(hub);

			if (devCount > 0) {
				D2xxManager.FtDeviceInfoListNode[] deviceList = new D2xxManager.FtDeviceInfoListNode[devCount];
				IPCamApplication.usbHub.getDeviceInfoList(devCount, deviceList);

				for (int i = 0; i < devCount; i++) {
					ItemPeerDevice tmpItemPeerDevice = new ItemPeerDeviceUSB(DEVICE_INTERFACE.USB, deviceList[i].description, deviceList[i].serialNumber, deviceList[i].location);

					//check if connected !!!
					if (hub.droneClient.isConnected() & (hub.droneClient.getPeerAddress().equals(tmpItemPeerDevice.getAddress()))) {
						tmpItemPeerDevice.setState(PEER_DEV_STATE.DEV_STATE_CONNECTED);
					}

					devList.add(tmpItemPeerDevice);
				}

			}

			sort();
		}else{

            UsbManager manager = (UsbManager) hub.getSystemService(Context.USB_SERVICE);
			List<UsbDevice> availableDevices = UsbSerialProber.getAvailableSupportedDevices(manager);
			if (availableDevices.isEmpty()) {
				Log.d(TAG, "ListPeerDevicesUSB => No Devices found");
			}else{
				for (int i = 0; i < availableDevices.size(); i++) {
					ItemPeerDevice tmpItemPeerDevice = new ItemPeerDeviceUSB(DEVICE_INTERFACE
					.USB, availableDevices.get(i).getDeviceName(), availableDevices.get(i)
							.describeContents(),availableDevices.get(i).getVendorId(),
							availableDevices.get(i).getProductId());
                    if (hub.droneClient.isConnected()) {
                        tmpItemPeerDevice.setState(PEER_DEV_STATE.DEV_STATE_CONNECTED);
                    }
                    devList.add(tmpItemPeerDevice);
				}
			}
		}


		if (devList.size() > 0) {
			return DEV_LIST_STATE.LIST_OK_USB;
		}
		else {
			return DEV_LIST_STATE.ERROR_NO_USB_DEVICES;
		}

		// return DEV_LIST_STATE.ERROR_NO_ADAPTER;
		// return DEV_LIST_STATE.ERROR_ADAPTER_OFF;
		// return DEV_LIST_STATE.ERROR_NO_BONDED_DEV;

	}

}
