package com.moremote.copilot.connection.xmpp;


import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.moremote.copilot.connection.ipcam.wrap.UserStatus;

import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.ConnectionListener;
import org.jivesoftware.smack.ReconnectionManager;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.StanzaListener;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.chat.Chat;
import org.jivesoftware.smack.chat.ChatManager;
import org.jivesoftware.smack.chat.ChatManagerListener;
import org.jivesoftware.smack.chat.ChatMessageListener;
import org.jivesoftware.smack.filter.StanzaTypeFilter;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.packet.Stanza;
import org.jivesoftware.smack.roster.Roster;
import org.jivesoftware.smack.roster.RosterEntry;
import org.jivesoftware.smack.roster.RosterListener;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jivesoftware.smack.tcp.XMPPTCPConnectionConfiguration;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

/**
 * Created by lintzuhsiu on 14/10/21.
 */
public class XMPPConnector {

    public static final String TAG = "xmpp";
    public static final int DEFAULT_PORT = 5222;

    private String ip;
    private String username;
    private String password;
    private String resource;
    private ExecutorService threadPool;
    private XMPPTCPConnectionConfiguration connectConfig;
    private XMPPTCPConnection connection;
    private Roster roster;
    private List<ConnectionListener> connectionListeners;
    private java.util.concurrent.CopyOnWriteArrayList<ChatMessageListener> messageListeners;
    private List<RosterListener> rosterListeners;
    private HashMap<String, Chat> chats;

    public static class MessageType {
        public static int GET_ROSTER = 0;
    }

    public static class DataType {
        public static String FRIEND_LIST = "get_roster";
    }

    private ChatMessageListener defaultMessageListener = new ChatMessageListener() {

        @Override
        public void processMessage(Chat chat,
                                   org.jivesoftware.smack.packet.Message message) {
            for (ChatMessageListener listener : messageListeners) {
                listener.processMessage(chat, message);
            }
        }
    };

    private ConnectionListener defaultConnectionListener = new ConnectionListener() {

        @Override
        public void connected(XMPPConnection c) {
            setRosterListener(c);
            try {
                connection.login(username, password, resource);
            } catch (XMPPException e) {
                Log.d(TAG, "XMPPException");
            } catch (SmackException e) {
                Log.d(TAG, "SmackException");
            } catch (IOException e) {
                Log.d(TAG, "IOException");
            }

            for (ConnectionListener listener : connectionListeners) {
                listener.connected(connection);
            }
        }

        @Override
        public void authenticated(XMPPConnection connection, boolean resumed) {
            setMessageListener(defaultMessageListener);
            for (ConnectionListener listener : connectionListeners) {
                listener.authenticated(connection, resumed);
            }
        }

        @Override
        public void connectionClosed() {
            for (ConnectionListener listener : connectionListeners) {
                listener.connectionClosed();
            }
        }

        @Override
        public void connectionClosedOnError(Exception e) {
            for (ConnectionListener listener : connectionListeners) {
                listener.connectionClosedOnError(e);
            }
        }

        @Override
        public void reconnectingIn(int seconds) {
            for (ConnectionListener listener : connectionListeners) {
                listener.reconnectingIn(seconds);
            }
        }

        @Override
        public void reconnectionSuccessful() {
            for (ConnectionListener listener : connectionListeners) {
                listener.reconnectionSuccessful();
            }
        }

        @Override
        public void reconnectionFailed(Exception e) {
            for (ConnectionListener listener : connectionListeners) {
                listener.reconnectionFailed(e);
            }
        }
    };

    private RosterListener defaultRosterListener = new RosterListener() {
        @Override
        public void entriesAdded(Collection<String> addresses) {
            for (RosterListener listener : rosterListeners) {
                listener.entriesAdded(addresses);
            }
        }

        @Override
        public void entriesUpdated(Collection<String> addresses) {
            for (RosterListener listener : rosterListeners) {
                listener.entriesUpdated(addresses);
            }
        }

        @Override
        public void entriesDeleted(Collection<String> addresses) {
            for (RosterListener listener : rosterListeners) {
                listener.entriesDeleted(addresses);
            }
        }

        @Override
        public void presenceChanged(Presence presence) {
            for (RosterListener listener : rosterListeners) {
                listener.presenceChanged(presence);
            }
        }
    };

    private StanzaListener defaultPresenceListener = new StanzaListener() {
        @Override
        public void processPacket(Stanza packet) throws SmackException.NotConnectedException {

            Presence presence = (Presence) packet;
            if (presence.getType() == Presence.Type.subscribe) {

                // Subscribe each other
                //Log.d(TAG,"Subcribe request from : " + packet.getFrom());

                Presence subscribe = new Presence(Presence.Type.subscribe);
                subscribe.setTo(presence.getFrom());
                connection.sendStanza(subscribe);

            } else if (presence.getType() == Presence.Type.unsubscribe) {
                // unfriend each other
                //Log.d(TAG, "unSubcribe request from : " + packet.getFrom());

                try {
                    roster.removeEntry(roster.getEntry(packet.getFrom()));
                } catch (SmackException.NotLoggedInException e) {
                    e.printStackTrace();
                } catch (SmackException.NoResponseException e) {
                    e.printStackTrace();
                } catch (XMPPException.XMPPErrorException e) {
                    e.printStackTrace();
                }

            } else {

            }
        }
    };

    public XMPPConnector(Context context) {
        connectionListeners = new ArrayList<ConnectionListener>();
        messageListeners = new java.util.concurrent.CopyOnWriteArrayList<ChatMessageListener>();
        rosterListeners = new ArrayList<RosterListener>();
        chats = new HashMap<String, Chat>();
        threadPool = Executors.newSingleThreadExecutor(new ThreadFactory() {
            @Override
            public Thread newThread(Runnable r) {
                Thread thread = new Thread(r, "Background executor service");
                thread.setPriority(Thread.NORM_PRIORITY);
                thread.setDaemon(true);
                return thread;
            }
        });
    }

    public void removeMessageListener(ChatMessageListener listener) {
        for (int i = 0; i < messageListeners.size(); i++) {
            if (listener == messageListeners.get(i)) {
                messageListeners.remove(i);
                return;
            }
        }
    }

    public void addMessageListener(ChatMessageListener listener) {
        messageListeners.add(listener);
    }

    public void addConnectionListener(ConnectionListener listener) {
        connectionListeners.add(listener);
    }

    public void addRosterListener(RosterListener listener) {
        rosterListeners.add(listener);
    }

    public boolean loginToXMPPServer(final String username, final String password, final String resource) {
        final XMPPConnector self = this;
        String[] userData = null;

        if (!username.matches("^.*@.*$")) {
            return false;
        }

        userData = username.split("@");
        this.username = userData[0];
        this.ip = userData[1];
        this.password = password;
        this.resource = resource;

        threadPool.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    setConnectionConfig(ip, DEFAULT_PORT);
                    connection = new XMPPTCPConnection(connectConfig);
                    connection.addConnectionListener(defaultConnectionListener);
                    connection.addAsyncStanzaListener(defaultPresenceListener, new StanzaTypeFilter(Presence.class));
                    connection.connect();
                    /** Enable Reconnect */
                    ReconnectionManager reconnectionManager = ReconnectionManager.getInstanceFor(connection);
                    reconnectionManager.enableAutomaticReconnection();
                } catch (XMPPException e) {
                    Log.e(TAG, "XMPPException");
                } catch (SmackException e) {
                    defaultConnectionListener.connectionClosedOnError(e);
                } catch (IOException e) {
                    Log.e(TAG, "IOException");
                }
            }
        });

        return true;
    }

//    /*** Delete For Never Use - ken kao 11/3 ***/
//    public void createChat(final String username, final ChatMessageListener messageListener) {
//        messageListeners.add(messageListener);
//
//        threadPool.execute(new Runnable() {
//            @Override
//            public void run() {
//                ChatManager chatManager = ChatManager.getInstanceFor(connection);
//                Chat newChat = chatManager.createChat(username, messageListener);
//                chats.put(username, newChat);
//            }
//        });
//    }

    public void sendMessage(final String username, final String message) {
        threadPool.execute(new Runnable() {
            @Override
            public void run() {
                Chat chat = chats.get(username);

                if (chat == null) {
                    ChatManager chatManager = ChatManager.getInstanceFor(connection);
                    chat = chatManager.createChat(username, new ChatMessageListener() {
                        @Override
                        public void processMessage(Chat chat, org.jivesoftware.smack.packet.Message message) {
                        }
                    });
                    chats.put(username, chat);
                }

                try {
                    if (connection != null) {
                        chat.sendMessage(message);
                    }
                } catch (SmackException.NotConnectedException e) {
                    Log.e(TAG, "NotConnectedException");
                }
            }
        });
    }

    public void disconnect() {
        threadPool.execute(new Runnable() {
            @Override
            public void run() {

                if (connection != null) {
                    connection.disconnect();
                }

                connectionListeners.clear();
                messageListeners.clear();
                rosterListeners.clear();
                chats.clear();
            }
        });
    }

    public void changeStatus(final String status) {
        threadPool.execute(new Runnable() {
            @Override
            public void run() {
                Presence presence = new Presence(Presence.Type.available);
                presence.setStatus(status);
                try {
                    connection.sendStanza(presence);
                } catch (SmackException.NotConnectedException e) {
                    Log.d(TAG, "NotConnectedException");
                }
            }
        });
    }

    public void getRoster(HashMap<String, UserStatus> friends) {
        if (connection == null) {
            return;
        }

        Roster roster = Roster.getInstanceFor(connection);
        Collection<RosterEntry> entries = roster.getEntries();
        Presence presence;

        for (RosterEntry entry : entries) {
            presence = roster.getPresence(entry.getUser());
            UserStatus item = friends.get(entry.getUser());
            if (item == null) {
                item = new UserStatus();
            }

            item.setType(presence.getType().name());
            item.setStatus(presence.getStatus());
            friends.put(entry.getUser(), item);
        }
    }

    public void getFriendRoster(final Handler messageHandler) {
        threadPool.execute(new Runnable() {
            @Override
            public void run() {
                Collection<RosterEntry> entries = roster.getEntries();
                ArrayList<String> friendList = new ArrayList<String>();
                Message msg = new Message();
                Bundle extras = new Bundle();

                for (RosterEntry entry : entries) {
                    friendList.add(entry.getUser());
                }

                extras.putStringArrayList(DataType.FRIEND_LIST, friendList);
                msg.setData(extras);
                msg.what = MessageType.GET_ROSTER;
                messageHandler.sendMessage(msg);
            }
        });
    }

    private void setMessageListener(final ChatMessageListener messageListener) {
        ChatManager chatManager = ChatManager.getInstanceFor(connection);
        chatManager.addChatListener(new ChatManagerListener() {
            @Override
            public void chatCreated(Chat chat, boolean createdLocally) {
                chat.addMessageListener(messageListener);
            }
        });
    }

    public void addFriend(final String friendName, final String nickname) {
        threadPool.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    roster.createEntry(friendName, nickname, new String[]{});
                } catch (SmackException.NotLoggedInException e) {
                    Log.d(TAG, "NotLoggedInException");
                } catch (SmackException.NoResponseException e) {
                    Log.d(TAG, "NoResponseException");
                } catch (XMPPException.XMPPErrorException e) {
                    Log.d(TAG, "XMPPErrorException");
                } catch (SmackException.NotConnectedException e) {
                    Log.d(TAG, "NotConnectedException");
                }
            }
        });
    }

    private void setConnectionConfig(String xmppServerIP, int port) {
        //connectConfig = new ConnectionConfiguration(xmppServerIP, port);
        //connectConfig.setReconnectionAllowed(true);
        //connectConfig.setSecurityMode(ConnectionConfiguration.SecurityMode.disabled);
        //connectConfig.setDebuggerEnabled(true);

        connectConfig = XMPPTCPConnectionConfiguration.builder()
                .setHost(xmppServerIP)
                .setPort(port)
                .setServiceName(xmppServerIP)
                .setDebuggerEnabled(false)
                .setSecurityMode(ConnectionConfiguration.SecurityMode.disabled)
                .build();
    }

    private void setRosterListener(XMPPConnection connection) {
        roster = Roster.getInstanceFor(connection);
        roster.addRosterListener(defaultRosterListener);
    }

}

