package com.moremote.copilot.connection.oauth;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.FileEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;

/**
 * Created by gaozongyong on 2016/1/12.
 */
public class RestfulFile extends AsyncTask<String,Void,String> {

    private static final String TAG = "HttpPostFile";
    private Handler handler;
    private String uploadPath;
    private String authorize;
    public static final int UPLOAD_VIDEO_WHAT = 1111;
    public static final String key_status_code = "StatusCode";
    public static final String key_content = "Content";

    public RestfulFile(Handler handler , String uploadPath , String authorize) {
        this.handler = handler;
        this.uploadPath = uploadPath;
        this.authorize = authorize;
    }

    @Override
    protected String doInBackground(String... urls){

        String url = urls[0];
        String content="";
        try{


            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpPost post = new HttpPost(url);
            if(uploadPath != null) {
                File file = new File(uploadPath);
                FileEntity fileEntity = new FileEntity(file, "");
                post.addHeader(fileEntity.getContentType());
                post.setEntity(fileEntity);
            }
            post.addHeader("Authorization",authorize);
            HttpResponse resp = httpClient.execute(post);
            content = EntityUtils.toString(resp.getEntity());
            String status = String.valueOf(resp.getStatusLine().getStatusCode());
            Log.d(TAG, "Status Code" + status + "--- filepath : " + uploadPath + " -- Authorize : "+authorize);
            if(content.isEmpty()){
                Log.d(TAG, "Response Empty");
            }else{
                Log.d(TAG,"Content:"+content);
            }


            if(handler != null) {
                Bundle bundle = new Bundle();
                bundle.putString(key_status_code, status);
                bundle.putString(key_content, content);
                Message msg = new Message();
                msg.what = UPLOAD_VIDEO_WHAT;
                msg.setData(bundle);
                handler.sendMessage(msg);
            }

        }catch(Exception e){
            Log.e(TAG, e.toString());
        }
        return content;
    }

    protected void onPostExecute(String result)
    {
        try {
            JSONObject data = new JSONObject(result.toString());
            Log.d(TAG,"JSON DATA"+data);
        } catch (JSONException e) {
            Log.e(TAG, e.toString());
        }

    }
}
