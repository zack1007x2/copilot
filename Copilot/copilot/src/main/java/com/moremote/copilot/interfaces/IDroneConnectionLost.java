package com.moremote.copilot.interfaces;

public interface IDroneConnectionLost {

	void onDroneConnectionLost();

}
