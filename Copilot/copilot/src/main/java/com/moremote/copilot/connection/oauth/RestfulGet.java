package com.moremote.copilot.connection.oauth;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Message;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Handler;

/**
 * Created by gaozongyong on 2015/9/9.
 */
public class RestfulGet extends AsyncTask<String,Void,String> {

    private static final String TAG = "HttpGet";
    private Handler handler;


    public RestfulGet(Handler handler) {
        this.handler = handler;
    }



    @Override
    protected String doInBackground(String... urls){

        String url = urls[0];
        String content="";
        try{

            DefaultHttpClient client = new DefaultHttpClient() ;
            HttpGet get = new HttpGet(url);
            HttpResponse rp = client.execute(get);
            content = EntityUtils.toString(rp.getEntity());
            String status = String.valueOf(rp.getStatusLine().getStatusCode());
            Log.d(TAG, "Status Code" + status);
            client.getConnectionManager().shutdown();

            if(content.isEmpty()){
                Log.d(TAG,"Response Empty");
            }

            if(handler != null) {
                Bundle bundle = new Bundle();
                bundle.putString("StatusCode", status);
                bundle.putString("Content", content);
                Message msg = new Message();
                msg.setData(bundle);
                handler.sendMessage(msg);
            }

        }catch(Exception e){
            Log.d(TAG, e.toString());
        }
        return content;
    }

    protected void onPostExecute(String result)
    {
        try {
            JSONObject data = new JSONObject(result.toString());
            Log.d(TAG,"JSON DATA"+data);
        } catch (JSONException e) {
            Log.d(TAG, e.toString());
        }

    }

}
