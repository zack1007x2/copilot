package com.moremote.copilot.connection.oauth;

import android.content.Context;
import android.content.SharedPreferences;

import com.moremote.copilot.IPCamApplication;


/**
 * Created by chingchun on 2015/7/20.
 */
public class UserAccount {

    public static final int cameraType = IPCamApplication.cameraType.AndroidCam;

    public int OAuthType = OAUTHTYPE.NONEAUTH;
    public String ACCOUNT_NAME;
    public String ACCOUNT_EMAIL;
    public String ACCOUNT_PASSWORD;
    public String ACCOUNT_TOKEN;
    public String XMPP_ACCOUNT;
    public String XMPP_PASSWORD;
    public String RELAY_UUID;
    public String RELAY_KEY;
    public String STORAGE_TOKEN;

    private Context mContext;
    private SharedPreferences settings;
    private static final String oauthField = "OAUTH";
    private static final String nameField = "NAME";
    private static final String emailField = "EMAIL";
    private static final String passwordField = "PASSWORD";
    private static final String tokenField = "TOKEN";
    private static final String xmppAccField = "XMPPACC";
    private static final String xmppPWDField = "XMPPPWD";
    private static final String relayUUIDField = "RELAYUUID";
    private static final String relayKeyField = "RELAYKEY";
    private static final String storageTokenField = "STORAGETOKEN";

    public static class OAUTHTYPE{
        public static final int NONEAUTH = -1;
        public static final int MOREMOTE = 0;
        public static final int GOOGLE = 1;
        public static final int FACEBOOK = 2;
        public static final int LOGOUT = 3;
    }

    public UserAccount(Context lContext)
    {
        this.mContext = lContext;
        settings = mContext.getSharedPreferences("ipcam", Context.MODE_PRIVATE);
        readUserData();
    }

    public void readUserData()
    {
        OAuthType = settings.getInt(oauthField, OAUTHTYPE.NONEAUTH);
        ACCOUNT_NAME = settings.getString(nameField, "");
        ACCOUNT_EMAIL = settings.getString(emailField, "");
        ACCOUNT_PASSWORD = settings.getString(passwordField,"");
        ACCOUNT_TOKEN = settings.getString(tokenField, "");
        XMPP_ACCOUNT = settings.getString(xmppAccField, "");
        XMPP_PASSWORD = settings.getString(xmppPWDField, "");
        RELAY_UUID = settings.getString(relayUUIDField , "");
        RELAY_KEY = settings.getString(relayKeyField , "");
        STORAGE_TOKEN = settings.getString(storageTokenField,"");

    }

    public void SaveUserData()
    {
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt(oauthField, OAuthType);
        editor.putString(nameField, ACCOUNT_NAME);
        editor.putString(emailField, ACCOUNT_EMAIL);
        editor.putString(tokenField, ACCOUNT_TOKEN);
        editor.putString(passwordField, ACCOUNT_PASSWORD);
        editor.putString(xmppAccField, XMPP_ACCOUNT);
        editor.putString(xmppPWDField, XMPP_PASSWORD);
        editor.putString(relayUUIDField, RELAY_UUID);
        editor.putString(relayKeyField, RELAY_KEY);
        editor.putString(storageTokenField, STORAGE_TOKEN);
        editor.commit();//提交修改
    }
}
