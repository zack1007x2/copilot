package com.moremote.copilot.interfaces;

/**
 * Created by Zack on 2016/1/29.
 */
public interface IDroneStateChangeListener {
        void onArmedChange(boolean isArmed);
        void onConnectionChange(boolean isConnected);
        void onFlyingStateChange(boolean isFlying);
        void onModeChange(long mode);
}
