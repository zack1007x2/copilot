package com.moremote.copilot.interfaces;

public interface IServerStarted {

	void onServerStarted(String txt);

}
