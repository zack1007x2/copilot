package com.moremote.copilot.utils;

import android.app.ActivityManager;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.content.res.Configuration;
import android.media.MediaCodecInfo;
import android.media.MediaCodecList;
import android.media.MediaMetadataRetriever;
import android.net.ConnectivityManager;
import android.net.DhcpInfo;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.telephony.CellIdentityLte;
import android.telephony.CellInfo;
import android.telephony.CellInfoCdma;
import android.telephony.CellInfoGsm;
import android.telephony.CellInfoLte;
import android.telephony.CellInfoWcdma;
import android.telephony.CellSignalStrengthCdma;
import android.telephony.CellSignalStrengthGsm;
import android.telephony.CellSignalStrengthLte;
import android.telephony.CellSignalStrengthWcdma;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.moremote.copilot.enums.SCREEN_SIZE;

import org.apache.http.conn.util.InetAddressUtils;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.nio.ByteBuffer;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import javax.security.auth.x500.X500Principal;

/**
 * Created by Zack on 15/11/10.
 */
public class Utils {


    /**
     * Convert byte array to hex string
     *
     * @param bytes
     * @return
     */
    public static String bytesToHex(byte[] bytes) {
        StringBuilder sbuf = new StringBuilder();
        for (int idx = 0; idx < bytes.length; idx++) {
            int intVal = bytes[idx] & 0xff;
            if (intVal < 0x10) sbuf.append("0");
            sbuf.append(Integer.toHexString(intVal).toUpperCase(Locale.US));
        }
        return sbuf.toString();
    }

    /**
     * Get utf8 byte array.
     *
     * @param str
     * @return array of NULL if error was found
     */
    public static byte[] getUTF8Bytes(String str) {
        try {
            return str.getBytes("UTF-8");
        }
        catch (Exception ex) {
            return null;
        }
    }

    /**
     * Load UTF8withBOM or any ansi text file.
     *
     * @param filename
     * @return
     * @throws IOException
     */
    public static String loadFileAsString(String filename) throws IOException {
        final int BUFLEN = 1024;
        BufferedInputStream is = new BufferedInputStream(new FileInputStream(filename), BUFLEN);
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream(BUFLEN);
            byte[] bytes = new byte[BUFLEN];
            boolean isUTF8 = false;
            int read, count = 0;
            while ((read = is.read(bytes)) != -1) {
                if (count == 0 && bytes[0] == (byte) 0xEF && bytes[1] == (byte) 0xBB && bytes[2] == (byte) 0xBF) {
                    isUTF8 = true;
                    baos.write(bytes, 3, read - 3); // drop UTF8 bom marker
                }
                else {
                    baos.write(bytes, 0, read);
                }
                count += read;
            }
            return isUTF8 ? new String(baos.toByteArray(), "UTF-8") : new String(baos.toByteArray());
        }
        finally {
            try {
                is.close();
            }
            catch (Exception ex) {
            }
        }
    }

    /**
     * Returns MAC address of the given interface name.
     *
     * @param interfaceName
     *            eth0, wlan0 or NULL=use first interface
     * @return mac address or empty string
     */
    public static String getMACAddress(String interfaceName) {
        try {
            List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface intf : interfaces) {
                if (interfaceName != null) {
                    if (!intf.getName().equalsIgnoreCase(interfaceName)) continue;
                }
                byte[] mac = intf.getHardwareAddress();
                if (mac == null) return "";
                StringBuilder buf = new StringBuilder();
                for (int idx = 0; idx < mac.length; idx++)
                    buf.append(String.format("%02X:", mac[idx]));
                if (buf.length() > 0) buf.deleteCharAt(buf.length() - 1);
                return buf.toString();
            }
        }
        catch (Exception ex) {
        } // for now eat exceptions
        return "";
		/*try {
		    // this is so Linux hack
		    return loadFileAsString("/sys/class/net/" +interfaceName + "/address").toUpperCase().trim();
		} catch (IOException ex) {
		    return null;
		}*/
    }

    /**
     * Get IP address from first non-localhost interface
     *
     * @param ipv4
     *            true=return ipv4, false=return ipv6
     * @return address or empty string
     */
    public static String getIPAddress(boolean useIPv4) {
        try {
            List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface intf : interfaces) {
                List<InetAddress> addrs = Collections.list(intf.getInetAddresses());
                for (InetAddress addr : addrs) {
                    if (!addr.isLoopbackAddress()) {
                        String sAddr = addr.getHostAddress().toUpperCase(Locale.US);
                        boolean isIPv4 = InetAddressUtils.isIPv4Address(sAddr);
                        if (useIPv4) {
                            if (isIPv4) return sAddr;
                        }
                        else {
                            if (!isIPv4) {
                                int delim = sAddr.indexOf('%'); // drop ip6 port suffix
                                return delim < 0 ? sAddr : sAddr.substring(0, delim);
                            }
                        }
                    }
                }
            }
        }
        catch (Exception ex) {
        } // for now eat exceptions
        return "";
    }

    public static InetAddress getBroadcastAddress(Context mContext) throws IOException {
        WifiManager wifi = (WifiManager) mContext.getSystemService(Context.WIFI_SERVICE);
        DhcpInfo dhcp = wifi.getDhcpInfo();
        // handle null somehow

        int broadcast = (dhcp.ipAddress & dhcp.netmask) | ~dhcp.netmask;
        byte[] quads = new byte[4];
        for (int k = 0; k < 4; k++)
            quads[k] = (byte) ((broadcast >> k * 8) & 0xFF);
        return InetAddress.getByAddress(quads);
    }

    public static SCREEN_SIZE getScreenSize(Context context) {
        int screenLayout = context.getResources().getConfiguration().screenLayout;
        screenLayout &= Configuration.SCREENLAYOUT_SIZE_MASK;

        switch (screenLayout) {
            case Configuration.SCREENLAYOUT_SIZE_SMALL:
                return SCREEN_SIZE.SMALL;
            case Configuration.SCREENLAYOUT_SIZE_NORMAL:
                return SCREEN_SIZE.NORMAL;
            case Configuration.SCREENLAYOUT_SIZE_LARGE:
                return SCREEN_SIZE.LARGE;
            case 4: // Configuration.SCREENLAYOUT_SIZE_XLARGE is API >= 9
                return SCREEN_SIZE.XLARGE;
            default:
                return SCREEN_SIZE.UNDEF;
        }
    }

    private static final X500Principal DEBUG_DN = new X500Principal("CN=Android Debug,O=Android,C=US");

    public static boolean isDebuggable(Context ctx) {
        boolean debuggable = false;

        try {
            PackageInfo pinfo = ctx.getPackageManager().getPackageInfo(ctx.getPackageName(), PackageManager.GET_SIGNATURES);
            Signature signatures[] = pinfo.signatures;

            CertificateFactory cf = CertificateFactory.getInstance("X.509");

            for (int i = 0; i < signatures.length; i++) {
                ByteArrayInputStream stream = new ByteArrayInputStream(signatures[i].toByteArray());
                X509Certificate cert = (X509Certificate) cf.generateCertificate(stream);
                debuggable = cert.getSubjectX500Principal().equals(DEBUG_DN);
                if (debuggable) break;
            }
        }
        catch (NameNotFoundException e) {
            //debuggable variable will remain false
        }
        catch (CertificateException e) {
            //debuggable variable will remain false
        }
        return debuggable;
    }

    public static boolean isMyServiceRunning(Context context, Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context
                .ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    public static String getMacAddress(Context context) {
        WifiManager wifiMan = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        WifiInfo wifiInf = wifiMan.getConnectionInfo();
        return wifiInf.getMacAddress();
    }

    public static void showCodecInfo(){
        final String TAG = "SHOWMediaCodecInfo";
        int numCodecs = MediaCodecList.getCodecCount();
        MediaCodecInfo codecInfo = null;
        for (int i = 0; i < numCodecs && codecInfo == null; i++)
        {
            MediaCodecInfo info = MediaCodecList.getCodecInfoAt(i);
            Log.d(TAG, "Found MediaCodecInfo = "+info.getName());
            if (!info.isEncoder())
            {
                continue;
            }
            String[] types = info.getSupportedTypes();
            boolean found = false;
            for (int j = 0; j < types.length && !found; j++)
            {
                Log.d(TAG, "Found MediaCodecInfo = "+types[j]);
                if (types[j].equals("video/avc"))
                {
                    found = true;
                }
            }
            if (!found)
                continue;
            codecInfo = info;
        }
        Log.d(TAG, codecInfo.getName() + " supporting " + "video/avc");

        //检查所支持的colorspace
        int colorFormat = 0;
        MediaCodecInfo.CodecCapabilities capabilities = codecInfo.getCapabilitiesForType("video/avc");
        System.out.println("length-" + capabilities.colorFormats.length + "=="
                + Arrays.toString(capabilities.colorFormats));
        for (int i = 0; i < capabilities.colorFormats.length
                && colorFormat == 0; i++)
        {
            int[] format = capabilities.colorFormats;
            switch (format[i])
            {
                case MediaCodecInfo.CodecCapabilities.COLOR_FormatYUV420Planar:
                    System.out.println("-");
                    break;
                case MediaCodecInfo.CodecCapabilities.COLOR_FormatYUV420PackedPlanar:
                    System.out.println("-");
                    break;
                case MediaCodecInfo.CodecCapabilities.COLOR_FormatYUV420SemiPlanar:
                    System.out.println("-");
                    break;
                case MediaCodecInfo.CodecCapabilities.COLOR_FormatYUV420PackedSemiPlanar:
                    System.out.println("-");
                    break;
                case MediaCodecInfo.CodecCapabilities.COLOR_TI_FormatYUV420PackedSemiPlanar:
                    colorFormat = format[i];
                    System.out.println("-");
                    break;
                default:
                    Log.d(TAG, "Skipping unsupported color format " + format[i]);
                    break;
            }
        }
        Log.d(TAG, "color format " + colorFormat);
    }

    public static ByteBuffer ByteBufferCopy(ByteBuffer source, ByteBuffer target) {

        int sourceP = source.position();
        int sourceL = source.limit();

        if (null == target) {
            target = ByteBuffer.allocate(source.remaining());
        }
        target.put(source);
        target.flip();

        source.position(sourceP);
        source.limit(sourceL);
        return target;
    }

    public static long getVideoDuration(Context context, File videoFile){
        MediaMetadataRetriever retriever = new MediaMetadataRetriever();
        retriever.setDataSource(context, Uri.fromFile(videoFile));
        String time = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
        long timeInMillisec = Long.parseLong(time );
        return timeInMillisec;
    }

    public static String getIP(Context context){
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo inf = cm.getActiveNetworkInfo();
        if(!(inf.getState()==NetworkInfo.State.CONNECTED)){
            return null;
        }

        if(inf.getType()==ConnectivityManager.TYPE_MOBILE){
            return getIPAddress(true);
        }else if(inf.getType()==ConnectivityManager.TYPE_WIFI){
            WifiManager wifiMan = (WifiManager) (context.getSystemService(Context.WIFI_SERVICE));
            WifiInfo wifiInf = wifiMan.getConnectionInfo();
            long ip = wifiInf.getIpAddress();
            if( ip != 0 )
                return String.format( "%d.%d.%d.%d",
                        (ip & 0xff),
                        (ip >> 8 & 0xff),
                        (ip >> 16 & 0xff),
                        (ip >> 24 & 0xff));
        }else if(inf.getType()==ConnectivityManager.TYPE_ETHERNET){
            return getIPAddress(true);
        }
        return "";
//        return new StringBuilder()
//                .append("Connect State :" + cm.getActiveNetworkInfo().getState())
//                .append("Connect Detail State :" + cm.getActiveNetworkInfo().getDetailedState())
//                .append("Connect TypeName :" + cm.getActiveNetworkInfo().getTypeName())
//                .append("Connect Type :" + cm.getActiveNetworkInfo().getType())
//                .toString();
    }
    public static int getRssi(Context context){
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo inf = cm.getActiveNetworkInfo();
        final String TAG = "RSSI";
        Log.d(TAG,"RSSI TYPE : "+ inf.getType());
        switch (inf.getType()){
            case ConnectivityManager.TYPE_WIFI:
                WifiManager wifiManager = (WifiManager)context.getSystemService(Context.WIFI_SERVICE);
                return wifiManager.getConnectionInfo().getRssi();
            case ConnectivityManager.TYPE_MOBILE:
                TelephonyManager telephonyManager = (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);
                List<CellInfo> cellList = telephonyManager.getAllCellInfo();
                Log.d(TAG, "getAllCellInfo size : " + cellList.size());
                if(cellList!=null && !cellList.isEmpty()){
                    for(CellInfo info:cellList){
                        if (info instanceof CellInfoWcdma) {
                            final CellSignalStrengthWcdma wcdma = ((CellInfoWcdma) info).getCellSignalStrength();
                            Log.d(TAG, "WCDMA Strength : " + wcdma.toString());
                            if(wcdma.getDbm()!=0)
                                return wcdma.getDbm();
                        } else if (info instanceof CellInfoCdma) {
                            final CellSignalStrengthCdma cdma = ((CellInfoCdma) info).getCellSignalStrength();
                            Log.d(TAG, "CDMA Strength : " + cdma.toString());
                            if(cdma.getDbm()!=0)
                                return cdma.getDbm();
                        }else if (info instanceof CellInfoLte){
                            final CellSignalStrengthLte lte = ((CellInfoLte) info).getCellSignalStrength();
                            Log.d(TAG, "LTE Strength : " + lte.toString());
                            if(lte.getDbm()!=0)
                                return lte.getDbm();
                        }else if (info instanceof CellInfoGsm){
                            final CellSignalStrengthGsm gsm = ((CellInfoGsm) info).getCellSignalStrength();
                            Log.d(TAG, "GSM Strength : " + gsm.toString());
                            if(gsm.getDbm()!=0)
                                return gsm.getDbm();
                        }

                    }
                }
                return 0;
            default:
                return 0;
        }
    }

    public static boolean isConnect(Context context){
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo inf = cm.getActiveNetworkInfo();
        return inf!=null && inf.isConnected();
    }

    public static NetworkInfo getNetWorkInfo(Context context){
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo inf = cm.getActiveNetworkInfo();
        return inf;
    }

}
