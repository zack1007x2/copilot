package com.MAVLink.Messages;

import java.util.ArrayList;
import java.util.List;

public enum ApmModes {
    FIXED_WING_MANUAL(0, "Manual", 1),
    FIXED_WING_CIRCLE(1, "Circle", 1),
    FIXED_WING_STABILIZE(2, "Stabilize", 1),
    FIXED_WING_TRAINING(3, "Training", 1),
    FIXED_WING_FLY_BY_WIRE_A(5, "FBW A", 1),
    FIXED_WING_FLY_BY_WIRE_B(6, "FBW B", 1),
    FIXED_WING_AUTO(10, "Auto", 1),
    FIXED_WING_RTL(11, "RTL", 1),
    FIXED_WING_LOITER(12, "Loiter", 1),
    FIXED_WING_GUIDED(15, "Guided", 1),
    ROTOR_STABILIZE(0, "Stabilize", 2),
    ROTOR_ACRO(1, "Acro", 2),
    ROTOR_ALT_HOLD(2, "Alt Hold", 2),
    ROTOR_AUTO(3, "Auto", 2),
    ROTOR_GUIDED(4, "Guided", 2),
    ROTOR_LOITER(5, "Loiter", 2),
    ROTOR_RTL(6, "RTL", 2),
    ROTOR_CIRCLE(7, "Circle", 2),
    ROTOR_POSITION(8, "Pos Hold", 2),
    ROTOR_LAND(9, "Land", 2),
    ROTOR_TOY(11, "Drift", 2),
    ROTOR_TAKEOFF(13, "Sport", 2),
    ROVER_MANUAL(0, "MANUAL", 10),
    ROVER_LEARNING(2, "LEARNING", 10),
    ROVER_STEERING(3, "STEERING", 10),
    ROVER_HOLD(4, "HOLD", 10),
    ROVER_AUTO(10, "AUTO", 10),
    ROVER_RTL(11, "RTL", 10),
    ROVER_GUIDED(15, "GUIDED", 10),
    ROVER_INITIALIZING(16, "INITIALIZING", 10),
    UNKNOWN(-1, "Unknown", 0);

    private final int number;
    private final String name;
    private final int type;

    private ApmModes(int number, String name, int type) {
        this.number = number;
        this.name = name;
        this.type = type;
    }

    public int getNumber() {
        return this.number;
    }

    public String getName() {
        return this.name;
    }

    public int getType() {
        return this.type;
    }

    public static ApmModes getMode(long i, int type) {
        ApmModes[] var5;
        int var4 = (var5 = values()).length;

        for (int var3 = 0; var3 < var4; ++var3) {
            ApmModes mode = var5[var3];
            if (i == mode.getNumber() & type == mode.getType()) {
                return mode;
            }
        }

        return UNKNOWN;
    }

    public static ApmModes getMode(String str, int type) {
        ApmModes[] var5;
        int var4 = (var5 = values()).length;

        for (int var3 = 0; var3 < var4; ++var3) {
            ApmModes mode = var5[var3];
            if (str.equals(mode.getName()) & type == mode.getType()) {
                return mode;
            }
        }

        return UNKNOWN;
    }

    public static List<ApmModes> getModeList(int type) {
        ArrayList modeList = new ArrayList();
        if (isCopter(type)) {
            type = 2;
        }

        ApmModes[] var5;
        int var4 = (var5 = values()).length;

        for (int var3 = 0; var3 < var4; ++var3) {
            ApmModes mode = var5[var3];
            if (isValid(mode) & mode.getType() == type) {
                modeList.add(mode);
            }
        }

        return modeList;
    }

    public static boolean isValid(ApmModes mode) {
        return mode != UNKNOWN;
    }

    public static boolean isCopter(int type) {
        switch (type) {
            case 1:
            case 3:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
            case 11:
            case 12:
            default:
                return false;
            case 2:
            case 4:
            case 13:
            case 14:
            case 15:
                return true;
        }
    }
}
